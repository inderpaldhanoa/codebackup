/************************************************************************************************************
    Apex Class Name	: RESTMobileAPITest.cls
    Version 			: 1.0
    Created Date  	: 19 April 2017
    Function 			: Test class for RESTMobileAPITest.
    Modification Log	:
    Developer				Date				Description
    -----------------------------------------------------------------------------------------------------------
    Tom Clayton			19/04/2017 			Created Class.
    Tom Clayton			26/07/2017 			Updated to cover latest version.
************************************************************************************************************/

@isTest
private class RESTMobileAPITest {
// Commenting out because this is replaced by MobileRestHelperTest
/*
	public static Account account1;
	public static Contact contact1, contact2;
	public static Case case1, case2;

	@testSetup static void setupTestData() {
		// Create test data
		account1 = TestDataFactoryMobile.createAccounts(1)[0];
		insert account1;
		list <Contact> lstContacts = TestDataFactoryMobile.createContacts(2);
		lstContacts[0].AccountId = account1.Id;
		lstContacts[1].AccountId = account1.Id;
		insert lstContacts;
		Public_Knowledge_Topic__c publicKnowledgeTopic = TestDataFactoryMobile.createPublicKnowledgeTopics(1)[0];
		insert publicKnowledgeTopic;
		list <Case> lstCases = TestDataFactoryMobile.createCases(2);
		lstCases[0].AccountId = account1.Id;
		lstCases[0].ContactId = lstContacts[0].Id;
		lstCases[1].AccountId = account1.Id;
		lstCases[1].ContactId = lstContacts[1].Id;
		insert lstCases;
	}

	static testMethod void GetPublicKnowledgeTopicsTest() {
		// Create test request
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		req.requestURI = '/services/apexrest/RESTMobileAPI?APICall=getPublicKnowledgeTopics';
		req.params.put('APICall', 'getPublicKnowledgeTopics');
		// Fake the passed call
		req.requestBody = Blob.valueOf('Test Body');
		req.httpMethod = 'GET';
		RestContext.request = req;
		RestContext.response = res;
		Test.startTest();
		RESTMobileAPI.getMobile();
		Test.stopTest();
		String testResponseBody = res.responseBody.toString();
		system.debug('--The test responseBody was: ' + testResponseBody);
		system.assert(testResponseBody.contains('Thing 9000'), 'The responseBody did not contain the Title of the test data\'s Public Knowledge Topic.');
	}

	static testMethod void createPaymentNotificationTest() {
		// Create test request
		String jsonInput = '{'
		                   + '  "type": "Outstanding payment",'
		                   + '  "subType": "Day 5",'
		                   + '  "status": "In Progress",'
		                   + '  "subStatus": "Payment failed notification sent",'
		                   + '  "octaneCustomerID": "100001",'
		                   + '  "orderId": "ddmmyyyy88888888-stno",'
		                   + '  "rejectCode": "2001",'
		                   + '  "rejectReason": "Insufficient funds",'
		                   + '  "accountBalance": "25",'
		                   + '  "duedate": "2017-01-01",'
		                   + '  "submittedDate": "2017-01-01",'
		                   + '  "lastUpdatedDate": "2017-01-01",'
		                   + '  "userID": "payment-job",'
		                   + '  "source": "System"'
		                   + '}';
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		req.requestURI = '/services/apexrest/RESTMobileAPI?APICall=createPaymentNotification';
		req.params.put('APICall', 'createPaymentNotification');
		// Fake the passed call
		req.requestBody = Blob.valueOf(jsonInput);
		req.httpMethod = 'POST';
		RestContext.request = req;
		RestContext.response = res;
		Test.startTest();
		RESTMobileAPI.postMobile();
		Test.stopTest();
	}
	*/
}