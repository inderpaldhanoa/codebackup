@isTest
Public class MarketingServiceEmailTriggerHandlerTest{
    Public static testmethod void TaskCreationonInsertMSEtest(){
    Map<String,Schema.RecordTypeInfo>accRecordTypeInfoName = GlobalUtil.getRecordTypeByName('Account');
        
        Account sAccount = new Account();
        sAccount.RecordTypeId = accRecordTypeInfoName.get('Residential').getRecordTypeId();
        sAccount.Name = 'Test Account';
        sAccount.Phone = '0412345678';
        sAccount.Customer_Status__c = 'Active';
        sAccount.Octane_Customer_Number__c = '123456';
        insert sAccount;
        
        Contact sContact = new Contact();
        sContact.FirstName = 'Joe';
        sContact.LastName = 'Belong';
        sContact.AccountId = sAccount.Id;
        sContact.Contact_Role__c = 'Primary';
        insert sContact;
        Contact Con= [Select id from contact where id=:sContact.id limit 1];
        string conid= con.id;
        
         List<Marketing_Service_Email__c> lmse = new List<Marketing_Service_Email__c>();
         Marketing_Service_Email__c m1= new Marketing_Service_Email__c(Name='Test Mail',
                                                                       JobID__c='15036',
                                                                       Email_Link__c='http://something.something.com',
                                                                       Contact__c=sContact.id,
                                                                       Email_Header__c='Welocome to belong');
            Marketing_Service_Email__c m2= new Marketing_Service_Email__c(Name='Test Mail',
                                                                          JobID__c='15038',
                                                                          Email_Link__c='http://something.something.com',
                                                                          Contact__c=sContact.id,
                                                                          Email_Header__c='Welocome to belong');  
              lmse.add(m1);
              lmse.add(m2);
              insert lmse;
              MarketingServiceEmailTriggerHandler.TaskCreationonInsertMSE(lmse,true);
              MarketingServiceEmailTriggerHandler.TaskCreationonInsertMSE(lmse,false);
    
    }


}