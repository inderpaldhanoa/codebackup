/************************************************************************************************************
* Apex Class Name	: AttachmentTriggerHelper.cls
* Version 			: 1.0
* Created Date  	: 29 March 2017
* Function 			: Helper for triggers on Attachment object.
* Modification Log	:
* Developer				Date				Description
* -----------------------------------------------------------------------------------------------------------
* Tom Clayton			29/03/2017 			Created Class.
* Tom Clayton 			30/03/2017 			Added update to delivery summary status.
* Tom Clayton 			30/03/2017 			Changed delivery lookup to be based on attachment, added constants.
* Tom Clayton 			31/03/2017 			Added error logging.
* Tom Clayton 			04/04/2017 			Fixed to match recent changes to status fields.
* Girish P 				10/04/2017 			Removed constant Method and added to global util
* Tom Clayton			19/04/2017 			Added special handling for when the CSV file ends with a comma (null value in final field)
************************************************************************************************************/

public with sharing class AttachmentTriggerHelper {
	//All constants
	Public static Final String CONFIRMATION_FILENAME = 'SimDispatchConfirmation';
	Public static Final String RECEIVED_FROM_GE_STATUS = 'Delivery Dispatched';
	Public static Final String SENT_TO_SERVICE_LAYER_STATUS = 'Delivery Sent';
	//Other variables
	private static Boolean isInitialized = false;
	private static List<Asset> lstAssets;
	private static List<Log__c> lstLogs;
	private static Delivery_Summary__c deliverySummary;
	public static void initializeProperties() {
		if (isInitialized) {
			return;
		}
		lstAssets = new List<Asset>();
		lstLogs = new List<Log__c>();
		deliverySummary = new Delivery_Summary__c();
		isInitialized = true;
	}
	public static void processAttachment(Attachment eachAttachment) {
		system.debug('processAttachment');
		if (GlobalUtil.getObjectNameFromId(eachAttachment.parentId) == 'Delivery_Summary__c') {
			Delivery_Summary__c deliverySum = [Select id, Delivery_File_Status__c
			                                   from Delivery_Summary__c
			                                   where Id = :eachAttachment.ParentId];
			System.debug('Delivery_Summary__c: ' + deliverySum);
			if (deliverySum.Delivery_File_Status__c == SENT_TO_SERVICE_LAYER_STATUS) {
				AttachmentTriggerHelper.processConfirmation(eachAttachment);
			}
		}
	}
	public static void processConfirmation(sObject newSObj) {
		// If the triggering attachment is attached to a Delivery Summary
		//Girish: This check should be executing in Handler
		//if (GlobalUtil.getObjectNameFromId((Id)newSObj.get('ParentId')) == 'Delivery_Summary__c') {
		Asset currentAsset;
		// Check filename to make sure the attachment is a Confirmation and not a Request
		String currentFileName = (String)newSObj.get('Name');
		if (currentFileName.contains(CONFIRMATION_FILENAME)) {
			String[] strFileLines = new String[] {};
			String strFileText = '';
			strFileText = ((blob)newSObj.get('Body')).toString();
			// Split out the lines of the CSV file
			strFileLines = strFileText.split('\n');
			// Split out the values in the lines, skipping the header (by starting at row 1 instead of 0)
			for (Integer i = 1; i < strFileLines.size(); i++) {
				// If the last comma on the row is within 1 character of the end of the row, add a blank space to ensure the next split still sees an empty field
				if (strFileLines[i].length() - strFileLines[i].lastIndexOf(',') <= 1) {
					strFileLines[i] += ' ';
				}
				String[] strLineValues = new String[] {};
				strLinevalues = strFileLines[i].split(',');
				currentAsset = new Asset();
				// The date value requires some conversion from string
				String[] strDateParts = strLineValues[3].trim().split('-');
				Date dDispatchDate = Date.newInstance( integer.valueOf(strDateParts[0]), integer.valueOf(strDateParts[1]), integer.valueOf(strDateParts[2]) );
				// Assign the values from the CSV to an Asset to put in the list for update later
				// Remove leading or trailing white space and double quotes too
				currentAsset.Id = strLineValues[0].trim().remove('"');
				currentAsset.Dispatch_Date__c = dDispatchDate;
				currentAsset.Status = strLineValues[4].trim().remove('"');
				//Replace
				currentAsset.SIM__c = strLineValues[6].trim().remove('"').replaceAll('[^0-9]', '');
				currentAsset.Dispatch_Reference_Number__c = strLineValues[7].trim().remove('"');
				system.debug('--About to add asset for update: ' + currentAsset);
				lstAssets.add(currentAsset);
			}
		}
		// If the related Delivery Summary hasn't been noted yet
		if (deliverySummary.Id == null && currentAsset != null) {
			// Get the Id of the Delivery Summary which the Attachment is on
			deliverySummary.Id = (Id)newSObj.get('ParentId');
			// And set the status which will be updated after processing
			deliverySummary.Delivery_File_Status__c = RECEIVED_FROM_GE_STATUS;
		}
		//}
	}

	public static void updateConfirmedAssets() {
		if (!lstAssets.isEmpty()) {
			// Use Database.update so we can log any errors
			Database.SaveResult[] srList = Database.update(lstAssets, false);
			// Iterate through each returned result
			for (Database.SaveResult sr : srList) {
				if (!sr.isSuccess()) {
					// Operation failed, so get all errors
					for (Database.Error err : sr.getErrors()) {
						setExceptionResponse(err, 'AttachmentTriggerHelper.updateConfirmedAssets()', 'Exception occured while updating Assets.');
					}
				}
			}
		}
	}

	public static void updateRelatedDeliverySummary() {
		if (deliverySummary.Id != null) {
			// Use Database.update so we can log any errors
			Database.SaveResult sr = Database.update(deliverySummary, false);
			if (!sr.isSuccess()) {
				// Operation failed, so get all errors
				for (Database.Error err : sr.getErrors()) {
					setExceptionResponse(err, 'AttachmentTriggerHelper.updateRelatedDeliverySummary()', 'Exception occured while updating Delivery Summary.');
				}
			}
		}
	}

	public static void saveLogs() {
		if (!lstLogs.isEmpty()) {
			insert lstLogs;
		}
	}

	public static void afterProcessingResetInitialization() {
		isInitialized = false;
	}

	public static void setExceptionResponse(Database.Error err, String sMethod, String sRequest) {
		System.debug('--The following error has occurred:' + err.getStatusCode() + ': ' + err.getMessage());
		lstLogs.add(GlobalUtil.createLog(
		                'ERROR',
		                err.getMessage().abbreviate(225),
		                sMethod,
		                sRequest + ' (' + err.getStatusCode() + ')')
		           );
	}
}