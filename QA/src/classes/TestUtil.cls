@isTest
public class TestUtil {

    public static Account creatTestAccount() {
        Account sAccount     = new Account();
        sAccount.Name           = 'Test Account';
        sAccount.Phone          = '0412345678';
        sAccount.Customer_Status__c      = 'Active';
        sAccount.Octane_Customer_Number__c = '123456';
        return sAccount;
    }
    public static Contact creatTestContact(Id accountId) {
        Contact sContact = new Contact();
        sContact.FirstName     = 'Joetest';
        sContact.LastName      = 'Smithtest';
        sContact.AccountId     = accountId;
        sContact.Contact_Role__c = 'Primary';
        sContact.Email        = 'Joe@test.com';
        return sContact;
    }
    public static Case creatTestCase(Id accountId, Id contactId) {
        Case sCase = new Case();
        sCase.AccountId = accountId;
        sCase.ContactId = contactId;
        sCase.Complaint_Category__c = 'General';
        sCase.Status = 'Open';
        sCase.Origin = 'Phone';
        return sCase;
    }
    public static Order creatTestOrder(Id accountId, String sessionId) {
        Order sOrder = new Order();
        sOrder.AccountId = accountId;
        sOrder.effectiveDate = Date.today();
        sOrder.status = 'New';
        sOrder.LiveAgent_Session_Id__c = sessionId;
        return sOrder;
    }
    public static Survey__c creatTestSurvey() {
        Survey__c sSurvey = new Survey__c();
        sSurvey.Hide_Survey_Name__c = true;
        sSurvey.Submit_Response__c = 'Test';
        sSurvey.Survey_Container_CSS__c = 'Test';
        sSurvey.Survey_Header__c = 'Test';
        sSurvey.thankYouLink__c = 'Test';
        sSurvey.Thank_You_Text__c = 'Test';
        sSurvey.thankYouText__c = 'Test';
        sSurvey.URL__c = 'Test';
        return sSurvey;
    }
    public static LiveChatTranscript creatTestLiveChat() {
        LiveChatVisitor ct = new LiveChatVisitor();
        insert ct;
        LiveChatTranscript trns = new LiveChatTranscript();
        trns.ChatKey = 'Test';
        trns.LiveChatVisitorId = ct.id;
        trns.Session_Id__c = 'Test';
        return trns;
    }
    public static SurveyTaker__c creatTestSurveyTaker(Id orderId, Id contactId, Id caseId, Id surveyId, String liveChatKey ) {
        SurveyTaker__c surveyTaker     = new SurveyTaker__c();
        surveyTaker.Live_Chat_Key__c = liveChatKey;
        surveyTaker.Case__c = caseId;
        surveyTaker.Order__c = orderId;
        surveyTaker.Survey__c = surveyId;
        surveyTaker.Contact__c = contactId;
        surveyTaker.Taken__c = 'Test';
        return surveyTaker;
    }

    public static Lead creatTestLead(Id accountId) {
        Lead sLead = new Lead();
        sLead.Company = accountId;
        sLead.LastName = 'John';
        sLead.Status = 'New';
        sLead.Phone = '0435667897';
        return sLead;
    }

    public static List<SocialPost> creatTestSocialPost() {
        List<SocialPost> sptList = new List<SocialPost>();
        SocialPost spt = new SocialPost();
        spt.AttachmentUrl = 'https://scontent.xx.fbcdn.net/v/t34.0-12/15057871_730466050441584_626288805_n.jpg?oh=22f528d22a58ad31dce1abddc3c8d00f&oe=582CB6D4';
        spt.name = 'Test';
        spt.Posted = DateTime.parse('11/6/2014 12:00 AM');
        spt.Headline = 'Test';
        sptList.add(spt);
        SocialPost spt1 = new SocialPost();
        spt1.name = 'Test';
        spt1.Posted = DateTime.parse('11/6/2014 12:00 AM');
        spt1.Headline = 'Test';
        sptList.add(spt1);
        return sptList;
    }


    public static void createKnowledgeSettings() {
        List<Public_Knowledge_Topic__c> topicList = new List<Public_Knowledge_Topic__c>();
        Public_Knowledge_Topic__c prodTopic = new Public_Knowledge_Topic__c();
        prodTopic.Name = 'Getting Internet';
        prodTopic.Active__c = false;
        prodTopic.Details__c = 'Are there issues connecting your internet?';
        prodTopic.Page_Name__c = 'https://www.belong.com.au/internet-speed';
        prodTopic.Title__c = 'Getting Internet';
        topicList.add(prodTopic);
        prodTopic = new Public_Knowledge_Topic__c();
        prodTopic.Name = 'Joining Belong';
        prodTopic.Active__c = false;
        prodTopic.Details__c = 'Have more questions about our products?';
        prodTopic.Page_Name__c = 'https://www.belong.com.au/internet-speed';
        prodTopic.Title__c = 'Joining Belong';
        topicList.add(prodTopic);
        prodTopic = new Public_Knowledge_Topic__c();
        prodTopic.Name = 'Moving House';
        prodTopic.Active__c = true;
        prodTopic.Details__c = 'Experiencing slow internet or dropouts?';
        prodTopic.Page_Name__c = 'https://www.belong.com.au/internet-speed';
        prodTopic.Title__c = 'Moving House';
        topicList.add(prodTopic);
        prodTopic = new Public_Knowledge_Topic__c();
        prodTopic.Name = 'Speed';
        prodTopic.Active__c = true;
        prodTopic.Details__c = 'Experiencing slow internet or dropouts?';
        prodTopic.Page_Name__c = 'https://www.belong.com.au/internet-speed';
        prodTopic.Title__c = 'Speed';
        topicList.add(prodTopic);
        insert topicList;
        List<Public_Knowledge_URL_Mapping__c> URLMapList = new List<Public_Knowledge_URL_Mapping__c>();
        Public_Knowledge_URL_Mapping__c prodURLMap;
        prodURLMap = new Public_Knowledge_URL_Mapping__c();
        prodURLMap.Name = 'Article';
        prodURLMap.Visualforce_Page_Name__c = 'Belong_Support_Article';
        prodURLMap.Meta_Description__c = 'Answer__c';
        prodURLMap.Meta_Keyword__c = 'Keywords_Public_Knowledge__c';
        prodURLMap.Meta_OG_Description__c = 'Answer__c';
        prodURLMap.Meta_OG_Image__c = 'Test';
        prodURLMap.Meta_OG_Keywords__c = 'Keywords_Public_Knowledge__c';
        prodURLMap.Meta_OG_Title__c = 'Title';
        prodURLMap.Meta_OG_URL__c = 'Normalize_URL_Name__c';
        prodURLMap.Meta_Title__c = 'Title';
        prodURLMap.SERP_Title__c = 'Test';
        URLMapList.add(prodURLMap);
        prodURLMap = new Public_Knowledge_URL_Mapping__c();
        prodURLMap.Name = 'Product';
        prodURLMap.Visualforce_Page_Name__c = 'Belong_Support_Category';
        prodURLMap.Meta_Description__c = 'Meta_Description__c';
        prodURLMap.Meta_Keyword__c = 'Meta_Keyword__c';
        prodURLMap.Meta_OG_Description__c = 'Meta_Description__c';
        prodURLMap.Meta_OG_Image__c = 'Test';
        prodURLMap.Meta_OG_Keywords__c = 'Meta_Keyword__c';
        prodURLMap.Meta_OG_Title__c = 'Meta_Name__c';
        prodURLMap.Meta_OG_URL__c = 'Test';
        prodURLMap.Meta_Title__c = 'Meta_Name__c';
        prodURLMap.SERP_Title__c = '| Belong';
        URLMapList.add(prodURLMap);
        prodURLMap = new Public_Knowledge_URL_Mapping__c();
        prodURLMap.Name = 'Search';
        prodURLMap.Visualforce_Page_Name__c = 'Search';
        prodURLMap.Meta_Description__c = 'Test';
        prodURLMap.Meta_Keyword__c = 'Test';
        prodURLMap.Meta_OG_Description__c = 'Test';
        prodURLMap.Meta_OG_Image__c = 'Test';
        prodURLMap.Meta_OG_Keywords__c = 'Test';
        prodURLMap.Meta_OG_Title__c = 'Test';
        prodURLMap.Meta_OG_URL__c = 'Test';
        prodURLMap.Meta_Title__c = 'Test';
        prodURLMap.SERP_Title__c = 'Test';
        URLMapList.add(prodURLMap);
        prodURLMap = new Public_Knowledge_URL_Mapping__c();
        prodURLMap.Name = 'Error';
        prodURLMap.Visualforce_Page_Name__c = ' Belong_Support_Error';
        prodURLMap.Meta_Description__c = 'Test';
        prodURLMap.Meta_Keyword__c = 'Test';
        prodURLMap.Meta_OG_Description__c = 'Test';
        prodURLMap.Meta_OG_Image__c = 'Test';
        prodURLMap.Meta_OG_Keywords__c = 'Test';
        prodURLMap.Meta_OG_Title__c = 'Test';
        prodURLMap.Meta_OG_URL__c = 'Test';
        prodURLMap.Meta_Title__c = 'Test';
        prodURLMap.SERP_Title__c = 'Test';
        URLMapList.add(prodURLMap);
        prodURLMap = new Public_Knowledge_URL_Mapping__c();
        prodURLMap.Name = 'Home';
        prodURLMap.Visualforce_Page_Name__c = ' Belong_Support_Home';
        prodURLMap.Meta_Description__c = 'Test';
        prodURLMap.Meta_Keyword__c = 'Test';
        prodURLMap.Meta_OG_Description__c = 'Test';
        prodURLMap.Meta_OG_Image__c = 'Test';
        prodURLMap.Meta_OG_Keywords__c = 'Test';
        prodURLMap.Meta_OG_Title__c = 'Test';
        prodURLMap.Meta_OG_URL__c = 'Test';
        prodURLMap.Meta_Title__c = 'Test';
        prodURLMap.SERP_Title__c = 'Test';
        URLMapList.add(prodURLMap);
        insert URLMapList;
        List<Public_Knowledge_Product__c> PubKnowProd = new List<Public_Knowledge_Product__c>();
        Public_Knowledge_Product__c knowProd = new Public_Knowledge_Product__c();
        knowProd.Name = 'ADSL01';
        knowProd.Title__c = 'Getting started';
        knowProd.Product_Category__c = 'adsl';
        knowProd.Data_Category__c = 'ADSL_Order__c';
        knowProd.Article_Category__c = 'order';
        knowProd.Meta_Description__c = 'Test';
        knowProd.Meta_Keyword__c = 'Test';
        knowProd.Meta_Name__c = 'Test';
        PubKnowProd.add(knowProd);
        knowProd = new Public_Knowledge_Product__c();
        knowProd.Name = 'ADSL02';
        knowProd.Title__c = 'Getting started';
        knowProd.Product_Category__c = 'adsl';
        knowProd.Data_Category__c = 'ADSL_Join__c';
        knowProd.Article_Category__c = 'join';
        knowProd.Meta_Description__c = 'Test';
        knowProd.Meta_Keyword__c = 'Test';
        knowProd.Meta_Name__c = 'Test';
        PubKnowProd.add(knowProd);
        knowProd = new Public_Knowledge_Product__c();
        knowProd.Name = 'NBN01';
        knowProd.Title__c = 'Getting started';
        knowProd.Product_Category__c = 'nbn';
        knowProd.Data_Category__c = 'NBN_Order__c';
        knowProd.Article_Category__c = 'order';
        knowProd.Meta_Description__c = 'Test';
        knowProd.Meta_Keyword__c = 'Test';
        knowProd.Meta_Name__c = 'Test';
        PubKnowProd.add(knowProd);
        knowProd = new Public_Knowledge_Product__c();
        knowProd.Name = 'NBN02';
        knowProd.Title__c = 'Getting started';
        knowProd.Product_Category__c = 'nbn';
        knowProd.Data_Category__c = 'NBN_Join__c';
        knowProd.Article_Category__c = 'join';
        knowProd.Meta_Description__c = 'Test';
        knowProd.Meta_Keyword__c = 'Test';
        knowProd.Meta_Name__c = 'Test';
        PubKnowProd.add(knowProd);
        knowProd = new Public_Knowledge_Product__c();
        knowProd.Name = 'Voice01';
        knowProd.Title__c = 'Getting started';
        knowProd.Product_Category__c = 'voice';
        knowProd.Data_Category__c = 'Voice_Order__c';
        knowProd.Article_Category__c = 'order';
        knowProd.Meta_Description__c = 'Test';
        knowProd.Meta_Keyword__c = 'Test';
        knowProd.Meta_Name__c = 'Test';
        PubKnowProd.add(knowProd);
        knowProd = new Public_Knowledge_Product__c();
        knowProd.Name = 'Voice02';
        knowProd.Title__c = 'Getting started';
        knowProd.Product_Category__c = 'voice';
        knowProd.Data_Category__c = 'Voice_Join__c';
        knowProd.Article_Category__c = 'join';
        knowProd.Meta_Description__c = 'Test';
        knowProd.Meta_Keyword__c = 'Test';
        knowProd.Meta_Name__c = 'Test';
        PubKnowProd.add(knowProd);
        insert PubKnowProd;
        List<Public_Knowledge_Product_Display_Setting__c> PubKnowDispList = new List<Public_Knowledge_Product_Display_Setting__c>();
        Public_Knowledge_Product_Display_Setting__c knowProdDisp = new Public_Knowledge_Product_Display_Setting__c();
        knowProdDisp.Name = 'adsl';
        knowProdDisp.Active__c = true;
        knowProdDisp.Display_order__c = 2;
        knowProdDisp.Product_Label__c = 'ADSL';
        PubKnowDispList.add(knowProdDisp);
        knowProdDisp = new Public_Knowledge_Product_Display_Setting__c();
        knowProdDisp.Name = 'nbn';
        knowProdDisp.Active__c = true;
        knowProdDisp.Display_order__c = 1;
        knowProdDisp.Product_Label__c = 'NBN';
        PubKnowDispList.add(knowProdDisp);
        knowProdDisp = new Public_Knowledge_Product_Display_Setting__c();
        knowProdDisp.Name = 'voice';
        knowProdDisp.Active__c = true;
        knowProdDisp.Display_order__c = 3;
        knowProdDisp.Product_Label__c = 'Belong Voice';
        PubKnowDispList.add(knowProdDisp);
        insert PubKnowDispList;
    }
}