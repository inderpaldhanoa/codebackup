/************************************************************************************************************
    Apex Class Name   : APIIdentityUser.cls
    Version           : 1.0
    Created Date      : 22 March 2017
    Function          : Helper Class for creating Identity customer User
    Modification Log  :
    Developer         :        Date                Description
    -----------------------------------------------------------------------------------------------------------
    Girish P                  18/04/2017          Created Class.
************************************************************************************************************/
public class APIIdentityUser {
    public static WrapperIdentityUser setIdentityUser(Contact oContact, String password) {
        System.debug('ContactID:***' + oContact);
        Contact existingCon = [Select id, Octane_Customer_Number__c, Mobile_Octane_Customer_Number__c, FirstName, LastName,
                               Email from Contact where id = :oContact.Id];
        String profileName = Label.EXTERNAL_IDENTITY_PROFILE_NAME;
        Database.DMLOptions dlo = new Database.DMLOptions();
        dlo.EmailHeader.triggerUserEmail = false;
        User oUser ;
        Profile extrernalIdentityProfile = [SELECT id from
                                            Profile
                                            where Name = :profileName];
        try {
            List<User> existingUser = [SELECT Id, isActive, Username, LastName, FirstName, Broadband_Octane_Id__c, Name,
                                       Mobile_Octane_Id__c, Email
                                       FROM User
                                       WHERE Profile.Id = :extrernalIdentityProfile.Id
                                               AND ContactId = :oContact.Id];
            if (!existingUser.isEmpty()) {
                //existing user cannot have a password string passed on to API
                if (NULL != password) {
                    return new WrapperIdentityUser(false, 'Cannot set new password for Exsiting User', existingUser[0]);
                }
                //if user already exists , UI should force login Users
                oUser = existingUser[0];
                if (existingCon.Octane_Customer_Number__c != null) {
                    oUser.Broadband_Octane_Id__c = String.valueof(existingCon.Octane_Customer_Number__c);
                }
                if (existingCon.Mobile_Octane_Customer_Number__c != null) {
                    oUser.Mobile_Octane_Id__c = String.valueOf(existingCon.Mobile_Octane_Customer_Number__c);
                }
                update oUser;
            }
            else {
                if (NULL == password) {
                    return new WrapperIdentityUser(false, 'Password cannot be null', NUll);
                }
                oUser = new User();
                oUser.FirstName = existingCon.FirstName;
                oUser.LastName = existingCon.LastName;
                oUser.Email = existingCon.Email;
                oUser.Username = existingCon.Email;
                oUser.ProfileId = extrernalIdentityProfile.Id;
                oUser.CommunityNickName = oUser.LastName + GlobalUtil.generateRandomString(7);
                oUser.Alias = GlobalUtil.generateRandomString(7);
                oUser.EmailEncodingKey = 'ISO-8859-1';
                oUser.TimeZoneSidKey = 'Australia/Sydney';
                oUser.LocaleSidKey = 'en_AU';
                oUser.LanguageLocaleKey = 'en_US';
                oUser.ContactId = existingCon.Id;
                oUser.IsActive = true;
                if (existingCon.Octane_Customer_Number__c != null) {
                    oUser.Broadband_Octane_Id__c = String.valueof(existingCon.Octane_Customer_Number__c);
                }
                if (existingCon.Mobile_Octane_Customer_Number__c != null) {
                    oUser.Mobile_Octane_Id__c = String.valueof(existingCon.Mobile_Octane_Customer_Number__c);
                }
                insert ouser;
                //Database.saveresult sr = Database.insert(oUser, dlo);
                //changeUserPassword(oUser, password);
				            System.setPassword(oUser.Id, password);

            }
        }
        catch (Exception e) {
            //MobileRestUtility.setExceptionResponse(e, 'setIdentityUser', JSON.serialize(oContact));
            return new WrapperIdentityUser(False, 'Exception while creating user:**' + e.getMessage(), oUser);
            throw e;
        }
        return new WrapperIdentityUser(true, 'Successfully created/Updated User', oUser);
    }

    public static void changeUserPassword(User user, String sPassword) {
        try {
            System.setPassword(user.Id, sPassword);
        }
        catch (Exception e) {
            throw e;
        }
        //return new WrapperIdentityUser(true, 'Successfully created/Updated User', user);
    }
}