@istest
public class CustomSiteLoginControllerTest {
    @testSetup
    static void setupTestData() {
        UserRole ur = [SELECT Id FROM UserRole Where name = 'Belong MOBILE'];
        // Setup test data
        // This code runs as the system user
        Profile p  = [Select id from Profile where name = 'Belong Integration API'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
                          EmailEncodingKey = 'UTF-8',
                          LastName = 'Testing',
                          LanguageLocaleKey = 'en_US',
                          LocaleSidKey = 'en_US',
                          ProfileId = p.Id,
                          UserRoleId = ur.id,
                          TimeZoneSidKey = 'America/Los_Angeles',
                          UserName = 'belongapiuser@testorg.com');
                          insert u;
    }

    public static TestMethod void CheckFunction () {
        UserRole ur = [SELECT Id FROM UserRole Where name = 'Belong MOBILE'];
        User u = [Select id
                  from user
                  where UserName = 'belongapiuser@testorg.com'];
        System.runAs(u) {
            Account Acc = new Account();
            Acc.name = 'test and Acc ';
            //Acc.octane_customer_number__c = '985654';
            insert Acc;
            Contact con = new Contact();
            con.lastname = Acc.Id;
            con.email = '1234hjvshjsjhdhv@mailinator.com';
            con.accountid = Acc.id;
            insert con;
            test.startTest();
            updateUserAEMPassword(con.id) ;
            test.stopTest();
            user u2 = [select id, Username, AEMPassword__c  from user where contactid = :con.id];
            system.assert(u2.Username == '1234444@mailinator.com');
        }
    }

    @future
    public static void updateUserAEMPassword(string cid) {
        UserRole ur = [SELECT Id FROM UserRole Where name = 'Belong MOBILE'];
        User u2 = [Select id
                   from user
                   where UserName = 'belongapiuser@testorg.com'];
        System.runAs(u2) {
            user u = new user();
            u.firstname = 'anthony' ;
            u.LastName  = 'francis' ;
            u.contactId = cid   ;
            u.Email = '1234444@mailinator.com'  ;
            u.UserName = '1234444@mailinator.com'   ;
            profile p = [SELECT id FROM profile WHERE name = 'External Identity User'];
            u.ProfileID  = p.id  ;
            u.CommunityNickName = 'ssss'    ;
            u.Alias = 'ssss'    ;
            u.EmailEncodingKey   = 'ISO-8859-1' ;
            u.TimeZoneSidKey    = 'Australia/Sydney'    ;
            u.LocaleSidKey  = 'en_AU'   ;
            u.LanguageLocaleKey = 'en_US';
            u.AEMPassword__c = 'YFKfV6oRJFD4AK2gKlstXjVIsJ/jDZj4VmlP3g==';
            insert u;
            CustomSiteLoginController.login('1234444@mailinator.com', 'Welcome45', 'https://www.google.com');
        }
    }
}