/************************************************************************************************************
* Apex Class Name	: MobileBatchReadAssetHeaderConfirmation.cls
* Version 			: 1.0 
* Created Date  	: 21 March 2017
* Function 			: Finds recently created sim dispatch confirmations attached to Delivery Summaries, reads them, and updates Assets.
* Modification Log	:
* Developer				Date				Description
* -----------------------------------------------------------------------------------------------------------
* Tom Clayton			21/03/2017 			Created Class.
* Tom Clayton			31/03/2017 			Commented out - no longer in use. Replaced by trigger on Attachment.
************************************************************************************************************/

/* Command to test this batch in execute anonymous: */
/* Uses this format: Id <variable name>= Database.executeBatch(new <Class name>(), batch size); */
// Id batchJobId = Database.executeBatch(new MobileBatchReadAssetHeaderConfirmation(), 200);

/* Commands to schedule this process: */
/* Seconds, Minutes, Hours, Day, Month, (Optional: Week), Year */
/* At 5:00 PM every day every month */
// MobileBatchReadAssetHeaderConfirmation BatchReadHeader = new MobileBatchReadAssetHeaderConfirmation();
// String sch = '0 0 17 * * ?';
// String jobID = System.schedule('Batch to nightly read sim dispatch confirmations and update Assets', sch, BatchReadHeader);

//global class MobileBatchReadAssetHeaderConfirmation implements Database.Batchable<sObject>, Schedulable,  Database.stateful
global class MobileBatchReadAssetHeaderConfirmation
{
	/*
	// COMMENTED OUT WHOLE CLASS - This has been replaced by triggers on Attachment
	
    // Implements the execute() method for Schedulable to call the Batchable method
    global void execute(SchedulableContext sc)
    {
        Database.executeBatch(new MobileBatchReadAssetHeaderConfirmation(), 200);
    }
    
    public List<Asset> lstAssets = new List<Asset>();
	
    // Start method to get data
    global List<Attachment> start(Database.BatchableContext bc)
    {
    	// Using the existing GlobalUtil to identify record type Id
    	Map<String,Schema.RecordTypeInfo> mapAssetRecordTypeInfo = GlobalUtil.getRecordTypeByName('Asset');
    	Id recTypeId = mapAssetRecordTypeInfo.get('Mobile').getRecordTypeId();
    	
    	try 
        {
            System.debug('--Batch Start: query beginning...');
	    	// Query the Attachments
	    	List<Attachment> lstAttachments = new List<Attachment>();
			lstAttachments = [SELECT Id, ParentId, Name, Body, ContentType FROM Attachment WHERE ParentId = 'a5LO00000009O7n']; // Hard coded to a specific Delivery Summary for testing purposes
	    	
	        return lstAttachments;
        }
        catch (Exception e)
        {
        	system.debug('--Error in batch start. Query had problem: ' + e.getmessage());
        	return null;
        }
    }
    
    // Process the records: update the...
    global void execute(Database.BatchableContext bc, List<Attachment> lstAttachments)
    {
    	for( Attachment attach: lstAttachments )
		{
			// Check filename to make sure the attachment is a Confirmation and not a Request
			if(attach.Name.contains('SimDispatchConfirmation'))
			{
				String[] strFileLines = new String[]{};
				String strFileText = '';
				strFileText = attach.body.toString();
				
				// Split out the lines of the CSV file
				strFileLines = strFileText.split('\n');
		 		
		 		// Split out the values in the lines, skipping the header
				for(Integer i=1; i < strFileLines.size(); i++)
				{
					String[] strLineValues = new String[]{};
					strLinevalues = strFileLines[i].split(',');
					
					Asset currentAsset = new Asset();
					
					// The date value requires some conversion from string
					String[] strDateParts = strLineValues[3].trim().split('-');
					Date dDispatchDate = Date.newInstance( integer.valueOf(strDateParts[0]), integer.valueOf(strDateParts[1]), integer.valueOf(strDateParts[2]) );
					
					// Assign the values from the CSV to an Asset to put in the list for update later
					// Remove leading or trailing white space and double quotes too
					currentAsset.Id = strLineValues[0].trim().remove('"');
					currentAsset.Dispatch_Date__c = dDispatchDate;
					currentAsset.Delivery_Status__c = strLineValues[4].trim().remove('"');
					currentAsset.SIM__c = strLineValues[6].trim().remove('"');
					currentAsset.Dispatch_Reference_Number__c = strLineValues[7].trim().remove('"');
					
					lstAssets.add(currentAsset);
				}
			}
		}
    }
    
    global void finish(Database.BatchableContext bc)
    {
    	// Save the changes to the fields on all the Assets mentioned in Confirmation attachments
    	update lstAssets;
    }
    */
}