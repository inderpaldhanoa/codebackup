/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  @created : 22/06/2016
  @Description : Order Trigger handler
*/

public with sharing class OrderTriggerHandler implements TriggerInterface
{
	private Boolean runAfterProcess = false;
    public static Boolean executeTrigger = true;
    
    /**
    *   This public method caches related data for before triggers, called once on start of the trigger execution
    */
    public void cacheBefore()
    {
        System.debug(LoggingLevel.ERROR, '====>cacheBefore is called');
        OrderTriggerHelper.initializeProperties();
        if(!(trigger.isDelete || trigger.isUnDelete))
		{
        	OrderTriggerHelper.collectParentAccount();
		}
        
    }

    /**
    *   This public method caches related data for after triggers, called once on start of the trigger execution
    */
    public void cacheAfter()
    {
        System.debug(LoggingLevel.ERROR, '====>cacheAfter is called');
        OrderTriggerHelper.initializeProperties();
    } 
    
    /**
    *   This public method process the business logic on 1 sObject at before insert
    */
    public void beforeInsert(sObject newSObj)
    {
        /*System.debug(LoggingLevel.ERROR, 'Order handler beforeInsert is called' + newSObj);*/
        OrderTriggerHelper.setPrimaryContactShipContact(newSObj);
    }
    
    /**
    *   This public method process the business logic on 1 sObject at after insert
    */
    public void afterInsert(sObject newSObj)
    {
    	OrderTriggerHelper.createHashCode(newSObj);
    }
    
    /**
    *   This public method process the business logic on 1 sObject at before update
    */
    public void beforeUpdate(sObject oldSObj, sObject newSObj)
    {
        /*System.debug(LoggingLevel.ERROR,'====>ContactTriggerHandler.beforeUpdate');
        System.debug(LoggingLevel.ERROR,'====>OrderTriggerHandler.beforeUpdate newSObj:' + newSObj);
        System.debug(LoggingLevel.ERROR,'====>OrderTriggerHandler.beforeUpdate oldSObj:' + oldSObj);*/
    }
    
    /**
    *   This public method process the business logic on 1 sObject at after update
    */
    public void afterUpdate(sObject oldSObj, sObject newSObj)
    {
        /*System.debug(LoggingLevel.ERROR,'====>OrderTriggerHandler.afterUpdate');
        System.debug(LoggingLevel.ERROR,'====>OrderTriggerHandler.afterUpdate newSObj:' + newSObj);
        System.debug(LoggingLevel.ERROR,'====>OrderTriggerHandler.afterUpdate oldSObj:' + oldSObj);*/
    }
    
    /**
    *   This public method process the business logic on 1 sObject at before delete
    */
    public void beforeDelete(sObject oldSObj)
    {
        
    }
    
    /**
    *   This public method process the business logic on 1 sObject at after delete
    */
    public void afterDelete(sObject oldSObj)
    {
        
    }
    
    /**
    *   This public method process the business logic on 1 sObject at after undelete
    */
    public void afterUndelete(sObject newSObj)
    {}
    
    /**
    *   Ater all rows have been processed this method will be called
    *   Usualy DML are placed in this method
    */
    public void afterProcessing()
    {
        //Avoid calling DML if not required
        OrderTriggerHelper.updateOrderHashCode();
    } 
    
}