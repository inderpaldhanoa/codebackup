/**********************************************************************************************************************
 * Class			: ContactTriggerHelper
 * Created Date		: 22-March-2016
 * Created By		: Daniel Garzon(dgarzon@deloitte.com)
 * Description		: Contact Trigger Helper<Provide the actions taken in this class>
 * Test Classes		: <Provide a list of test classes>
 * ------------------------------------------------------Change Log------------------------------------------------------
 * Date				Updated By			Description
 * 22-Mar-2016		Daniel Garzon		Created
 ***********************************************************************************************************************/
public with sharing class ContactTriggerHelper 
{
    private static Boolean isInitialized = false;
    private static Set<Id> contactIdList;
    private static Set<Id> AccountPrimContactSet;
    private static Set<Id> contactPrimaryList;
    private static Set<Id> parentAccountList;
    private static Map<Id, String> accountNameChangeMap;
    private static Map<Id, Account> parentAccountKeyMap;
    private static Map<Id, String> contactIdHashId;
    private static map<id,integer> contactMap;
    public static void initializeProperties()
    {
        if(isInitialized)
            return;
        contactPrimaryList = new Set<Id>();
        contactIdList = new Set<Id>();
        AccountPrimContactSet = new Set<Id>();
        accountNameChangeMap = new Map<Id,String>();
        parentAccountList = new Set<Id>();
        parentAccountKeyMap = new Map<Id, Account>();
        contactIdHashId = new Map<id,String>();
        contactMap=new map<id,integer>();
        isInitialized = true;
    }
    public static void getPrimaryContact()
    {
        System.debug(logginglevel.error, '@getPrimaryContact  parentAccountList => ' + parentAccountList);
        System.debug(logginglevel.error, '@getPrimaryContact  contactPrimaryList => ' + contactPrimaryList);
        for(sObject conObj : trigger.new)
        {
            if(conObj.get('AccountId') == Null)
                continue;
            if((Id)conObj.get('Id') != NULL)
            {
                contactIdList.add((Id)conObj.get('Id'));
            }
            if((Id)conObj.get('Id') != NULL && (String)conObj.get('Contact_Role__c') == 'Primary')
            {
                contactPrimaryList.add((Id)conObj.get('Id'));
            }
            parentAccountList.add((Id)conObj.get('AccountId'));
        }
        System.debug(logginglevel.error, '@getPrimaryContact  parentAccountList => ' + parentAccountList);
        System.debug(logginglevel.error, '@getPrimaryContact  contactPrimaryList => ' + contactPrimaryList);
        for(Contact conObj :  [SELECT Id, AccountId, Contact_Role__c 
                               FROM Contact 
                               WHERE AccountId IN: parentAccountList AND 
                               Contact_Role__c = 'Primary' AND 
                               Id NOT IN: contactPrimaryList])
        {
            AccountPrimContactSet.add(conObj.AccountId);
        }
        System.debug(logginglevel.error, '@getPrimaryContact  AccountPrimContactSet => ' + AccountPrimContactSet);
        for(Account accObj :  [SELECT Id, Encryption_Key__c 
                               FROM Account 
                               WHERE Id IN: parentAccountList AND 
                               Encryption_Key__c != null])
        {
            parentAccountKeyMap.put(accObj.Id, accObj);
        }
        
        System.debug(logginglevel.error, '@getPrimaryContact  parentAccountKeyMap => ' + parentAccountKeyMap);
        for(AggregateResult ar:[select count(id),accountid from contact where accountid IN:AccountPrimContactSet and authorized__c=true group by accountid]){       
                contactMap.put(string.valueof(ar.get('accountid')),integer.valueof(ar.get('expr0')));           
            }
    }
    public static void createGUID(sObject newSObj)
    {
        newSObj.put('Subscribe_Token__c', GuidUtil.NewGuid());
    }
    
    /******************************************************************************
Method Name: verifyPrimaryContactDuplicate
Description: It will ensure that an account has only one Primary contact.
Developer Name: Mitali Telang, Daniel Garzon
*****************************************************************************/
    public static void verifyPrimaryContactDuplicate(sObject newSObj)
    {
        if(newSObj.get('AccountId') == Null)
            return;
        Id accid = (Id) newSObj.get('AccountId');
        if(AccountPrimContactSet.contains(accid) && (String)newSObj.get('Contact_Role__c') == 'Primary')
        {
            newSObj.addError('This account already has a Primary Contact');
        }
    }
    
    /******************************************************************************
Method Name: verifyPrimaryContactExist
Description: It will ensure that an account always has a Primary contact.
Developer Name: Mitali Telang, Daniel Garzon
*****************************************************************************/
    public static void verifyPrimaryContactExist(sObject newSObj)
    {
        if(newSObj.get('AccountId') == Null)
            return;
        Id accid = (Id) newSObj.get('AccountId');
        System.debug(logginglevel.error, '@verifyPrimaryContactExist  AccountId => ' + accId);
        System.debug(logginglevel.error, '@verifyPrimaryContactExist  AccountPrimContactSet => ' + AccountPrimContactSet);
        if(!AccountPrimContactSet.contains(accid) && (String)newSObj.get('Contact_Role__c') == 'Delegated Authority' )
        {
            newSObj.addError('Account needs a Primary Contact');
        }
    }
    public static void changeNameOfAccount(sObject newSObj)
    {
        if(newSObj.get('AccountId') == Null)
            return;
        if((String)newSObj.get('Contact_Role__c') == 'Primary')
        {
            String accName = (String)newSObj.get('FirstName') + ' ' + (String)newSObj.get('LastName');
            Id accId = (Id)newSObj.get('AccountId');
            accountNameChangeMap.put(accId, accName);
        }
    }
    
    /******************************************************************************
Method Name: changeNameOfAccount
Description: It will ensure that when Name of the primary contact changes, the name of related Account changes accordingly
Developer Name: Mitali Telang, Daniel Garzon
*****************************************************************************/  
    public static void changeNameOfAccount(sObject oldSObj, sObject newSObj)
    {
        if(newSObj.get('AccountId') == Null)
            return;
        if((String)newSObj.get('Contact_Role__c') == 'Primary' && 
           ((String)newSObj.get('FirstName') != (String)oldSObj.get('FirstName') || (String)newSObj.get('Lastname') != (String)newSObj.get('Lastname')))
        {
            String accName = (String)newSObj.get('FirstName') + ' ' + (String)newSObj.get('LastName');
            Id accId = (Id)newSObj.get('AccountId');
            accountNameChangeMap.put(accId, accName);
        }
    }
    
    /******************************************************************************
Method Name: updateAccountName
Description: Up[date the Account Name record
Developer Name: Mitali Telang, Daniel Garzon
*****************************************************************************/  
    public static void updateAccountName()
    {
        if(!accountNameChangeMap.isEmpty())
        {
            List<Account> updateAccNamesList = new List<Account>();
            for(id accId : accountNameChangeMap.keySet())
            {
                String AccName = accountNameChangeMap.get(accID);
                updateAccNamesList.add(new Account(id = accId, Name = accName));
            }
            update updateAccNamesList;
        }
    }
    
    //create a Hash code using the encription key from the account
    public static void createHashCode(sObject newSObj)
    {
        if(string.isNotBlank((String)newSObj.get('AccountId')) && parentAccountKeyMap.containsKey((String)newSObj.get('AccountId')))
        {
            String accountKeyId = parentAccountKeyMap.get((String)newSObj.get('AccountId')).Encryption_Key__c;
            Id ContactId    = (Id)newSObj.get('Id');
            String hashCode = GlobalUtil.encrypt(ContactId, 128, EncodingUtil.base64Decode(accountKeyId));
            contactIdHashId.put(ContactId, hashCode);
        }
    }  
    
    //Update the hashcode value to the create contact record
    public static void updateContactHashCode()
    {  
        if(contactIdHashId.size() > 0)
        {
            List<Contact> listContact = new List<Contact>();
            
            for(SObject sObj: Cache.getRecords(contactIdHashId.keySet()))
            {
                Contact conObj = (Contact)sObj;
                conObj.Id_Hashcode__c = contactIdHashId.get(conObj.Id);
                listContact.add(conObj);
            }
            
            ContactTriggerHandler.executeTrigger = false;
            update listContact;
            ContactTriggerHandler.executeTrigger = true;
        }
    }
     /*Stop creating more than 2 contacts per account*/     
    public static void restrictContactCreation(sObject newSobj){        
        contact c=(contact)newSobj;       
        if(contactMap.get(c.accountid)>2 ){       
            newSobj.addError('You cannot create more than 2 Authorized contacts');      
        }         
    }
    public static void afterProcessingResetInitialization()
    {
        isInitialized = false;
    }   
}