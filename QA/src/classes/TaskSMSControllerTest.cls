/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  @created : 23/07/2016
  @Description : Test class for TaskSMSController class
*/
@isTest
private class TaskSMSControllerTest 
{
	@testSetup 
	static void setupTestData() 
	{
		Map<Id,Schema.RecordTypeInfo> accRecordTypeInfoId		= GlobalUtil.getRecordTypeById('Account'); //getting all Recordtype for the Sobject
	    Map<String,Schema.RecordTypeInfo> accRecordTypeInfoName		= GlobalUtil.getRecordTypeByName('Account');//getting all Recordtype for the Sobject
	    
		Account sAccount 		= new Account();
        sAccount.RecordTypeId   = accRecordTypeInfoName.get('Residential').getRecordTypeId();
        sAccount.Name           = 'Test Account';
        sAccount.Phone          = '0412345678';
        sAccount.Customer_Status__c      = 'Active';
        sAccount.Octane_Customer_Number__c = '123456';
        insert sAccount;
        
        
        Contact sContact       	= new Contact();
        sContact.FirstName  	= 'Joe';
        sContact.LastName  		= 'Belong';
        sContact.AccountId  	= sAccount.Id;
        sContact.MobilePhone	= '0412365498';
        sContact.Contact_Role__c = 'Primary';
        insert sContact;
        
        
       	SMS_Template__c SMSTemplate = new SMS_Template__c();
       	
       	SMSTemplate.Name				= 'Template SMS Test';
        SMSTemplate.Active__c			= true;
        SMSTemplate.Available_to_FOH__c	= true;
        SMSTemplate.Code__c				= 'Test01';
        SMSTemplate.SMS_Template__c		= 'Test Content';
        SMSTemplate.Template_Category__c = 'Payments';
        
        insert SMSTemplate; 
	}
	
    static testMethod void SendSMSMessage() 
    {
        Test.startTest();
        
        Account accObj = [Select id FROM Account LIMIT 1];
        
        PageReference pageRef = Page.TaskSMS;
        pageRef.getParameters().put('Id', accObj.Id);
        Test.setCurrentPageReference(pageRef);
                
        ApexPages.currentPage().getParameters().put('search','test title');
        
        TaskSMSController smsCntr = new TaskSMSController();
        smsCntr.contactSelected = smsCntr.contactsList[1].getValue();
        smsCntr.getContactPhone();
        
        smsCntr.smsTemplateCat = 'Payments';
		smsCntr.getSMSTemplateList();
		
		smsCntr.smsTemplateID = smsCntr.SMSTemplates[1].getValue();
        smsCntr.getSMSBody();
        smsCntr.SendSMS();
        Test.stopTest();
        
    }
    
    static testMethod void SendSMSMessageBlankOptions() 
    {
    	Test.startTest();
        
        Account accObj = [SELECT id FROM Account LIMIT 1];
        
        PageReference pageRef = Page.TaskSMS;
        pageRef.getParameters().put('Id', accObj.Id);
        Test.setCurrentPageReference(pageRef);
                
        ApexPages.currentPage().getParameters().put('search','test title');
        
        TaskSMSController smsCntr = new TaskSMSController();
        
        smsCntr.contactSelected = smsCntr.contactsList[1].getValue();
        smsCntr.getContactPhone();
        smsCntr.contactSelected = '';
        smsCntr.getContactPhone();
        smsCntr.taskSMS.WhatId = NULL;
        smsCntr.SendSMS();
        
        smsCntr.contactSelected = smsCntr.contactsList[1].getValue();
        smsCntr.getContactPhone();
        
        smsCntr.smsTemplateCat = 'Payments';
		smsCntr.getSMSTemplateList();
		smsCntr.smsTemplateCat = '';
		smsCntr.getSMSTemplateList();
		smsCntr.smsTemplateCat = 'Payments';
		smsCntr.getSMSTemplateList();
		
		smsCntr.smsTemplateID = '';
        smsCntr.getSMSBody();
		
        smsCntr.Cancel();
        Test.stopTest();
    }
}