/*
    @author  : Daniel Garzon(dgarzon@deloitte.com)
    @created : 22/03/2016
    @Description : util class for trigger handlers and others
*/
public with sharing class GlobalUtil {
    private static Map<String, Schema.SObjectType> sObjectMap = Schema.getGlobalDescribe();
    private static Map<String, Event_Matrix__C> eventMatrixGlobalMap;
    private static Set<String> excludedFields = new Set<String> {
        'email_template_code__c',
        'sms_template_code__c',
        'notification_type__c',
        'key_name__c'
    };

    private static List<Holiday> holidays {
        get
        {
            if (holidays == null) {
                holidays = [SELECT StartTimeInMinutes, Name, ActivityDate FROM Holiday ];
            }
            return holidays;
        }
        private set;
    }

    public static Map<String, Schema.RecordTypeInfo> getRecordTypeByName(String sObjectName) {
        Schema.SObjectType s;
        Schema.DescribeSObjectResult resSchema;
        s = sObjectMap.get(sObjectName) ; // getting Sobject Type
        resSchema   = s.getDescribe() ;
        return resSchema.getRecordTypeInfosByName(); //getting all Recordtype for the Sobject
    }

    // Draft of method which would find record type ID by developer name. Not easily doable unfortunately
    /*
        public Map<String, Schema.RecordTypeInfo> getRecordTypeByDeveloperName(String sObjectName)
        {
        Map<String, Schema.RecordTypeInfo> mapRecordTypeInfo = getRecordTypeByName(sObjectName);
        Map<String, Schema.RecordTypeInfo> mapDevNameRecordTypeInfo;

        For(Schema.RecordTypeInfo currentRecType : mapRecordTypeInfo.GetValues)
        {
        if(!mapDevNameRecordTypeInfo.contaiskey(currentRecType.getDevelopername()))
        {
        mapDevNameRecordTypeInfo.put(currentRecType.getDevelopername(),Null);
        }
        mapDevNameRecordTypeInfo.put(currentRecType.getDevelopername(),currentRecType);
        }

        return mapDevNameRecordTypeInfo;
        }*/

    public static Map<Id, Schema.RecordTypeInfo> getRecordTypeById(String sObjectName) {
        Schema.SObjectType s;
        Schema.DescribeSObjectResult resSchema;
        s           = sObjectMap.get(sObjectName) ; // getting Sobject Type
        resSchema   = s.getDescribe() ;
        return resSchema.getRecordTypeInfosById(); //getting all Recordtype for the Sobject
    }

    // METHOD: addBusinessDays
    // PURPOSE: increments a date by numDays, while not counting weekend days
    public static Date addBusinessDays(Date theDate, integer numDays) {
        Set <String> weekendDays = new Set <String> {'Saturday', 'Sunday'};
        DateTime addedToDate = DateTime.newInstance (theDate, Time.newInstance(0, 0, 0, 0));
        // Loop numDays times and add days to the date. Skip over weekend days.
        for (integer i = 0; i < numDays; i++) {
            addedToDate = addedToDate.addDays(1);
            while (weekendDays.contains(addedToDate.format('EEEEE'))) {
                addedToDate = addedToDate.addDays(1);
            }
        }
        return addedToDate.date();
    }

    public static DateTime AddBusinessDays(DateTime StartDate, integer BusinessDaysToAdd) {
        //Add or decrease in BusinessDaysToAdd days
        DateTime finalDate = StartDate;
        integer direction = BusinessDaysToAdd < 0 ? -1 : 1;
        system.debug('direction = ' + direction);
        while (BusinessDaysToAdd != 0) {
            finalDate = finalDate.AddDays(direction);
            if (checkifWorkDay(finalDate.date())) {
                BusinessDaysToAdd -= direction;
            }
        }
        return finalDate;
    }

    //To check if sent date is a working day by comparing with org holidays
    public static boolean checkifWorkDay(Date sentDate) {
        Date weekStart  = sentDate.toStartofWeek();
        for (Holiday hday : holidays) {
            if (sentDate.daysBetween(hday.ActivityDate) == 0) {
                return false;
            }
        }
        Datetime dt = DateTime.newInstance(sentDate, Time.newInstance(0, 0, 0, 0));
        if (dt.format('EEEE') == 'Saturday' || dt.format('EEEE') == 'Sunday') {
            return false;
        }
        else {
            return true;
        }
    }

    private static Map<String, Schema.SObjectType> mapSObjectNameSObjType = Schema.getGlobalDescribe();

    public static String mergeSMSTemplateWithSObject(String templateContent, String sobjectName, String recordId) {
        // Get all fields' description of the sboject
        Map<String, Schema.SObjectField> mapSObjectFields = mapSObjectNameSObjType.get(sobjectName).getDescribe().fields.getMap();
        // Find all sobject fields' placeholder in the template content. i.e sObject.fieldName.
        String fieldPlaceHolderRegex = '((?i)\\{' + sobjectName + '\\.[^\\}]+\\})';
        Pattern patrn = Pattern.compile(fieldPlaceHolderRegex);
        Matcher matcr = patrn.matcher(templateContent);
        Set<String> set_placeHolders = new Set<String>();
        String queryString = 'SELECT ';
        Set<String> set_validFieldName = new set<String>(); // keep track of fields in query
        while (matcr.find()) {
            String fieldFind = templateContent.subString(matcr.start() + 1, matcr.end() - 1); // REMOVE THE {}
            String fieldName = fieldFind.split('\\.').get(1).toLowerCase();
            // Found one place holder
            set_placeHolders.add(fieldFind);
            if (!set_validFieldName.contains(fieldName) && mapSObjectFields.containsKey(fieldName)) {
                // the field name is a valid field of this sobject, add it to valid field name set and query string
                queryString += fieldName + ', ';
                set_validFieldName.add(fieldName);
            }
        }
        // Query sobject record and merge with the template
        if (!set_placeHolders.isEmpty()) {
            queryString  = queryString.subString(0, queryString.length() - 2);
            queryString += ' FROM ' + sobjectName + ' WHERE id =: recordId';
            for (Sobject record : database.query(queryString)) {
                // Merge all placeholders with the template
                for (String key : set_placeHolders) {
                    String fieldName = key.split('\\.').get(1).toLowerCase();
                    // Get field value
                    String placeValue = '';
                    if (set_validFieldName.contains(fieldName)) {
                        Schema.DisplayType fieldType = mapSObjectFields.get(fieldName).getDescribe().getType();
                        Object fieldValue = record.get(fieldName);
                        if (fieldValue == null) {
                            placeValue = '';
                        }
                        else
                            if (fieldType == Schema.DisplayType.Date) {
                                placeValue = date.valueOf(fieldValue).format();
                            }
                            else
                                if (fieldType == Schema.DisplayType.DateTime) {
                                    placeValue = datetime.valueOf(fieldValue).format();
                                }
                                else {
                                    placeValue = fieldValue + '';
                                }
                    }
                    // Replace value with placeholders
                    templateContent = templateContent.replace('{' + key + '}', placeValue);
                }
            }
        }
        return templateContent;
    }

    //merge SMS template with a set of Values
    public static String mergeSMSTemplateWithFields(SMS_Template__c smsTemplate, Map<String, String> fieldValueMap) {
        // Find all fields' placeholder in the template content. i.e FieldMap Values.
        String templateContent = smsTemplate.SMS_Template__c;
        for (String keyValue :  fieldValueMap.keySet()) {
            String fieldValue = fieldValueMap.get(keyValue);
            keyValue = '{' + keyValue + '}';
            templateContent = templateContent.replace(keyValue, fieldValue);
        }
        return templateContent;
    }

    //Convert a list of Formaty VALUE;VALUE into as map
    public static Map<String, String> getHashMap(List<String> fieldValuePair) {
        Map<String, String> fieldValueMap = new Map<String, String>();
        for (String fieldValue : fieldValuePair) {
            //List<String> strArr = fieldValue.split(':');
            fieldValueMap.put(fieldValue.substringBefore(':'), fieldValue.substringAfter(':'));
        }
        return fieldValueMap;
    }

    public static String encrypt(String valueToEncrypt, Integer keyLength, Blob cryptoKey) {
        // Generate the data to be encrypted.
        Blob data = Blob.valueof(valueToEncrypt);
        // Encrypt the data and have Salesforce.com generate the initialization vector
        Blob encryptedData = Crypto.encryptWithManagedIV('AES' + String.valueOf(keyLength), cryptoKey, data);
        return EncodingUtil.convertToHex(encryptedData);
    }

    public static String decrypt(String valueToDecrypt, Integer keyLength, Blob cryptoKey) {
        //Decrypt the data - the first 16 bytes contain the initialization vector
        Blob encryptedData = Encodingutil.convertFromHex(valueToDecrypt);
        Blob decryptedData = Crypto.decryptWithManagedIV('AES' + String.valueOf(keyLength), cryptoKey, encryptedData);
        return decryptedData.toString();
    }

    //Get Tamplate code and notification type from EVENY_MATRIX custom setting
    //based on inputs - Event type,subtype and others
    /*  public static Event_Matrix__C getTemplateDetails2(List<String> eventAttributes)
        {
        Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.Event_Matrix__C.fields.getMap();
        Set<String> requiredFields = new Set<String>();

        for(String keyname : fieldMap.keySet())
        {
        if(fieldMap.get(keyname).getDescribe().isCustom() && !excludedFields.contains(keyname))
        {
        requiredFields.add(keyname);
        }
        }

        Map<String,String> eventmatrixMap = getHashMap(eventAttributes);

        //System.debug('eventmatrixMap: '+eventmatrixMap);

        String query = 'select Email_Template_Code__c, SMS_Template_Code__c, notification_type__c from Event_Matrix__C where ';

        for(String field : requiredFields)
        {
        //System.debug('field: '+ field);
        if(String.isNotBlank(eventmatrixMap.get(field.replace('__c',''))))
        {
        query += field + ' = ' + '\'' + eventmatrixMap.get(field.replace('__c',''))+ '\' and ';
        }
        else
        {
        query += field + ' = ' + '\'\' and ';
        }

        }

        System.debug('query: ' + query);

        List<Event_Matrix__C> eventMatrixList = Database.query(query.removeEnd(' and '));
        Event_Matrix__C eventMatrix;

        if(eventMatrixList.size() > 0)
        {
        eventMatrix = eventMatrixList[0];
        }else
        {
        eventMatrix = null;
        }

        return eventMatrix;

        }*/

    public static Event_Matrix__C getTemplateDetails(List<String> eventAttributes) {
        if (eventMatrixGlobalMap == null) {
            eventMatrixGlobalMap = GlobalUtil.getAllEventMatrixMap();
        }
        Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.Event_Matrix__C.fields.getMap();
        List<String> requiredFields = new List<String>();
        Event_Matrix__C eventMatricObj = null;
        for (String keyname : fieldMap.keySet()) {
            if (fieldMap.get(keyname).getDescribe().isCustom() && !excludedFields.contains(keyname)) {
                requiredFields.add(keyname);
            }
        }
        requiredFields.sort();
        String keyName = '';
        Map<String, String> eventAttrMap = getHashMap(eventAttributes);
        for (String field : requiredFields) {
            if (String.isNotBlank(eventAttrMap.get(field.replace('__c', '')))) {
                keyName += eventAttrMap.get(field.replace('__c', '')) + ':';
            }
        }
        System.debug('KeyName: ' + keyName.removeEnd(':'));
        if (String.isNotBlank(keyName)) {
            eventMatricObj = eventMatrixGlobalMap.get(keyName.removeEnd(':'));
        }
        return eventMatricObj;
    }

    public static Map<String, Event_Matrix__C> getAllEventMatrixMap() {
        Map<String, Event_Matrix__C> eventMatrixMap = new Map<String, Event_Matrix__C>();
        Map<String, Event_Matrix__C> customSettings = Event_Matrix__C.getAll();
        for (Event_Matrix__C customSetting : customSettings.values()) {
            eventMatrixMap.put(customSetting.key_name__c, customSetting);
        }
        return eventMatrixMap;
    }

    public static Task createSMSTask(Account accountObj, String subjectName, boolean isBillingFlag, String descriptionText) {
        Task taskSMS = new Task();
        taskSMS.WhatId = accountObj.Id;
        taskSMS.status = 'Completed';
        taskSMS.SMS_Send_Status__c = 'Send';
        taskSMS.Subject = subjectName;
        Map<String, Schema.RecordTypeInfo> taskRecordTypeInfoName  = GlobalUtil.getRecordTypeByName('Task');
        taskSMS.RecordTypeId    = taskRecordTypeInfoName.get('SMS').getrecordTypeId();
        taskSMS.Billing__c = isBillingFlag;
        for (Contact conObj : accountObj.Contacts) {
            if (conObj.Contact_Role__c == 'Primary') {
                taskSMS.WhoId = conObj.Id;
                taskSMS.SMS_Recipient__c = String.valueof(conObj.MobilePhone);
                break;
            }
        }
        taskSMS.Description = descriptionText;
        return taskSMS;
    }

    public static Log__c createLog(String level, String message, String methodName, String request) {
        Log__c logObj = new Log__c(Level__c = level, Message__c = message, Method_Name__c = methodName, Request__c = request);
        return logObj;
    }

    public static String validateNotificationRequest(SFWrapperObject.Notification notifObj) {
        if (String.isNotBlank(notifObj.octaneNumber) && notifObj.eventAttributes != null && !notifObj.eventAttributes.isEmpty()) {
            return null;
        }
        else {
            if (String.isBlank(notifObj.octaneNumber)) {
                return 'No Octane Customer number in request';
            }
            else {
                return notifObj.octaneNumber + ' - Event attributes are null or Empty';
            }
        }
    }

    public class UndefinedSObjectTypeException extends Exception {}

    public static AuthProperties__c getAuthPropertiesByName(String sName) {
        return [SELECT Name, Client_Id__c, Client_Secret__c, Password__c, Username__c, Endpoint__c FROM AuthProperties__c WHERE Name = :sName ];
    }
    //Mobile: Changes Starts here
    public static List<Contact> getContactByOctane(String octaneCustomerNumber) {
        List<Contact> contactMatches = (List<Contact>)getSObjectRecords('Contact', 'Mobile_Octane_Customer_Number__c', octaneCustomerNumber, NULL);
        return contactMatches;
    }

    /*public static List<sObject> getSobjectrecordsWithFields(String sObjectName,
            Set<String> setFields,
            String sWhereClause) {
        String query = 'SELECT ';
        String sAllFields = String.join(new List<String>(setFields), ',');
        query += sAllFields + ' FROM ' + sObjectName + ' WHERE ' + sWhereClause;
        System.debug('Query****' + query);
        return database.query(query);
    }*/
    /***************************************************************************************
        @input  :sObjectName-Name of sObject to e queried
        sIdentifieldAPI - Field API Name to be used as identifier
        sIdentifiers - data for identifier field
        @output : returns Lead id for newly created lead
        @Description : upsert the lead and asset corresponding to the newly activation sim.
    ****************************************************************************************/
    public static List<sObject> getSObjectRecords(
        String sObjectName,
        String sIdentifieldAPI,
        String sIdentifiers,
        String sWhereClause
    ) {
        String OPERATOR_WHERE;
        String sWhereClauseDynamic;
        List<String> identifiers = new List<String>();
        if (NULL != sWhereClause) {
            sWhereClauseDynamic = sWhereClause;
        }
        else
            if (sIdentifiers.contains(',')) {
                identifiers = sIdentifiers.split(',');
                OPERATOR_WHERE = ' in ';
                sWhereClauseDynamic = sIdentifieldAPI + OPERATOR_WHERE + ' :identifiers ';
            }
            else {
                identifiers.add(sIdentifiers);
                OPERATOR_WHERE = ' = ';
                sWhereClauseDynamic = sIdentifieldAPI + OPERATOR_WHERE + ' :identifiers ';
            }
        String query = 'SELECT ';
        Set<String> sFields = Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap().keySet();
        String sAllFields = String.join(new List<String>(sFields), ',');
        query += sAllFields + ' FROM ' + sObjectName + ' WHERE ' + sWhereClauseDynamic;
        System.debug('Query****' + query);
        return database.query(query);
    }
    /***************************************************************************************
        @input  : json Lead String
        @output : returns Lead id for newly created lead
        @Description : upsert the lead and asset corresponding to the newly activation sim.
    ****************************************************************************************/
    public static Database.LeadConvertResult leadConvert(String sLeadID, String sContactId,
            Boolean sCreateOpportunity,
            String sLeadStatus,
            String sOwnerID) {
        Database.LeadConvert lc;
        LeadStatus convertStatus;
        Database.LeadConvertResult lcr;
        try {
            lc = new Database.LeadConvert();
            convertStatus = [SELECT Id, MasterLabel
                             FROM LeadStatus
                             WHERE IsConverted = true
                                                 AND MasterLabel = :sLeadStatus];
            lc.setConvertedStatus(convertStatus.MasterLabel);
            lc.setDoNotCreateOpportunity(sCreateOpportunity);
            //check if the incoming lead is matched wth existing contact, if yes mpa account and contact.
            if (!String.isEmpty(sContactId)) {
                lc.setContactId(sContactId);
                lc.setAccountId([Select AccountID
                                 from Contact
                                 where Id = :sContactId].AccountID);
            }
            lc.setLeadId(sLeadID);
            //set the owner of the contact and Accounts getting converted
            lc.setOwnerId(!String.isEmpty(sOwnerID) ? sOwnerID : UserInfo.getUserId());
            lcr = Database.convertLead(lc);
            System.debug('***lc.getContactID()***' + lcr.getContactID()
                         + 'Successfullly Converted: ' + '***lc.getAccountId()***' + lcr.getAccountId());
        }
        catch (Exception e) {
            //throw exeption to parent class and handle accordingly
            throw e;
        }
        return lcr;
    }


    /******************************************************************************************************************
        @input  : sConfigurationName -  configurationName of custom setting record, This will search for CONTAINS records,
        so unique Names should be given for custom setting record.
        sSOurceData - data JSON for the matching crietria
        {
        "lastName":"Hank",
        "email":"tomhan@gmail.com",
        "Phone":"0405166282",
        "Mobile":"0405166282",
        "dateOfBirth":"12/12/2017"
        }
        sAddtionalWhereClause - additional where clause, if needed
        bExactMatch - to find exact match with one search result or multiple
        @output : returns Lead id for newly created lead
        @Description : upsert the lead and asset corresponding to the newly activation sim.
        Here is the sample of mapping object that should be deifned in custom setting
        {
        "LastName":"lastName",
        "Email":"email",
        "Phone":"Phone",
        "MobilePhone":"Mobile",
        "Birthdate":"dateOfBirth"
        }
        in above json-
        key  :API field name of target object against which data has to be matched
        value: field in the source JSON that will return the data. See source data above. Birthdate will get the data from
        dateOfBirth tag in source json, in this case data will be  12/12/2017
    *******************************************************************************************************************/
    /*public Static MatchingResponse matchSObjectRecord(String sConfigurationName,
            String sSOurceData,
            String sAddtionalWhereClause,
            Boolean bSkipNullDataMatch,
            Boolean bExactMatch) {
        String sMessage = 'Matching Completed';
        //create the source data map form the sObject received in the parmeteres. This map stores the data mapping for search criteria
        Map<String, Object> mapSourcedata;
        //This is the source data against with Matching will be executed.
        //Stored in the custom setting record for each criteria
        Map<String, String> mapJSONDataMapping ;
        Boolean bMatchFound = false;
        list<sObject> listSObjectMatches = null;
        Find_Match_Config__c eachConfig;
        String sWhereClause;
        String sSourceObject;
        String VARIABLE_ASSIGNMENT_OPERATOR = '=';
        String BLANK_CHARACTER = ' ' ;
        String sWhereClauseAll = ' where ' ;
        String QUOTE_CHARACTER = '\'' ;
        String SPLIT_DELIMETER = ',' ;
        String AND_OPERATOR = ' AND ' ;
        String DATE_LITERAL = 'date' ;
        Boolean bFirst = true;
        //Set<String> setMatchingDataFields = new Set<String>();
        Map<Decimal, Find_Match_Config__c> mergeConfig = new Map<Decimal, Find_Match_Config__c>();
        String configSearchString = '%' + sConfigurationName + '%';
        System.debug(configSearchString + '***configSearchString**');
        //Fetch the merge configuration to merge process criterias
        For(Find_Match_Config__c eachConfigVariable: [Select id, name, Execution_Order__c,
                Match_Identifier_Fields__c, Target_Object__c, FieldDataTypes__c, JSON_Data_Mapping__c
                from Find_Match_Config__c
                where Name like :configSearchString and JSON_Data_Mapping__c != null
                order by Execution_Order__c] ) {
            mergeConfig.put(eachConfigVariable.Execution_Order__c, eachConfigVariable);
        }
        if (String.isEmpty(sSOurceData)) {
            bMatchFound = false;
            sMessage = 'No Data for Matching criteria.';
            return new MatchingResponse(bMatchFound, sMessage, NULL);
        }
        mapSourcedata = (Map<String, Object>) JSON.deserializeUntyped(sSOurceData.toLowerCase());
        System.debug(mapSourcedata + '***mapSourcedata**');
        Boolean bValidCriteriaToBeMatched = false;
        For(Decimal executionOrder: mergeConfig.keySet()) {
            eachConfig = mergeConfig.get(executionOrder);
            System.debug('executionOrder***' + executionOrder);
            sWhereClause = ' Where Name!=null ' ;
            bFirst = true;
            String sTargetSObject = eachConfig.Target_Object__c;
            String sQuery = 'Select ';
            List<String> listFieldDataType = eachConfig.FieldDataTypes__c.split(SPLIT_DELIMETER);
            //this order will help to iterate over the data types for the matching criterias
            Integer fieldOrder = 0;
            sQuery += eachConfig.Match_Identifier_Fields__c + ' From ' + sTargetSObject;
            if (!bMatchFound) {
                mapJSONDataMapping = (Map<String, String>)JSON.deserialize(eachConfig.JSON_Data_Mapping__c.toLowerCase(), Map<String, String>.class);
                //this stores the data type of the merge identifiers, based on data type dynamic SOQL construct will change
                listFieldDataType = eachConfig.FieldDataTypes__c.split(SPLIT_DELIMETER);
                fieldOrder = 0;
                sWhereClause = ' Where Name!=null ' ;
                //This will be used to identifiy if the source object field values for croetria is not null
                bValidCriteriaToBeMatched = false;
                System.debug('mapJSONDataMapping***' + mapJSONDataMapping);
                System.debug('eachConfig.Match_Identifier_Fields__c***' + eachConfig.Match_Identifier_Fields__c);
                //List<String> listMergeIdentifiers=eachConfig.Match_Identifier_Fields__c.split(',');
                for (String eachIdentifier : eachConfig.Match_Identifier_Fields__c.toLowerCase().split(SPLIT_DELIMETER)) {
                    System.debug('eachIdentifier***' + eachIdentifier);
                    string eachMappedDataIdentifier = mapJSONDataMapping.get(eachIdentifier);
                    Boolean bMappingDataInSourceExists = (Boolean)mapSourcedata.containsKey(eachMappedDataIdentifier);
                    Boolean bMappingDataNull = bMappingDataInSourceExists
                                               && String.isEmpty((String)mapSourcedata.get(eachMappedDataIdentifier)) ?
                                               TRUE : FALSE;
                    //  break if
                    //    1.data does not exists in the source JSON file. This is based on the JSON mapping defined
                    //    in the each custom setting record for merge configuration.
                    //    2. Data to be Matched is Null.
                    //    Based on parameter (bSkipNullDataMatch) passed to the source method this check is allowed or blocked.
                    //
                    if (!bMappingDataInSourceExists || (bMappingDataNull && bSkipNullDataMatch )) {
                        System.debug('inside Break Loop***');
                        bValidCriteriaToBeMatched = false;
                        fieldOrder++;
                        System.debug(LoggingLevel.DEBUG, 'Skipped Matching criteria****' + eachConfig.Match_Identifier_Fields__c);
                        //exit current loop if the data against matching criteria is NULL.
                        //in this case the matching wont be exact and we will getting lot of dupllciates, hence not a valid criteria to match
                        break;
                    }
                    if (mapJSONDataMapping.containsKey(eachIdentifier)) {
                        if (!String.isEmpty(eachMappedDataIdentifier)) {
                            String sWhereClauseDataForMatch = (String) mapSourcedata.get(eachMappedDataIdentifier);
                            System.debug('sWhereClauseDataForMatch***' +
                                         sWhereClauseDataForMatch);
                            bValidCriteriaToBeMatched = true;
                            sWhereClause += AND_OPERATOR + eachIdentifier + VARIABLE_ASSIGNMENT_OPERATOR;
                            if (listFieldDataType[fieldOrder].equalsIgnorecase(DATE_LITERAL)) {
                                String dateTimeInString = sWhereClauseDataForMatch;
                                System.debug('dateTimeInString***' + dateTimeInString);
                                // Convert String into DateTime using "replace" method of String and "Valueof" method of DateTime
                                Date acceptableDate = Date.Valueof(dateTimeInString);
                                sWhereClause +=   ' :acceptableDate';
                            }
                            else {
                                sWhereClause += QUOTE_CHARACTER + sWhereClauseDataForMatch
                                                + QUOTE_CHARACTER;
                            }
                        }
                    }
                    fieldOrder++;
                }
            }
            //Process only if all the Matching criterias mentioned in custom setting do have values and they are not NULL
            if (bValidCriteriaToBeMatched) {
                sQuery += sWhereClause;
                System.debug('sWhereClause***' + sWhereClause);
                System.debug('sQuery***' + executionOrder + '***' + sQuery + sAddtionalWhereClause);
                sQuery += !String.isEmpty(sAddtionalWhereClause) ? sAddtionalWhereClause : '';
                listSObjectMatches = Database.query(sQuery);
                //return the matching results only if exact match with one result is found
                if ((bExactMatch && listSObjectMatches.size() == 1) || (!bExactMatch && listSObjectMatches.size() > 0)) {
                    System.debug('sQuery that gave result***' + sQuery);
                    bMatchFound = TRUE;
                    break;
                }
            }
        }
        if (!bMatchFound) {
            sMessage = 'No Match Found';
            System.debug(sMessage);
        }
        System.debug('sWhereClauseAll***' + sWhereClauseAll);
        return new MatchingResponse(bMatchFound, sMessage, listSObjectMatches);
    }
    public Class MatchingResponse {
        public Boolean bMatched;
        public String sMessage;
        public List<sObject> listSObjectMatch;
        public MatchingResponse(Boolean bMatched, String sMessage, List<sObject> listSObjectMatch) {
            this.bMatched = bMatched;
            this.listSObjectMatch = listSObjectMatch;
            this.sMessage = sMessage;
        }
    }*/

    //attach blob csv to an sobject
    public static Id attachBlobCSV(string strDataForCSV, String sFileName, Id sParentID) {
        // Add the header row to the top of the file
        // Make CSV file as blob
        Blob blobCSV = Blob.valueOf(strDataForCSV);
        // Attach CSV file to the Delivery Summary
        Attachment AttachmentObj = new Attachment(Body = blobCSV,
                Name = sFileName,
                parentId = sParentID);
        insert AttachmentObj;
        return AttachmentObj.Id;
    }
    // Method to get the name of an sObject based on the prefix of an ID
    public static String getObjectNameFromId(Id objId) {
        String output;
        if (objId != null) {
            Schema.SObjectType sobjtype = objId.getSObjectType();
            if (sobjtype != null) {
                output = sobjtype.getDescribe().getName();
            }
        }
        return output;
    }
    // Method to check if a string is the valid format to be a Salesforce ID
    public static boolean isValidId(String inputId) {
        boolean returnValue = false;
        Pattern regexPattern = pattern.compile('[a-zA-Z0-9]{18}|[a-zA-Z0-9]{15}');
        Matcher regexMatcher = regexPattern.matcher(inputId);
        if (regexMatcher.matches()) {
            returnValue = true;
        }
        return returnValue;
    }
    /***********************************************************************************************
        input: objectName: for which data category map has to be generated, dataCategoryGroup: group
    ***********************************************************************************************/
    public static List<Schema.DataCategory> getDataCategory(String sObjectName, String dataCategoryGroup) {
        List <DataCategoryGroupSobjectTypePair> pairs =  new List<DataCategoryGroupSobjectTypePair>();
        DataCategoryGroupSobjectTypePair pair1 =  new DataCategoryGroupSobjectTypePair();
        pair1.setSobject(sObjectName);
        pair1.setDataCategoryGroupName(dataCategoryGroup);
        pairs.add(pair1);
        List<Schema.DescribeDataCategoryGroupStructureResult> results = Schema.describeDataCategoryGroupStructures(pairs, false);
        System.debug('****Data Category Tree*******' + Json.serializePretty(results[0].getTopCategories()));
        return results[0].getTopCategories();
    }

    /***********************************************************************************************
        input: string for which null shold be escaped and an Emplty string is returned if Null
    ***********************************************************************************************/
    public static String escapeNull(String sInput) {
        return String.isEmpty(sInput) ? '' : sInput;
    }
    /***********************************************************************************************
        input: string for which null shold be escaped and an Emplty string is returned if Null
    ***********************************************************************************************/
    public static Boolean isNull(Object sInput) {
        return NULL == sInput;
    }

    /******************************************************************************************
        input: sObjectName: sObject for which data category has to be fetched
        description: find existing customer using mobile octane, fixed octane or email Address
    *******************************************************************************************/
    public static List<Contact> findExistingCustomer(String octaneNumberMobile, String octaneNumberFixed, String email) {
        List<Contact> listContact;
        List<String> setWhereClause = new List<String>();
        String sWhereClause;
        //if appropriate contact is not found via utilbillID, execute the find match logic.
        if (!String.isEmpty(octaneNumberMobile)) {
            sWhereClause = ' (Mobile_Octane_Customer_Number__c=\'' + octaneNumberMobile + '\' AND Account.Mobile_Customer_Status__c=\'Active\')';
            setWhereClause.add(sWhereClause);
        }
        if (!String.isEmpty(octaneNumberFixed)) {
            sWhereClause = ' (Octane_Customer_Number__c=\'' + octaneNumberFixed + '\' AND Account.Customer_Status__c=\'Active\')';
            setWhereClause.add(sWhereClause);
        }
        if (!String.isEmpty(email)) {
            sWhereClause = ' email=\'' + email + '\'';
            setWhereClause.add(sWhereClause);
        }
        sWhereClause = ' Contact_Role__c=\'Primary\' AND (' + String.join(setWhereClause, ' OR ') + ')';
        System.debug('sWhereClause****' + sWhereClause);
        //Check incoming customer acivation request data against the contact database
        listContact = (List<Contact>) getSObjectRecords('Contact', NULL, NULL, sWhereClause);
        return listContact;
    }

    /******************************************************************************************
        input: len: slength of random string
        description: generate random string
    *******************************************************************************************/
    public static string generateRandomString(Integer len) {
        Blob blobKey = crypto.generateAesKey(192);
        String key = EncodingUtil.base64encode(blobKey);
        return key.substring(0, len);
    }
	
	public static string generateVerificationCode() {
		String randomNumber = string.valueof(Math.abs(Crypto.getRandomLong()));
		String verificationCode = randomNumber.substring(0, 6);
		return verificationCode;
	}
}