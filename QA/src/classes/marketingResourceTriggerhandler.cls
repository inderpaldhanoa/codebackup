public class marketingResourceTriggerhandler{

    public static void handleValidation(list<Marketing_Resource__c> mrkResource){
        for(Marketing_Resource__c mkR:mrkResource){
            
            mkR.Unique_ID__c=mkR.account__c==null?mkR.Lead__c:mkR.account__c;
            
        }
    }
    
    public static void mapPrimaryContact(list<Marketing_Resource__c> mrkResource){
        list<id> accountIds=new list<id>();
        for(Marketing_Resource__c mkR:mrkResource){
            accountIds.add(mkR.account__c);
        }
        
        map<id,id> accountMap=new map<id,id>();
        for(contact c:[select id,accountid from contact where accountid IN:accountIds and Contact_Role__c='Primary']){
            accountMap.put(c.accountid,c.id);
        }
        
        for(Marketing_Resource__c mkR:mrkResource){
            if(accountMap.containskey(mkr.account__c) && mkr.account__c!=null)
                mkR.contact__c=accountMap.get(mkR.account__c);   
        }
    }
}