public class passwordDecoder {
  
    public static boolean checkPaswordValidity(string RawPassword, string encodedPassword){
        string parsedPwd;
        if(encodedPassword.contains('}')){
            integer indexNum=encodedPassword.indexOf('}');
            parsedPwd=encodedPassword.substring(indexNum+1);
        }else{
            parsedPwd=encodedPassword;
        }
        if(parsedPwd.length()>28){
          string hexaValueRawPass= getHexaDecimal(blob.valueOf(RawPassword));
            string hexaValueEncodedPassword=getHexaDecimal(Encodingutil.base64Decode(parsedPwd));
            string salt=getSalt(hexaValueEncodedPassword);
            blob bSHA=generateSHA(converFromHexa(hexaValueRawPass+salt));
            string finalEncodedPwd=getBase64Encode(converFromHexa(getHexaDecimal(bSHA)+salt));
            if((parsedPwd).equals(finalEncodedPwd)){
              return true;    
            }
        }else{
          blob bSHA= generateSHA(blob.valueOf(RawPassword));
            string finalEncodedPwd=getBase64Encode(bSHA);
            if(finalEncodedPwd.equals(parsedPwd)){
              return true;    
            }
        } 
        return false;
    }
    
    private static string getHexaDecimal(blob password){
        return Encodingutil.convertToHex(password);
    }
    
    private static string getBase64Encode(blob toConvertStr){
      String sStr = EncodingUtil.base64Encode(toConvertStr);
        return sStr;
    }
    
    private static string getSalt(string hexaValue){
        return hexaValue.subString(40);    
    }
    private static blob generateSHA(blob hexaValue){
      Blob hash = Crypto.generateDigest('SHA1', hexaValue);
        return hash;
    }
    
    public static blob converFromHexa(string hexaValue){
      return Encodingutil.convertFromHex(hexaValue);    
    }
}