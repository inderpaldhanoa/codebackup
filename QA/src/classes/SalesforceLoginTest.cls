@istest
public class SalesforceLoginTest {
    public static TestMethod void CheckFunction () {
        
        Account Acc = new Account();
        Acc.name = 'test and Acc ';
        //Acc.octane_customer_number__c = '985654';
        insert Acc;
        
        Contact con = new Contact();
        con.lastname = Acc.Id;
        con.email = '1234hjvshjsjhdhv@mailinator.com';
        con.accountid = Acc.id;
        insert con;
        
        updateUserAEMPassword1(con.id);
        
        //user u2 = [select id, Username  from user where contactid = :con.id];
        //system.debug('U customSite::::::'+u2);
        //updateUserAEMPassword(u2.Id);
    }
    
    @future
    public static void updateUserAEMPassword(Id userId){
        User u = [SELECT Id, Username, AEMPassword__c 
                  FROM User 
                  WHERE Id = :userId
                  LIMIT 1];
        u.AEMPassword__c = 'YFKfV6oRJFD4AK2gKlstXjVIsJ/jDZj4VmlP3g==';
        update u;  
        SalesforceLogin.credentials(u.username, 'Welcome45');
    }

    @future
    public static void updateUserAEMPassword1(Id ConId){
        user u = new user();
        u.firstname = 'anthony'	;
        u.LastName	= 'francis'	;
        u.contactId	= conId	;
        u.Email	= '1234444@mailinator.com'	;
        u.UserName = '1234444@mailinator.com'	;
        profile p = [SELECT id FROM profile WHERE name = 'External Identity User'];
        u.ProfileID	 = p.id	 ;
        u.CommunityNickName	= 'ssss'	;
        u.Alias	= 'ssss'	;
        u.EmailEncodingKey	 = 'ISO-8859-1'	;
        u.TimeZoneSidKey	= 'Australia/Sydney'	;
        u.LocaleSidKey	= 'en_AU'	;
        u.LanguageLocaleKey = 'en_US';
        u.AEMPassword__c = 'YFKfV6oRJFD4AK2gKlstXjVIsJ/jDZj4VmlP3g==';
        insert u; 
        SalesforceLogin.credentials(u.username, 'Welcome45');
    }
}