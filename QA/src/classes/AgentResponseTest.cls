@istest
public class AgentResponseTest {
    public static TestMethod void CheckFunction () {
        Test.startTest();
        BusinessHours businessHour = [select id from BusinessHours where Name = 'Social Post SLA'];
        
        account a = new account();
        a.Octane_Customer_Number__c = '111';
        a.name= 'test Customer Account';
        insert a;
        
        Contact sContact = new contact();
        sContact.FirstName       = 'Joetest';
        sContact.LastName        = 'Smithtest';
        sContact.AccountId       = a.Id;
        sContact.Contact_Role__c = 'Primary';
        sContact.Email           = 'Joe@test.com';
        //insert sContact;
        
        case c = new case();
        c.accountid = a.id;
        //c.contactid = sContact.id;
        c.Complaint_Category__c = 'General';
        c.Status = 'Open';
        c.Origin = 'Phone';
        c.origin = 'Web';
        insert c;
        
        socialpost s = new socialpost();
        s.name = 'customerPost';
        s.headline = 'test';
        s.whoid = a.Id; 
        s.parentid = c.id;
        s.posted =  datetime.now();//2017-04-11 00:13:38.000+0000;
        insert s;
        
        
        socialpost s1 = new socialpost();
        s1.name = 'agentPost';
        s1.headline = 'test';
        s1.parentid = c.id;
        s1.posted =  datetime.now();//2017-04-11 00:13:38.000+0000;
        insert s1;
        
        datetime agentRespondTimeWithoutSeconds = datetime.newInstance(2017, 04, 19, 20, 06, 08);
        datetime custRecordTimeWithoutSeconds = datetime.newInstance(2017, 04, 19, 10, 02, 07);
        
        agentResponse.businesHoursDifference(agentRespondTimeWithoutSeconds, custRecordTimeWithoutSeconds, string.valueof(businessHour.id));
        
        test.stopTest();
    }
}