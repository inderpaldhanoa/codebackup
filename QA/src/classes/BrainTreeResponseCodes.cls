public with sharing class BrainTreeResponseCodes
{
	private static Map<String, String> brainTreeResponseMap = new Map<String, String>
	{
		'2001' => 'INVALID_FUNDS',
		'2002' => 'INVALID_FUNDS',
		'2003' => 'INVALID_FUNDS',
		'2004' => 'INVALID_CARD',
		'2005' => 'INVALID_CARD',
		'2006' => 'INVALID_CARD',
		'2007' => 'INVALID_CARD',
		'2008' => 'INVALID_CARD',
		'2009' => 'INVALID_CARD',
		'2010' => 'INVALID_CARD',
		'2017' => 'INVALID_CARD',
		'2018' => 'INVALID_CARD',
		'2022' => 'INVALID_CARD',
		'2024' => 'INVALID_CARD',
		'2039' => 'INVALID_CARD',
		'2051' => 'INVALID_CARD',
		'2057' => 'INVALID_CARD',
		'2070' => 'INVALID_CARD',
		'2071' => 'INVALID_CARD',
		'2072' => 'INVALID_CARD',
		'2074' => 'INVALID_CARD',
		'2000' => 'BANK_DECLINED',
		'2011' => 'BANK_DECLINED',
		'2012' => 'BANK_DECLINED',
		'2013' => 'BANK_DECLINED',
		'2014' => 'BANK_DECLINED',
		'2015' => 'BANK_DECLINED',
		'2038' => 'BANK_DECLINED',
		'2041' => 'BANK_DECLINED',
		'2043' => 'BANK_DECLINED',
		'2044' => 'BANK_DECLINED',
		'2046' => 'BANK_DECLINED',
		'2047' => 'BANK_DECLINED',
		'2053' => 'BANK_DECLINED',
		'3000' => 'MANUAL_INVESTIGATION',
		'2025' => 'MANUAL_INVESTIGATION',
		'2026' => 'MANUAL_INVESTIGATION',
		'2027' => 'MANUAL_INVESTIGATION',
		'2028' => 'MANUAL_INVESTIGATION',
		'2029' => 'MANUAL_INVESTIGATION',
		'2030' => 'MANUAL_INVESTIGATION',
		'2045' => 'MANUAL_INVESTIGATION',
		'2079' => 'MANUAL_INVESTIGATION',
		'2054' => 'MANUAL_INVESTIGATION'
	};

	public static String getFailureType(String brainTreeResponseCode)
	{
		String failureType = brainTreeResponseMap.get(brainTreeResponseCode);
		return String.isNotBlank(failureType) ? failureType : 'OTHERS';
	}
}