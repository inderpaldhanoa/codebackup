//Class is used to save the attachment in social cases which are captured from FaceBook/Twitter

Global class SocialAttachmentHandler {
 
   //Webservice call out method to the SocialPst URL
   @future(callout=true)
   webservice static void createAttachment(String AttachmentUrl, Id SocialPostId){
         
         Map<String,String> httpHeaderMap = new Map<String,String>();  
         
         String endPointURL = AttachmentUrl;           
         httpHeaderMap.put('Content-Type', 'image/jpeg');
         
         RestUtility util = new RestUtility();
         
         Http  http = util.createHttpRequest();
         HttpRequest httpRst = util.createHttpRequest('', '', endPointURL, 'GET', httpHeaderMap);     
         HttpResponse httpRsp = util.createHttpResponse(httpRst,http);
         Integer responseStatusCode = httpRsp.getStatusCode();
         
         string responseValue = '';
         responseValue = httpRsp.getBody();                 
         blob image = httpRsp.getBodyAsBlob();
        
        if(image.size()>0){
        
           Attachment attch = insertSocialPostAttachment(SocialPostId, image);
           insert attch; 
        }
                
        
     }
     
     //This method is used to insert attachment in SocialPost record
     public static Attachment insertSocialPostAttachment(Id SocialPostId, Blob image){
       Attachment n = new Attachment();  
             
        n.ParentId = SocialPostId;
        n.Name = 'myImage.jpg';
        n.Body = image;          
        n.contentType = 'image/jpeg';
        
        return n;
     }
   
}