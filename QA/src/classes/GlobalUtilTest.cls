@isTest
private class GlobalUtilTest
{
	public static Map<String,Schema.RecordTypeInfo> accRecordTypeInfoName;
	public static Account sAccount;
  	public static Contact sContact;

	@testSetup static void setupTestData() 
    {
        accRecordTypeInfoName	= GlobalUtil.getRecordTypeByName('Account');//getting all Recordtype for the Sobject
        
        sAccount                            = new Account();
        sAccount.RecordTypeId               = accRecordTypeInfoName.get('Residential').getRecordTypeId();
        sAccount.Name                       = 'Test Account';
        sAccount.Phone                      = '0456123789';
        sAccount.Octane_Customer_Number__c  = '123456';
        sAccount.Customer_Status__c      	= 'Active';
        insert sAccount;
        
        sContact       			 = new Contact();
        sContact.FirstName  	 = 'Joetest';
        sContact.LastName 		 = 'Smithtest';
        sContact.AccountId  	 = sAccount.Id;
        sContact.Contact_Role__c = 'Primary';
        sContact.Email  	 	 = 'Joe@test.com';
        insert sContact;        
        
    }

    static testmethod void testMergeSMSTemplateWithSObject()
    {
    	Test.startTest();
    	sAccount = [Select id FROM Account];
        sContact = [Select id FROM Contact];

    	String templateContent = 'Dear {Contact.FirstName},I really enjoyed speaking with';
    	String sobjectName = 'Contact';
    	String recordId = '123456';
    	GlobalUtil.mergeSMSTemplateWithSObject(templateContent, sobjectName, sContact.Id);
    	Test.stopTest();
    }
}