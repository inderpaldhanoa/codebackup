public class AgentResponse {
    
    public static void updateRespTime (list<socialpost> allPosts){
        set<id> caseId = new set<id>();
        list<SocialPost> allSocialPosts;
        for(SocialPost sp : allPosts){
            if(sp.ParentId != NULL && sp.WhoId == NULL){
                caseId.add(sp.ParentId);    
            }
        }
        /// Call the Function only when Agent Post created
        if(!caseId.isEmpty()){
            allSocialPosts = [SELECT id, WhoId , Agent_Response_Time__c, CreatedDate, ParentID 
                              FROM SocialPost 
                              WHERE ParentId in :CaseId
                              AND Agent_Response_Time__c = NULL
                              ORDER BY CreatedDate desc];
                list<SocialPost> postToBeUpdate = new list<SocialPost>();
                map<id, List<SocialPost>> mapCaseIdSocialPosts = new map<id, List<SocialPost>>();
                for(SocialPost sp : allSocialPosts){
                    list<SocialPost> postsForACase ;
                    if(mapCaseIdSocialPosts!= NULL && mapCaseIdSocialPosts.containsKey(sp.ParentId)){
                        postsForACase = mapCaseIdSocialPosts.get(sp.ParentId);
                    }
                    else { 
                        postsForACase = new list<SocialPost>();
                    }
                    postsForACase.add(sp);
                    mapCaseIdSocialPosts.put(sp.ParentId, postsForACase);
                }
            businesshours businessHour = [select id, name from businesshours where name = 'Social Post SLA'];
                for(id ParentID : mapCaseIdSocialPosts.keyset()){
                    list<SocialPost> allPostForAParent = mapCaseIdSocialPosts.get(parentID);
                    SocialPost agentRecord = new SocialPost();
                    decimal hoursDifference;
                    for(SocialPost sp : allPostForAParent){
                        if(sp.WhoId == NULL){ // Agent Post
                            agentRecord = sp;   
                        }
                        if(sp.WhoId != NULL){ // Cust Post
                            if(agentRecord != NULL && sp.Agent_Response_Time__c == NULL){ // Only Cust Post created after Agent posts 
                                if(agentRecord.CreatedDate >= sp.CreatedDate){
                                    hoursDifference = 0.00;
                                    hoursDifference = businesHoursDifference(agentRecord.CreatedDate, sp.CreatedDate, string.valueof(businessHour.id));
                                    sp.Agent_Response_Time__c = hoursDifference.setscale(2);
                                    if(hoursDifference >= 2){
                                        sp.SLA_Breach__c= true;
                                    }else {
                                        sp.SLA_Breach__c= false;    
                                    }
                                }
                            }                            
                            postToBeUpdate.add(sp);
                        }
                    }
                }
                update postToBeUpdate;  
            
        }
    }
    
    public static decimal businesHoursDifference (dateTime agentRespondTime, dateTime custRecordTime, string businessHoursId){
        decimal noOfHours;
        datetime agentRespondTimeWithoutSeconds = datetime.newInstance(agentRespondTime.year(), agentRespondTime.month(), agentRespondTime.day(), agentRespondTime.hour(), agentRespondTime.minute(), 0);
        datetime custRecordTimeWithoutSeconds = datetime.newInstance(custRecordTime.year(), custRecordTime.month(), custRecordTime.day(), custRecordTime.hour(), custRecordTime.minute(), 0);
        decimal noOfMin = BusinessHours.diff(businessHoursId, custRecordTimeWithoutSeconds, agentRespondTimeWithoutSeconds)/1000/60;
        if (noOfMin <= 0){
            noOfHours = 0;
        }else if(noOfMin < 60){
            noOfHours = noOfMin/100;
        } else {
            noOfHours = noOfMin/60;
            decimal modulus = (math.mod(Integer.valueof(noOfMin), 60));
            modulus = modulus / 100;
            noOfHours = Math.floor(noOfHours) + modulus;            
        }
        return noOfHours.setScale(2);
    }
}