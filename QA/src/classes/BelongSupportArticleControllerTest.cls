/*
    @author  : Daniel Garzon(dgarzon@deloitte.com)
    @created : 12/05/2016
    @Description : Test Class for BelongSupportArticleController.
*/
@isTest
public with sharing class BelongSupportArticleControllerTest {

    @testSetup static void setupTestData() {
        FAQ__kav faq = new FAQ__kav();
        faq.title = 'test title';
        faq.Leave_it_with_us__c = true;
        faq.Login_State__c = 'Login required';
        faq.URLNAME = 'testUrl';
        faq.Question__c = 'testUrl';
        faq.Answer__c = '<ol><li><a href="/articles/FAQ/How-to-Change-an-NBN-Appointment?artName=nbn-withdraw-order-work-instruction-faq" target="_blank">How to Change an NBN Appointment?';
        faq.Answer__c += '</a></li><li><a href="/articles/FAQ/Why-is-My-NBN-Connection-Speed-Slow?artName=nbn-withdraw-order-work-instruction-faq" target="_blank">Why is My NBN Connection Speed Slow?';
        faq.Answer__c += '</a></li><li><a href="/articles/FAQ/How-to-Change-an-NBN-Appointment?artName=nbn-withdraw-order-work-instruction-faq" target="_blank">How to Change an NBN Appointment?</a>';
        faq.Answer__c += '</li><li><a href="/articles/FAQ/What-is-Belong-on-the-NBN?artName=nbn-withdraw-order-work-instruction-faq" target="_blank">What is Belong on the NBN?</a></li></ol>';
        faq.Answer__c += '<br><b>Note: When you have followed the instructions below you will have:</b>';
        faq.Language  = 'en_US';
        insert faq;
        TestUtil.createKnowledgeSettings();
    }

    static testmethod void testGetTopicArticle() {
        Test.startTest();
        BelongSupportArticleController controller = new BelongSupportArticleController();
        controller.getTopicArticle();
        Test.stopTest();
    }

    static testmethod void testBelongSupportArticleController() {
        Test.startTest();
        FAQ__kav faq = new FAQ__kav();
        faq.Leave_it_with_us__c = false;
        faq.Login_State__c = 'Login required';
        PageReference articlePage = Page.Belong_Support_Article;
        system.Test.setCurrentPage(articlePage);
        ApexPages.StandardController sc = new ApexPages.StandardController(faq);
        ApexPages.currentPage().getParameters().put('artName', 'testUrl');
        BelongSupportArticleController controller = new BelongSupportArticleController(sc);
        String testStr;
        testStr = controller.FAQTitle;
        testStr = controller.dataCategoryAPI;
        faq = [Select id, Title, UrlName, Normalize_URL_Name__c, Summary, Question__c, Answer__c, Keywords_Public_Knowledge__c
               FROM FAQ__kav
               WHERE PublishStatus = 'Draft' AND Language = 'en_US' LIMIT 1];
        controller.constructMetadata(faq);
        controller.replaceInternalURL(faq.Answer__c);
        testStr = controller.pkbTpcCon.publishStatus;
        testStr = controller.siteName;
        testStr = controller.keywordsMeta;
        testStr = controller.descriptionMeta;
        testStr = controller.titleMeta;
        testStr = controller.urlMetaOG;
        testStr = controller.keywordsMetaOG;
        testStr = controller.pageTitle;
        testStr = controller.currentSiteUrl;
        Map<String, String> categoryKeySiteURL = controller.categoryKeySiteURL;
        boolean x = controller.isSite;
        Test.stopTest();
    }
}