@isTest(seealldata = false)
public with sharing class MobileRestCustomerApiHelperTest {
    static String jsonInput = ' {"firstName": "Staging Test",'
                              + '  "lastName": "G",'
                              + '  "email": "testData@a51.com",'
                              + '  "phoneNumber": "0404571176",'
                              + '  "braintreeCustId": "1x281s48010",'
                              + '  "simCardNo": "1456789",'
                              + '  "dateTimePortTerms": "2017-07-14T01:13:33.072Z",'
                              + '  "mobileNo": "0490995650",'
                              + '  "dateOfBirth": "1981-05-04",'
                              + '  "userPassword": "testUserPassword123",'
                              + '  "currentMobileProvider": null,'
                              + '  "acnCustomerNo": null,'
                              + '  "portDateOfBirth": null,'
                              + '  "portServiceType": null,'
                              + '  "termsAndConditions": "AGREE",'
                              + '  "address": {'
                              + '    "additionalAddress": "Melbounrne central",'
                              + '    "subType": "subType",'
                              + '    "subNo": "90",'
                              + '    "streetNo": "394-400",'
                              + '    "streetName": "Lonsdale",'
                              + '    "streetType": "St",'
                              + '    "streetAddress": "Lonsdale Gardens, 394-400 Lonsdale St Lonsdale Gardens, 394-400 Lonsdale St Lonsdale Gardens, 394-400 Lonsdale St",'
                              + '    "suburb": "MELBOURNE",'
                              + '    "state": "VIC",'
                              + '    "postcode": "3000",'
                              + '    "gnafId": "GAVIC422035807",'
                              + '    "addrText": "Lonsdale Gardens, 394-400 Lonsdale Street, MELBOURNE  VIC  3000"'
                              + '  },'
                              + '  "identities": ['
                              + '    {'
                              + '      "utilibillId": "201001",'
                              + '      "accountType": "mobile"'
                              + '    },'
                              + '    {'
                              + '      "utilibillId": "53600",'
                              + '      "accountType": "fixed"'
                              + '    }'
                              + '  ]'
                              + '}';
    static WrapperMobileCustomer wrapperCustomer;

    @testSetup
    static void setupTestData() {
        UserRole ur = [SELECT Id FROM UserRole Where name = 'Belong MOBILE'];
        // Setup test data
        // This code runs as the system user
        Profile p  = [Select id from Profile where name = 'Belong Integration API'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
                          EmailEncodingKey = 'UTF-8',
                          LastName = 'Testing',
                          LanguageLocaleKey = 'en_US',
                          LocaleSidKey = 'en_US',
                          ProfileId = p.Id,
                          UserRoleId = ur.id,
                          TimeZoneSidKey = 'America/Los_Angeles',
                          UserName = 'belongapiuser@testorg.com');
        System.runAs(u) {
            // The following code runs as user 'u'
            TestDataFactoryMobile.createAccountsWithContacts(201, 1);
            //List<Asset> listAsset = TestDataFactoryMobile.createAssets(201);
            //insert listAsset;
        }
    }
    static testMethod void simOrderPostHelperTest() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        WrapperHTTPResponse theResponse;
        // Could get more coverage here by setting up an "External Identity User" which also matches the Octane Id
        string jsonInput = MobileApiTestUtil.sLeadJson;
        req.requestURI = '/services/apexrest/MobileLead/V1';
        // Fake the passed call
        req.requestBody = Blob.valueOf(jsonInput);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        RESTMobileSimOrderResource.createMobileLead();
        Test.stopTest();
    }
    //existing customer
    static testMethod void customerPostHelperTest2() {
        UserRole ur = [SELECT Id FROM UserRole Where name = 'Belong MOBILE'];
        User u = [Select id
                  from user
                  where UserName = 'belongapiuser@testorg.com'];
        wrapperCustomer = WrapperMobileCustomer.parseCustomer(jsonInput) ;
        System.runAs(u) {
            // The following code runs as user 'u'
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            WrapperHTTPResponse theResponse;
            // Fake the passed call
            req.requestURI = '/services/apexrest/MobileCustomer/V1';
            // set request body, existing Asset record that comes for activation from ui
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;
            Test.startTest();
            //password null for existing user
            wrapperCustomer.identities[0].utilibillId = '120001';
            wrapperCustomer.identities[1].utilibillId = '120001';
            req.requestBody = Blob.valueOf(JSON.serialize(wrapperCustomer));
            RESTMobileCreateCustomerResource.createCustomer();
            Test.stopTest();
        }
    }
    static testMethod void customerPostHelperTest() {
        UserRole ur = [SELECT Id FROM UserRole Where name = 'Belong MOBILE'];
        // This code runs as the system user
        User u = [Select id
                  from user
                  where UserName = 'belongapiuser@testorg.com'];
        wrapperCustomer = WrapperMobileCustomer.parseCustomer(jsonInput) ;
        System.runAs(u) {
            // The following code runs as user 'u'
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            WrapperHTTPResponse theResponse;
            req.requestURI = '/services/apexrest/MobileLead/V1';
            // Fake the passed call
            req.requestURI = '/services/apexrest/MobileCustomer/V1';
            // set request body, existing Asset record that comes for activation from ui
            req.requestBody = Blob.valueOf(JSON.serialize(wrapperCustomer));
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;
            Test.startTest();
            //create a new sim record and activate it
            //RESTMobileCreateCustomerResource.createCustomer();
            //customer bought sim from website and than activates
            //this call will create a lead and than an asset assoctaed to this lead
            req.requestURI = '/services/apexrest/MobileLead/V1';
            req.requestBody = Blob.valueOf(JSON.serialize(wrapperCustomer));
            RESTMobileSimOrderResource.createMobileLead();
            List<asset> asss=[Select Sim__c, id from Asset];
                                    System.debug(asss+'************ass');

            Asset ass = [Select Sim__c, id from Asset where Lead__r.Email = 'testData@a51.com'];

            //update the asset record with sim number so that api can see that sm
            ass.Sim__c = '1456789';
            update ass;
            List<Asset> listsimAsset = [Select id, Sim__c, Lead__r.API_Request_JSON__c, Address_Line1__c, Address_Line2__c, Address_Line3__c,
                                        Status, Lead__r.Email, Suburb__c, State__c, Postcode__c, Lead__c, Lead__r.Residential_address__c
                                        from Asset
                                        where Sim__c = '1456789'];
            System.debug('listsimA4526789sset***' + listsimAsset);
            req.requestURI = '/services/apexrest/MobileCustomer/V1';
            req.requestBody = Blob.valueOf(JSON.serialize(wrapperCustomer));
            //this call will pick lead from above and converts to contact
            RESTMobileCreateCustomerResource.createCustomer();
            wrapperCustomer.simCardNo = '456789';
            wrapperCustomer.userPassword = null ;
            wrapperCustomer.identities[0].utilibillId = '105001';
            wrapperCustomer.identities[1].utilibillId = '105001';
            req.requestBody = Blob.valueOf(JSON.serialize(wrapperCustomer));
            RESTMobileCreateCustomerResource.createCustomer();
            //Error, user password null for new user
            //wrapperCustomer.userPassword = Null;
            //req.requestBody = Blob.valueOf(JSON.serialize(wrapperCustomer));
            //RESTMobileCreateCustomerResource.createCustomer();
            //password cannot be blank for new user
            wrapperCustomer = WrapperMobileCustomer.parseCustomer(jsonInput) ;
            wrapperCustomer.userPassword = null;
            wrapperCustomer.simCardNo = '123456';
            wrapperCustomer.identities[0].utilibillId = '123456';
            wrapperCustomer.identities[1].utilibillId = '123452';
            wrapperCustomer.email = '123452@tets.com';
            System.debug('wrapperCustomer***' + wrapperCustomer);
            req.requestBody = Blob.valueOf(JSON.serialize(wrapperCustomer));
            RESTMobileCreateCustomerResource.createCustomer();
            Test.stopTest();
        }
    }
    static testMethod void indirectCustomerPostHelperTest() {
        UserRole ur = [SELECT Id FROM UserRole Where name = 'Belong MOBILE'];
        // Setup test data
        // This code runs as the system user
        User u = [Select id
                  from user
                  where UserName = 'belongapiuser@testorg.com'];
        System.runAs(u) {
            // The following code runs as user 'u'
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            WrapperHTTPResponse theResponse;
            req.requestURI = '/services/apexrest/MobileCustomer/V1';
            // Fake the passed call
            string jsonInput = ' {"firstName": "Staging Test",'
                               + '  "lastName": "G",'
                               + '  "email": "testData@a51.com",'
                               + '  "phoneNumber": "0404571176",'
                               + '  "braintreeCustId": "1x281s48010",'
                               + '  "simCardNo": "45678990",'
                               + '  "dateTimePortTerms": "2017-07-14T01:13:33.072Z",'
                               + '  "mobileNo": "0490995650",'
                               + '  "dateOfBirth": "1981-05-04",'
                               + '  "userPassword": "testUserPassword123",'
                               + '  "currentMobileProvider": null,'
                               + '  "acnCustomerNo": null,'
                               + '  "portDateOfBirth": null,'
                               + '  "portServiceType": null,'
                               + '  "termsAndConditions": "AGREE",'
                               + '  "address": {'
                               + '    "additionalAddress": "Melbounrne central",'
                               + '    "subType": "subType",'
                               + '    "subNo": "90",'
                               + '    "streetNo": "394-400",'
                               + '    "streetName": "Lonsdale",'
                               + '    "streetType": "St",'
                               + '    "streetAddress": "Lonsdale Gardens, 394-400 Lonsdale St Lonsdale Gardens, 394-400 Lonsdale St Lonsdale Gardens, 394-400 Lonsdale St",'
                               + '    "suburb": "MELBOURNE",'
                               + '    "state": "VIC",'
                               + '    "postcode": "3000",'
                               + '    "gnafId": "GAVIC422035807",'
                               + '    "addrText": "Lonsdale Gardens, 394-400 Lonsdale Street, MELBOURNE  VIC  3000"'
                               + '  },'
                               + '  "identities": ['
                               + '    {'
                               + '      "utilibillId": "120001",'
                               + '      "accountType": "mobile"'
                               + '    },'
                               + '    {'
                               + '      "utilibillId": "130201",'
                               + '      "accountType": "fixed"'
                               + '    }'
                               + '  ]'
                               + '}';
            // Fake the passed call
            req.requestBody = Blob.valueOf(jsonInput);
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;
            Test.startTest();
            RESTMobileCreateCustomerResource.createCustomer();
            List<Asset> listsimAsset = [Select id, Sim__c, Lead__r.API_Request_JSON__c, Address_Line1__c, Address_Line2__c, Address_Line3__c,
                                        Status, Lead__r.Email, Suburb__c, State__c, Postcode__c, Lead__c, Lead__r.Residential_address__c
                                        from Asset
                                        where Sim__c = '456789'
                                                AND ContactID != null];
            System.debug('listsimAsset***' + listsimAsset);
            req.requestBody = Blob.valueOf(jsonInput);
            RESTMobileCreateCustomerResource.createCustomer();
            Test.stopTest();
        }
    }
    static testMethod void getCustomerAPIHelperTest() {
        string jsonInput;
        UserRole ur = [SELECT Id
                       FROM UserRole
                       Where name = 'Belong MOBILE'];
        // Setup test data
        // This code runs as the system user
        User u = [Select id
                  from user
                  where UserName = 'belongapiuser@testorg.com'];
        System.runAs(u) {
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            WrapperHTTPResponse theResponse;
            req.requestURI = '/services/apexrest/MobileCustomer/V1';
            req.params.put('utilibillId', '100000');
            req.httpMethod = 'GET';
            RestContext.request = req;
            RestContext.response = res;
            Test.startTest();
            RESTMobileCreateCustomerResource.getCustomer();
            //invalid UtiliBillID test case
            req.params.put('utilibillId', '130s000');
            RESTMobileCreateCustomerResource.getCustomer();
            //invalid 'Could not Find a valid customer with provided utilibillId'; test case
            req.params.remove('utilibillId');
            RESTMobileCreateCustomerResource.getCustomer();
            //Text Cases for PUT API
            //invalid 'Could not Find a valid customer with provided utilibillId'; test case for PUT API
            jsonInput = ' {"firstName": "Staging Test",'
                        + '  "email": "testData@a51.com",'
                        + '  "identities": ['
                        + '    {'
                        + '      "utilibillId": "1000w00",'
                        + '      "accountType": "mobile"'
                        + '    }'
                        + '  ]'
                        + '}';
            req.requestBody = Blob.valueOf(jsonInput);
            RESTMobileCreateCustomerResource.updateCustomer();
            System.assertEquals(MobileRestUtility.message, 'Could not Find a valid customer with provided utiliBillID');
            //Identity cannont be NUll in PUT call ; test case for PUT API
            jsonInput = ' {"firstName": "Staging Test",'
                        + '  "email": "testData@a51.com"'
                        + '}';
            req.requestBody = Blob.valueOf(jsonInput);
            RESTMobileCreateCustomerResource.updateCustomer();
            System.assertEquals(MobileRestUtility.message, 'UtilBillId missing or Invalid');
            //Exception for PUT call ; test case for PUT API
            jsonInput = ' {"firstName": "Staging Test",'
                        + '  "email": "testData@a51.com",,'
                        + '}';
            req.requestBody = Blob.valueOf(jsonInput);
            RESTMobileCreateCustomerResource.updateCustomer();
            Test.stopTest();
        }
    }
    static testMethod void putCustomerAPIHelperTest() {
        string jsonInput;
        UserRole ur = [SELECT Id FROM UserRole Where name = 'Belong MOBILE'];
        // Setup test data
        // This code runs as the system user
        User u = [Select id
                  from user
                  where UserName = 'belongapiuser@testorg.com'];
        System.runAs(u) {
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            WrapperHTTPResponse theResponse;
            req.requestURI = '/services/apexrest/MobileCustomer/V1';
            // Fake the passed call
            req.httpMethod = 'PUT';
            RestContext.request = req;
            RestContext.response = res;
            //update braintree
            jsonInput = ' {"firstName": "Staging Test",'
                        + '  "braintreeCustId": "1x281s48010",'
                        + '  "identities": ['
                        + '    {'
                        + '      "utilibillId": "100000",'
                        + '      "accountType": "mobile"'
                        + '    }'
                        + '  ]'
                        + '}';
            req.requestBody = Blob.valueOf(jsonInput);
            Test.startTest();
            RESTMobileCreateCustomerResource.updateCustomer();
            //update email
            jsonInput = ' {"firstName": "Staging Test",'
                        + '  "email": "testData@a51.com",'
                        + '  "identities": ['
                        + '    {'
                        + '      "utilibillId": "100000",'
                        + '      "accountType": "mobile"'
                        + '    }'
                        + '  ]'
                        + '}';
            req.requestBody = Blob.valueOf(jsonInput);
            RESTMobileCreateCustomerResource.updateCustomer();
            //update phone
            jsonInput = ' {"firstName": "Staging Test",'
                        + '  "mobileNo": "0404571176",'
                        + '  "identities": ['
                        + '    {'
                        + '      "utilibillId": "100000",'
                        + '      "accountType": "mobile"'
                        + '    }'
                        + '  ]'
                        + '}';
            req.requestBody = Blob.valueOf(jsonInput);
            RESTMobileCreateCustomerResource.updateCustomer();
            //update address
            jsonInput = ' {"firstName": "Staging Test",'
                        + '  "address": {'
                        + '    "additionalAddress": "Melbounrne central",'
                        + '    "subType": "subType",'
                        + '    "subNo": "90",'
                        + '    "streetNo": "394-400",'
                        + '    "streetName": "Lonsdale",'
                        + '    "streetType": "St",'
                        + '    "streetAddress": "Lonsdale Gardens, 394-400 Lonsdale St Lonsdale Gardens, 394-400 Lonsdale St Lonsdale Gardens, 394-400 Lonsdale St",'
                        + '    "suburb": "MELBOURNE",'
                        + '    "state": "VIC",'
                        + '    "postcode": "3000",'
                        + '    "gnafId": "GAVIC422035807",'
                        + '    "addrText": "Lonsdale Gardens, 394-400 Lonsdale Street, MELBOURNE  VIC  3000"'
                        + '  },'
                        + '  "identities": ['
                        + '    {'
                        + '      "utilibillId": "100000",'
                        + '      "accountType": "mobile"'
                        + '    }'
                        + '  ]'
                        + '}';
            req.requestBody = Blob.valueOf(jsonInput);
            RESTMobileCreateCustomerResource.updateCustomer();
            Test.stopTest();
        }
    }
    static testMethod void patchCustomerAPIHelperTest() {
        string jsonInput;
        UserRole ur = [SELECT Id
                       FROM UserRole
                       Where name = 'Belong MOBILE'];
        // Setup test data
        // This code runs as the system user
        User u = [Select id
                  from user
                  where UserName = 'belongapiuser@testorg.com'];
        System.runAs(u) {
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            WrapperHTTPResponse theResponse;
            req.requestURI = '/services/apexrest/MobileCustomer/V1';
            // Fake the passed call
            req.httpMethod = 'PUT';
            RestContext.request = req;
            RestContext.response = res;
            //update braintree
            jsonInput = ' {"firstName": "Staging Test",'
                        + '  "simCardNo": "456789",'
                        + '  "dateOfBirth": "1981-05-04",'
                        + '  "identities": ['
                        + '    {'
                        + '      "utilibillId": "100000",'
                        + '      "accountType": "mobile"'
                        + '    }'
                        + '  ]'
                        + '}';
            req.requestBody = Blob.valueOf(jsonInput);
            Test.startTest();
            RESTMobileCreateCustomerResource.patchCustomer();
            jsonInput = ' {"firstName": "Staging Test",'
                        + '  "simCardNo": "456789",'
                        + '  "portDateOfBirth": "1981-05-04",'
                        + '  "identities": ['
                        + '    {'
                        + '      "utilibillId": "100000",'
                        + '      "accountType": "mobile"'
                        + '    }'
                        + '  ]'
                        + '}';
            req.requestBody = Blob.valueOf(jsonInput);
            RESTMobileCreateCustomerResource.patchCustomer();
            jsonInput = ' {"firstName": "Staging Test",'
                        + '  "simCardNo": "45678ss9",'
                        + '  "portDateOfBirth": "1981-05-04",'
                        + '  "identities": ['
                        + '    {'
                        + '      "utilibillId": "10ss0000",'
                        + '      "accountType": "mobile"'
                        + '    }'
                        + '  ]'
                        + '}';
            req.requestBody = Blob.valueOf(jsonInput);
            RESTMobileCreateCustomerResource.patchCustomer();
            Test.stopTest();
        }
    }
    static testMethod void errorsCustomerPostHelperTest() {
        string jsonInput;
        UserRole ur = [SELECT Id
                       FROM UserRole
                       Where name = 'Belong MOBILE'];
        // Setup test data
        // This code runs as the system user
        User u = [Select id
                  from user
                  where UserName = 'belongapiuser@testorg.com'];
        System.runAs(u) {
            // The following code runs as user 'u'
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            WrapperHTTPResponse theResponse;
            req.requestURI = '/services/apexrest/MobileCustomer/V1';
            // Fake the passed call
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;
            Test.startTest();
            //test case:
            //identities null
            jsonInput = ' {'
                        + '  "simCardNo": "456789",'
                        + '  "identities":null'
                        + '}';
            req.requestBody = Blob.valueOf(jsonInput);
            RESTMobileCreateCustomerResource.createCustomer();
            //Null sim card test case
            jsonInput = ' {'
                        + '  "simCardNo": null,'
                        + '  "identities":null'
                        + '}';
            req.requestBody = Blob.valueOf(jsonInput);
            RESTMobileCreateCustomerResource.createCustomer();
            jsonInput = ' {'
                        + '  "email": "456789",'
                        + '  "identities":null'
                        + '}';
            req.requestBody = Blob.valueOf(jsonInput);
            RESTMobileCreateCustomerResource.createCustomer();
            jsonInput = '';
            //null post body
            req.requestBody = Blob.valueOf(jsonInput);
            RESTMobileCreateCustomerResource.createCustomer();
            //mobile bill id is required
            jsonInput = ' {"firstName": "Staging Test",'
                        + '  "lastName": "G",'
                        + '  "email": "testData@a51.com",'
                        + '  "phoneNumber": "0404571176",'
                        + '  "braintreeCustId": "1x281s48010",'
                        + '  "simCardNo": "45678990",'
                        + '  "dateTimePortTerms": "2017-07-14T01:13:33.072Z",'
                        + '  "mobileNo": "0490995650",'
                        + '  "dateOfBirth": "1981-05-04",'
                        + '  "userPassword": "testUserPassword123",'
                        + '  "currentMobileProvider": null,'
                        + '  "acnCustomerNo": null,'
                        + '  "portDateOfBirth": null,'
                        + '  "portServiceType": null,'
                        + '  "termsAndConditions": "AGREE",'
                        + '  "address": {'
                        + '    "additionalAddress": "Melbounrne central",'
                        + '    "subType": "subType",'
                        + '    "subNo": "90",'
                        + '    "streetNo": "394-400",'
                        + '    "streetName": "Lonsdale",'
                        + '    "streetType": "St",'
                        + '    "streetAddress": "Lonsdale Gardens, 394-400 Lonsdale St Lonsdale Gardens, 394-400 Lonsdale St Lonsdale Gardens, 394-400 Lonsdale St",'
                        + '    "suburb": "MELBOURNE",'
                        + '    "state": "VIC",'
                        + '    "postcode": "3000",'
                        + '    "gnafId": "GAVIC422035807",'
                        + '    "addrText": "Lonsdale Gardens, 394-400 Lonsdale Street, MELBOURNE  VIC  3000"'
                        + '  },'
                        + '  "identities": ['
                        + '    {'
                        + '      "utilibillId": "10ss0000",'
                        + '      "accountType": "fixed"'
                        + '    }'
                        + '  ]'
                        + '}';
            req.requestBody = Blob.valueOf(jsonInput);
            RESTMobileCreateCustomerResource.createCustomer();
            //password cannot be null for new users
            jsonInput = ' {"firstName": "Staging Test",'
                        + '  "lastName": "G",'
                        + '  "email": "testDasta@a51.com",'
                        + '  "phoneNumber": "0404571176",'
                        + '  "braintreeCustId": "1x281s48010",'
                        + '  "simCardNo": "456s78990",'
                        + '  "dateTimePortTerms": "2017-07-14T01:13:33.072Z",'
                        + '  "mobileNo": "0490995650",'
                        + '  "dateOfBirth": "1981-05-04",'
                        + '  "userPassword": null'
                        + '  "currentMobileProvider": null,'
                        + '  "acnCustomerNo": null,'
                        + '  "portDateOfBirth": null,'
                        + '  "portServiceType": null,'
                        + '  "termsAndConditions": "AGREE",'
                        + '  "address": {'
                        + '    "additionalAddress": "Melbounrne central",'
                        + '    "subType": "subType",'
                        + '    "subNo": "90",'
                        + '    "streetNo": "394-400",'
                        + '    "streetName": "Lonsdale",'
                        + '    "streetType": "St",'
                        + '    "streetAddress": "Lonsdale Gardens, 394-400 Lonsdale St Lonsdale Gardens, 394-400 Lonsdale St Lonsdale Gardens, 394-400 Lonsdale St",'
                        + '    "suburb": "MELBOURNE",'
                        + '    "state": "VIC",'
                        + '    "postcode": "3000",'
                        + '    "gnafId": "GAVIC422035807",'
                        + '    "addrText": "Lonsdale Gardens, 394-400 Lonsdale Street, MELBOURNE  VIC  3000"'
                        + '  },'
                        + '  "identities": ['
                        + '    {'
                        + '      "utilibillId": "120s001",'
                        + '      "accountType": "mobile"'
                        + '    },'
                        + '    {'
                        + '      "utilibillId": "1302s01",'
                        + '      "accountType": "fixed"'
                        + '    }'
                        + '  ]'
                        + '}';
            // Fake the passed call
            req.requestBody = Blob.valueOf(jsonInput);
            RESTMobileCreateCustomerResource.createCustomer();
            //cannot set password for existing users
            jsonInput = ' {"firstName": "Staging Test",'
                        + '  "lastName": "G",'
                        + '  "email": "tesstData@a51.com",'
                        + '  "phoneNumber": "0404571176",'
                        + '  "braintreeCustId": "1x281s48010",'
                        + '  "simCardNo": "4s5678990",'
                        + '  "dateTimePortTerms": "2017-07-14T01:13:33.072Z",'
                        + '  "mobileNo": "0490995650",'
                        + '  "dateOfBirth": "1981-05-04",'
                        + '  "userPassword": "test123456"'
                        + '  "currentMobileProvider": null,'
                        + '  "acnCustomerNo": null,'
                        + '  "portDateOfBirth": null,'
                        + '  "portServiceType": null,'
                        + '  "termsAndConditions": "AGREE",'
                        + '  "address": {'
                        + '    "additionalAddress": "Melbounrne central",'
                        + '    "subType": "subType",'
                        + '    "subNo": "90",'
                        + '    "streetNo": "394-400",'
                        + '    "streetName": "Lonsdale",'
                        + '    "streetType": "St",'
                        + '    "streetAddress": "Lonsdale Gardens, 394-400 Lonsdale St Lonsdale Gardens, 394-400 Lonsdale St Lonsdale Gardens, 394-400 Lonsdale St",'
                        + '    "suburb": "MELBOURNE",'
                        + '    "state": "VIC",'
                        + '    "postcode": "3000",'
                        + '    "gnafId": "GAVIC422035807",'
                        + '    "addrText": "Lonsdale Gardens, 394-400 Lonsdale Street, MELBOURNE  VIC  3000"'
                        + '  },'
                        + '  "identities": ['
                        + '    {'
                        + '      "utilibillId": "100001",'
                        + '      "accountType": "mobile"'
                        + '    },'
                        + '    {'
                        + '      "utilibillId": "130201",'
                        + '      "accountType": "fixed"'
                        + '    }'
                        + '  ]'
                        + '}';
            req.requestBody = Blob.valueOf(jsonInput);
            RESTMobileCreateCustomerResource.createCustomer();
            //USer creation error, last name not equal to password
            jsonInput = ' {"firstName": "Staging Test",'
                        + '  "lastName": "test123456",'
                        + '  "email": "tesstData@a51.com",'
                        + '  "phoneNumber": "0404571176",'
                        + '  "braintreeCustId": "1x281s48010",'
                        + '  "simCardNo": "4s5678990",'
                        + '  "dateTimePortTerms": "2017-07-14T01:13:33.072Z",'
                        + '  "mobileNo": "0490995650",'
                        + '  "dateOfBirth": "1981-05-04",'
                        + '  "userPassword": "test123456"'
                        + '  "currentMobileProvider": null,'
                        + '  "acnCustomerNo": null,'
                        + '  "portDateOfBirth": null,'
                        + '  "portServiceType": null,'
                        + '  "termsAndConditions": "AGREE",'
                        + '  "address": {'
                        + '    "additionalAddress": "Melbounrne central",'
                        + '    "subType": "subType",'
                        + '    "subNo": "90",'
                        + '    "streetNo": "394-400",'
                        + '    "streetName": "Lonsdale",'
                        + '    "streetType": "St",'
                        + '    "streetAddress": "Lonsdale Gardens, 394-400 Lonsdale St Lonsdale Gardens, 394-400 Lonsdale St Lonsdale Gardens, 394-400 Lonsdale St",'
                        + '    "suburb": "MELBOURNE",'
                        + '    "state": "VIC",'
                        + '    "postcode": "3000",'
                        + '    "gnafId": "GAVIC422035807",'
                        + '    "addrText": "Lonsdale Gardens, 394-400 Lonsdale Street, MELBOURNE  VIC  3000"'
                        + '  },'
                        + '  "identities": ['
                        + '    {'
                        + '      "utilibillId": "100001",'
                        + '      "accountType": "mobile"'
                        + '    },'
                        + '    {'
                        + '      "utilibillId": "130201",'
                        + '      "accountType": "fixed"'
                        + '    }'
                        + '  ]'
                        + '}';
            req.requestBody = Blob.valueOf(jsonInput);
            RESTMobileCreateCustomerResource.createCustomer();
            //Error Lead scenario
            String sCustomerJsoni = ' {'
                                    + '  "address":null,'
                                    + '}';
            req.requestBody = Blob.valueOf(sCustomerJsoni);
            RESTMobileSimOrderResource.createMobileLead();
            Test.stopTest();
        }
    }
}