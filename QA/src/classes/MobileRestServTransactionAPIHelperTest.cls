/************************************************************************************************************
* Apex Class Name	: MobileRestServTransactionAPIHelperTest.cls
* Version 			: 1.0 
* Created Date  	: 27 July 2017
* Function 			: Test class for MobileRestServTransactionAPIHelper.
* Modification Log	:
* Developer				Date				Description
* -----------------------------------------------------------------------------------------------------------
* Tom Clayton			27/07/2017 			Created Class.
************************************************************************************************************/

@isTest
private class MobileRestServTransactionAPIHelperTest {
	public static Account account1;
	public static Contact contact1, contact2;
	public static Case case1, case2;
	
	@testSetup static void setupTestData()
	{
		// Create test data 
		account1 = TestDataFactoryMobile.createAccounts(1)[0];
		system.debug('--Created an account: ' + account1);
		insert account1;
		
		list <Contact> lstContacts = TestDataFactoryMobile.createContacts(2);
		system.debug('--Created a list of contacts: ' + lstContacts);
		lstContacts[0].AccountId = account1.Id;
		lstContacts[1].AccountId = account1.Id;
		insert lstContacts;
		
		Public_Knowledge_Topic__c publicKnowledgeTopic = TestDataFactoryMobile.createPublicKnowledgeTopics(1)[0];
		insert publicKnowledgeTopic;
		
		list <Case> lstCases = TestDataFactoryMobile.createCases(2);
		lstCases[0].AccountId = account1.Id;
		lstCases[0].ContactId = lstContacts[0].Id;
		lstCases[1].AccountId = account1.Id;
		lstCases[1].ContactId = lstContacts[1].Id;
		insert lstCases;
		
		list <Service_Transactions__c> lstServTrans = TestDataFactoryMobile.createServiceTransactions(2);
		insert lstServTrans;
	}
	
	static testMethod void getServiceTransactionsTest() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        account1 = [SELECT Id, Name, Mobile_Octane_Customer_Number__c FROM Account][0];
        
        req.requestURI = '/services/apexrest/RESTMobileAPI?APICall=getServiceTransactions&octaneId=' + account1.Mobile_Octane_Customer_Number__c + '&startOffset=0&maxNumOfResults=10&startDate=2016-01-01&endDate=2030-01-01';
        req.params.put('APICall', 'getServiceTransactions');
        req.params.put('octaneId', account1.Mobile_Octane_Customer_Number__c);
        req.params.put('startOffset', '0');
        req.params.put('maxNumOfResults', '10');
        req.params.put('startDate', '2016-01-01');
        req.params.put('endDate', '2030-01-01');
        
        // Fake the passed call
        req.requestBody = Blob.valueOf('Test Body');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        	RESTMobileAPI.getMobile();
        Test.stopTest();
        
        String testResponseBody = res.responseBody.toString();
        
        system.debug('--The GetServiceTransactionsTest responseBody was: ' + testResponseBody);
        system.assert(testResponseBody.contains('200'), 'The getServiceTransactionsTest responseBody did not include the success message.');
    }
    
    static testMethod void upsertServiceTransactionTest() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        account1 = [SELECT Id, Name, Mobile_Octane_Customer_Number__c FROM Account][0];
        
        string jsonInput = '{ "telFlowId": "ORD000123411111", '
        					+ '"octaneId": "' + account1.Mobile_Octane_Customer_Number__c + '",'
        					+ '"status": "Complete"}';
        
        req.requestURI = '/services/apexrest/RESTMobileAPI?APICall=upsertServiceTransaction';
        req.params.put('APICall', 'upsertServiceTransaction');
        
        // Fake the passed call
        req.requestBody = Blob.valueOf(jsonInput);
        req.httpMethod = 'PATCH';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        	RESTMobileAPI.patchMobile();
        Test.stopTest();
        
        String testResponseBody = res.responseBody.toString();
        
        system.debug('--The upsertServiceTransactionTest responseBody was: ' + testResponseBody);
        system.assert(testResponseBody.contains('200'), 'The upsertServiceTransactionTest responseBody did not include the success message.');
    }
}