@isTest
private class GuidUtilTest
{
     static testmethod void testGuidUtil()
    {
        Test.startTest();
        String id = GuidUtil.NewGuid();
        System.assertNotEquals(id,'');
        Test.stopTest();
    }
}