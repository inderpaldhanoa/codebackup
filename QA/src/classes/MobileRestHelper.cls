/************************************************************************************************************
    Apex Class Name   : MobileRestHelper.cls
    Version           : 1.0
    Created Date      : 22 March 2017
    Function          : Mobile Rest helper class. This is used by Mobile API
    Modification Log  :
    Developer                 Date                Description
    -----------------------------------------------------------------------------------------------------------
    Girish P                  18/04/2017          Created Class.
    Tom Clayton / Girish P    01/05/2017          Ongoing changes and build.
************************************************************************************************************/
public without sharing class MobileRestHelper {
    //All constants
    Public static Final String HTTP_SUCCESS = 'Success';
    Public static Final String HTTP_ERROR = 'Error';
    Public static Final String STATUS_EXCEPTION = 'Exception';
    Public static Lead mobileLead;
    public static Object[] results ;
    public static final Integer STATUS_BAD = RestUtility.STATUS_BAD;
    public static final Integer STATUS_OK = RestUtility.STATUS_OK;
    public static final Integer STATUS_ISE = RestUtility.STATUS_ISE;
    public static WrapperVerifyMobileNo wrapperVerify;
    /*
        @input  : GET request with no parameters
        @output : A list of Public Knowledge Topics
        @Description : Helper method for the HTTP GET method for RESTMobileAPI.cls
    */
    public static WrapperHTTPResponse getPublicKnowledgeTopicsHelper() {
        List<Public_Knowledge_Topic__c> lstPublicKnowledgeTopic = new List<Public_Knowledge_Topic__c>();
        try {
            // Query all the custom setting values into the list
            string whereClause = 'Active__c = True AND Product__c = \'' + MobileConstants.DEFAULT_PUBLIC_KNOWLEDGE_TYPE + '\'';
            lstPublicKnowledgeTopic = (List<Public_Knowledge_Topic__c>)GlobalUtil.getSObjectRecords('Public_Knowledge_Topic__c', '', '', whereClause);
            if (!lstPublicKnowledgeTopic.isEmpty()) {
                MobileRestUtility.status = HTTP_SUCCESS;
                MobileRestUtility.statuscode = STATUS_OK;
                MobileRestUtility.message = 'Successfully found ' + lstPublicKnowledgeTopic.size() + ' Public Knowledge Topics.';
            }
            else {
                MobileRestUtility.message = 'Error: No Public Knowledge Topics found.';
                return MobileRestUtility.returnWithError('getPublicKnowledgeTopicsHelper', '');
            }
        }
        catch (Exception e) {
            System.debug('Exception!' + e);
            MobileRestUtility.setExceptionResponse(e, 'getPublicKnowledgeTopicsHelper', NULL);
        }
        return RestUtility.getResponse(MobileRestUtility.status, MobileRestUtility.message, MobileRestUtility.statuscode, lstPublicKnowledgeTopic, MobileRestUtility.logs);
    }
    /*  @input  : JSON data from API
        @output : returns the tree strcuture of data category
        @Description : Helper method for the HTTP - method for RESTMobileAPI.cls
    */
    public static WrapperHTTPResponse ivrApiHelper(String jsonData) {
        Task newTask;
        String objectName;
        String whoID;
        String whatID;
        // Get the values out of the JSON string into a map
        try {
            WrapperIVR wrapperIVRdata = (WrapperIVR) JSON.deserialize(jsonData, WrapperIVR.class);
            if (String.isEmpty(wrapperIVRdata.salesforceId)) {
                MobileRestUtility.message = 'Invalid SalesforceID  cannot be null';
                return MobileRestUtility.returnWithError('ivrApiHelper', jsonData);
            }
            objectName = Globalutil.getObjectNameFromId(wrapperIVRdata.salesforceId);
            //check the incoming ID
            if ('Lead' == objectName) {
                //assign to lead
                List<Lead> listLead = [Select id
                                       from Lead
                                       where Id = :wrapperIVRdata.salesforceId];
                whoID = !listLead.isEmpty() ? listLead[0].id : null;
            }
            else
                if ('Contact' == objectName) {
                    List<Contact> listContact = [Select id, Accountid
                                                 from Contact
                                                 where Id = :wrapperIVRdata.salesforceId];
                    if (!listContact.isEmpty()) {
                        whoID = listContact[0].id;
                        whatID = listContact[0].Accountid;
                    }
                }
                else {
                    MobileRestUtility.message = 'Invalid SalesforceID';
                    return MobileRestUtility.returnWithError('ivrApiHelper', jsonData);
                }
            //If who Id is not populated, provided ID was invalid
            if (String.isEmpty(whoID)) {
                MobileRestUtility.message = 'Customer Not Found. Please provide valid utilibillId';
                return MobileRestUtility.returnWithError('ivrApiHelper', jsonData);
            }
            newTask = new Task(Subject = wrapperIVRdata.taskType,
                               Status = 'New',
                               Type = 'IVR Task',
                               Task_Type__c = wrapperIVRdata.taskType,
                               whoId = whoID,
                               whatID = whatID,
                               //todo recordtype with daniel
                               RecordTypeId = (Id)GlobalUtil.getRecordTypeByName('Task').get(Label.MOBILE_RECORDTYPE_IVR).getRecordTypeId()
                              );
            insert newTask ;
            MobileRestUtility.message = 'Successfully created task';
        }
        catch (Exception e) {
            MobileRestUtility.setExceptionResponse(e, 'ivrApiHelper', jsonData);
        }
        return RestUtility.getResponse(MobileRestUtility.status, MobileRestUtility.message, MobileRestUtility.statuscode, new list<Task> {newTask}, MobileRestUtility.logs);
    }

    /*  @input  : - void
        @output : - returns the tree strcuture of data category
        @Description : Helper method for the HTTP - method for RESTMobileAPI.cls
    */
    public static Object getDataCategory() {
        String response ;
        RestResponse res = RestContext.response;
        // Get the values out of the JSON string into a map
        try {
            response = JSON.serialize((List<Schema.DataCategory>)Globalutil.getDataCategory('KnowledgeArticleVersion', 'Mobile'));
            System.debug('*******' + response);
        }
        catch (Exception e) {
            MobileRestUtility.setExceptionResponse(e, 'getDataCategory', '');
            return RestUtility.getResponse(MobileRestUtility.status, MobileRestUtility.message, MobileRestUtility.statuscode, NULL, MobileRestUtility.logs);
        }
        res.responseBody = Blob.valueOf(response);
        return response;
    }
    /*  @input  : - void
        @output : - returns the tree strcuture of data category
        @Description : Helper method for the HTTP - method for RESTMobileAPI.cls
    */
    /*  Not in UI commenting for now*/
    /*
        public static WrapperHTTPResponse validateMaxSim(Map<String, String> mapParameters) {
        Integer LIMIT_SIM = Integer.valueOf(System.label.MOBILE_LIMIT_SIM_MAXIMUM);
        //LIMIT_SIM = 1;
        Integer simCount = 0;
        Map<String, Object> mapSIMCount = new Map<String, Object> {
            'SimCount' => 0,
            'SimLimit' => LIMIT_SIM,
            'isHuman' => true
        };
        String email;
        String gnaf;
        MobileRestUtility.message = 'This is Human.';
        if (!mapParameters.containsKey('email') || !mapParameters.containsKey('gnafId')) {
            MobileRestUtility.message = 'Email / gnafId Parameter missing';
            return MobileRestUtility.returnWithError('validateMaxSim', JSON.serialize(mapParameters));
        }
        email = mapParameters.get('email');
        gnaf = mapParameters.get('gnafId');
        if (String.isEmpty(gnaf)) {
            return RestUtility.getResponse('Error', 'Invalid GnafID', 400, NULL, MobileRestUtility.logs);
        }
        // Get the values out of the JSON string into a map
        try {
            List<AggregateResult> countSimResults = [Select count(id) simCount
                                                    from Asset
                                                    where RecordTypeId = :(Id)GlobalUtil.getRecordTypeByName('Asset').get('Mobile').getRecordTypeId()
                                                            AND(
                                                                    (Lead__r.Email = :email
                                                                            AND Lead__r.isConverted = false
                                                                                    AND Lead__r.Product_Type__c = 'Mobile'
                                                                    )
                                                                    OR Contact.Email = :email
                                                            )
                                                            AND Status = :MobileConstants.ASSET_STATUS_REQUESTED
                                                                    AND  DeliveryAddressID__c = :gnaf
                                                                            AND Createddate < LAST_N_DAYS:30
                                                    ];
            simCount = Integer.valueOf(countSimResults[0].get('simCount'));
            mapSIMCount.put('SimCount', Integer.valueOf(countSimResults[0].get('simCount')));
            if (simCount > LIMIT_SIM) {
                mapSIMCount.put('isHuman', false);
                MobileRestUtility.statuscode = 400;
                MobileRestUtility.status = HTTP_ERROR;
                MobileRestUtility.message = 'Looks like this is a robot. ' + email + ' already ordered ' + simCount + ' SIMs. Max Sim limit is ' + LIMIT_SIM + ' per Email Address';
            }
        }
        catch (Exception e) {
            mapSIMCount.put('isHuman', null);
            MobileRestUtility.setExceptionResponse(e, 'validateMaxSim', JSON.serialize(mapParameters));
            return RestUtility.getResponse(MobileRestUtility.status, MobileRestUtility.message, MobileRestUtility.statuscode, NULL, MobileRestUtility.logs);
        }
        return RestUtility.getResponse(MobileRestUtility.status, MobileRestUtility.message, MobileRestUtility.statuscode, new List<Object> {mapSIMCount}, MobileRestUtility.logs);
        }
    */

    public static WrapperHTTPResponse changePassword(map<String, String> mapParameters) {
        Task newTask;
        List<User> listCust;
        try {
            //return if utilibillId or user password is missing
            if (!mapParameters.containsKey('utilibillId') || !mapParameters.containsKey('newPassword')) {
                MobileRestUtility.message = 'Utilibill id or Password missing';
                return MobileRestUtility.returnWithError('changePassword', JSON.serialize(mapParameters));
            }
            //listCust = GlobalUtil.getSObjectRecords('User', 'Mobile_Octane_Id__c', mapParameters.get('utilibillId'), NULL);
            listCust = [Select id, Mobile_Octane_Id__c
                        from User
                        where Mobile_Octane_Id__c = : mapParameters.get('utilibillId')];
            //return if could not find valid user
            if (listCust.isEmpty()) {
                MobileRestUtility.message = 'User Not Found. Please provide valid utilibillId';
                return MobileRestUtility.returnWithError('changePassword',  JSON.serialize(mapParameters));
            }
            //TODO field mapping
            System.setPassword(listCust[0].Id, mapParameters.get('newPassword'));
            //APIIdentityUser.changeUserPassword(listCust[0], mapParameters.get('newPassword'));
            MobileRestUtility.message = 'Successfully changed Password';
        }
        catch (Exception e) {
            MobileRestUtility.setExceptionResponse(e, 'changePassword', JSON.serialize(mapParameters));
        }
        return RestUtility.getResponse(MobileRestUtility.status, MobileRestUtility.message, MobileRestUtility.statuscode, NULL, MobileRestUtility.logs);
    }

    /*  @input  : - json input from service layer
        @output : - returns newly created task to send the payment notification
        @Description : Helper method for the HTTP - method for RESTMobileAPI.cls
    */
    public static WrapperHTTPResponse notificationTasksHelper(String jsonData) {
        Task newTask;
        String notificationType;
        String RECORD_TYPE_MOBILE_SMS = 'Mobile SMS';
        try {
            WrapperPaymentNotification wrapperPayment = WrapperPaymentNotification.parse(jsonData);
            if (String.isEmpty(wrapperPayment.octaneCustomerID)) {
                MobileRestUtility.message = 'octaneCustomerID attribute cannot be null. Please provide valid octaneCustomerID';
                return MobileRestUtility.returnWithError('notificationTasksHelper', jsonData);
            }
            List<Contact> listExistingCust = GlobalUtil.findExistingCustomer(wrapperPayment.octaneCustomerID, null, null);
            if (listExistingCust.isEmpty()) {
                MobileRestUtility.message = 'Customer Not Found. Please provide valid octaneCustomerID';
                return MobileRestUtility.returnWithError('notificationTasksHelper', jsonData);
            }
            notificationType = 'Notification';
            //TODO field mapping
            newTask = new Task(Subject = wrapperPayment.type,
                               Status = 'New',
                               Billing__c = True,
                               Type = notificationType,
                               Task_Type__c = wrapperPayment.type,
                               Task_Sub_Type__c = wrapperPayment.subtype,
                               Priority = 'Normal',
                               ActivityDate = !String.isEmpty(wrapperPayment.duedate) ? Date.valueOf(wrapperPayment.duedate) : Null,
                               WhoId = !listExistingCust.isEmpty() ? listExistingCust[0].Id : null,
                               SMS_Recipient__c = !listExistingCust.isEmpty() ? listExistingCust[0].phone : null,
                               Reject_Code__c = String.valueOf(wrapperPayment.rejectCode),
                               Reject_Reason__c = wrapperPayment.rejectReason,
                               Account_Balance__c = wrapperPayment.accountBalance,
                               RecordTypeId = (Id)GlobalUtil.getRecordTypeByName('Task').get(RECORD_TYPE_MOBILE_SMS).getRecordTypeId()
                              );
            insert newTask ;
            MobileRestUtility.message = 'Successfully created ' + wrapperPayment.type + '-' + wrapperPayment.subtype + '  task';
        }
        catch (Exception e) {
            MobileRestUtility.setExceptionResponse(e, 'notificationTasksHelper', jsonData);
        }
        return RestUtility.getResponse(MobileRestUtility.status, MobileRestUtility.message, MobileRestUtility.statuscode, new list<Task> {newTask}, MobileRestUtility.logs);
    }
    /*  @input  : - json input from service layer
            @output : - returns newly created task to send the payment notification
            @Description : Helper method for the HTTP - method for RESTMobileAPI.cls
    */
    public static WrapperHTTPResponse notificationDismissHelper(String jsonData) {
        Task newTask;
        List<Task> listTaskDismiss;
        //String RECORD_TYPE_MOBILE_SMS = 'Mobile SMS';
        try {
            WrapperPaymentNotification wrapperPayment = WrapperPaymentNotification.parse(jsonData);
            if (String.isEmpty(wrapperPayment.octaneCustomerID) || String.isEmpty(wrapperPayment.taskID)) {
                MobileRestUtility.message = 'octaneCustomerID or taskID attribute cannot be null. Please provide valid inputs';
                return MobileRestUtility.returnWithError('notificationTasksHelper', jsonData);
            }
            List<Contact> listExistingCust = GlobalUtil.findExistingCustomer(wrapperPayment.octaneCustomerID, null, null);
            if (listExistingCust.isEmpty()) {
                MobileRestUtility.message = 'Customer Not Found. Please provide valid octaneCustomerID';
                return MobileRestUtility.returnWithError('notificationTasksHelper', jsonData);
            }
            listTaskDismiss = GlobalUtil.getSObjectRecords('Task', 'ID', wrapperPayment.taskID, NULL);
            if (listTaskDismiss.isEmpty()) {
                MobileRestUtility.message = 'Notification not Found.';
                return MobileRestUtility.returnWithError('notificationTasksHelper', jsonData);
            }
            listTaskDismiss[0].Status = 'Completed';
            listTaskDismiss[0].Subject = 'Dismissed: ' + listTaskDismiss[0].Subject;
            update listTaskDismiss;
            MobileRestUtility.message = 'Successfully Dismissed Notification Id: ' + wrapperPayment.taskID + ' for Customer with octaneCustomerID: ' + wrapperPayment.octaneCustomerID;
        }
        catch (Exception e) {
            MobileRestUtility.setExceptionResponse(e, 'notificationTasksHelper', jsonData);
        }
        return RestUtility.getResponse(MobileRestUtility.status, MobileRestUtility.message, MobileRestUtility.statuscode, listTaskDismiss, MobileRestUtility.logs);
    }
    /*  @input  : - json input from service layer
        @output : - returns newly created task to send the payment notification
        @Description : Helper method for the HTTP - method for RESTMobileAPI.cls
    */
    public static WrapperHTTPResponse getNotificationsHelper(Map<String, String> mapParameters) {
        List<Task> listNotifications;
        List<Contact> listCon;
        String RECORD_TYPE_MOBILE_SMS = 'Mobile SMS';
        String RECORD_TYPE_MOBILE_NOTIFICATION = 'Mobile Notification';
        String recordTypeIdPaymentSMS = (Id)GlobalUtil.getRecordTypeByName('Task').get(RECORD_TYPE_MOBILE_SMS).getRecordTypeId();
        String recordTypeIdNotification = (Id)GlobalUtil.getRecordTypeByName('Task').get(RECORD_TYPE_MOBILE_NOTIFICATION).getRecordTypeId();
        try {
            //WrapperPaymentNotification wrapperPayment = WrapperPaymentNotification.parse(jsonData);
            if (!mapParameters.containsKey('utilibillId')) {
                MobileRestUtility.message = 'UtilBillId missing or Invalid. Valid utilibillId is required to fetch notifications for customer';
                return MobileRestUtility.returnWithError('getNotificationsHelper', JSON.serialize(mapParameters));
            }
            listCon = GlobalUtil.findExistingCustomer(mapParameters.get('utilibillId'), null, null);
            if (listCon.isEmpty() || listCon.size() > 1) {
                MobileRestUtility.message = 'Could not Find a valid customer with provided utilibillId';
                return MobileRestUtility.returnWithError('getNotificationsHelper', JSON.serialize(mapParameters));
            }
            String sWhereClause = '(Status=\'Pending\' OR Status=\'New\') AND Type=\'Notification\' AND WhoId=\'' + listCon[0].Id + '\' AND (recordTypeId=\'' + recordTypeIdPaymentSMS + '\' OR recordTypeId=\'' + recordTypeIdNotification + '\')';
            //get utilibill id fro mobile
            listNotifications = GlobalUtil.getSObjectRecords('Task', NULL, NULL, sWhereClause);
            MobileRestUtility.message = 'Successfully found ' + listNotifications.size() + ' notifications.';
            // results = new List<Object> {listNotifications};
        }
        catch (Exception e) {
            MobileRestUtility.setExceptionResponse(e, 'getNotificationsHelper', Json.serialize(mapParameters));
        }
        return RestUtility.getResponse(MobileRestUtility.status, MobileRestUtility.message, MobileRestUtility.statuscode, listNotifications, MobileRestUtility.logs);
    }

    public static WrapperHTTPResponse sendVerificationCodeHelper(String jsonInput) {
        String DEFAULT_LEAD_PRODUCT_TYPE = 'Mobile';
        list<Contact> lstRelatedContact;
        list<Lead> lstRelatedLead;
        list<Task> lstExistingTasks;
        Contact contactToUpdate = new Contact();
        Task taskToInsert = new Task();
        Lead leadToUpsert = new Lead();
        String strVerificationCode, whereClause;
        Boolean successResult = false;
        try {
            // Get the values out of the JSON string
            wrapperVerify = WrapperVerifyMobileNo.parseVerification(jsonInput);
            // If the JSON has the required Id fields
            if (wrapperVerify.mobileNo != null && wrapperVerify.email != null && wrapperVerify.firstName != null  && wrapperVerify.lastName != null) {
                // Try to find the Contact or Lead for the mobileNo and email, then update or create it as needed
                whereClause = 'Contact_Role__c = \'Primary\'' + ' AND Email = \'' + wrapperVerify.email + '\'';
                lstRelatedContact = (List<Contact>) GlobalUtil.getSObjectRecords('Contact', null, null, whereClause);
                if( !lstRelatedContact.isEmpty() ) {
                	// There is a matching Contact
                	contactToUpdate = lstRelatedContact[0];
                }
                else {
	                whereClause = 'Phone = \'' + wrapperVerify.mobileNo.deleteWhiteSpace() + '\' AND Email = \'' + wrapperVerify.email + '\'';
	                lstRelatedLead = (List<Lead>) GlobalUtil.getSObjectRecords('Lead', null, null, whereClause);
	                if( !lstRelatedLead.isEmpty() ) {
	                	// There is a matching Lead
	                    leadToUpsert = lstRelatedLead[0];
	                }
	                else {
	                	// We are just creating a new Lead
	                    leadToUpsert.Phone = wrapperVerify.mobileNo.deleteWhiteSpace();
	                }
	                leadToUpsert.Email = !string.isEmpty(wrapperVerify.email) ? wrapperVerify.email : leadToUpsert.Email;
	                leadToUpsert.FirstName = !string.isEmpty(wrapperVerify.firstName) ? wrapperVerify.firstName : leadToUpsert.FirstName;
	                leadToUpsert.LastName = !string.isEmpty(wrapperVerify.lastName) ? wrapperVerify.lastName : leadToUpsert.LastName;
	                leadToUpsert.Company = !string.isEmpty(leadToUpsert.Company) ? leadToUpsert.Company : leadToUpsert.FirstName + ' ' + leadToUpsert.LastName;
	                leadToUpsert.Product_Type__c = DEFAULT_LEAD_PRODUCT_TYPE;
	                upsert leadToUpsert;
                }
                // Generate a verification code and put it in a Task with date to expire in 30 mins
                Id checkWhoId;
                strVerificationCode = GlobalUtil.generateVerificationCode();
                if( contactToUpdate.Id != null ) {
                	system.debug('-- There is a Contact: ' + contactToUpdate);
                	taskToInsert = generateVerificationTask((SObject) contactToUpdate, wrapperVerify.mobileNo.deleteWhiteSpace(), strVerificationCode);
                	checkWhoId = contactToUpdate.Id;
                }
                else {
                	system.debug('-- There is a Lead: ' + leadToUpsert);
                	taskToInsert = generateVerificationTask((SObject) leadToUpsert, wrapperVerify.mobileNo.deleteWhiteSpace(), strVerificationCode);
                	checkWhoId = leadToUpsert.Id;
                }
                // Check for existing valid verification codes
                whereClause = 'WhoId = \'' + checkWhoId + '\' AND Subject = \'Verification SMS\' AND Code_Valid_To__c > ' + DateTime.now().formatGMT('y-MM-dd\'T\'HH:mm:ss.SSS+0000');
                lstExistingTasks = (List<Task>) GlobalUtil.getSObjectRecords('Task', null, null, whereClause);
                system.debug('--Found these valid Tasks when a new verification code was requested (will invalidate them): ' + lstExistingTasks);
                if (!lstExistingTasks.isEmpty()) {
                    for (Task currentTask : lstExistingTasks) {
                        currentTask.Code_Valid_To__c = null;
                        currentTask.Status = 'Completed';
                    }
                }
                update lstExistingTasks;
                // Put the task on the Lead
                insert taskToInsert;
                // Send code by SMS (done by marketing cloud from the Task)
                // Return success if we got this far
                successResult = true;
                MobileRestUtility.status = HTTP_SUCCESS;
                MobileRestUtility.statuscode = STATUS_OK;
                MobileRestUtility.message = 'Successfully made request to send Validation Code SMS. Stored in Task on this Lead / Contact: ' + checkWhoId;
            }
            else {
                MobileRestUtility.message = 'Error: Invalid JSON provided. Must include mobileNo, email, firstName and lastName.';
                MobileRestUtility.returnWithError('sendVerificationCodeHelper', jsonInput);
            }
        }
        catch (Exception e) {
            MobileRestUtility.setExceptionResponse(e, 'sendVerificationCodeHelper', jsonInput);
        }
        List<Boolean>lstResult = new List<Boolean>();
        lstResult.add(successResult);
        return RestUtility.getResponse(MobileRestUtility.status, MobileRestUtility.message, MobileRestUtility.statuscode, lstResult, MobileRestUtility.logs);
    }

    public static Task generateVerificationTask(SObject personToVerify, String newMobileno, String verificationCode) {
        String RECORD_TYPE_MOBILE_SMS = 'Mobile SMS';
        DateTime dtNowPlusThirtyMin = system.now();
        dtNowPlusThirtyMin = dtNowPlusThirtyMin.addMinutes(30);
        Task newTask = new Task(Subject = 'Verification SMS',
                                Status = 'New',
                                Billing__c = False,
                                Type = 'Other',
                                Task_Type__c = 'GENERAL_HELP',
                                Task_Sub_Type__c = null,
                                Priority = 'Normal',
                                Code_Valid_To__c = dtNowPlusThirtyMin,
                                WhoId = personToVerify.Id,
                                SMS_Recipient__c = newMobileno,
                                SMS_Send_Status__c = 'Send',
                                Reject_Code__c = null,
                                Reject_Reason__c = null,
                                Account_Balance__c = null,
                                Description = 'Your validation code is: ' + verificationCode,
                                RecordTypeId = (Id)GlobalUtil.getRecordTypeByName('Task').get(RECORD_TYPE_MOBILE_SMS).getRecordTypeId()
                               );
        return newTask;
    }

    public static WrapperHTTPResponse checkVerificationCodeHelper(String jsonInput) {
    	list<Contact> lstRelatedContact;
        list<Lead> lstRelatedLead;
        list<Task> lstExistingTasks;
        Contact contactToCheck = new Contact();
        Lead leadToCheck = new Lead();
        Task validCode = new Task();
        string whereClause;
        Boolean successResult = false;
        try {
            // Get the values out of the JSON string
            wrapperVerify = WrapperVerifyMobileNo.parseVerification(jsonInput);
            // If the JSON has the required Id fields
            if (wrapperVerify.mobileNo != null && wrapperVerify.email != null && wrapperVerify.code != null ) {
                // Find the Contact or Lead
                whereClause = 'Contact_Role__c = \'Primary\'' + ' AND Email = \'' + wrapperVerify.email + '\'';
                lstRelatedContact = (List<Contact>) GlobalUtil.getSObjectRecords('Contact', null, null, whereClause);
                if (!lstRelatedContact.isEmpty()) {
                	contactToCheck = lstRelatedContact[0];
                	// Set query to find existing valid verification codes
                	whereClause = 'WhoId = \'' + contactToCheck.Id + '\' AND Subject = \'Verification SMS\' AND Code_Valid_To__c > ' + DateTime.now().formatGMT('y-MM-dd\'T\'HH:mm:ss.SSS+0000');
                }
                else {
                	whereClause = 'Phone = \'' + wrapperVerify.mobileNo.deleteWhiteSpace() + '\' AND Email = \'' + wrapperVerify.email + '\'';
                	lstRelatedLead = (List<Lead>) GlobalUtil.getSObjectRecords('Lead', null, null, whereClause);
	                if (!lstRelatedLead.isEmpty()) {
	                    leadToCheck = lstRelatedLead[0];
	                    // Set query to find existing valid verification codes
	                    whereClause = 'WhoId = \'' + leadToCheck.Id + '\' AND Subject = \'Verification SMS\' AND Code_Valid_To__c > ' + DateTime.now().formatGMT('y-MM-dd\'T\'HH:mm:ss.SSS+0000');
	                }
                }
                
                if( !lstRelatedContact.isEmpty() || !lstRelatedLead.isEmpty()) {
                    lstExistingTasks = (List<Task>) GlobalUtil.getSObjectRecords('Task', null, null, whereClause);
                    system.debug('--Found these valid Tasks with Verification Codes: ' + lstExistingTasks);
                    if (!lstExistingTasks.isEmpty() && lstExistingTasks.size() == 1) {
                        // There should only ever be one valid verification code
                        validCode = lstExistingTasks[0];
                        system.debug('--checkVerificationCodeHelper: Comparing task value: ' + validCode.Description + ', with JSON value: ' + wrapperVerify.code);
                        if (validCode.Description.contains( wrapperVerify.code )) {
                            // Return success if we got this far
                            system.debug('--checkVerificationCodeHelper: The code is valid.');
                            successResult = true;
                            MobileRestUtility.status = HTTP_SUCCESS;
                            MobileRestUtility.statuscode = STATUS_OK;
                            MobileRestUtility.message = 'Successfully verified the code for this Task: ' + validCode.Id;
                        }
                        else {
                            // Return success but bad status, indicating that the code was not validated
                            system.debug('--checkVerificationCodeHelper: The code is NOT valid.');
                            MobileRestUtility.status = HTTP_SUCCESS;
                            MobileRestUtility.statuscode = STATUS_OK;
                            MobileRestUtility.message = 'The provided Verification Code is not valid. Relates to Task: ' + validCode.Id;
                        }
                    }
                    else {
                        MobileRestUtility.message = 'Error: No valid validation code (Task) found to check against.';
                        MobileRestUtility.returnWithError('checkVerificationCodeHelper', jsonInput);
                    }
                }
                else {
                    MobileRestUtility.message = 'Error: Couldnt find Contact or Lead for that mobile number / email.';
                    MobileRestUtility.returnWithError('checkVerificationCodeHelper', jsonInput);
                }
            }
            else {
                MobileRestUtility.message = 'Error: Invalid JSON provided.  Must include mobileNo, email and verification code.';
                MobileRestUtility.returnWithError('checkVerificationCodeHelper', jsonInput);
            }
        }
        catch (Exception e) {
            MobileRestUtility.setExceptionResponse(e, 'checkVerificationCodeHelper', jsonInput);
        }
        List<Boolean>lstResult = new List<Boolean>();
        lstResult.add(successResult);
        return RestUtility.getResponse(MobileRestUtility.status, MobileRestUtility.message, MobileRestUtility.statuscode, lstResult, MobileRestUtility.logs);
    }
}