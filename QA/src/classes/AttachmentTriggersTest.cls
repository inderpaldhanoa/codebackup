/************************************************************************************************************
* Apex Class Name	: AttachmentTriggersTest.cls
* Version 			: 1.0 
* Created Date  	: 30 March 2017
* Function 			: Test class for Attachment trigger process.
* Modification Log	:
* Developer				Date				Description
* -----------------------------------------------------------------------------------------------------------
* Tom Clayton			30/03/2017 			Created Class.
* Tom Clayton 			04/04/2017 			Fixed to match recent changes to status fields.
* Tom Clayton 			06/04/2017 			Changed quotes around date in CSV
* Tom Clayton 			07/04/2017 			Confirmed the headers should be hard coded. This is the response file so can't use Custom Label. Updated comments.
* Tom Clayton 			10/04/2017 			Changed first field to "Asset ID" as per description from Daniel
************************************************************************************************************/

@isTest
private class AttachmentTriggersTest
{
	public static Delivery_Summary__c ds1;
	public static Asset asset1;
	
	@testSetup static void setupTestData()
	{
		Map<Id,Schema.RecordTypeInfo> assetRecordTypeInfoId = GlobalUtil.getRecordTypeById('Asset'); //getting all Recordtype for the Sobject
		
		// Create test data		
		ds1 = new Delivery_Summary__c();
		ds1.Delivery_File_Status__c = 'Delivery Sent';
		insert ds1;
		
		asset1 = new Asset();
		asset1.Name = 'test asset';
		asset1.Address_Line1__c = 'Building name 1';
		asset1.Address_Line2__c = '21 Test Street';
		asset1.Delivery_Summary__c = ds1.Id;
		insert asset1;
	}
	
	static testMethod void testConfirmationAttachment()
	{
        Test.startTest();
        	// Get test data
        	ds1 = [SELECT Id FROM Delivery_Summary__c];
        	asset1 = [SELECT Id FROM Asset];
        	
        	// Create and attach new attachment
        	
	        // Add the header row to the CSV string
	    	string strFinalCSV = 'Asset ID, Product Code, Date Requested, First Name, Last Name, Mobile Number, Email Address, Address Line1, Address Line2, Address Line3, Suburb, State, Postcode, Country\n';
	    	strFinalCSV += '"' + asset1.Id + '", "mouse2@test.com", "Mobile SIM", 2017-01-01, "Dispatched", "", "89612345678901234567", "RP38099738"';
	    	
	    	// Make confirmation CSV file as blob
	    	blob blobCSV = Blob.valueOf(strFinalCSV);
	    	
	    	// Attach CSV file to the Delivery Summary
	        Attachment AttachmentObj = new Attachment(Body = blobCSV,
	        	Name = 'SimDispatchConfirmation_1_20170101',
	        	parentId = ds1.Id);
	        	
			insert AttachmentObj;
		Test.stopTest();
    }
    
    static testMethod void testOtherAttachment()
	{
        Test.startTest();
        	// Get test data
        	ds1 = [SELECT Id FROM Delivery_Summary__c];
        	asset1 = [SELECT Id FROM Asset];
        	
        	// Create and attach new attachment
        	
	        // Add the header row to the CSV string
	    	string strFinalCSV = 'Asset ID, Product Code, Date Requested, First Name, Last Name, Mobile Number, Email Address, Address Line1, Address Line2, Address Line3, Suburb, State, Postcode, Country\n';
	    	strFinalCSV += '"' + asset1.Id + '", "mouse2@test.com", "Mobile SIM", 2017-01-01, "Dispatched", "", "89612345678901234567", "RP38099738"';
	    	
	    	// Make confirmation CSV file as blob
	    	blob blobCSV = Blob.valueOf(strFinalCSV);
	    	
	    	// Attach CSV file to the Delivery Summary
	        Attachment AttachmentObj = new Attachment(Body = blobCSV,
	        	Name = 'OtherFileNameTest_0170101',
	        	parentId = ds1.Id);
	        	
			insert AttachmentObj;
		Test.stopTest();
    }
}