@isTest
public class ININServicesTest {
    
    // Test data setup 
    @testSetup public static void setupData(){
        AuthProperties__c ap1 = new AuthProperties__c(Name='ININ_OAUTH',Client_Id__c='testclient',Client_Secret__c='-kBTk_2Iz1YA715jGE2pPouLTJUgXLVpthPQKZYVFng',Password__c='1234',Username__c='N/A',Endpoint__c='https://login.mypurecloud.com.au');
        insert ap1;
        AuthProperties__c ap2 = new AuthProperties__c(Name='ININ_API',Client_Id__c='',Client_Secret__c='',Password__c='',Username__c='',Endpoint__c='https://api.mypurecloud.com.au');
        insert ap2;
    }
    
    //Webservice Mock class 
    public class MockHttpRes implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest req) {
            
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            //res.setBody('{"id":"fb018fe5-9a06-40a6-9d8b-b283210763f8", "participants":[{"id":"f74d0e1d-23c9-4065-9279-7caabe846938", "name":"Australia", "address":"tel:+61470370712","startTime":"2016-11-21T07:41:27.681Z","connectedTime":"2016-11-21T07:41:27.906Z","endTime":"2016-11-21T07:43:39.965Z","purpose":"customer","state":"disconnected","direction":"inbound","disconnectType":"peer","held":false,"wrapupRequired":false,"queue":{"id":"d12c03b9-b38c-4e8a-a7d4-83fdd9066736","selfUri":"/api/v2/routing/queues/d12c03b9-b38c-4e8a-a7d4-83fdd9066736"},"attributes":{"Customer_ANI":"0470370712","NPS_Resolution":"1","NPS_Verbatim":"Yes","SF_SearchValue":"0470370712","NPS_Score":"5","CWC_allowACD":"true"},"provider":"Edge","muted":false,"confined":false,"recording":false,"recordingState":"none","ani":"tel:+61470370712","dnis":"tel:+61399296236"}' );                   
            res.setBody('{"id":"fb018fe5-9a06-40a6-9d8b-b283210763f8","participants":[{"attributes":{"Customer_ANI":"0470370712","NPS_Resolution":"1","NPS_Verbatim":"Yes","SF_SearchValue":"0470370712","NPS_Score":"5","CWC_allowACD":"true"}}]}');
            res.setStatusCode(200);
            return res;
        }
    } 
     public class MockHttpRes1 implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest req) {
            
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            //res.setBody('{"id":"fb018fe5-9a06-40a6-9d8b-b283210763f8", "participants":[{"id":"f74d0e1d-23c9-4065-9279-7caabe846938", "name":"Australia", "address":"tel:+61470370712","startTime":"2016-11-21T07:41:27.681Z","connectedTime":"2016-11-21T07:41:27.906Z","endTime":"2016-11-21T07:43:39.965Z","purpose":"customer","state":"disconnected","direction":"inbound","disconnectType":"peer","held":false,"wrapupRequired":false,"queue":{"id":"d12c03b9-b38c-4e8a-a7d4-83fdd9066736","selfUri":"/api/v2/routing/queues/d12c03b9-b38c-4e8a-a7d4-83fdd9066736"},"attributes":{"Customer_ANI":"0470370712","NPS_Resolution":"1","NPS_Verbatim":"Yes","SF_SearchValue":"0470370712","NPS_Score":"5","CWC_allowACD":"true"},"provider":"Edge","muted":false,"confined":false,"recording":false,"recordingState":"none","ani":"tel:+61470370712","dnis":"tel:+61399296236"}' );                   
            res.setBody(null);
            res.setStatusCode(200);
            return res;
        }
    }     
    
    public static testMethod void testOne(){
        Test.setMock(HttpCalloutMock.class, new MockHttpRes()); 
        Test.startTest();
        ININServices.getININConversationDetails('test','test');
         ININServices.getOAuthToken();
        Test.stopTest();
    }
}