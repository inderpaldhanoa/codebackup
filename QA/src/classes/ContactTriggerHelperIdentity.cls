public without sharing class ContactTriggerHelperIdentity 
{
    private static Boolean isInitialized = false;
    private static List<User> lstCustomerUsers;
    private static string delegatedAuthority = 'Delegated Authority';
    private static list<string> newUserEmails;
    private static list<contact> changedContacts;
    private static list<id> contactsIDUsername;
    private static map<id, Contact> contactsWithUpdatedRoles;
    public static Map<Id, Account> relatedAccountContactMap;
    public static List<User> agentResetPasswordEmail;
    
    public static void initializeProperties()
    {
        if(isInitialized)
            return;
        lstCustomerUsers = new List<User>();
        changedContacts = new list<contact>();
        contactsIDUsername = new list<id>();
        newUserEmails = new list<string>();
        contactsWithUpdatedRoles = new map<id, Contact>();
        relatedAccountContactMap = new Map<Id, Account>();
        agentResetPasswordEmail = new List<User>();
        isInitialized  = true;
    }
    
    //create user account from customer details. 
    // New user creation for change of lease, new Join for Primary Contact
    
    public static void setupUserFromContact(Contact oContact, Account oAccount)
    {
        // Do not create User if the Contact is created without link to Account (Social Cases Radian 6 Connector)
        if(oContact.Accountid != NULL){
            // Joins (Regular & Lead Conversion) create contact anyways
            // If Octane is present create User at time of first time people logging into Website & joining as Customers 
            // Lead merge Join, Account and Contact is created without Octane Number. Octane is put in after some time 
            // If Octane Number is not in, don't create User. Once Octane is updated in Account, Account Trigger will create the User
            if((oContact.Contact_Role__c != 'Delegated Authority' && oAccount.Octane_Customer_Number__c != NULL) || 
               // Inbound & Outbound Agents join, create contact & User
               ((oContact.Contact_Role__c == 'Inbound Sales' || oContact.Contact_Role__c == 'Outbound Sales') && oAccount.Octane_Customer_Number__c == NULL))
            {
                User u = createUser(oContact);
                lstCustomerUsers.add(u);  
                if(oContact.Contact_Role__c == 'Inbound Sales' || oContact.Contact_Role__c == 'Outbound Sales'){
                    agentResetPasswordEmail.add(u);
                }
            }
        }
    }
    public static User createUser(Contact oContact)
    {
        User oUser = new User();
        oUser.FirstName = oContact.FirstName;
        oUser.LastName = oContact.LastName;
        oUser.Email = oContact.Email;
        oUser.Username = oContact.Email;
        oUser.Contact_Role__c = oContact.Contact_Role__c;
        if(oContact.Octane_Customer_Number__c != NULL){
            oUser.Broadband_Octane_Id__c = oContact.Octane_Customer_Number__c;    
        }
        ouser.ProfileId = Label.ExternalIdentityProfileCustomLabel;    
        oUser.CommunityNickName = oContact.Id;
        oUser.Alias = oContact.FirstName != null? oContact.FirstName.substring(0,1):'' + oContact.LastName.substring(0,1);
        oUser.EmailEncodingKey = 'ISO-8859-1';
        oUser.TimeZoneSidKey = 'Australia/Sydney';
        oUser.LocaleSidKey = 'en_AU';
        oUser.LanguageLocaleKey = 'en_US';
        OUser.isActive = True;
        oUser.ContactId = oContact.Id;
        return oUser;
    }
    
    //create users from contacts
    
    public static void createUsersFromContact()
    {   
        if(!lstCustomerUsers.isEmpty()){
        List<log__c> logs = new List<Log__c>();
            List<database.Saveresult> srlist = database.insert(lstCustomerUsers,false);
             //Error Logging
            for (Database.Saveresult sr: srList) {
                if (!sr.isSuccess()) {
                    // Operation failed, so get all errors
                    for (Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Fields that affected this error: ' + err.getFields());
                        logs.add(GlobalUtil.createLog(
                                     'ERROR: ', err.getMessage(),
                                     'User Create Error: ContactTriggerHelperIdentity',
                                     'request:' + '\n' + err.getFields() + '\n' + JSON.serialize(lstCustomerUsers) + '\n'));
                    }
                }
            }
            if(!logs.isEmpty()){
                insert logs;
            }
            // Send Reset Password EMail when Inbound OutBound Agents are craeted 
            if(!agentResetPasswordEmail.isEmpty()){
                for(User u : agentResetPasswordEmail){
                    system.setPassword(u.Id, Label.Channel_Partners_hardcoded_password);
                }
            }
        }
    }
    
    
    //Update the User if Primary , Inbound Sales, Outbound Sales role based Contact is updated 
    
    public static void getUserListToUpdate(sObject oldSObj, sObject newSObj){
        Contact oSobj = (Contact)oldSObj;
        Contact nSobj = (Contact)newSObj;  
        if((oSObj.Email != nSObj.Email || oSObj.lastname != nSObj.lastname || oSObj.firstname != nSObj.firstname) && nSObj.Contact_Role__c != delegatedAuthority){
            changedContacts.add((contact)newSObj);        
            if(oSObj.Email != nSObj.Email){
                contactsIDUsername.add(nSObj.id);
                newUserEmails.add(nSObj.Email);
            }
        }
    }
    
    public static void updateUsersListFromContact()
    { 
        if(!changedContacts.isEmpty()){
            // Check if the Email, Username already exists
            
            list<user> duplicateUserEmail = [SELECT Email, Username 
                                             FROM User 
                                             WHERE Username IN :newUserEmails];
            if(duplicateUserEmail.isEmpty()){
                // No Duplicates then update the User Object
                Map<id,user> userMap = new Map<id,user>();
                for(user u : [SELECT Id, Email, Username, Contactid, Firstname, Lastname 
                              FROM User 
                              WHERE Contactid IN:changedContacts]){
                                  userMap.put(u.ContactId, u);    
                              }
                for(contact con : changedContacts){ 
                    if(userMap.containsKey(con.id)){
                        userMap.get(con.id).email = con.email;
                        userMap.get(con.id).firstname = con.firstname;
                        userMap.get(con.id).lastname = con.lastname;
                    }         
                }
                update UserMap.values();
                
                // Calling future function to update the UserName field of the user Object only
                FutureMethods.updateUsernamesFromContact(contactsIDUsername);
            }
            else {
                // Duplicate Email 
                changedContacts[0].addError('The EMail already exists');
            }
        }
    }
    
    public static void getAccountContactMap (list<Contact> contactTriggerList){
        system.debug('contactTriggerList::Identitu'+contactTriggerList);
        List<Contact> relatedAccounts = [SELECT id, accountid, account.name, account.Lease_Transferred_from__c, account.Octane_Customer_Number__c, account.Mobile_Octane_Customer_Number__c 
                                         FROM contact 
                                         WHERE id in : contactTriggerList]; 
        for(Contact con : relatedAccounts){
            Account relatedAccount = new Account();
            relatedAccount.name = con.account.name;
            relatedAccount.id = con.AccountId;
            relatedAccount.Lease_Transferred_from__c = con.account.Lease_Transferred_from__c;
            relatedAccount.Octane_Customer_Number__c = con.account.Octane_Customer_Number__c;
            relatedAccount.Mobile_Octane_Customer_Number__c = con.account.Mobile_Octane_Customer_Number__c;
            system.debug('relatedAccount:::'+relatedAccount+'con:::::'+con);
            relatedAccountContactMap.put(con.id, relatedAccount);
        }
        system.debug('ContactTriggerHelperIdentity.relatedAccountContactMap::::Identity'+relatedAccountContactMap);
    }
    
    public static void afterProcessingResetInitialization()
    {
        isInitialized = false;
    }
    
}