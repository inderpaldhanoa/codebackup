/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  @created : 23/07/2016
  @Description : Test class for BelongCSVfileUploadController class
*/
@isTest
private class BelongCSVfileUploadControllerTest 
{
	@testSetup 
	static void setupTestData() 
	{
		Map<Id,Schema.RecordTypeInfo> accRecordTypeInfoId		= GlobalUtil.getRecordTypeById('Account'); //getting all Recordtype for the Sobject
	    Map<String,Schema.RecordTypeInfo> accRecordTypeInfoName		= GlobalUtil.getRecordTypeByName('Account');//getting all Recordtype for the Sobject
	    
	    List<Account> newAccObjs = new List<Account>();
		Account sAccount 		= new Account();
        sAccount.RecordTypeId   = accRecordTypeInfoName.get('Residential').getRecordTypeId();
        sAccount.Name           = 'Test Account';
        sAccount.Phone          = '0412345678';
        sAccount.Customer_Status__c      = 'Active';
        sAccount.Octane_Customer_Number__c = '123456';
        newAccObjs.add(sAccount);
        
        
        sAccount = new Account();
        sAccount.RecordTypeId   = accRecordTypeInfoName.get('Residential').getRecordTypeId();
        sAccount.Name           = 'Test Account';
        sAccount.Phone          = '0412345678';
        sAccount.Customer_Status__c      = 'Active';
        sAccount.Octane_Customer_Number__c = '123654';
        newAccObjs.add(sAccount);
        
        sAccount 		= new Account();
        sAccount.RecordTypeId   = accRecordTypeInfoName.get('Residential').getRecordTypeId();
        sAccount.Name           = 'Test Account';
        sAccount.Phone          = '0412345678';
        sAccount.Customer_Status__c      = 'Active';
        sAccount.Octane_Customer_Number__c = '654321';
        newAccObjs.add(sAccount);
        
        insert newAccObjs;
        
        List<Contact> newConObjs = new List<Contact>();
        Contact sContact       	= new Contact();
        sContact.FirstName  	= 'Joe';
        sContact.LastName  		= 'Belong';
        sContact.AccountId  	= newAccObjs[0].Id;
        sContact.MobilePhone	= '0412365498';
        sContact.Contact_Role__c = 'Primary';
        newConObjs.add(sContact);
        
        sContact       	= new Contact();
        sContact.FirstName  	= 'Joe';
        sContact.LastName  		= 'Belong';
        sContact.AccountId  	= newAccObjs[1].Id;
        sContact.MobilePhone	= '0412365498';
        sContact.Contact_Role__c = 'Primary';
        newConObjs.add(sContact);
        
        sContact       	= new Contact();
        sContact.FirstName  	= 'Joe';
        sContact.LastName  		= 'Belong';
        sContact.AccountId  	= newAccObjs[2].Id;
        sContact.MobilePhone	= '0412365498';
        sContact.Contact_Role__c = 'Primary';
        newConObjs.add(sContact);
        
        insert newConObjs;
        
        
       	SMS_Template__c SMSTemplate = new SMS_Template__c();
       	
       	SMSTemplate.Name				= 'Template SMS Test';
        SMSTemplate.Active__c			= true;
        SMSTemplate.Available_to_FOH__c	= true;
        SMSTemplate.Code__c				= 'Test01';
        SMSTemplate.SMS_Template__c		= 'Test Content {Balance} {Date}';
        SMSTemplate.Template_Category__c = 'Payments';
        
        insert SMSTemplate; 
	}



    static testMethod void LoadCSV_SMSMessage() 
    {
    	Test.startTest();
        
        String fileContent = 'CustomerId,Balance,Date' + '\n';
        fileContent += '123456,500,5/05/2016' + '\n';
        fileContent += '123654,500,5/05/2016' + '\n';
        fileContent += '654321,500,5/05/2016';
        
        PageReference pageRef = Page.BelongCSVfileUpload;
        Test.setCurrentPageReference(pageRef);
                
		BelongCSVfileUploadController smsCSVCntr = new BelongCSVfileUploadController();
        
		smsCSVCntr.uploadOption = 'SMS';
       	smsCSVCntr.getUploadSelection();
       	smsCSVCntr.smsTemplateCat = 'Payments';
       	smsCSVCntr.getSMSTemplateList();
       	smsCSVCntr.smsTemplateID = smsCSVCntr.SMSTemplatesList[1].getValue();
       	ApexPages.currentPage().getParameters().put('csvParam', fileContent);
       	smsCSVCntr.setContentFile();
       	smsCSVCntr.loadFile();
       	
       	smsCSVCntr.SMSNotificationList[0].SMS_Recipient__c = '';
       	smsCSVCntr.SMSNotificationList[0].Description = '';
       	smsCSVCntr.SendSMS();
       	
       	smsCSVCntr.SMSNotificationList[0].SMS_Recipient__c = '0412365498';
       	smsCSVCntr.SMSNotificationList[0].Description = 'Test Content';
       	smsCSVCntr.SendSMS();
       	
       
        
        Test.stopTest();
    }
    
    static testMethod void LoadCSV_ContactLog() 
    {
    	Test.startTest();
        
        String fileContent = 'CustomerId,Body,Title' + '\n';
        fileContent += '123456,"Bulk ""Contact"" Log uh?' + '\n' + 'Man it\'s too hard any other way?","Bulk Contact Log hmm… 1"' + '\r\n';
        fileContent += '123654,"Bulk Contact Log uh?' + '\n' + 'Man it\'s too hard any other way?","Bulk Contact Log hmm… 2"';
        
        PageReference pageRef = Page.BelongCSVfileUpload;
        Test.setCurrentPageReference(pageRef);
                
		BelongCSVfileUploadController smsCSVCntr = new BelongCSVfileUploadController();
        
		smsCSVCntr.uploadOption = 'conLog';
       	smsCSVCntr.getUploadSelection();
       	ApexPages.currentPage().getParameters().put('csvParam', fileContent);
       	smsCSVCntr.setContentFile();
       	smsCSVCntr.loadFile();
       	
       	smsCSVCntr.contactLogList[0].Title = '';
       	smsCSVCntr.contactLogList[0].Body = '';
       	smsCSVCntr.saveContactNotes();
       	
       	smsCSVCntr.contactLogList[0].Title = 'Test Title';
       	smsCSVCntr.contactLogList[0].Body = 'Test Body';
       	smsCSVCntr.saveContactNotes();
       
        
        Test.stopTest();
    }
    
    static testMethod void LoadCSV_ContactLogNoRecordFound() 
    {
    	Test.startTest();
        
        String fileContent = 'CustomerId,Body,Title' + '\n';
        fileContent += '951487,"Bulk ""Contact"" Log uh?' + '\n' + 'Man it\'s too hard any other way?","Bulk Contact Log hmm… 1"' + '\r\n';
        fileContent += '753698,"Bulk Contact, Log uh?' + '\n' + 'Man it\'s too hard any other way?","Bulk Contact, Log hmm… 2"';
        
        PageReference pageRef = Page.BelongCSVfileUpload;
        Test.setCurrentPageReference(pageRef);
                
		BelongCSVfileUploadController smsCSVCntr = new BelongCSVfileUploadController();
        
		smsCSVCntr.uploadOption = 'conLog';
       	smsCSVCntr.getUploadSelection();
       	ApexPages.currentPage().getParameters().put('csvParam', fileContent);
       	smsCSVCntr.setContentFile();
       	smsCSVCntr.loadFile();
       	
        Test.stopTest();
    }
    
}