/************************************************************************************************************
* Apex Class Name   : MobileBatchPrepareAssetHeader.cls
* Version           : 1.0
* Created Date      : 15 March 2017
* Function          : Finds recently created Assets and creates a Delivery Summary with CSV.
* Modification Log  :
* Developer             Date                Description
* -----------------------------------------------------------------------------------------------------------
* Tom Clayton           15/03/2017          Created Class.
* Tom Clayton           16/03/2017          Fixed file name, added fields to query, fixed cross object query.
* Tom Clayton           17/03/2017          Improved readability, added try catch, moved summary creation to finish method, added record count, added "null" removal.
* Tom Clayton           20/03/2017          Added calls to get record type instead of query.
* Daniel Bennet         21/03/2017          Updates to field order and names.
* Tom Clayton           04/04/2017          Added logic to set each Asset's reference to its new Delivery Summary.
* Girish P              07/04/2017          Fixed issue with null Leads.
* Tom Clayton           07/04/2017          Removed excess debug statements. Moved header creation to finish method.
* Tom Clayton           10/04/2017          Changed logic to create Delivery Summary in start, and update chunks of assets within each execute.
************************************************************************************************************/

/* Command to test this batch in execute anonymous: */
/* Uses this format: Id <variable name>= Database.executeBatch(new <Class name>(), batch size); */
// Id batchJobId = Database.executeBatch(new MobileBatchPrepareAssetHeader(), 200);

/* Commands to schedule this process: */
/* Seconds, Minutes, Hours, Day, Month, (Optional: Week), Year */
/* At 5:00 PM every day every month */
// MobileBatchPrepareAssetHeader BatchPrepHeader = new MobileBatchPrepareAssetHeader();
// String sch = '0 0 17 * * ?';
// String jobID = System.schedule('Batch to nightly prepare Asset Headers (Delivery Summary)', sch, BatchPrepHeader);

global class MobileBatchPrepareAssetHeader implements Database.Batchable<sObject>, Schedulable, Database.stateful, Database.AllowsCallouts {
    //All constants
    public static Final String PRODUCT_CODE = 'MOB-SIM-ONL-001';
    public static Final String ASSET_MOBILE_RECORDTYPE = 'Mobile';
    public static Final String ASSET_SOBJECT = 'Asset';
    public static Final String CSV_DELIMETER = ',';
    public static Final String REQUEST_FILENAME = 'SimDispatchRequest';
    public static Final Set<String> ASSET_STATUS_TO_CHECK = new Set<String>{
    'Requested',
    'Ready to Activate'};
    public static Final String ASSET_STATUS_TO_SET = 'Extracted';
    public static Final String DELIVERY_SUMMARY_STATUS_TO_SET = 'Delivery Ready';

    // Other variables
    public Integer intRecordsProcessed = 0;
    public List<sObject> assetRecords = new List<sObject>();
    public String strDataForCSV = '';
    public String strHeaderForCSV = '';
    public String query;
    public Delivery_Summary__c deliverySummary = new Delivery_Summary__c();
    Map<String, String> headerMap = createCsvHeaderMapping(System.Label.MOBILE_GND_CSV_HEADER);
    list<string> lstColumnsInRow = new List<String>();

    // Implements the execute() method for Schedulable to call the Batchable method
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new MobileBatchPrepareAssetHeader(), 200);
    }

    // Start method to get data
    global Database.QueryLocator start(Database.BatchableContext bc) {
        // Using the existing GlobalUtil to identify record type Id
        Map<String, Schema.RecordTypeInfo> mapAssetRecordTypeInfo = GlobalUtil.getRecordTypeByName(ASSET_SOBJECT);
        Id recTypeId = mapAssetRecordTypeInfo.get(ASSET_MOBILE_RECORDTYPE).getRecordTypeId();
        // Create the empty Delivery Summary to be referenced throughout processing
        deliverySummary.Delivery_File_Status__c = DELIVERY_SUMMARY_STATUS_TO_SET;
        insert deliverySummary;
        try {
            System.debug('--Batch Start: query beginning...');
            // Query the Assets
            return Database.getQueryLocator([SELECT Id, Mobile_Number__c, Address_Line1__c, First_Name__c,Last_Name__c,Address_Line2__c, Address_Line3__c,
                                             Suburb__c, State__c, Postcode__c, Product_Code__c, Country__c,
                                             Lead__r.FirstName, Lead__r.LastName, Lead__r.Email
                                             FROM Asset
                                             WHERE Status in : ASSET_STATUS_TO_CHECK
                                                     AND RecordTypeId = : recTypeId
                                                     AND ( Lead__c != null OR ContactID!=null)
                                                     AND Address_Line1__c != null]);
        }
        catch (Exception e) {
            system.debug('--Error in batch start. Query had problem: ' + e.getmessage());
            return null;
        }
    }

    // Method for putting quotes around a field being added to the CSV file (or empty quotes for blank value).
    public void addCSVColumn(String sData) {
        sData = !String.isEmpty(sData) ? sData.escapeCSV() : '';
        lstColumnsInRow.add(sData);
    }

    // Process the records: create the Delivery Summary and the CSV
    global void execute(Database.BatchableContext bc, List<sObject> records) {
        String strValue;
        assetRecords = records;
        for (sObject eachAsset : assetRecords) {
            string strDataForCSVtemp = '';
            lstColumnsInRow = new List<string>();
            // The purpose of the section of code below is to build the CSV file without hard coding any of the field names in this class (use Custom Label instead).
            // Each Asset's information is a row in the string for the CSV file
            for (String eachHeader : headerMap.keySet()) {
                String headerDataKey = headerMap.get(eachHeader);
                if (headerDataKey.contains('__r.')) {
                    // This field is a cross object relationship so we need to specify object name to get value
                    string sObjectname = headerDataKey.split('__r.')[0] + '__r';
                    string sObjectFieldName = headerDataKey.split('__r.')[1];
                    String jsonObject = JSON.serialize(eachAsset.getsobject(sObjectname));
                    Map<String, Object> sObjectData = (Map<String, Object>)JSON.deserializeUntyped(jsonObject);
                    if (NULL != sObjectData && sObjectData.containsKey(sObjectFieldName)) {
                        strValue = (String)sObjectData.get(sObjectFieldName);
                    }
                }
                else
                    if (headerDataKey.contains('<PRODUCT_CODE>')) {
                        // This field needs a global constant value
                        strValue = PRODUCT_CODE;
                    }
                    else
                        if (headerDataKey.contains('<DATE_PLACEHOLDER>')) {
                            // This field needs a current date value
                            strValue = system.now().format('yyyy-MM-dd');
                        }
                        else {
                            // Just plain get the value
                            strValue = (String)eachAsset.get(headerDataKey);
                        }
                addCSVColumn(strValue);
            }
            strDataForCSVtemp = String.join(lstColumnsInRow, ',');
            strDataForCSV += strDataForCSVtemp + '\n'; // The new line at the end of a row
            // Mark currentAsset status as extracted and relate to the Delivery Summary
            eachAsset.put('Status', ASSET_STATUS_TO_SET);
            eachAsset.put('Delivery_Summary__c', deliverySummary.Id);
            // Keep a count of how many Assets have been processed
            intRecordsProcessed++;
        }
        // Updating assets once for each chunk of 200 to come through the batch
        update assetRecords;
    }

    global void finish(Database.BatchableContext bc) {
        // Loop to build the string for the header row
        strHeaderForCSV = '';
        integer i = 0;
        String fileName;
        system.debug('--About to build header row');
        for (String eachHeader : headerMap.keySet()) {
            system.debug('--Found a header item: ' + eachHeader);
            i++;
            // If there are still more values we add a comma on the end
            if (headerMap.size() > i) {
                strHeaderForCSV += eachHeader + ',';
            }
            else {
                strHeaderForCSV += eachHeader + '\n';
            }
        }
        // Update the "number of records" field on the Delivery Summary
        deliverySummary.Records_In_File__c = intRecordsProcessed;
        // Also will attach a CSV file to the Delivery Summary based on the string we made. Use the returned Id as the "delivery extract Id"
        fileName = REQUEST_FILENAME + '_' + deliverySummary.Id + '_' + system.now().format('HHmm') + '_' + system.now().format('yyyyMMdd') + '.csv';
        deliverySummary.Delivery_Extract_ID__c = GlobalUtil.attachBlobCSV(
                    strHeaderForCSV + strDataForCSV,
                    fileName,
                    deliverySummary.Id);
        update deliverySummary;
        //Send dispatch file in for to service layer
        //This batch just send a notification to service layer. The reason to create this batch class just for notogfication was.
        //Notification is a callout and in finish we are already doing a DML after which cannot call a webservice,a hence to overcome this this batch
        //was created
        Database.executeBatch(new MobileBatchSendDispatchFileNotification(deliverySummary.Id, deliverySummary.Delivery_Extract_ID__c));
    }
    /*
    @input  : dispatch file information object
    @output : returns customer Mapping id
    @Description : send disatch file info to service layer
    */
    public class DispatchFileNotification {
        String deliverySummaryId {set;}
        String attachmentId {set;}
        public DispatchFileNotification(String deliverySummaryId, String attachmentId) {
            this.deliverySummaryId = deliverySummaryId;
            this.attachmentId = attachmentId;
        }
    }

    // Method for getting the header and value pairs out of the Custom Label and into the map
    public static Map<String, String> createCsvHeaderMapping(String sHeaderMappping) {
        Map<String, String> headerMap = new Map<String, String>();
        For(String eachParam: sHeaderMappping.split(',')) {
            headerMap.put(eachParam.split('=')[0], eachParam.split('=')[1]);
        }
        return headerMap;
    }


}