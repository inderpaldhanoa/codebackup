/*
    @author  : Girish P(Girish.pauriyal@infosys.com)
    @created : 22/03/2017
    @Description : Helper to populate the Related Topics JSON object in each article
*/
public with sharing class PopulateRelatedTopicIds {
    @InvocableMethod(label = 'Populate Related Topics' description = 'Returns the list of account names corresponding to the specified account IDs.')
    public static void populate(List<Id> knowldegeId) {
        String START_IDENTIFIER = '/articles/FAQ/';
        String END_IDENTIFIER = '" ';
        String SPLIT_TAG = '</a>';
        System.debug('knowldegeId******' + knowldegeId);
        Set<String> setRelatedArticle;
        Set<String> setArticleRelatedArticles = new Set<String>();
        Map<id, Set<String>> mapArticleRelatedArticle = new Map<id, Set<String>>();
        //query the artcile that has been updated
        Map<Id, FAQ__kav> mapEditedFaq = new Map<Id, FAQ__kav>([Select id, PublishStatus, Related_Topics__c, Related_Topic_Ids__c
                from FAQ__kav
                where id in :knowldegeId
                and language = 'en_US']);
        for (FAQ__kav editedFAQ : mapEditedFaq.values() ) {
            if (editedFAQ.PublishStatus == 'Online') {
                continue;
            }           setRelatedArticle = new Set<String>();
            if (NULL != editedFAQ.Related_Topics__c) {
                //split the related articles rich text field
                for (String realtedTopic : editedFAQ.Related_Topics__c.split(SPLIT_TAG)) {
                    System.debug(realtedTopic.substringBetween(START_IDENTIFIER, END_IDENTIFIER));
                    if (realtedTopic.contains(START_IDENTIFIER) && realtedTopic.contains(END_IDENTIFIER)) {
                        //create a list of related article
                        setRelatedArticle.add(realtedTopic.substringBetween(START_IDENTIFIER, END_IDENTIFIER));
                        setArticleRelatedArticles.add(realtedTopic.substringBetween(START_IDENTIFIER, END_IDENTIFIER));
                    }
                }
                mapArticleRelatedArticle.put(editedFAQ.id, setRelatedArticle);
            }
        }
        if (mapArticleRelatedArticle.size() > 0) {
            List<RelatedTopic> listRelatedTopics;
            FAQ__kav updateFAQ;
            Map<Id, String> mapArtcileRelatedArticleJSON = new Map<Id, String>();
            Map<String, FAQ__kav> mapRelatedArticles = new Map<String, FAQ__kav>();
            For(FAQ__kav eachRelatedArticle: [Select id, URLName, (SELECT id, DataCategoryName, DataCategoryGroupName FROM DataCategorySelections), Related_Topics__c, Title, Related_Topic_Ids__c
                                              from FAQ__kav
                                              where URLName in :setArticleRelatedArticles
                                              and PublishStatus = 'Online' and language = 'en_US'
                                                      With Data Category Mobile__c Below All__c]) {
                mapRelatedArticles.put(eachRelatedArticle.URLName, eachRelatedArticle);
            }
            for (Id articleid : mapArticleRelatedArticle.keySet()) {
                listRelatedTopics = new List<RelatedTopic>() ;
                FAQ__kav editedFAQ = new FAQ__kav(Id = articleid);
                for (String eachRelatedArticleURL : mapArticleRelatedArticle.get(articleid)) {
                    if (mapRelatedArticles.containsKey(eachRelatedArticleURL)) {
                        String parent;
                        FAQ__kav eachRelatedArticle = mapRelatedArticles.get(eachRelatedArticleURL);
                        for (FAQ__DataCategorySelection parentLabel : eachRelatedArticle.DataCategorySelections) {
                            parent = parentLabel.DataCategoryName;
                        }
                        listRelatedTopics.add(new RelatedTopic(eachRelatedArticle.Id,  eachRelatedArticle.Title, eachRelatedArticle.UrlName, parent));
                    }
                }
                editedFAQ.Related_Topic_Ids__c = JSON.serialize(listRelatedTopics);
                mapEditedFaq.put(editedFAQ.id, editedFAQ);
            }
            if (mapEditedFaq.size() > 0) {
                update mapEditedFaq.values();
            }
            System.debug('listRelatedTopics******' + JSON.serialize(listRelatedTopics));
        }
    }
    public class RelatedTopic {
        String topicId;
        String label;
        String parentLabel;
        String urlName;
        public RelatedTopic(String topicId, String label, String urlName, String parentLabel) {
            this.topicId = topicId;
            this.label = label;
            this.parentLabel = parentLabel;
            this.urlName = urlName;
        }
    }
}