/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  @created : 12/05/2016
  @Description : Controller Class for Belong Support Article page .
*/
public with sharing class BelongSupportArticleController 
{
/* ***** HANDLE TO CURRENT INSTANCE OF CONTROLLER (to be passed to rendered VF components, avoids re-instantiation of controller) ***** */
	public BelongSupportArticleController pkbTpcCon { get { return this; } }
	
	public static String DEFAULT_PUBLISH_STATUS = 'online';
	public static String DEFAULT_SITE_NAME = Site.getName();
	
	public String articleSelected { get; set; }
	public String FAQTitle { get; set; }
	public String dataCategoryAPI { get; set; }
	
	public String keywordsMeta 			{ get; set; }
	public String descriptionMeta 		{ get; set; }
	public String titleMeta 			{ get; set; }
	public String urlMetaOG 			{ get; set; }
	public String keywordsMetaOG 		{ get; set; }
	public String descriptionMetaOG 	{ get; set; }
	public String titleMetaOG 			{ get; set; }
	public String imageMetaOG 			{ get; set; }
	public String pageTitle 			{ get; set; }
	
	
	public Public_Knowledge_URL_Mapping__c knowledgeMetaSetting	{ get; set; }
	
	public FAQ__kav faqArticle { get; set; }
	public String publishStatus { get { return DEFAULT_PUBLISH_STATUS; } }
	public String siteName { get { return DEFAULT_SITE_NAME; } }
	public Boolean isSite { get { return String.isNotBlank(Site.getName());}}
	
	public Map<String,String> categoryKeySiteURL
		{
			set;
	    	get
	    	{
	    		if(categoryKeySiteURL == null)
	    		{
	    			categoryKeySiteURL = new Map<String,String>();
	    			String prodCat;
	    			for(String keyMap : knowledgeProducts.keySet())
	      			{
	      				prodCat = knowledgeProducts.get(keyMap)[0].Product_Category__c;
	      				categoryKeySiteURL.put(keyMap,  prodCat + '/' + knowledgeProducts.get(keyMap)[0].Article_Category__c);
	      				for(Public_Knowledge_Product__c knowProd : knowledgeProducts.get(keyMap))
	      				{
	      					if(!categoryKeySiteURL.containsKey(knowProd.Name))
	      					{
	      						prodCat = knowProd.Product_Category__c;
	      						categoryKeySiteURL.put(knowProd.Name, prodCat + '/' + knowProd.Article_Category__c);
	      					}
	      					
	      					if(!categoryKeySiteURL.containsKey(knowProd.Data_Category__c))
	      					{
	      						categoryKeySiteURL.put(knowProd.Data_Category__c, knowProd.Product_Category__c + '/' + knowProd.Article_Category__c);
	      					}
	      				}
	      			}
	    		}
	    		return categoryKeySiteURL;
	    	}
		}
		
	public Map<String,List<Public_Knowledge_Product__c>> knowledgeProducts
		{
			set;
	    	get 
	    	{
	    		if (knowledgeProducts == null) 
	      		{
	      			knowledgeProducts = KnowledgeSiteUtil.buildProductDisplay();
	      		}
	      		return knowledgeProducts;
	    	}
		}
	
	public String currentSiteUrl 
	  	{
	    	set;
	    	get 
	    	{
	      		if (currentSiteUrl == null) 
	      		{
	        		currentSiteUrl = Site.getBaseUrl()+'/';//Site.getCurrentSiteUrl();
	      		}
	      		return currentSiteUrl;
	    	}
	      	
		}
	
	
    public BelongSupportArticleController() { }
	public BelongSupportArticleController(ApexPages.StandardController sc) 
	{
		if (ApexPages.currentPage().getParameters().containsKey('artName') && 
			String.IsNotBlank(ApexPages.currentPage().getParameters().get('artName')))
		{
			articleSelected = ApexPages.currentPage().getParameters().get('artName');
			system.debug(logginglevel.error, 'artName => ' + articleSelected);
			getTopicArticle();
		}
	}
	
	public void getTopicArticle()
	{
		String query = 'Select id, Title, UrlName, Normalize_URL_Name__c, Summary, Question__c, Answer__c, Keywords_Public_Knowledge__c FROM FAQ__kav '; 	
		query += 'WHERE PublishStatus = \'' + DEFAULT_PUBLISH_STATUS +'\' AND Language = \'en_US\' AND ';
		query += 'Normalize_URL_Name__c = \'' + articleSelected +'\'';
		query += ' UPDATE VIEWSTAT';
		
		system.debug(logginglevel.error, 'query => ' + query);
		for (SObject sObj: Database.query(query))
		{
			faqArticle 			= (FAQ__kav) sObj;
			constructMetadata(faqArticle);
			faqArticle.Answer__c = replaceInternalURL(faqArticle.Answer__c);
			pageTitle	= faqArticle.Title;
        }
	}
	
	public void constructMetadata(FAQ__kav faqArticle)
	{
		knowledgeMetaSetting = KnowledgeSiteUtil.buildURLMapping().get('Article');
		
		integer	sizeMetaTitle = 155;
		String normTitleMeta = '';
		String normDescMeta = '';
		if(String.IsNotBlank(knowledgeMetaSetting.SERP_Title__c))
		{
			sizeMetaTitle -= knowledgeMetaSetting.SERP_Title__c.length() + 1;
		}
		
		if(String.IsNotBlank((String)(faqArticle.get(knowledgeMetaSetting.Meta_Title__c))))
		{
			normTitleMeta = (String)(faqArticle.get(knowledgeMetaSetting.Meta_Title__c));
			if(normTitleMeta.length() > sizeMetaTitle)
			{
				normTitleMeta = normTitleMeta.substring(0, sizeMetaTitle);
			}
		}
		
		if(String.IsNotBlank(knowledgeMetaSetting.SERP_Title__c))
		{
			normTitleMeta +=  ' ' + knowledgeMetaSetting.SERP_Title__c;
		}
		
		if(String.IsNotBlank((String)faqArticle.get(knowledgeMetaSetting.Meta_Description__c)))
		{
			normDescMeta  = (String)faqArticle.get(knowledgeMetaSetting.Meta_Description__c);
			if(normDescMeta.length() > 155)
			{
				normDescMeta +=  normDescMeta.substring(0, 155);
			}
		}
		
		titleMeta 			= normTitleMeta;
		descriptionMeta		= normDescMeta;
		
		normTitleMeta = '';
		normDescMeta = '';
		
		if(String.IsNotBlank((String)faqArticle.get(knowledgeMetaSetting.Meta_OG_Title__c)))
		{
			normTitleMeta = (String)faqArticle.get(knowledgeMetaSetting.Meta_OG_Title__c);
			if(normTitleMeta.length() > sizeMetaTitle)
			{
				normTitleMeta = normTitleMeta.substring(0, sizeMetaTitle);
			}
		}
		
		if(String.IsNotBlank(knowledgeMetaSetting.SERP_Title__c))
		{
			normTitleMeta +=  ' ' + knowledgeMetaSetting.SERP_Title__c;
		}
		
		if(String.IsNotBlank((String)faqArticle.get(knowledgeMetaSetting.Meta_OG_Description__c)))
		{
			normDescMeta  = (String)faqArticle.get(knowledgeMetaSetting.Meta_OG_Description__c);
			if(normDescMeta.length() > 155)
			{
				normDescMeta =  normDescMeta.substring(0, 155);
			}
		}
		
    	titleMetaOG 		= normTitleMeta;
    	descriptionMetaOG		= normDescMeta;
    	
    	if(String.IsNotBlank((String)faqArticle.get(knowledgeMetaSetting.Meta_OG_Keywords__c)))
    	{
    		keywordsMetaOG = (String)faqArticle.get(knowledgeMetaSetting.Meta_OG_Keywords__c);
    	}
    	
    	if(String.IsNotBlank((String)faqArticle.get(knowledgeMetaSetting.Meta_Keyword__c)))
		{
			keywordsMeta = (String)faqArticle.get(knowledgeMetaSetting.Meta_Keyword__c);	
		}
		
		if(String.IsNotBlank((String)faqArticle.get(knowledgeMetaSetting.Meta_OG_URL__c)))
		{
			urlMetaOG = (String)faqArticle.get(knowledgeMetaSetting.Meta_OG_URL__c);	
		}
		
		if(String.IsNotBlank(knowledgeMetaSetting.Meta_OG_Image__c))
		{
			imageMetaOG	= knowledgeMetaSetting.Meta_OG_Image__c;	
		}
	}
	
	public String replaceInternalURL(String Answer)
	{
		Map<String, String> translateURL = new Map<String,String>();
		List<String> URLlist = Answer.split('href="');
		
		for(String qualifiedURL : URLlist)
		{
			integer endIndex = qualifiedURL.indexOf('"');
			if(endIndex > 0)
			{
				String urlConvert = 	qualifiedURL.substring(0, endIndex);
				if(urlConvert.contains('articles'))
				{
					translateURL.put(urlConvert,NULL);
				}
			}
		}
		
		List<String> articleUrls = new List<String>();
		if(translateURL.size() > 0)
		{
			for(String URL : translateURL.keySet())
			{
				/*
					Format of smart link always follows this pattern for public site
					/articles/FAQ/URLNAME?artName=URLNAME parent
					i.e = " "/articles/FAQ/Why-is-My-NBN-Connection-Speed-Slow?artName=nbn-withdraw-order-work-instruction-faq"
					Note: string after the "?" is  a bug where Salesforce apply the same to all inner URL or smart links when using the URl Rewritter  
					*/
				integer endIndex = URL.indexOf('?');
				String urlConvert = URL.substring(0, endIndex);
				urlConvert = urlConvert.reverse();
				endIndex = urlConvert.indexOf('/');
				urlConvert = urlConvert.substring(0, endIndex);
				urlConvert = urlConvert.reverse();
				articleUrls.add(urlConvert.toLowerCase());
				translateURL.put(URL,urlConvert.toLowerCase());
			}
			
			Map<String, String> artURLNamePublicURL = new Map<String, String>();
			if(!articleUrls.isEmpty())
			{
				for(FAQ__DataCategorySelection faq: [SELECT Id, ParentId, DataCategoryGroupName,DataCategoryName, Parent.URLNAME, Parent.Normalize_URL_Name__c
														FROM FAQ__DataCategorySelection 
														WHERE Parent.Normalize_URL_Name__c IN: articleUrls])
				{
					String DataCategoryAPI = faq.DataCategoryName + '__c';
					if(categoryKeySiteURL.containsKey(DataCategoryAPI))
					{
						String artURL = currentSiteUrl + categoryKeySiteURL.get(DataCategoryAPI) + '/' + faq.Parent.Normalize_URL_Name__c;
						artURLNamePublicURL.put(faq.Parent.Normalize_URL_Name__c, artURL);
					}
				}
			}
			
			for(String URL : translateURL.keySet())
			{
				String urlName = translateURL.get(URL);
				if(artURLNamePublicURL.containskey(urlName))
				{
					String link = artURLNamePublicURL.get(urlName);
					Answer = Answer.replace(URL, link);
				}
			}
		}
		
		return Answer;
	}
}