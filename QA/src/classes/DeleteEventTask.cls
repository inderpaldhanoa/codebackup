global class DeleteEventTask implements Database.Batchable<sObject>, Database.Stateful
{
	
	global DeleteEventTask()
	{
		
	}	
	
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		String Query = 'SELECT Id, TBD__c FROM Event where TBD__c = true LIMIT 50000';		
		System.debug('DeleteEvents Query ' + Query);
		return Database.getQueryLocator(Query);
	}
	
	global void execute(Database.BatchableContext BC, List<Event> scope)
	{
		delete scope;
		
	}
	
	global void finish(Database.BatchableContext BC)
	{
		
	}
}