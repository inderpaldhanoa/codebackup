/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  @created : 20/07/2016
  @Description : Controller for BelongCSVfileUpload page
				Loads a CSV file, creates a Task "SMS" or a Note record
*/
public with sharing class BelongCSVfileUploadController 
{
    public List<SelectOption> uploadOptionList 			{set; get;}
    public List<SelectOption> SMSTemplatesList			{set; get;}
    public List<SelectOption> smsTempCatsOptions 		{set; get;}
    public List<Task>		  SMSNotificationList		{set; get;}
    public List<Note>		  contactLogList			{set; get;}
    
    
    public String fileContent			{set; get;}
    public String contentFile			{set; get;}
    public String uploadOption			{set; get;}
    public String smsTemplateID			{set; get;}
    public String smsTemplateCat 		{set; get;}
    public Id 	  smsRecordType 		{set; get;}
    
    public Boolean isSMSSelected  		{set; get;}
    public Boolean isLoadSuccess		{set; get;}
    public Boolean isFileLoad			{set; get;}
    public Boolean isConLogSelected    	{set; get;}
    public Boolean isADSLNBNSelected   	{set; get;}
     
    
    public List<String> filelines;
    public List<String>	tableHeaders;
    public List<String>	fieldTagLabel;
    
    public BelongCSVfileUploadController()
    {
    	uploadOption = '';
    	initializeForm();
    	Map<String,Schema.RecordTypeInfo> taskRecordTypeInfoName;
	    taskRecordTypeInfoName		= GlobalUtil.getRecordTypeByName('Task');//getting all Recordtype for the Sobject
        smsRecordType = taskRecordTypeInfoName.get('SMS').getrecordTypeId();
    	
    	getCsvUploadOptions();
    	getTemplateCategory();
    }
    
    public void initializeForm()
    {
    	isSMSSelected		= false;
    	isConLogSelected	= false;
    	isADSLNBNSelected	= false;
    	isLoadSuccess		= false;
    	isFileLoad			= false;
    	smsTemplateCat		= '';
    	smsTemplateID		= '';
    	contentFile			= '';
    	contactLogList 		= new List<Note>();
    	SMSTemplatesList	= new List<SelectOption>();
    	
    	resetSMSTaskTable();
    }
    
    public void setContentFile()
    {
    	contentFile = Apexpages.currentPage().getParameters().get('csvParam');
    	isFileLoad = true;
    	System.debug(logginglevel.error, '@setContentFile ' + contentFile);
    }
    
    /***This function reads the CSV file and inserts records into the Account object. ***/
    public Pagereference loadFile()
    {
    	fieldTagLabel 		= new List<String>();
    	filelines 			= new List<String>();
    	SMSNotificationList = new List<Task>();
    	contactLogList		= new List<Note>();
    	
    	isLoadSuccess = false;
    	boolean recordsFound = false;
        try
        {
        	integer headerSize;
            fileContent = contentFile;
			
            if(uploadOption == 'SMS')
            {
        		Map<Account, Map<String,String>> accountsFromFile = readCSVFileSMSLog();
        		
        		if(!accountsFromFile.isEmpty())
        		{
    				recordsFound = true;
    				SMS_Template__c smsTemplate = (SMS_Template__c) Cache.getRecord(smsTemplateID);
    				
    				for(Account accObj : accountsFromFile.keySet())
    				{
    					Map<String,String> fieldValueMap = accountsFromFile.get(accObj);
    					Task taskSMS = new Task();
    					taskSMS.Status = 'Completed';
    					taskSMS.SMS_Send_Status__c = 'Send';
    					taskSMS.WhatId = accObj.Id;
    					taskSMS.RecordTypeId = smsRecordType;
    					
    					for(Contact conObj : accObj.Contacts)
    					{
    						if(conObj.Contact_Role__c == 'Primary')
    						{
    							taskSMS.WhoId = conObj.Id;
    							taskSMS.SMS_Recipient__c = String.valueof(conObj.MobilePhone);
    							break;
    						}
    					}
    					taskSMS.Description = GlobalUtil.mergeSMSTemplateWithFields(smsTemplate, fieldValueMap);
    					taskSMS.Subject = smsTemplate.Name;
    					SMSNotificationList.add(taskSMS);
    					isLoadSuccess = true;
    				}
    			}
            }
            else if(uploadOption == 'conLog')
			{
				Map<Account, Map<String,String>> accountsFromFile = readCSVFileSMSLog();
				if(!accountsFromFile.isEmpty())
        		{
        			recordsFound = true;
        			
        			for(Account accObj : accountsFromFile.keySet())
    				{
    					Map<String,String> fieldValueMap = accountsFromFile.get(accObj);
    					Note contactNote 		= new Note();
    					contactNote.parentId	= accObj.Id;
    					contactNote.isPrivate	= false;
    					for(String fieldAPI : fieldValueMap.keySet())
    					{
    						String fieldValue = fieldValueMap.get(fieldAPI);
							contactNote.put(fieldAPI, fieldValue);
    					}
    					contactLogList.add(contactNote);
    				}
    				isLoadSuccess = true;
        		}
			}
            else if(uploadOption == 'ADSLNBN')
            {
            	
            }
            
            if(!recordsFound)
    		{
    			errorMsg('No records found');
    		}
            
		}catch(Exception e){ errorMsg('An error has occured reading the CSV file'+e.getMessage()); }
        
        return null;
    }
    
    public boolean validateSaving()
    {
    	boolean isValid = true;
    	if(uploadOption == 'SMS')
        {
        	for(task taskSMS : SMSNotificationList)
        	{
        		if(String.isBlank(taskSMS.SMS_Recipient__c))
        		{
        			taskSMS.SMS_Recipient__c.addError('Field cannot be blank');
        			isValid = false;
        		}
        		
        		if(String.isBlank(taskSMS.Description))
        		{
        			taskSMS.Description.addError('Field cannot be blank');
        			isValid = false;
        		}
        		
        	}
        	
        }
        else if(uploadOption == 'conLog')
		{
			for(Note noteLog : contactLogList)
        	{
        		if(String.isBlank(noteLog.Title))
        		{
        			noteLog.Title.addError('Field cannot be blank');
        			isValid = false;
        		}
        		
        		if(String.isBlank(noteLog.Body))
        		{
        			noteLog.Body.addError('Field cannot be blank');
        			isValid = false;
        		}
        	}
		}
		
		return isValid;
    }
    
    public void getTemplateCategory()  
    {
    	Schema.DescribeFieldResult templateSMSCats = Schema.sObjectType.SMS_Template__c.fields.Template_Category__c;
    	smsTempCatsOptions = new List<SelectOption>();
    	smsTempCatsOptions.add(new selectOption('', '--None--'));
        for (Schema.PickListEntry smsCatPickVal : templateSMSCats.getPicklistValues())
	    {
	    	smsTempCatsOptions.add(new SelectOption(smsCatPickVal.getValue(), smsCatPickVal.getLabel()));
	    	isLoadSuccess = false;
	    }
    }
    
    public Map<Account, Map<String,String>> readCSVFileSMSLog()
    {
    	Map<Account, Map<String,String>> accountsFromFile = new Map<Account, Map<String,String>>();
    	Map<String,Map<String,String>> fieldMapping = new Map<String, Map<String,String>>();
    	Set<String> accountNumbers = new Set<String>();
    	List<Account> accountResults = new List<Account>();
    	List<String>  inputvalues;
    	
    	// replace instances where a double quote begins a field containing a comma
		// in this case you get a double quote followed by a doubled double quote
		// do this for beginning and end of a field
        fileContent = fileContent.replaceAll('","','END_DBLQT,INITIAL_DBLQT');
        fileContent = fileContent.replaceAll(',"',',INITIAL_DBLQT');
        fileContent = fileContent.replaceAll('",','END_DBLQT,');
        
        // now replace all remaining double quotes - we do this so that we can reconstruct
		// fields with commas inside assuming they begin and end with a double quote
        fileContent = fileContent.replaceAll('""','DBLQT');
        
		// we are not attempting to handle fields with a newline inside of them
		// so, split on newline to get the spreadsheet rows
        //Now sepatate every row of the excel file
        fileContent = fileContent.replaceAll('\n','END_LINE');
        fileContent = fileContent.replaceAll('\r', '');
        fileContent = fileContent.replaceAll('"END_LINE','END_DBLQTEND_LINE');
        
        if(fileContent.endsWith('"'))
        	fileContent = fileContent.replaceAll('"','END_DBLQTEND_LINE');
        
        
    	try 
		{
    		filelines = fileContent.split('END_LINE');
		}catch (System.ListException e){errorMsg('Limits exceeded?' + e.getMessage());}
        
        fieldTagLabel = filelines[0].split(',');
        
    	try
    	{
    		String accountNumber='', fieldValue = '';
    		Boolean isCompositeField = false;
    		Boolean newRow = true;
	        integer labelPosition = 1;
	    	for (Integer i=1; i<filelines.size(); i++)
	        {
	        	inputvalues = new List<String>();
	            inputvalues = filelines[i].split(',');
	            
	            for(integer j=0; j<inputvalues.size(); j++)
	            {
	            	//Get the Account Number
	            	if(newRow)
	            	{
	            		accountNumber = inputvalues[j].trim();
			            accountNumbers.add(accountNumber);
			            fieldMapping.put(accountNumber, new Map<String, String>());
	            		newRow = false;
	            		continue;
	            	}
	            	String bracketStr = inputvalues[j];
	            	if(bracketStr.startsWith('INITIAL_DBLQT') && bracketStr.endsWith('END_DBLQT'))
            		{
            			fieldValue = bracketStr;
            		}
            		else if(bracketStr.startsWith('INITIAL_DBLQT') && !bracketStr.endsWith('END_DBLQT'))
            		{
            			fieldValue += bracketStr + ', ';
            			isCompositeField = true;
            		}
            		else if(isCompositeField && !bracketStr.endsWith('END_DBLQT'))
            		{
            			fieldValue += bracketStr + ', ';
            		}
            		else if(isCompositeField && bracketStr.endsWith('END_DBLQT'))
            		{
            			fieldValue += bracketStr;
            			isCompositeField = false;
            		}
            		else if(!isCompositeField)
            		{
            			fieldValue += bracketStr;
            		}
            		if(!isCompositeField)
            		{
            			fieldValue = fieldValue.replaceAll('INITIAL_DBLQT','').replaceAll('END_DBLQT','').replaceAll('DBLQT','"');
            			fieldMapping.get(accountNumber).put(fieldTagLabel[labelPosition].trim(), fieldValue);
            			isCompositeField = false;
            			fieldValue = '';
            			labelPosition ++;
            			
            			if(labelPosition == fieldTagLabel.size())
            			{
            				newRow = true;
            				labelPosition = 1;
            			}
            		}
	            }
	            
	            if(isCompositeField)
	            {
	            	fieldValue = fieldValue.substring(0, fieldValue.length()-2);
	            	fieldValue += '\n';
	            }
	        }
	    	
	        accountResults = (List<Account>) Cache.getRecordsFromCustomerNumber(accountNumbers, new List<String>{'Contact'});
	        
	        for(Account accObj : accountResults)
	        {
	        	String octaneID = accObj.Octane_Customer_Number__c;
	        	accountsFromFile.put(accObj, fieldMapping.get(octaneID));
	        }
        }catch (Exception e){System.debug(logginglevel.error, '@readCSVFileSMSContactLog Exception' + e);}
    	
        return accountsFromFile;
    }
    
    public void resetSMSTaskTable()
    {
    	SMSNotificationList = new List<Task>();
    }
    
    public void SendSMS()
    {
    	if(!SMSNotificationList.isEmpty() && validateSaving())
    	{
    		insert SMSNotificationList;
    		initializeForm();
    		uploadOption = '';
    	}
    }
    
    public void saveContactNotes()
    {
    	if(!contactLogList.isEmpty() && validateSaving())
    	{
    		insert contactLogList;
    		initializeForm();
    		uploadOption = '';
    	}
    }
    
    public void getCsvUploadOptions()
    {
    	uploadOptionList = new List<SelectOption>(); 
    	uploadOptionList.add(new SelectOption('', '--None--'));
    	uploadOptionList.add(new SelectOption('SMS', 'Bulk SMS'));
    	uploadOptionList.add(new SelectOption('conLog', 'Contact Log'));
    	//uploadOptionList.add(new SelectOption('ADSLNBN', 'ADSL to NBN'));
    }
    
    public void getUploadSelection()
    {
    	initializeForm();
    	if(uploadOption == 'SMS')
    	{
    		isSMSSelected = true;
    	}
    	else if(uploadOption == 'conLog')
    	{
    		isConLogSelected = true;
    	}
    }
    
    //SMS Template available
    public void getSMSTemplateList()  
    {
        SMSTemplatesList = new List<selectOption>();
        SMSTemplatesList.add(new selectOption('', '--None--'));
        
        resetSMSTaskTable();
        
        List<SMS_Template__c> tempList = new List<SMS_Template__c>();
        
        tempList = (List<SMS_Template__c>) cache.getRecordsFieldSetValue('SMS_Template__c', 'Template_Category__c', new Set<String>{smsTemplateCat});
        
        for(SMS_Template__c smsTemp : tempList)
        {
        	if(smsTemp.Active__c)
        	{
            	SMSTemplatesList.add(new selectOption(smsTemp.Id, smsTemp.name));
        	}
        }
    }
    
    public  void errorMsg(String s) { ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, s));   }
    private void infoMsg(String s)  { ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, s));    }
}