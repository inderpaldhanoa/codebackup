/************************************************************************************************************
* Apex Class Name   : MobileConstants.cls
* Version           : 1.0
* Created Date      : 22 March 2017
* Function          : Stores all COnstnats or String Literals common across code base
* Modification Log  :
* Developer         :        Date                Description
* -----------------------------------------------------------------------------------------------------------
* Girish P                  18/04/2017          Created Class.
************************************************************************************************************/
public class MobileConstants {
	public static final String HTTP_SUCCESS = 'Success';
	public static final String HTTP_ERROR = 'Error';
	public static final String STATUS_EXCEPTION = 'Exception';
	public static final String DEFAULT_PUBLIC_KNOWLEDGE_TYPE = 'Mobile';
	public static final String ASSET_STATUS_REQUESTED = 'Requested';
	public static final String KNOWLEDGE_ARTICLE_NAME = 'FAQ__ka';
	public static final String KNOWLEDGE_ARTICLE_VERSION_NAME = 'FAQ__kav';
	public static final String ASSET_SOBJECT = 'Asset';
	public static final String MOBILE_ASSET_RECORDTYPE = 'Mobile';
	public static final String NEW_CASE_STATUS = 'Open';
	public static final String NEW_CASE_ORIGIN = 'Website';
	public static final String PORTFAIL_STATUS = 'PCN_REJ';
	public static final String UTILBILL_MOBILE = 'Mobile';
	public static final String UNAUTH_CASE_LEAD_TYPE = 'Mobile Enquiry';
	public static final String LEAD_PRODUCT_TYPE = 'Mobile';
	public static final Integer STATUS_BAD = RestUtility.STATUS_BAD;
	public static final Integer STATUS_OK = RestUtility.STATUS_OK;
	public static final Integer STATUS_ISE = RestUtility.STATUS_ISE;
}