public with sharing class MobileRestServiceTransactionAPIHelper {
    //All constants
    Public static Final String HTTP_SUCCESS = 'Success';
    Public static Final String HTTP_ERROR = 'Error';
    Public static Final String STATUS_EXCEPTION = 'Exception';
    Public static Lead mobileLead;
    Public static WrapperServiceTransaction wrapperServTrans;
    Public static WrapperCaseWithAttachment wrapperInput;
    Public static WrapperCaseCommentWithAttachment wrapperCommentInput;
    Public static List<sObject> listSObjectUpdate = new List<sObject>();
    public static Object[] results ;
    public static final Integer STATUS_BAD = RestUtility.STATUS_BAD;
    public static final Integer STATUS_OK = RestUtility.STATUS_OK;
    public static final Integer STATUS_ISE = RestUtility.STATUS_ISE;

    /*
        @input  : PATCH request with TelFlow ID, OctaneId and optionally other fields if available
        @output : Confirmation a Service Transaction has been created or updated
        @Description : Helper method for the HTTP PATCH method for RESTMobileAPI.cls
    */
    public static WrapperHTTPResponse upsertServiceTransactionHelper(String jsonInput) {
        Service_Transactions__c servTransToUpsert = new Service_Transactions__c();
        Set<String> setFinalServiceTranStatus = new Set<String> {
            'Complete',
            'Rejected'
        };
        system.debug('-- About to try upsertServiceTransactionHelper');
        try {
            // Get the values out of the JSON string
            wrapperServTrans = WrapperServiceTransaction.parseTransaction(jsonInput);
            // If the JSON has the required Id fields
            if (wrapperServTrans.telFlowId != null && wrapperServTrans.octaneId != null ) {
                // If this is an update then we can query the existing values so only the new values are changed
                //list<Service_Transactions__c> lstQueryResults = (List<Service_Transactions__c>) GlobalUtil.getSObjectRecords('Service_Transactions__c', 'ID__c', wrapperServTrans.telFlowId, NULL);
                
                // Testing the "FOR UPDATE" locking below, starting at 4:00 PM 01/08/2017. This may help with the DUPLICATE ID defect.
                String query = 'SELECT ';
                Set<String> sFields = Schema.getGlobalDescribe().get('Service_Transactions__c').getDescribe().fields.getMap().keySet();
                String sAllFields = String.join(new List<String>(sFields), ',');
                query += sAllFields + ' FROM Service_Transactions__c WHERE ID__c = \'' + wrapperServTrans.telFlowId + '\' FOR UPDATE';
                list<Service_Transactions__c> lstQueryResults = database.query(query);
                
                if (!lstQueryResults.isEmpty()) {
                    servTransToUpsert = lstQueryResults[0];
                }
                
                // Get the relevant Contact if it hasn't been set before
                List<Contact> lstRelatedContact = new List<Contact>();
                if (servTransToUpsert == null || servTransToUpsert.Primary_Contact__c == null) {
                    lstRelatedContact = GlobalUtil.getContactByOctane(wrapperServTrans.octaneId);
                }
                // Also get the contact for receiver
                List<Contact> lstReceiverContact = new List<Contact>();
                if ((servTransToUpsert == null || servTransToUpsert.Receiver_Name__c == null)
                        && wrapperServTrans.receiverOctaneId != null ) {
                    lstReceiverContact = GlobalUtil.getContactByOctane(wrapperServTrans.receiverOctaneId);
                }
                // Set the primary contact and Account according to the Service Transaction type
                if( !String.isEmpty(wrapperServTrans.type) && wrapperServTrans.type.equals('Data gifting')
                        && !String.isEmpty(wrapperServTrans.subType) && wrapperServTrans.subType.equals('Receive') ) {
                    servTransToUpsert.Primary_Contact__c        = !lstReceiverContact.isEmpty() ? lstReceiverContact[0].Id : servTransToUpsert.Primary_Contact__c;
                    servTransToUpsert.Account__c                = !lstReceiverContact.isEmpty() ? lstReceiverContact[0].AccountId : servTransToUpsert.Account__c;
                }
                else {
                    servTransToUpsert.Primary_Contact__c        = !lstRelatedContact.isEmpty() ? lstRelatedContact[0].Id : servTransToUpsert.Primary_Contact__c;
                    servTransToUpsert.Account__c                = !lstRelatedContact.isEmpty() ? lstRelatedContact[0].AccountId : servTransToUpsert.Account__c;
                }
                servTransToUpsert.Gifter_Name__c            = !lstRelatedContact.isEmpty() ? lstRelatedContact[0].Name : servTransToUpsert.Gifter_Name__c;
                servTransToUpsert.Receiver_Name__c          = !lstReceiverContact.isEmpty() ? lstReceiverContact[0].Name : servTransToUpsert.Receiver_Name__c;
                // Get the relevant Asset if it hasn't been set before                
                List<String> lstInServiceStatuses = Label.MOBILE_IN_SERVICE_ASSET_STATUS.split(',');
				Set<String> setInserviceStatuses = new Set<String>(lstInServiceStatuses);
                List<Asset> lstRelatedAsset = new List<Asset>();
                if ((servTransToUpsert == null || servTransToUpsert.Asset__c == null) && servTransToUpsert.Primary_Contact__c != null) {
                    lstRelatedAsset = [SELECT Id
                                       FROM Asset
                                       WHERE ContactId = :servTransToUpsert.Primary_Contact__c
                                       AND Status IN: setInserviceStatuses];
                }
                servTransToUpsert.Asset__c                  = !lstRelatedAsset.isEmpty() ? lstRelatedAsset[0].Id : servTransToUpsert.Asset__c;
                // Fill in fields for the new Service Transaction
                servTransToUpsert.Action__c                 = !string.isEmpty(wrapperServTrans.action) ? wrapperServTrans.action : servTransToUpsert.Action__c;
                servTransToUpsert.Agent_ID__c               = !string.isEmpty(wrapperServTrans.agentId) ? wrapperServTrans.agentId : servTransToUpsert.Agent_ID__c;
                servTransToUpsert.Billing_cycle__c          = !string.isEmpty(wrapperServTrans.billingCycle) ? integer.valueOf(wrapperServTrans.billingCycle) : servTransToUpsert.Billing_cycle__c;
                servTransToUpsert.Completion_Date__c        = !string.isEmpty(wrapperServTrans.completionDate) ? (DateTime)JSON.deserialize('"' + wrapperServTrans.completionDate + '"', DateTime.class) : servTransToUpsert.Completion_Date__c;
                servTransToUpsert.Gifter_Mobile_No__c       = !string.isEmpty(wrapperServTrans.gifterMobileNo) ? wrapperServTrans.gifterMobileNo : servTransToUpsert.Gifter_Mobile_No__c;
                servTransToUpsert.Mobile_No__c              = !string.isEmpty(wrapperServTrans.mobileNo) ? wrapperServTrans.mobileNo.deleteWhiteSpace() : servTransToUpsert.Mobile_No__c;
                servTransToUpsert.Offer_contract_term__c    = !string.isEmpty(wrapperServTrans.offerContractTerm) ? wrapperServTrans.offerContractTerm : servTransToUpsert.Offer_contract_term__c;
                servTransToUpsert.Offer_Description__c      = !string.isEmpty(wrapperServTrans.offerDescriptions) ? wrapperServTrans.offerDescriptions : servTransToUpsert.Offer_Description__c;
                servTransToUpsert.Offer_ID__c               = !string.isEmpty(wrapperServTrans.offerIds) ? wrapperServTrans.offerIds : servTransToUpsert.Offer_ID__c;
                servTransToUpsert.Offer_Type__c             = !string.isEmpty(wrapperServTrans.offerType) ? wrapperServTrans.offerType : servTransToUpsert.Offer_Type__c;
                servTransToUpsert.Offer_version_date_valid_from__c  = !string.isEmpty(wrapperServTrans.offerVersionDateValidFrom) ? wrapperServTrans.offerVersionDateValidFrom : servTransToUpsert.Offer_version_date_valid_from__c;
                servTransToUpsert.Offer_version_date_valid_to__c    = !string.isEmpty(wrapperServTrans.offerVersionDateValidTo) ? wrapperServTrans.offerVersionDateValidTo : servTransToUpsert.Offer_version_date_valid_to__c;
                servTransToUpsert.Offer_version_number__c   = !string.isEmpty(wrapperServTrans.offerVersionNumber) ? wrapperServTrans.offerVersionNumber : servTransToUpsert.Offer_version_number__c;
                servTransToUpsert.Offer_Price__c            = !string.isEmpty(wrapperServTrans.offerPrice) ? wrapperServTrans.offerPrice : servTransToUpsert.Offer_Price__c;
                servTransToUpsert.Price__c                  = !string.isEmpty(wrapperServTrans.totalPrice) ? decimal.valueOf(wrapperServTrans.totalPrice) : servTransToUpsert.Price__c;
                servTransToUpsert.Process_Date__c           = !string.isEmpty(wrapperServTrans.processDate) ? (DateTime)JSON.deserialize('"' + wrapperServTrans.processDate + '"', DateTime.class) : servTransToUpsert.Process_Date__c;
                servTransToUpsert.Receiver_Mobile_No__c     = !string.isEmpty(wrapperServTrans.receiverMobileNo) ? wrapperServTrans.receiverMobileNo : servTransToUpsert.Receiver_Mobile_No__c;
                servTransToUpsert.Reject_Code__c            = !string.isEmpty(wrapperServTrans.rejectCode) ? wrapperServTrans.rejectCode : servTransToUpsert.Reject_Code__c;
                servTransToUpsert.Reject_Reason__c          = !string.isEmpty(wrapperServTrans.rejectReason) ? wrapperServTrans.rejectReason : servTransToUpsert.Reject_Reason__c;
                servTransToUpsert.Size__c                   = !string.isEmpty(wrapperServTrans.dataGiftSize) ? decimal.valueOf(wrapperServTrans.dataGiftSize) : servTransToUpsert.Size__c;
                servTransToUpsert.Source__c                 = !string.isEmpty(wrapperServTrans.source) ? wrapperServTrans.source : servTransToUpsert.Source__c;
                servTransToUpsert.Status__c                 = !string.isEmpty(wrapperServTrans.status) ? wrapperServTrans.status : servTransToUpsert.Status__c;
                servTransToUpsert.Sub_Status__c             = !string.isEmpty(wrapperServTrans.subStatus) ? wrapperServTrans.subStatus : servTransToUpsert.Sub_Status__c;
                servTransToUpsert.Sub_Type__c               = !string.isEmpty(wrapperServTrans.subType) ? wrapperServTrans.subType : servTransToUpsert.Sub_Type__c;
                servTransToUpsert.Submitted_Date__c         = !string.isEmpty(wrapperServTrans.submittedDate) ? (DateTime)JSON.deserialize('"' + wrapperServTrans.submittedDate + '"', DateTime.class) : servTransToUpsert.Submitted_Date__c;
                servTransToUpsert.Type__c                   = !string.isEmpty(wrapperServTrans.type) ? wrapperServTrans.type : servTransToUpsert.Type__c;
                servTransToUpsert.ID__c                     = !string.isEmpty(wrapperServTrans.telFlowId) ? wrapperServTrans.telFlowId : servTransToUpsert.ID__c;
                servTransToUpsert.API_Request_JSON__c       = !string.isEmpty(servTransToUpsert.API_Request_JSON__c) ? servTransToUpsert.API_Request_JSON__c + '\nNEXT REQUEST ' + Datetime.now().format() + '\n' + JSON.serializePretty(wrapperServTrans) : JSON.serializePretty(wrapperServTrans);
                servTransToUpsert.New_SIM_Number__c         = !string.isEmpty(wrapperServTrans.newSIMNumber) ? wrapperServTrans.newSIMNumber : servTransToUpsert.New_SIM_Number__c;
                servTransToUpsert.Payment_Due_Date__c       = !string.isEmpty(wrapperServTrans.paymentDueDate) ? (Date)JSON.deserialize('"' + wrapperServTrans.paymentDueDate + '"', Date.class) : servTransToUpsert.Payment_Due_Date__c;
                servTransToUpsert.Initiating_User_Id__c     = !string.isEmpty(wrapperServTrans.initiatingUserId) ? wrapperServTrans.initiatingUserId : servTransToUpsert.Initiating_User_Id__c;
                servTransToUpsert.Previous_Mobile_No__c     = !string.isEmpty(servTransToUpsert.Previous_Mobile_No__c) ? servTransToUpsert.Previous_Mobile_No__c : servTransToUpsert.Mobile_No__c;
                servTransToUpsert.Account_Balance__c        = !string.isEmpty(wrapperServTrans.accountBalance) ? decimal.valueOf(wrapperServTrans.accountBalance) : servTransToUpsert.Account_Balance__c;
                upsert servTransToUpsert ID__c;
                MobileRestUtility.status = HTTP_SUCCESS;
                MobileRestUtility.statuscode = STATUS_OK;
                MobileRestUtility.message = 'Successfully created or updated Service Transaction: ' + servTransToUpsert.Id;
            }
            else {
                MobileRestUtility.message = 'Error: Invalid JSON provided. Must include TelFlowId and OctaneId at minimum.';
                MobileRestUtility.returnWithError('upsertServiceTransactionHelper', jsonInput);
            }
            system.debug('--The ID of the upserted Service Transaction: ' + servTransToUpsert.Id);
        }
        catch (Exception e) {
            MobileRestUtility.setExceptionResponse(e, 'upsertServiceTransactionHelper', jsonInput);
        }
        List<Service_Transactions__c> lstServTransToCreate = new List<Service_Transactions__c>();
        lstServTransToCreate.add(servTransToUpsert);
        return RestUtility.getResponse(MobileRestUtility.status, MobileRestUtility.message, MobileRestUtility.statuscode, lstServTransToCreate, MobileRestUtility.logs);
    }

    /*
        @input  : GET request with Octane Id, xxxxxxDate, Maximum Number of Results, Starting Offset
        @output : A list of Service Transactions for the provided Octane Id, xxxxxDate and yyyyy
        @Description : Helper method for the HTTP GET method for RESTMobileAPI.cls
    */
    public static WrapperHTTPResponse getServiceTransactionHelper(Map<String, String> mapParameters) {
        List<Service_Transactions__c> lstServiceTransactions = new List<Service_Transactions__c>();
        integer intMaxNumOfResults, intStartOffset;
        String whereClause;
        // If the parameters include the required fields
        if (mapParameters.containsKey('octaneId')) {
            // Find the Service Transactions for the provided Octane Id
            if (mapParameters.get('octaneId') != null) {
                whereClause = ' Octane_Customer_Number__c = \'' + (String) mapParameters.get('octaneId') + '\'';
                if (mapParameters.containsKey('startDate') && mapParameters.containsKey('endDate')) {
                    String[] strStartDateParts = mapParameters.get('startDate').split('-');
                    DateTime dStartDate =  DateTime.newInstance(integer.valueOf(strStartDateParts[0]), integer.valueOf(strStartDateParts[1]), integer.valueOf(strStartDateParts[2]), 8, 0, 0); // From 8 AM local time on specified date
                    String[] strEndDateParts = mapParameters.get('endDate').split('-');
                    DateTime dEndDate =  DateTime.newInstance(integer.valueOf(strEndDateParts[0]), integer.valueOf(strEndDateParts[1]), integer.valueOf(strEndDateParts[2]), 18, 0, 0); // To 6 PM local time on specified date
                    system.debug('-- The end dateTime: ' + dEndDate + ', The end jsonSerialize date: ' + JSON.serialize(dEndDate));
                    whereClause +=  ' AND CreatedDate >= ' + JSON.serialize(dStartDate) + ' AND CreatedDate <= ' + JSON.serialize(dEndDate);
                    whereClause = whereClause.remove('"');
                }
                if (mapParameters.containsKey('mobileNo')) {
                    whereClause +=  ' AND Mobile_No__c = \'' + mapParameters.get('mobileNo') + '\'';
                }
                if (mapParameters.containsKey('status')) {
                    whereClause +=  ' AND Status__c = \'' + mapParameters.get('status') + '\'';
                }
                if (mapParameters.containsKey('subStatus')) {
                    whereClause +=  ' AND Sub_Status__c = \'' + mapParameters.get('subStatus') + '\'';
                }
                if (mapParameters.containsKey('telflowId')) {
                    whereClause +=  ' AND ID__c = \'' + mapParameters.get('telflowId') + '\'';
                }
                whereClause +=       ' ORDER BY CreatedDate';
                if (mapParameters.containsKey('maxNumOfResults') && mapParameters.containsKey('startOffset')) {
                    whereClause +=  ' LIMIT ' + integer.valueOf(mapParameters.get('maxNumOfResults')) +
                                    ' OFFSET ' + integer.valueOf(mapParameters.get('startOffset'));
                }
                else {
                    whereClause +=  ' LIMIT 100';
                }
                lstServiceTransactions = (List<Service_Transactions__c>)GlobalUtil.getSObjectRecords('Service_Transactions__c', '', '', whereClause);
            }
            // If we have found relevant records
            if (!lstServiceTransactions.isEmpty()) {
                MobileRestUtility.status = HTTP_SUCCESS;
                MobileRestUtility.statuscode = STATUS_OK;
                MobileRestUtility.message = 'Successfully found ' + lstServiceTransactions.size() + ' Service Transaction(s) for the Octane Id (in the requested range).';
            }
            else {
                MobileRestUtility.status = HTTP_SUCCESS;
                MobileRestUtility.statuscode = STATUS_OK;
                MobileRestUtility.message = 'There are no further Service Transactions in Salesforce matching the given criteria.';
            }
        }
        else {
            MobileRestUtility.message = 'Error: Invalid parameters provided. You must at least provide OctaneId to select a range of Service Transactions.';
            MobileRestUtility.returnWithError('getServiceTransactionHelper', JSON.serialize(mapParameters));
        }
        return RestUtility.getResponse(MobileRestUtility.status, MobileRestUtility.message, MobileRestUtility.statuscode, lstServiceTransactions, MobileRestUtility.logs);
    }
}