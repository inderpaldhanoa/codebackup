<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Belong_Voice_Addition_email_alert</fullName>
        <description>Belong Voice Addition email alert</description>
        <protected>false</protected>
        <recipients>
            <field>ShipToContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Voice/Belong_Voice_Addition_email_template</template>
    </alerts>
    <alerts>
        <fullName>Belong_Voice_alteration_Email_alert</fullName>
        <description>Belong Voice alteration Email alert</description>
        <protected>false</protected>
        <recipients>
            <field>ShipToContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Voice/Belong_Voice_Alteration</template>
    </alerts>
    <alerts>
        <fullName>Belong_Voice_deletion_Email_alert</fullName>
        <description>Belong Voice deletion Email alert</description>
        <protected>false</protected>
        <recipients>
            <field>ShipToContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Voice/Belong_Voice_Alteration</template>
    </alerts>
    <alerts>
        <fullName>Belong_voice_follow_up_emails_for_join_up</fullName>
        <description>Belong voice follow up emails for join up</description>
        <protected>false</protected>
        <recipients>
            <field>ShipToContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Voice/Incomplete_setup_followup_mail</template>
    </alerts>
    <fieldUpdates>
        <fullName>Cooling_Off_Customer_TRUE</fullName>
        <description>Cooling Off Customer = True</description>
        <field>Cooling_Off_Customer__c</field>
        <literalValue>1</literalValue>
        <name>Cooling Off Customer TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Cooling Off Customer</fullName>
        <actions>
            <name>Cooling_Off_Customer_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.Order_Step__c</field>
            <operation>equals</operation>
            <value>STEP_COOLING_OFF</value>
        </criteriaItems>
        <description>Workflow checks if Order Step = STEP_COOLING_OFF</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
