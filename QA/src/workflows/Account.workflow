<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Customer_Status_Updated</fullName>
        <description>KH - P1S1: Update the Customer Status Updated field with today&apos;s date</description>
        <field>Customer_Status_Updated__c</field>
        <formula>TODAY()</formula>
        <name>Customer Status Updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetSyncToSent</fullName>
        <description>Change the Sync Status field to &quot;Sent&quot;</description>
        <field>Sync_Status__c</field>
        <literalValue>Sent</literalValue>
        <name>SetSyncToSent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Account_Customer_Status_update_outbound</fullName>
        <apiVersion>36.0</apiVersion>
        <endpointUrl>https://stag-elb.belongtest.com.au/project-eve/api/services/salesforce/account/status/notification</endpointUrl>
        <fields>Customer_Status__c</fields>
        <fields>Id</fields>
        <fields>Octane_Customer_Number__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>devops@belong.com.au</integrationUser>
        <name>Account  Customer Status update outbound</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Account_manual_created_outbound</fullName>
        <apiVersion>35.0</apiVersion>
        <endpointUrl>http://putsreq.com/9pH7EDSDaYHT3JEburj9</endpointUrl>
        <fields>Id</fields>
        <fields>Lease_Transferred_from__c</fields>
        <fields>Octane_Customer_Number__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>devops@belong.com.au</integrationUser>
        <name>Account manual created outbound</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Fixed_Braintree_Outbound_Message</fullName>
        <apiVersion>40.0</apiVersion>
        <endpointUrl>https://stag-elb.belongtest.com.au/project-eve/api/services/salesforce/braintreeid/notification</endpointUrl>
        <fields>Braintree_Customer_ID__c</fields>
        <fields>Id</fields>
        <fields>Octane_Customer_Number__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>belongadmin@belong.com.au</integrationUser>
        <name>Fixed Braintree Outbound Message</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Mobile_Send_To_SL_Braintree_ID</fullName>
        <apiVersion>40.0</apiVersion>
        <endpointUrl>https://mobile-preprod.belongtest.com.au/api/v1/service/update-braintree-customerid-event</endpointUrl>
        <fields>Braintree_Customer_ID__c</fields>
        <fields>Id</fields>
        <fields>Mobile_Octane_Customer_Number__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>belongadmin@belong.com.au</integrationUser>
        <name>Mobile: Send To SL: Braintree ID</name>
        <protected>false</protected>
        <useDeadLetterQueue>true</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>cooling_off_termination_outbound_message</fullName>
        <apiVersion>37.0</apiVersion>
        <description>cooling off termination outbound message</description>
        <endpointUrl>https://stag-elb.belongtest.com.au/project-eve/api/services/salesforce/account/coolingOff/status/notification</endpointUrl>
        <fields>Customer_Status__c</fields>
        <fields>Id</fields>
        <fields>Octane_Customer_Number__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>devops@belong.com.au</integrationUser>
        <name>cooling off termination outbound message</name>
        <protected>false</protected>
        <useDeadLetterQueue>true</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Account  Customer Category update</fullName>
        <actions>
            <name>SetSyncToSent</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Customer_Status_update_outbound</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Send outbound message to service layer to update Octane customer category status when updating customer status in Salesforce, can also re-triggered by administrator by update sync status to send</description>
        <formula>(ISCHANGED( Customer_Status__c ) &amp;&amp; (Lease_Transferred_from__c &lt;&gt;null ||  LastModifiedBy.LastName &lt;&gt; &apos;Integration&apos;))|| (ISCHANGED( Sync_Status__c ) &amp;&amp; ISPICKVAL(Sync_Status__c, &apos;Send&apos;) &amp;&amp; NOT(ISBLANK(Octane_Customer_Number__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account Customer Category update</fullName>
        <actions>
            <name>Account_Customer_Status_update_outbound</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Customer_Status__c ) &amp;&amp; (Lease_Transferred_from__c &lt;&gt;null ||  LastModifiedBy.LastName &lt;&gt; &apos;Integration&apos;) &amp;&amp; (NOT(ISBLANK(Octane_Customer_Number__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account Customer Category updated</fullName>
        <actions>
            <name>Account_Customer_Status_update_outbound</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Customer_Status__c )   &amp;&amp; NOT(ISBLANK(Octane_Customer_Number__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account manual created outbound</fullName>
        <actions>
            <name>Account_manual_created_outbound</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <formula>(Lease_Transferred_from__c &lt;&gt; &quot;&quot;) &amp;&amp;(ISBLANK(Octane_Customer_Number__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Customer Status Updated</fullName>
        <actions>
            <name>Customer_Status_Updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the date field &quot;Customer Status Updated&quot; with Today when the &quot;Customer Status&quot; field is updated.</description>
        <formula>ISCHANGED( Customer_Status__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Fixed Send BrainTreeId to Service Layer</fullName>
        <actions>
            <name>Fixed_Braintree_Outbound_Message</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Whenever salesforce gets an updated braintree id, it should be sent to mobile and fixed service layers</description>
        <formula>ISCHANGED(Braintree_Customer_ID__c) &amp;&amp; NOT (ISNEW()) &amp;&amp; NOT(ISNULL(PRIORVALUE(Braintree_Customer_ID__c))) &amp;&amp; NOT(ISBLANK(Octane_Customer_Number__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Mobile Send BrainTreeId to Service Layer</fullName>
        <actions>
            <name>Mobile_Send_To_SL_Braintree_ID</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>this will send notification to service layer to update the braintree id</description>
        <formula>AND( 				ISCHANGED(Braintree_Customer_ID__c), 				NOT(ISNULL(Mobile_Octane_Customer_Number__c)), 									NOT(ISNEW()) 													)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>cooling off termination</fullName>
        <actions>
            <name>cooling_off_termination_outbound_message</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>cooling off termination</description>
        <formula>AND((ISPICKVAL(PRIORVALUE(Customer_Status__c), &quot;CoolingOff&quot;)), (ISPICKVAL(Customer_Status__c, &quot;Terminated&quot;)), NOT(ISBLANK(Octane_Customer_Number__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
