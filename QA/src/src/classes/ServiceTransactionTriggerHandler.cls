/************************************************************************************************************
* Apex Class Name   : ServiceTransactionTriggerHandler.cls
* Version           : 1.0
* Created Date      : 20 June 2017
* Function          : Trigger handler for Service Transactions
* Modification Log  :
* Developer                 Date                Description
* -----------------------------------------------------------------------------------------------------------
* Tom Clayton               20/06/2017          Created Class.
* Tom Clayton               23/06/2017          Ongoing updates.
************************************************************************************************************/

public with sharing class ServiceTransactionTriggerHandler implements TriggerInterface
{
	public static Boolean executeTrigger = true; // To be used to prevent loop if logic will update another Service Transaction
	
	/**
	*	This public method caches related data for before triggers, called once on start of the trigger execution
	*/
	public void cacheBefore()
	{
		System.debug(LoggingLevel.ERROR, '====> ServiceTransacion cacheBefore is called');
		ServiceTransactionTriggerHelper.initializeProperties();
		if(trigger.isUpdate)
		{
			ServiceTransactionTriggerHelper.fixEmptyAssetRelationships();
		}
	}

	/**
	*	This public method caches related data for after triggers, called once on start of the trigger execution
	*/
	public void cacheAfter()
	{
		System.debug(LoggingLevel.ERROR, '====> ServiceTransacion cacheAfter is called');
		ServiceTransactionTriggerHelper.initializeProperties();
		if(!(trigger.isDelete || trigger.isUnDelete))
		{
			ServiceTransactionTriggerHelper.collectAssetsAndContacts();
		}
	} 
	
	/**
	*	This public method process the business logic on 1 sObject at before insert
	*/
	public void beforeInsert(sObject newSObj)
	{
		
	}
	
	/**
	*	This public method process the business logic on 1 sObject at after insert
	*/
	public void afterInsert(sObject newSObj)
	{
		if(ServiceTransactionTriggerHandler.executeTrigger)
		{
			ServiceTransactionTriggerHandler.executeTrigger = false;
			ServiceTransactionTriggerHelper.processServTransAndAsset(null, newSObj);
			ServiceTransactionTriggerHelper.processFailures(newSObj);
			ServiceTransactionTriggerHandler.executeTrigger = true;
		}
	}
	
	/**
	*	This public method process the business logic on 1 sObject at before update
	*/
	public void beforeUpdate(sObject oldSObj, sObject newSObj)
	{
		
	}
	
	/**
	*	This public method process the business logic on 1 sObject at after update
	*/
	public void afterUpdate(sObject oldSObj, sObject newSObj)
	{
		if(ServiceTransactionTriggerHandler.executeTrigger)
		{
			ServiceTransactionTriggerHandler.executeTrigger = false;
			ServiceTransactionTriggerHelper.processServTransAndAsset(oldSObj, newSObj);
			ServiceTransactionTriggerHelper.processFailures(newSObj);
			ServiceTransactionTriggerHandler.executeTrigger = true;
		}
	}
	
	/**
	*	This public method process the business logic on 1 sObject at before delete
	*/
	public void beforeDelete(sObject oldSObj)
	{
		
	}
	
	/**
	*	This public method process the business logic on 1 sObject at after delete
	*/
	public void afterDelete(sObject oldSObj)
	{
		
	}
	
	/**
	*	This public method process the business logic on 1 sObject at after undelete
	*/
	public void afterUndelete(sObject newSObj)
	{
			
	}
	
	/**
	*	Ater all rows have been processed this method will be called
	*	Usualy DML are placed in this method
	*/
	public void afterProcessing()
	{
		//Avoid calling DML if not required
		ServiceTransactionTriggerHandler.executeTrigger = false;
		ServiceTransactionTriggerHelper.updateContacts();
		ServiceTransactionTriggerHelper.updateAssets();
		ServiceTransactionTriggerHelper.insertAssets();
		ServiceTransactionTriggerHelper.updateServiceTransactions();
		ServiceTransactionTriggerHelper.insertCases();
		ServiceTransactionTriggerHandler.executeTrigger = true;
	}	
}