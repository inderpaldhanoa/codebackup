global without sharing class IdentityChangePasswordController{
    global static string checkUser(string userEmail) {
        list<User> validUser = [SELECT id 
                                FROM user 
                                WHERE email = :userEMail 
                                AND isActive = True LIMIT 1];
        if(!validUser.isempty()){
            return  string.valueof(validUser.get(0).id);
        }else {
            return NULL;
        }
    }
    
    @RemoteAction
    global static string changeUserPwd(String userEmail, String newPassword, string confirmPassword){
        system.debug('userEMail:::'+userEMail);       
        string returnValue = 'failure';
        string emailRegex = '([a-zA-Z0-9_\\-\\.]+)@((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})';
        pattern myPattern = Pattern.compile(emailRegex);
        Matcher myMatcher = myPattern.matcher(userEmail);
        if (!myMatcher.matches()) {
            return 'failure';
        }
        if(newPassword == NULL || newPassword == '' || confirmPassword == '' || confirmPassword == NULL){
            return 'Password is empty'; 
        }
        
        //try{ 
        if(!newPassword.equals(confirmPassword)){
            return 'Passwords are not identical';
        }
        string userId = checkUser(userEmail);
        if(userId == NULL){
            return 'Incorrect User';
        }
        system.debug('userId:::'+userId);
        if(userId != NULL){
            try{
            system.setPassword(userId, newpassword);
            }catch(exception e){
                return e.getMessage();
            }
            update new User(Id = userId, AEMPassword__c = null);
            return 'success';         
        } 
        return returnValue;
    }
}