@RestResource(urlMapping = '/RESTMobileRetail/V1/*')
global with sharing class MobileRestRetail {
	public static String restResponse;
	public static String apiCall = '';
	public static final String STATUS_EXCEPTION = 'Exception';
	public static final String HTTP_ERROR = 'Error';

	public static final String API_UPSERT_SIM = 'upsertSimdata';
	global static Set<String> allowedApiCalls = new Set<String> {
		API_UPSERT_SIM
	};

	@HttpPost
	global static void postMobileRetail() {
		MobileRestUtility.logs = new List<Log__c>();
		Savepoint sp = Database.setSavepoint();
		RestRequest req = RestContext.request;
		RestResponse res = Restcontext.response;
		Map<String, String> mapParameters = req.params;
		// Set header as JSON
		res.addHeader('Content-Type', 'application/json');
		// Call post helper class to process the POST request
		// Append the response to request body
		if (mapParameters.containsKey('APICall')) {
			apiCall = mapParameters.get('APICall');
		}
		else {
			restResponse = 'Invalid Api Call or parameter missing....';
			restResponse = JSON.serialize(RestUtility.getResponse('Error', restResponse, 404, NULL, NULL));
			res.responseBody = Blob.valueOf(restResponse);
			return;
		}
		System.debug(' req.requestBody********' + req.requestBody.toString());
		if (String.isEmpty(req.requestBody.toString())) {
			restResponse = 'JSON Body is required for Post requests.';
			restResponse = RestUtility.getResponse('Error', restResponse, 404, NULL, NULL).status;
		}
		else
			if (!allowedApiCalls.contains(apiCall)) {
				restResponse = RestUtility.getResponse('Error',  'Invalid Api Call or parameter missing', 404, NULL, NULL).status;
			}
			else
				if (apiCall.equalsIgnoreCase(API_UPSERT_SIM)) {
					restResponse = MobileRetailAPIHelper.retailPostSIMHelper(req.requestBody.toString()).status;
				}
				else {
					restResponse = HTTP_ERROR;
				}
		if (restResponse.equalsIgnoreCase(STATUS_EXCEPTION) || restResponse.equalsIgnoreCase(HTTP_ERROR)) {
			Database.rollback(sp);
		}
		if (!MobileRestUtility.logs.isEmpty()) {
			insert MobileRestUtility.logs;
		}
	}
}