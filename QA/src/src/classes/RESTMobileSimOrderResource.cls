/*
  @author  : Girish P(Girish.pauriyal@infosys.com)
  @created : 22/03/2017
  @Description : API Endpoint for creating Mobile Lead
 */
@RestResource(urlMapping = '/MobileLead/V1/*')
global without sharing class RESTMobileSimOrderResource {
	/*
	@input  : void
	@output	: returns Lead id for newly created lead
	@Description : POST API Endpoint for creating Mobile Lead
	*/
	@HttpPost
	global static void createMobileLead() {
		MobileRestUtility.logs = new List<Log__c>();
		Savepoint sp = Database.setSavepoint();
		RestRequest req = RestContext.request;
		RestResponse res = Restcontext.response;
		res.addHeader('Content-Type', 'application/json');
		//call post helper class to process the POST request
		String restResponse ;
		//append the response to request body
		if (NULL == req.requestBody) {
			restResponse = 'JSON Body is required for Post requests.';
			restResponse = RestUtility.getResponse('Error', restResponse, 404, NULL, NULL).status;
		}
		else {
			restResponse = MobileRestCustomerApiHelper.SimOrderPostHelper(req.requestBody.toString()).status;
		}
		if (restResponse.equalsIgnoreCase(MobileRestUtility.STATUS_EXCEPTION)
		        || restResponse.equalsIgnoreCase(MobileRestUtility.HTTP_ERROR)) {
			Database.rollback(sp);
		}
		if (!MobileRestUtility.logs.isEmpty()) {
			insert MobileRestUtility.logs;
		}
	}
}