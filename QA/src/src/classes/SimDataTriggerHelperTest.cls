/************************************************************************************************************
    Apex Class Name  : SimDataTriggerHelperTest.cls
    Version       : 1.0
    Created Date    : 06 sep 2017
    Function       : Test class for SimDataTriggerHelper and Simdata Trigger .
    Modification Log  : Covers SimDataTriggerHelper and Simdata Trigger
    Developer        Date        Description
    -----------------------------------------------------------------------------------------------------------
    Anuja Pandey       06 sep 2017    Created Test Class for SimDataTriggerHelper and Simdata Trigger
************************************************************************************************************/


@istest
public class SimDataTriggerHelperTest {

//Testing Sim data validation rules for Reverse Activation
    public static TestMethod void ReverseActivation_testone() {
        list<GD_Card_Value__c> testGCCard = new list<GD_Card_Value__c>();
        GD_Card_Value__c testGCCard1 = new GD_Card_Value__c();
        testGCCard1.Card_Value__c = '000000004000';
        testGCCard1.Name = 'MOB-SIM-RETAIL-001-40';
        testGCCard.add(testGCCard1);
        
        GD_Card_Value__c testGCCard2 = new GD_Card_Value__c();
        testGCCard2.Card_Value__c = '000000002500';
        testGCCard2.Name = 'MOB-SIM-RETAIL-001-25';
        testGCCard.add(testGCCard2);
        insert testGCCard;
        
         list<Mobile_Retail_Error_Mapping__c> errmap = new list<Mobile_Retail_Error_Mapping__c>();
        Mobile_Retail_Error_Mapping__c testerr = new Mobile_Retail_Error_Mapping__c();
        testerr.Error_Code__c = '04';
        testerr.Name = '04';
        testerr.Error_Message__c = 'Void error';
        errmap.add(testerr);
      
        insert errmap;
        
        Sim_Data__c sim = new Sim_Data__c ();
        sim.Product_Code_SKU__c = 'MOB-SIM-RETAIL-001-25';
        sim.Sim_Status__c='Purchased';
        sim.Card_Number__c='110000000034';
        insert sim;
        
        sim.Transaction_Amount__c='000000002500';
        sim.Transaction_Type__c='Reverse Activation';
        sim.Transaction_Currency_Code__c='036';
        update sim;
    }
    public static TestMethod void ReverseActivation_testtwo() 
    {
        list<GD_Card_Value__c> testGCCard = new list<GD_Card_Value__c>();
        GD_Card_Value__c testGCCard2 = new GD_Card_Value__c();
        testGCCard2.Card_Value__c = '000000002500';
        testGCCard2.Name = 'MOB-SIM-RETAIL-001-25';
        testGCCard.add(testGCCard2);
        insert testGCCard;
         list<Mobile_Retail_Error_Mapping__c> errmap = new list<Mobile_Retail_Error_Mapping__c>();
        Mobile_Retail_Error_Mapping__c testerr = new Mobile_Retail_Error_Mapping__c();
        testerr.Error_Code__c = '04';
        testerr.Name = '04';
        testerr.Error_Message__c = 'Void error';
        errmap.add(testerr);
      
        insert errmap;
        
        Sim_Data__c sim = new Sim_Data__c ();
        sim.Product_Code_SKU__c = 'MOB-SIM-RETAIL-001-25';
        sim.Sim_Status__c='Purchased';
        sim.Card_Number__c='110000000035';
        insert sim;
        sim.Transaction_Amount__c='000000002500';
        sim.Transaction_Type__c='Reverse Activation';
        sim.Transaction_Currency_Code__c='036';
        update sim;
    }
    public static TestMethod void ReverseActivation_testthree() 
    {
        list<GD_Card_Value__c> testGCCard = new list<GD_Card_Value__c>();
        GD_Card_Value__c testGCCard2 = new GD_Card_Value__c();
        testGCCard2.Card_Value__c = '000000002500';
        testGCCard2.Name = 'MOB-SIM-RETAIL-001-25';
        testGCCard.add(testGCCard2);
        insert testGCCard;
         list<Mobile_Retail_Error_Mapping__c> errmap = new list<Mobile_Retail_Error_Mapping__c>();
        Mobile_Retail_Error_Mapping__c testerr = new Mobile_Retail_Error_Mapping__c();
        testerr.Error_Code__c = '04';
        testerr.Name = '04';
        testerr.Error_Message__c = 'Void error';
        errmap.add(testerr);
      
        insert errmap;
        
        Sim_Data__c sim = new Sim_Data__c ();
        sim.Product_Code_SKU__c = 'MOB-SIM-RETAIL-001-25';
        sim.Sim_Status__c='Purchased';
        sim.Card_Number__c='110000000035';
        insert sim;
        sim.Transaction_Amount__c='000000002500';
        sim.Sim_Status__c='Available for Purchase';
        sim.Transaction_Type__c='Reverse Activation';
        sim.Transaction_Currency_Code__c='036';
        update sim;
    }
    //Testing Sim data validation rules for Activation
     public static TestMethod void Activation_testone() 
    {
        list<GD_Card_Value__c> testGCCard = new list<GD_Card_Value__c>();
        GD_Card_Value__c testGCCard2 = new GD_Card_Value__c();
        testGCCard2.Card_Value__c = '000000002500';
        testGCCard2.Name = 'MOB-SIM-RETAIL-001-25';
        testGCCard.add(testGCCard2);
        insert testGCCard;
         list<Mobile_Retail_Error_Mapping__c> errmap = new list<Mobile_Retail_Error_Mapping__c>();
        Mobile_Retail_Error_Mapping__c testerr = new Mobile_Retail_Error_Mapping__c();
        testerr.Error_Code__c = '04';
        testerr.Name = '04';
        testerr.Error_Message__c = 'Void error';
        errmap.add(testerr);
      
        insert errmap;
        
        Sim_Data__c sim = new Sim_Data__c ();
        sim.Product_Code_SKU__c = 'MOB-SIM-RETAIL-001-25';
        sim.Sim_Status__c='Available for Purchase';
        sim.Card_Number__c='110000000035';
        insert sim;
        sim.Transaction_Amount__c='000000002500';
        sim.Transaction_Type__c='Activation';
        sim.Transaction_Currency_Code__c='036';
        update sim;
    }
    //Testing Sim data validation rules for Void Activation
     public static TestMethod void VoidActivation_testone() 
    {
        list<GD_Card_Value__c> testGCCard = new list<GD_Card_Value__c>();
        GD_Card_Value__c testGCCard2 = new GD_Card_Value__c();
        testGCCard2.Card_Value__c = '000000002500';
        testGCCard2.Name = 'MOB-SIM-RETAIL-001-25';
        testGCCard.add(testGCCard2);
        insert testGCCard;
         list<Mobile_Retail_Error_Mapping__c> errmap = new list<Mobile_Retail_Error_Mapping__c>();
        Mobile_Retail_Error_Mapping__c testerr = new Mobile_Retail_Error_Mapping__c();
        testerr.Error_Code__c = '04';
        testerr.Name = '04';
        testerr.Error_Message__c = 'Void error';
        errmap.add(testerr);
      
        insert errmap;
        
        Sim_Data__c sim = new Sim_Data__c ();
        sim.Product_Code_SKU__c = 'MOB-SIM-RETAIL-001-25';
        sim.Sim_Status__c='Purchased';
        sim.Card_Number__c='110000000035';
        insert sim;
        sim.Transaction_Amount__c='000000002500';
        sim.Transaction_Type__c='Void Activation';
        sim.Transaction_Currency_Code__c='036';
        update sim;
    }
    //Testing Sim data validation rules for Reverse void Activation
    public static TestMethod void Reversevoidactivation_testone() 
    {
        list<GD_Card_Value__c> testGCCard = new list<GD_Card_Value__c>();
        GD_Card_Value__c testGCCard2 = new GD_Card_Value__c();
        testGCCard2.Card_Value__c = '000000002500';
        testGCCard2.Name = 'MOB-SIM-RETAIL-001-25';
        testGCCard.add(testGCCard2);
        insert testGCCard;
         list<Mobile_Retail_Error_Mapping__c> errmap = new list<Mobile_Retail_Error_Mapping__c>();
        Mobile_Retail_Error_Mapping__c testerr = new Mobile_Retail_Error_Mapping__c();
        testerr.Error_Code__c = '04';
        testerr.Name = '04';
        testerr.Error_Message__c = 'Void error';
        errmap.add(testerr);
      
        insert errmap;
        
        Sim_Data__c sim = new Sim_Data__c ();
        sim.Product_Code_SKU__c = 'MOB-SIM-RETAIL-001-25';
        sim.Sim_Status__c='Void';
        sim.Card_Number__c='110000000035';
        insert sim;
        sim.Transaction_Amount__c='000000002500';
        sim.Transaction_Type__c='Reverse void activation';
        sim.Transaction_Currency_Code__c='036';
        update sim;
    }
    //Testing Sim data validation rules for SAF Activation
    public static TestMethod void SAFactivation_testone() 
    {
        list<GD_Card_Value__c> testGCCard = new list<GD_Card_Value__c>();
        GD_Card_Value__c testGCCard2 = new GD_Card_Value__c();
        testGCCard2.Card_Value__c = '000000002500';
        testGCCard2.Name = 'MOB-SIM-RETAIL-001-25';
        testGCCard.add(testGCCard2);
        insert testGCCard;
         list<Mobile_Retail_Error_Mapping__c> errmap = new list<Mobile_Retail_Error_Mapping__c>();
        Mobile_Retail_Error_Mapping__c testerr = new Mobile_Retail_Error_Mapping__c();
        testerr.Error_Code__c = '04';
        testerr.Name = '04';
        testerr.Error_Message__c = 'Void error';
        errmap.add(testerr);
      
        insert errmap;
        Sim_Data__c sim = new Sim_Data__c ();
        sim.Product_Code_SKU__c = 'MOB-SIM-RETAIL-001-25';
        sim.Sim_Status__c='Available for Purchase';
        sim.Card_Number__c='110000000035';
        insert sim;
        sim.Transaction_Amount__c='000000002500';
        sim.Transaction_Type__c='SAF activation';
        sim.Transaction_Currency_Code__c='036';
        update sim;
    }
    
}