/************************************************************************************************************
* Apex Class Name   : CaseUtils.cls
* Version           : 1.0 
* Created Date      : 19 OCT 2015
* Function          : Utility class for Case Object
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Tom Clayton                  19/10/2014              Created CaseUtils Class
************************************************************************************************************/

public with sharing class CaseUtils
{
    /*// METHOD: setExpectedDates
    // PURPOSE: checks conditions to determine how many days ahead the "expected" date fields should be set in a new case
    public static void setExpectedDates(list<Case> lstNewCases)
    {
        RecordType complaintRecordType = [SELECT Id FROM RecordType
                                            WHERE SobjectType = 'Case'
                                            AND DeveloperName = 'Complaints'
                                            AND IsActive = true];
        
        for(Case currentCase : lstNewCases)
        {
            if(currentCase.RecordTypeId == complaintRecordType.Id)
            {
                // Set the exp res agr date depending on category
                if(currentCase.Complaint_Category__c == 'General')
                {
                    // Adding 5 days for General
                    // The date conversion in this will drop the time details
                    currentCase.Expected_Resolution_Agreed_Date__c = addBusinessDays(system.now().date(), 5);
                }
                else if(currentCase.Complaint_Category__c == 'TIO' || currentCase.Complaint_Category__c == 'High Risk')
                {
                    // Adding 3 days for TIO or High Risk
                    currentCase.Expected_Resolution_Agreed_Date__c = addBusinessDays(system.now().date(), 3);
                }
                
                // Set the exp res exec date only if "Resolution Agreed" has been ticked
                if((currentCase.Complaint_Category__c == 'General' || currentCase.Complaint_Category__c == 'High Risk' || currentCase.Complaint_Category__c == 'TIO')
                    && currentCase.Resolution_Agreed__c == true)
                {
                    // Adding 10 days
                    currentCase.Expected_Resolution_Execution_Date__c = addBusinessDays(system.now().date(), 10);
                }
            }
        }
    }
    
    // METHOD: updateExpectedDates
    // PURPOSE: updates the "expected" date fields if the Complaint Category or Resolution Agreed has been changed
    public static void updateExpectedDates(list<Case> lstUpdatingCases, map<ID, Case> mapOldCases)
    {
        RecordType complaintRecordType = [SELECT Id FROM RecordType
                                            WHERE SobjectType = 'Case'
                                            AND DeveloperName = 'Complaints'
                                            AND IsActive = true];
        
        for(Case currentCase : lstUpdatingCases)
        {
            if(currentCase.RecordTypeId == complaintRecordType.Id)
            {
                // If the Complaint Category has changed
                if(currentCase.Complaint_Category__c != mapOldCases.get(currentCase.Id).Complaint_Category__c)
                {
                    // Set the exp res agr date depending on category
                    if(currentCase.Complaint_Category__c == 'General')
                    {
                        // Adding 5 days for General
                        // The date conversion in this will drop the time details
                        currentCase.Expected_Resolution_Agreed_Date__c = addBusinessDays(currentCase.CreatedDate.date(), 5);
                    }
                    else if(currentCase.Complaint_Category__c == 'TIO' || currentCase.Complaint_Category__c == 'High Risk')
                    {
                        // Adding 3 days for TIO or High Risk
                        currentCase.Expected_Resolution_Agreed_Date__c = addBusinessDays(currentCase.CreatedDate.date(), 3);
                    }
                }
                
                // If the Resolution Agreed has been changed
                if(currentCase.Resolution_Agreed__c != mapOldCases.get(currentCase.Id).Resolution_Agreed__c)
                {
                    // Set the exp res exec date only if IT WASNT SET ALREADY (current value is null) and if "Resolution Agreed" has been ticked
                    if((currentCase.Complaint_Category__c == 'General' || currentCase.Complaint_Category__c == 'High Risk' || currentCase.Complaint_Category__c == 'TIO')
                        && currentCase.Resolution_Agreed__c == true && currentCase.Expected_Resolution_Execution_Date__c == null)
                    {
                        // Adding 10 days
                        currentCase.Expected_Resolution_Execution_Date__c = addBusinessDays(system.today(), 10);
                    }
                }
            }
        }
    }
    
    // METHOD: addBusinessDays
    // PURPOSE: increments a date by numDays, while not counting weekend days
    public static Date addBusinessDays(Date theDate, integer numDays)
    {
        Set <String> weekendDays = new Set <String> {'Saturday', 'Sunday'};
        DateTime addedToDate = DateTime.newInstance (theDate, Time.newInstance(0, 0, 0, 0));
        
        // Loop numDays times and add days to the date. Skip over weekend days.
        for(integer i=0; i < numDays; i++)
        {
            addedToDate = addedToDate.addDays(1);
            while (weekendDays.contains(addedToDate.format('EEEEE')))
            {
                addedToDate = addedToDate.addDays(1);
            }
        }

        return addedToDate.date();
    }
    
        public static void relateParentCase(List<Case> listCaseNew){
    
        Set<Id> setContactIds = new Set<Id>();
        Set<Id> setAccountIds = new Set<Id>();
        List<Case> listCases = new List<Case>();
        Map<Id, List<Case>> mapCases = new Map<Id,List<Case>>();
        
        for(Case c: listCaseNew){
            setContactIds.add(c.ContactId);
        }
        
        system.debug('****** contact  '+setContactIds);
        
        listCases = [Select Id, Subject, AccountId, ContactId, ParentId from Case where ContactId in : setContactIds order by CreatedDate asc];
        system.debug('****** list Cases  '+listCases);
        for(Case c: listCases){
            List<Case> listCaseTemp = new List<Case>();
            if(mapCases.containsKey(c.ContactId)){
                listCaseTemp = mapCases.get(c.ContactId);
            }
            listCaseTemp.add(c);
            mapCases.put(c.ContactId, listCaseTemp);
        }
        
        for(Case c: listCaseNew){
            List<Case> listCaseTemp = new List<Case>();
            if(mapCases != NULL && mapCases.containsKey(c.ContactId) && mapCases.get(c.ContactId) != NULL){
                listCaseTemp = mapCases.get(c.ContactId);
            }
            if(listCaseTemp != NULL && listCaseTemp.size() > 0){
                system.debug('****** list Cases Temp  '+listCaseTemp);
                for(Case cs: listCaseTemp){
                    if(cs.Subject != NULL && c.Subject != NULL && c.Subject.contains(cs.Subject)){
                        if(cs.ParentId != NULL){
                            c.ParentId = cs.ParentId;
                        } else{
                            c.ParentId = cs.Id;
                        }
                        break;
                    }
                }
            }
        }
        
    }

    
    public static void closeChildCasewithParent(List<Case> listCaseNew){
        
        
        Set<Id> setCaseIds = new Set<Id>();
        List<Case> listCases = new List<Case>();
        Map<Id, List<Case>> mapCases = new Map<Id,List<Case>>();
        List<Case> listCaseClosed = new List<Case>();
        
        for(Case c: listCaseNew){
            if(c.Status == 'Closed'){
                setCaseIds.add(c.Id);
            }
        }
        
        listCases = [Select Id, Subject, AccountId, Reason, ContactId, ParentId, RecordtypeId, Area__c from Case where ParentId in : setCaseIds];
        
        for(Case c: listCases){
            List<Case> listCaseTemp = new List<Case>();
            if(mapCases.containsKey(c.ParentId)){
                listCaseTemp = mapCases.get(c.ParentId);
            }
            listCaseTemp.add(c);
            mapCases.put(c.ParentId, listCaseTemp);
        }
        
        for(Case c: listCaseNew){
            List<Case> listCaseTemp = new List<Case>();
            if(c.Status == 'Closed' && mapCases != NULL && mapCases.containsKey(c.Id) && mapCases.get(c.Id) != NULL){
                listCaseTemp = mapCases.get(c.Id);
            }
            if(listCaseTemp != NULL && listCaseTemp.size() > 0){
                for(Case childCase: listCaseTemp){
                    childCase.Status = 'Closed';
                    childCase.Reason = c.Reason;
                    
                    listCaseClosed.add(childCase);
                }
            }
        }
        
        if(listCaseClosed != Null && listCaseClosed.size() > 0){
            system.debug(listCaseClosed);
            update listCaseClosed;
        }
        
    }*/
}