/************************************************************************************************************
    Apex Class Name	: MobileRestHelperTest.cls
    Version 			: 1.0
    Created Date  	: 27 July 2017
    Function 			: Test class for MobileRestHelper.
    Modification Log	:
    Developer				Date				Description
    -----------------------------------------------------------------------------------------------------------
    Girish P			27/07/2017 			Created Class.
    Tom Clayton			28/07/2017 			Added further test methods
************************************************************************************************************/

@isTest
private class MobileRestHelperTest {
	@testSetup
	static void setupTestData() {
		UserRole ur = [SELECT Id FROM UserRole Where name = 'Belong MOBILE'];
		// Setup test data
		// This code runs as the system user
		Profile p  = [Select id from Profile where name = 'Belong Integration API'];
		User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
		                  EmailEncodingKey = 'UTF-8',
		                  LastName = 'Testing',
		                  LanguageLocaleKey = 'en_US',
		                  LocaleSidKey = 'en_US',
		                  ProfileId = p.Id,
		                  UserRoleId = ur.id,
		                  TimeZoneSidKey = 'America/Los_Angeles',
		                  UserName = 'belongapiuser@testorg.com');
		System.runAs(u) {
			// The following code runs as user 'u'
			TestDataFactoryMobile.createAccountsWithContacts(201, 1);
			List<Asset> listAsset = TestDataFactoryMobile.createAssets(201);
			insert listAsset;
		}
	}
	static testMethod void createNotificationTest() {
		// Create test request
		String jsonInput = '{'
		                   + '  "type": "Outstanding payment",'
		                   + '  "subType": "Day 5",'
		                   + '  "status": "In Progress",'
		                   + '  "subStatus": "Payment failed notification sent",'
		                   + '  "octaneCustomerID": "100001",'
		                   + '  "orderId": "100001",'
		                   + '  "rejectCode": "2001",'
		                   + '  "rejectReason": "Insufficient funds",'
		                   + '  "accountBalance": "25",'
		                   + '  "duedate": "2017-01-01",'
		                   + '  "submittedDate": "2017-01-01",'
		                   + '  "lastUpdatedDate": "2017-01-01",'
		                   + '  "userID": "payment-job",'
		                   + '  "source": "System",'
		                   + '  "taskID": "System"'
		                   + '}';
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		req.requestURI = '/services/apexrest/RESTMobileAPI';
		req.params.put('APICall', 'createNotification');
		req.params.put('type', 'payment');
		// Fake the passed call
		req.requestBody = Blob.valueOf(jsonInput);
		req.httpMethod = 'POST';
		RestContext.request = req;
		RestContext.response = res;
		Test.startTest();
		//Test Create notification API
		RESTMobileAPI.postMobile();
		List<Contact> listContact = GlobalUtil.findExistingCustomer('100001', null, null);
		//Test Dismiss API
		Task t = [Select id, Type
		          from Task
		          where whoid = :listContact[0].Id];
		jsonInput = '{'
		            + '  "taskID": "' + t.id + '"'
		            + '}';
		RESTMobileAPI.patchMobile();
		Test.stopTest();
	}


	static testMethod void getPaymentNotificationsHelperTest() {
		// Create test request
		//create test task for this
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		String jsonInput = '{'
		                   + '  "type": "Outstanding payment",'
		                   + '  "subType": "Day 5",'
		                   + '  "status": "In Progress",'
		                   + '  "subStatus": "Payment failed notification sent",'
		                   + '  "octaneCustomerID": "100001",'
		                   + '  "orderId": "100001",'
		                   + '  "rejectCode": "2001",'
		                   + '  "rejectReason": "Insufficient funds",'
		                   + '  "accountBalance": "25",'
		                   + '  "duedate": "2017-01-01",'
		                   + '  "submittedDate": "2017-01-01",'
		                   + '  "lastUpdatedDate": "2017-01-01",'
		                   + '  "userID": "payment-job",'
		                   + '  "source": "System"'
		                   + '}';
		req.requestURI = '/services/apexrest/RESTMobileAPI?APICall=createNotification';
		req.params.put('APICall', 'createNotification');
		// Fake the passed call
		req.requestBody = Blob.valueOf(jsonInput);
		req.httpMethod = 'POST';
		RestContext.request = req;
		RestContext.response = res;
		//Task should have been created here
		RESTMobileAPI.postMobile();
		Test.startTest();
		//Actual GET TEST
		req.params.put('utilibillId', '100001');
		req.httpMethod = 'GET';
		req.requestURI = '/services/apexrest/RESTMobileAPI?APICall=getNotification';
		req.params.put('APICall', 'getNotification');
		req.httpMethod = 'GET';
		RestContext.request = req;
		RestContext.response = res;
		//Try to get the payment notification tasks
		RESTMobileAPI.getMobile();
		List<Contact> listContact = GlobalUtil.findExistingCustomer('100001', null, null);
		Task t = [Select id, Type from Task where whoid = :listContact[0].Id];
		System.assertEquals('Notification', t.Type);
		Test.stopTest();
	}

	static testMethod void ivrHelperTest() {
		// Create test request
		//create test task for this
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		//IVR Tasks against a customer
		Contact ivrContact = [Select id from contact limit 1];
		String jsonInput = '{'
		                   + '"taskType":"BLOCK_SIM_HELP",'
		                   + '"salesforceId":"' + ivrContact.Id + '",'
		                   + '"mobileNo":"0404571176"'
		                   + '}';
		req.requestURI = '/services/apexrest/RESTMobileAPI';
		req.params.put('APICall', 'ivr');
		Test.startTest();
		//Actual GET TEST
		req.requestBody = Blob.valueOf(jsonInput);
		req.httpMethod = 'POST';
		RestContext.request = req;
		RestContext.response = res;
		//Task should have been created here
		RESTMobileAPI.postMobile();
		//IVR Tasks against a lead
		Lead lead = new Lead(Lastname = 'Test Lead',
		                     Status = 'Received',
		                     Company = 'Test C');
		insert lead;
		System.debug('***lead****' + lead);
		req.params.put('APICall', 'ivr');
		jsonInput = '{'
		            + '"taskType":"BLOCK_SIM_HELP",'
		            + '"salesforceId":"' + lead.Id + '",'
		            + '"mobileNo":"0404571176"'
		            + '}';
		req.requestURI = '/services/apexrest/RESTMobileAPI';
		req.requestBody = Blob.valueOf(jsonInput);
		//Try to get the payment notification tasks
		RESTMobileAPI.postMobile();
		Test.stopTest();
	}

	static testMethod void errorsPaymentAndIvrTaskTest() {
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		//invalid customer for paymentNotification
		String jsonInput = '{'
		                   + '  "type": "Outstanding payment",'
		                   + '  "subType": "Day 5",'
		                   + '  "status": "In Progress",'
		                   + '  "subStatus": "Payment failed notification sent",'
		                   + '  "octaneCustomerID": "1ww00001",'
		                   + '  "orderId": "1ww00001",'
		                   + '  "rejectCode": "2001",'
		                   + '  "rejectReason": "Insufficient funds",'
		                   + '  "accountBalance": "25",'
		                   + '  "duedate": "2017-01-01",'
		                   + '  "submittedDate": "2017-01-01",'
		                   + '  "lastUpdatedDate": "2017-01-01",'
		                   + '  "userID": "payment-job",'
		                   + '  "source": "System"'
		                   + '}';
		req.requestURI = '/services/apexrest/RESTMobileAPI';
		req.params.put('APICall', 'createNotification');
		req.params.put('type', 'payment');
		// Fake the passed call
		req.requestBody = Blob.valueOf(jsonInput);
		req.httpMethod = 'POST';
		RestContext.request = req;
		RestContext.response = res;
		Test.startTest();
		RESTMobileAPI.postMobile();
		//Null octane ID customer for paymentNotification
		jsonInput = '{'
		            + '  "type": "Outstanding payment",'
		            + '  "subType": "Day 5",'
		            + '  "status": "In Progress",'
		            + '  "subStatus": "Payment failed notification sent",'
		            + '  "octaneCustomerID": null,'
		            + '  "orderId": "1ww00001",'
		            + '  "rejectCode": "2001",'
		            + '  "rejectReason": "Insufficient funds",'
		            + '  "accountBalance": "25",'
		            + '  "duedate": "2017-01-01",'
		            + '  "submittedDate": "2017-01-01",'
		            + '  "lastUpdatedDate": "2017-01-01",'
		            + '  "userID": "payment-job",'
		            + '  "source": "System"'
		            + '}';
		req.requestBody = Blob.valueOf(jsonInput);
		req.httpMethod = 'POST';
		RESTMobileAPI.postMobile();
		//Exception JSON
		jsonInput = '{'
		            + '  "octaneCustomerID": "100001",'
		            + '  "orderId": "1ww00001",'
		            + '  "rejectCode": "2001",'
		            + '  "source": "System",'
		            + '}';
		req.requestBody = Blob.valueOf(jsonInput);
		RESTMobileAPI.postMobile();
		/*IVR Testing fail Scenarios*/
		//invalid customer for IVR tasks
		jsonInput = '{'
		            + '"taskType":"BLOCK_SIM_HELP",'
		            + '"salesforceId":"003O000000zAFAq",'
		            + '"mobileNo":"0404571176"'
		            + '}';
		req.requestURI = '/services/apexrest/RESTMobileAPI';
		req.params.put('APICall', 'ivr');
		//Actual GET TEST
		req.requestBody = Blob.valueOf(jsonInput);
		//Task should have been created here
		RESTMobileAPI.postMobile();
		//Null Salesforce ID attribute
		jsonInput = '{'
		            + '"taskType":"BLOCK_SIM_HELP",'
		            + '"salesforceId":null,'
		            + '"mobileNo":"0404571176"'
		            + '}';
		req.requestURI = '/services/apexrest/RESTMobileAPI';
		//Actual GET TEST
		req.requestBody = Blob.valueOf(jsonInput);
		//Task should have been created here
		RESTMobileAPI.postMobile();
		/**************8Get call for Payment API******************/
		//missing utilibillId
		req.httpMethod = 'GET';
		req.requestURI = '/services/apexrest/RESTMobileAPI';
		req.params.put('APICall', 'getPaymentNotification');
		req.httpMethod = 'GET';
		RestContext.request = req;
		RestContext.response = res;
		//Try to get the payment notification tasks
		RESTMobileAPI.getMobile();
		//No valid customer with utilibillId
		req.httpMethod = 'GET';
		req.requestURI = '/services/apexrest/RESTMobileAPI';
		req.params.put('APICall', 'getPaymentNotification');
		req.params.put('utilibillId', '103001');
		req.httpMethod = 'GET';
		RestContext.request = req;
		RestContext.response = res;
		//Try to get the payment notification tasks
		RESTMobileAPI.getMobile();
		//Exception JSON
		jsonInput = '{'
		            + '  "octaneCustomerID": "100001",'
		            + '  "orderId": "1ww00001",'
		            + '  "rejectCode": "2001",'
		            + '  "source": "System",'
		            + '}';
		RESTMobileAPI.getMobile();
		Test.stopTest();
	}

	static testMethod void getPublicKnowledgeTopicsNoDataTest() {
		// Create test request
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		req.requestURI = '/services/apexrest/RESTMobileAPI?APICall=getPublicKnowledgeTopics';
		req.params.put('APICall', 'getPublicKnowledgeTopics');
		// Fake the passed call
		req.requestBody = Blob.valueOf('Test Body');
		req.httpMethod = 'GET';
		RestContext.request = req;
		RestContext.response = res;
		Test.startTest();
		RESTMobileAPI.getMobile();
		Test.stopTest();
		String testResponseBody = res.responseBody.toString();
		system.debug('--The getPublicKnowledgeTopicsNoDataTest responseBody was: ' + testResponseBody);
		system.assert(testResponseBody.contains('No Public Knowledge Topics found'), 'The getPublicKnowledgeTopicsNoDataTest responseBody did not return 0 matches as expected.');
	}

	static testMethod void getPublicKnowledgeTopicsTest() {
		Public_Knowledge_Topic__c publicKnowledgeTopic = TestDataFactoryMobile.createPublicKnowledgeTopics(1)[0];
		insert publicKnowledgeTopic;
		// Create test request
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		req.requestURI = '/services/apexrest/RESTMobileAPI?APICall=getPublicKnowledgeTopics';
		req.params.put('APICall', 'getPublicKnowledgeTopics');
		// Fake the passed call
		req.requestBody = Blob.valueOf('Test Body');
		req.httpMethod = 'GET';
		RestContext.request = req;
		RestContext.response = res;
		Test.startTest();
		RESTMobileAPI.getMobile();
		Test.stopTest();
		string testResponseBody = res.responseBody.toString();
		system.debug('--The getPublicKnowledgeTopicsTest responseBody was: ' + testResponseBody);
		system.assert(testResponseBody.contains('Thing 9000'), 'The getPublicKnowledgeTopicsTest responseBody did not contain the Title of the test data\'s Public Knowledge Topic.');
	}

	static testMethod void sendVerificationCodeTest() {
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		string jsonInput = '{ "mobileNo": "0436777111", '
		                   + '"email": "testeremail125@testmail.com.au",'
		                   + '"firstName": "Phrank",'
		                   + '"lastName": "Jonery"}';
		req.requestURI = '/services/apexrest/RESTMobileAPI?APICall=sendVerificationCode';
		req.params.put('APICall', 'sendVerificationCode');
		// Fake the passed call
		req.requestBody = Blob.valueOf(jsonInput);
		req.httpMethod = 'POST';
		RestContext.request = req;
		RestContext.response = res;
		Test.startTest();
		RESTMobileAPI.postMobile();
		Test.stopTest();
		String testResponseBody = res.responseBody.toString();
		system.debug('--The sendVerificationCodeTest responseBody was: ' + testResponseBody);
		system.assert(testResponseBody.contains('200'), 'The sendVerificationCodeTest responseBody did not include the success message.');
	}

	static testMethod void sendVerificationCodeInvalidJSONTest() {
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		string jsonInput = '{ "mobileNo": "0436777111", '
		                   + '"lastName": "onlyThisField"}';
		req.requestURI = '/services/apexrest/RESTMobileAPI?APICall=sendVerificationCode';
		req.params.put('APICall', 'sendVerificationCode');
		// Fake the passed call
		req.requestBody = Blob.valueOf(jsonInput);
		req.httpMethod = 'POST';
		RestContext.request = req;
		RestContext.response = res;
		Test.startTest();
		RESTMobileAPI.postMobile();
		Test.stopTest();
		String testResponseBody = res.responseBody.toString();
		system.debug('--The sendVerificationCodeInvalidJSONTest responseBody was: ' + testResponseBody);
		system.assert(testResponseBody.contains('Invalid JSON provided'), 'The sendVerificationCodeInvalidJSONTest responseBody did not include the expected error message.');
	}

	static testMethod void checkVerificationCodeTest() {
		String RECORD_TYPE_MOBILE_SMS = 'Mobile SMS';
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		Lead newLead = new Lead();
		newLead.Phone = '0436777111';
		newLead.Email = 'testeremail1256@testmail.com.au';
		newLead.FirstName = 'Phrank';
		newLead.LastName = 'Jonery';
		newLead.Company = 'Phrank Jonery';
		newLead.Product_Type__c = 'Mobile';
		insert newLead;
		DateTime dtNowPlusThirtyMin = system.now();
		dtNowPlusThirtyMin = dtNowPlusThirtyMin.addMinutes(30);
		Task newTask = new Task(Subject = 'Verification SMS',
		                        Status = 'New',
		                        Billing__c = False,
		                        Type = 'Other',
		                        Task_Type__c = 'GENERAL_HELP',
		                        Task_Sub_Type__c = null,
		                        Priority = 'Normal',
		                        Code_Valid_To__c = dtNowPlusThirtyMin,
		                        WhoId = newLead.Id,
		                        SMS_Recipient__c = newLead.Phone,
		                        SMS_Send_Status__c = 'Send',
		                        Reject_Code__c = null,
		                        Reject_Reason__c = null,
		                        Account_Balance__c = null,
		                        Description = '123456',
		                        RecordTypeId = (Id)GlobalUtil.getRecordTypeByName('Task').get(RECORD_TYPE_MOBILE_SMS).getRecordTypeId()
		                       );
		insert newTask;
		string jsonInput = '{ "mobileNo": "0436777111", '
		                   + '"email": "testeremail1256@testmail.com.au",'
		                   + '"code": "123456"}';
		req.requestURI = '/services/apexrest/RESTMobileAPI?APICall=checkVerificationCode';
		req.params.put('APICall', 'checkVerificationCode');
		// Fake the passed call
		req.requestBody = Blob.valueOf(jsonInput);
		req.httpMethod = 'POST';
		RestContext.request = req;
		RestContext.response = res;
		Test.startTest();
		RESTMobileAPI.postMobile();
		Test.stopTest();
		String testResponseBody = res.responseBody.toString();
		system.debug('--The checkVerificationCodeTest responseBody was: ' + testResponseBody);
		system.assert(testResponseBody.contains('Successfully verified'), 'The checkVerificationCodeTest responseBody did not include the success message.');
	}

	static testMethod void invalidPOSTAPICallTest() {
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		string jsonInput = '{ "asdad": "134143", '
		                   + '"sdfxwe": "we13fsdfsfd"}';
		req.requestURI = '/services/apexrest/RESTMobileAPI?APICall=invalidAPIName';
		req.params.put('APICall', 'invalidAPIName');
		// Fake the passed call
		req.requestBody = Blob.valueOf(jsonInput);
		req.httpMethod = 'POST';
		RestContext.request = req;
		RestContext.response = res;
		Test.startTest();
		RESTMobileAPI.postMobile();
		Test.stopTest();
		String testResponseBody = res.responseBody.toString();
		system.debug('--The invalidPOSTAPICallTest responseBody was: ' + testResponseBody);
		system.assert(testResponseBody.contains('Invalid Api Call'), 'The invalidPOSTAPICallTest responseBody did not include the expected error message.');
	}

	static testMethod void invalidGETAPICallTest() {
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		string jsonInput = '{ "asdad": "134143", '
		                   + '"sdfxwe": "we13fsdfsfd"}';
		req.requestURI = '/services/apexrest/RESTMobileAPI?invalid=invalidAPIName';
		req.params.put('invalid', 'invalidAPIName');
		// Fake the passed call
		req.requestBody = Blob.valueOf(jsonInput);
		req.httpMethod = 'GET';
		RestContext.request = req;
		RestContext.response = res;
		Test.startTest();
		RESTMobileAPI.getMobile();
		Test.stopTest();
		String testResponseBody = res.responseBody.toString();
		system.debug('--The invalidGETAPICallTest responseBody was: ' + testResponseBody);
		system.assert(testResponseBody.contains('Invalid Api Call'), 'The invalidGETAPICallTest responseBody did not include the expected error message.');
	}

	static testMethod void invalidPATCHAPICallTest() {
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		string jsonInput = '{ "asdad": "134143", '
		                   + '"sdfxwe": "we13fsdfsfd"}';
		req.requestURI = '/services/apexrest/RESTMobileAPI?APICall=invalidAPIName';
		req.params.put('APICall', 'invalidAPIName');
		// Fake the passed call
		req.requestBody = Blob.valueOf(jsonInput);
		req.httpMethod = 'PATCH';
		RestContext.request = req;
		RestContext.response = res;
		Test.startTest();
		RESTMobileAPI.patchMobile();
		Test.stopTest();
		String testResponseBody = res.responseBody.toString();
		system.debug('--The invalidPATCHAPICallTest responseBody was: ' + testResponseBody);
		system.assert(testResponseBody.contains('Invalid Api Call'), 'The invalidPATCHAPICallTest responseBody did not include the expected error message.');
	}
    
    static testMethod void validateSimTest() {
        Activation_Error_Code__c ec=new Activation_Error_Code__c();
            ec.name='Ready To Activate';
            ec.Response_Code__c='Err-01';
            ec.Error_Message__c='show error message';
            Activation_Error_Code__c ec1=new Activation_Error_Code__c();
            ec1.name='Rejected';
            ec1.Response_Code__c='Err-01';
            ec1.Error_Message__c='show error message';
            Activation_Error_Code__c ec2=new Activation_Error_Code__c();
            ec2.name='Blocked';
            ec2.Response_Code__c='Err-01';
            ec2.Error_Message__c='show error message';
            Activation_Error_Code__c ec3=new Activation_Error_Code__c();
            ec3.name='Redeemed';
            ec3.Response_Code__c='Err-01';
            ec3.Error_Message__c='show error message';
            Activation_Error_Code__c ec4=new Activation_Error_Code__c();
            ec4.name='Available for purchase';
            ec4.Response_Code__c='Err-01';
            ec4.Error_Message__c='show error message';
            Activation_Error_Code__c ec5=new Activation_Error_Code__c();
            ec5.name='Purchased';
            ec5.Response_Code__c='Err-01';
            ec5.Error_Message__c='show error message';
            Activation_Error_Code__c ec6=new Activation_Error_Code__c();
            ec6.name='Void';
            ec6.Response_Code__c='Err-01';
            ec6.Error_Message__c='show error message';
            Activation_Error_Code__c ec7=new Activation_Error_Code__c();
            ec7.name='Other';
            ec7.Response_Code__c='Err-01';
            ec7.Error_Message__c='show error message';
            list<Activation_Error_Code__c> eclist=new list<Activation_Error_Code__c>{ec,ec1,ec2,ec3,ec4,ec5,ec6,ec7};
                insert eclist;
        list<GD_Card_Value__c> testGCCard = new list<GD_Card_Value__c>();
        GD_Card_Value__c testGCCard1 = new GD_Card_Value__c();
        testGCCard1.Card_Value__c = '4000';
        testGCCard1.Name = 'MOB-SIM-RETAIL-001-40';
        testGCCard.add(testGCCard1);
        
        GD_Card_Value__c testGCCard2 = new GD_Card_Value__c();
        testGCCard2.Card_Value__c = '2500';
        testGCCard2.Name = 'MOB-SIM-RETAIL-001-25';
        testGCCard.add(testGCCard2);
        insert testGCCard;
        
        Sim_Data__c sim = new Sim_Data__c ();
        sim.Product_Code_SKU__c = 'MOB-SIM-RETAIL-001-25';
        sim.Card_Number__c='14567890';
        sim.Sim_Status__c='Available for purchase';
        insert sim;
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		req.requestURI = '/services/apexrest/RESTMobileAPI?APICall=validateSim';
		req.params.put('APICall', 'validateSim');
        req.params.put('simno', '14567890');
        req.params.put('email', 'test@test.com');
		// Fake the passed call
		req.requestBody = Blob.valueOf('Test Body');
		req.httpMethod = 'GET';
		RestContext.request = req;
		RestContext.response = res;
		Test.startTest();
		RESTMobileAPI.getMobile();
		Test.stopTest();
		String testResponseBody = res.responseBody.toString();
		system.debug('--The invalidPATCHAPICallTest responseBody was: ' + testResponseBody);
		//system.assert(testResponseBody.contains('Invalid Api Call'), 'The invalidPATCHAPICallTest responseBody did not include the expected error message.');
	}
}