public with sharing class SimdataTriggerHandler implements TriggerInterface
{
    private Boolean runAfterProcess = false;
    public static Boolean executeTrigger = true;
    
    /**
    *   This public method caches related data for before triggers, called once on start of the trigger execution
    */
    public void cacheBefore()
    {
        SimDataTriggerHelper.cacheBefore();    
    }

    /**
    *   This public method caches related data for after triggers, called once on start of the trigger execution
    */
    public void cacheAfter()
    {
       
    } 
    
    /**
    *   This public method process the business logic on 1 sObject at before insert
    */
    public void beforeInsert(sObject newSObj)
    {
        SimDataTriggerHelper.mapResponseCode(newSObj);   
    }
    
    /**
    *   This public method process the business logic on 1 sObject at after insert
    */
    public void afterInsert(sObject newSObj)
    {
        
    }
    
    /**
    *   This public method process the business logic on 1 sObject at before update
    */
    public void beforeUpdate(sObject oldSObj, sObject newSObj)
    {
        SimDataTriggerHelper.mapResponseCode(newSObj);  
        SimDataTriggerHelper.blackhawknull(newSObj,oldSObj);  
    }
    
    /**
    *   This public method process the business logic on 1 sObject at after update
    */
    public void afterUpdate(sObject oldSObj, sObject newSObj)
    {
        
    }
    
    /**
    *   This public method process the business logic on 1 sObject at before delete
    */
    public void beforeDelete(sObject oldSObj)
    {
        
    }
    
    /**
    *   This public method process the business logic on 1 sObject at after delete
    */
    public void afterDelete(sObject oldSObj)
    {
        
    }
    
    /**
    *   This public method process the business logic on 1 sObject at after undelete
    */
    public void afterUndelete(sObject newSObj)
    {}
    
    /**
    *   Ater all rows have been processed this method will be called
    *   Usualy DML are placed in this method
    */
    public void afterProcessing()
    {
        
    }   
}