@isTest
public class ININServicesTest {
    public static Map<Id,Schema.RecordTypeInfo> accRecordTypeInfoId;
    public static Map<String,Schema.RecordTypeInfo> accRecordTypeInfoName;
    public static Map<String,Schema.RecordTypeInfo> TaskRecordTypeInfoName;
   // public static List<Account> lAccount;
    public static Account sAccount;
    public static Account sAccount1;
    public static Contact sContact1 ;
    public static ININWebservice__c Pagenumber;

    
    // Test data setup 
    @testSetup public static void setupData(){
        AuthProperties__c ap1 = new AuthProperties__c(Name='ININ_OAUTH',Client_Id__c='testclient',Client_Secret__c='-kBTk_2Iz1YA715jGE2pPouLTJUgXLVpthPQKZYVFng',Password__c='1234',Username__c='N/A',Endpoint__c='https://login.mypurecloud.com.au');
        insert ap1;
        AuthProperties__c ap2 = new AuthProperties__c(Name='ININ_API',Client_Id__c='',Client_Secret__c='',Password__c='',Username__c='',Endpoint__c='https://api.mypurecloud.com.au');
        insert ap2;
        accRecordTypeInfoId     = GlobalUtil.getRecordTypeById('Account'); //getting all Recordtype for the Sobject
        accRecordTypeInfoName   = GlobalUtil.getRecordTypeByName('Account');//getting all Recordtype for the Sobject
        TaskRecordTypeInfoName=GlobalUtil.getRecordTypeByName('Task');
        
        
        sAccount                            = new Account();
        sAccount.RecordTypeId               = accRecordTypeInfoName.get('Residential').getRecordTypeId();
        sAccount.Name                       = 'Test Account';
        sAccount.Phone                      = '0456123789';
        sAccount.Customer_Status__c         = 'Active';
        //lAccount.add(sAccount);
        sAccount1                            = new Account();
        sAccount1.RecordTypeId               = accRecordTypeInfoName.get('Residential').getRecordTypeId();
        sAccount1.Name                       = 'Unknown Unknown';
        sAccount1.Phone                      = '0456123789';
        sAccount1.Customer_Status__c      = 'Active';
        //lAccount.add(sAccount1);
        //insert lAccount;
        insert sAccount;
        insert sAccount1;
        
      
        Contact sContact1 = new Contact();
        sContact1.FirstName  = 'Unknown';
        sContact1.LastName  = 'Unknown';
        sContact1.AccountId  = sAccount1.Id;
        sContact1.Contact_Role__c = 'Primary';
        insert sContact1;
        
        Pagenumber = new ININWebservice__c();
        Pagenumber.Name = '1';
        Pagenumber.pageNumber1__c = '1';
        insert  Pagenumber;
        
        
    }
    
    //Webservice Mock class 
    public class MockHttpRes implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest req) {
            
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            //res.setBody('{"id":"fb018fe5-9a06-40a6-9d8b-b283210763f8", "participants":[{"id":"f74d0e1d-23c9-4065-9279-7caabe846938", "name":"Australia", "address":"tel:+61470370712","startTime":"2016-11-21T07:41:27.681Z","connectedTime":"2016-11-21T07:41:27.906Z","endTime":"2016-11-21T07:43:39.965Z","purpose":"customer","state":"disconnected","direction":"inbound","disconnectType":"peer","held":false,"wrapupRequired":false,"queue":{"id":"d12c03b9-b38c-4e8a-a7d4-83fdd9066736","selfUri":"/api/v2/routing/queues/d12c03b9-b38c-4e8a-a7d4-83fdd9066736"},"attributes":{"Customer_ANI":"0470370712","NPS_Resolution":"1","NPS_Verbatim":"Yes","SF_SearchValue":"0470370712","NPS_Score":"5","CWC_allowACD":"true"},"provider":"Edge","muted":false,"confined":false,"recording":false,"recordingState":"none","ani":"tel:+61470370712","dnis":"tel:+61399296236"}' );                   
            res.setBody('{"id":"fb018fe5-9a06-40a6-9d8b-b283210763f8","participants":[{"attributes":{"Customer_ANI":"0470370712","NPS_Resolution":"1","NPS_Verbatim":"Yes","SF_SearchValue":"0470370712","NPS_Score":"5","CWC_allowACD":"true","SF_Product":"01228000000Na6L"}}]}');
            res.setStatusCode(200);
            return res;
        }
    } 
     public class MockHttpRes1 implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest req) {
            
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            //res.setBody('{"id":"fb018fe5-9a06-40a6-9d8b-b283210763f8", "participants":[{"id":"f74d0e1d-23c9-4065-9279-7caabe846938", "name":"Australia", "address":"tel:+61470370712","startTime":"2016-11-21T07:41:27.681Z","connectedTime":"2016-11-21T07:41:27.906Z","endTime":"2016-11-21T07:43:39.965Z","purpose":"customer","state":"disconnected","direction":"inbound","disconnectType":"peer","held":false,"wrapupRequired":false,"queue":{"id":"d12c03b9-b38c-4e8a-a7d4-83fdd9066736","selfUri":"/api/v2/routing/queues/d12c03b9-b38c-4e8a-a7d4-83fdd9066736"},"attributes":{"Customer_ANI":"0470370712","NPS_Resolution":"1","NPS_Verbatim":"Yes","SF_SearchValue":"0470370712","NPS_Score":"5","CWC_allowACD":"true"},"provider":"Edge","muted":false,"confined":false,"recording":false,"recordingState":"none","ani":"tel:+61470370712","dnis":"tel:+61399296236"}' );                   
            res.setBody(null);
            res.setStatusCode(200);
            return res;
        }
    }     
    
    public static testMethod void testOne(){
     //set<string> sString =
        Test.setMock(HttpCalloutMock.class, new MockHttpRes()); 
        Test.startTest();
        ININServices.Attributes att = new ININServices.Attributes();
        ININServices.getININConversationDetails('test','test');
         ININServices.getOAuthToken();
         //ININServices.findOrphanInteractions('test', 'test');
         //ININServices.createTaskFromWrapper('test',)
        Test.stopTest();
    }
    public static testMethod void testtwo(){
        Test.setMock(HttpCalloutMock.class, new MockHttpRes()); 
        Test.startTest();
        sAccount = [Select id FROM Account where Name= 'Test Account'];
        Contact sContact = new Contact();
        sContact.FirstName  = 'SalesforceTest';
        sContact.LastName  = 'Smithtest';
        sContact.AccountId  = sAccount.Id;
        //sContact.Contact_Role__c = 'Primary';
        insert sContact;
        
        ININServices.Attributes att = new ININServices.Attributes();
        att.NPS_Score= '';
        att.NPS_Resolution= '';
        att.NPS_Verbatim= '';
        att.Customer_ANI= '';
        att.CLI_Match= '';
        att.CLI_Time= '';
        att.Manual_CLI_Match= '';
        att.Manual_CLI_Time= '';
        att.Avalanche_DD= '';
        att.Avalanche_time= '';
        att.Avalanche_Continue= '';
        att.DOB_Match= '';
        att.AccountDetails_DD= '';
        att.AccountDetails_DD_Time= '';
        att.Preactivation= '';
        att.Hear_Balance= '';
        att.Acc_Suspended= '';
        att.Payment_Result= '';
        att.BlackHawk_DD= '';
        att.BlackHawk_DD_Time= '';
        att.Call_In= '';
        att.Transfer_Q_or_Menu= '';
        att.Plan_Type= '';
        att.Prepay_Result= '';
        att.Fetch_Prepay= '';
        att.Inactive_Customer= '';
        att.Outage= '';
        att.Avalanche_Status= '';
        att.SF_SearchValue= '';
        att.Disconnect= '';
        att.SF_Product= '01228000000Na6L';
        att.Mobile_Email_Task= '';
        att.Block_Number_Input= '';
        att.Contact_DD_BlockSIM= '';
        att.DOB_Match_BlockSIM= '';
        att.Return_To_Main_Menu= 0;
        att.MENU= '';
        ININServices.createTaskFromWrapper('fb018fe5-9a06-40a6-9d8b-b283210763f8',att, sContact);
        Test.stopTest();
    }
        public static testMethod void testthree(){
        Test.setMock(HttpCalloutMock.class, new MockHttpRes()); 
        Test.startTest();
       
        sContact1 =[Select id, AccountId from Contact];
        Set<String> ConversationId = new Set<String>{'fb018fe5-9a06-40a6-9d8b-b283210763f8', '024008cb-cfb5-46f7-b146-6359b3009c64'};

        ININServices.getOAuthToken();
        ININServices.ConversationWrapper3 convwrap3 = new ININServices.ConversationWrapper3();
        convwrap3.conversationId='fb018fe5-9a06-40a6-9d8b-b283210763f8';
        ININServices.ININConversationWrapper2 ININWrap2 = new ININServices.ININConversationWrapper2();
        ININServices.ININConversationWrapper inv = new ININServices.ININConversationWrapper();
        String OuthToken=ININServices.getOAuthToken();
        //ININServices.getININConversationDetails('fb018fe5-9a06-40a6-9d8b-b283210763f8',OuthToken);
        //ININServices.getFailedInteractions(OuthToken, 1);
        ININServices.getAllConversation(ConversationId, OuthToken);
        //ININServices.findOrphanInteractions(ConversationId, OuthToken);
         //ININServices.createTaskFromWrapper('test',)
        Test.stopTest();
    }
}