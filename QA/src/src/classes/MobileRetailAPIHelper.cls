public with sharing class MobileRetailAPIHelper {
    public static  WrapperHTTPResponse retailPostSIMHelper(String jsonData) {
        //
        List<Sim_Data__c> lstSimData = new List<Sim_Data__c>();
        Sim_Data__c simData;
        String sCardNumber;
        String errorCode;
        //new custom setting to map errors
        Map<String, Mobile_Retail_Error_Mapping__c> mapError = Mobile_Retail_Error_Mapping__c.getAll();
        try {
            //Validate data wherever needed
            simData = (Sim_Data__c)JSON.deserialize(jsonData, Sim_Data__c.class);
            if (String.isEmpty(simData.Card_number__c)) {
                MobileRestUtility.message = 'Card_number__c is empty. Please provide valid card number';
                return MobileRestUtility.returnWithError('MobileRetailAPIHelper.retailPostSIMHelper', jsonData);
            }
            /*  if (String.isEmpty(simData.Card_value__c)) {
                MobileRestUtility.message = 'Card_value__c is empty. Please provide valid card value';
                return MobileRestUtility.returnWithError('MobileRetailAPIHelper.retailPostSIMHelper', jsonData);
                }*/
            //sCardNumber = simData.Card_number__c;
            //lstSimData = new List<Sim_Data__c>();
            sCardNumber = simData.Card_value__c;
            //upsert simData Card_number__c;
            lstSimData.add(simData);
            Schema.SObjectField f = Sim_Data__c.fields.Card_number__c;
            //database.upsertResult[] g = database.upsert(lstSimData,f,false);
            Database.UpsertResult simupsert = Database.upsert(simdata, f, false);
            //todo comments
            if (simupsert.isSuccess()) {
                if (simupsert.iscreated() ) {
                    /*  success-----for us fail but its a fail for black hawak....
                        if this is insert and succesfully insert than status 08, coz sl is not able to find recrd n SF and it has to be iserted.
                        statusCode=400
                    */
                    MobileRestUtility.statusCode = 400;
                    MobileRestUtility.status = '08';
                    MobileRestUtility.message = '08';
                }
                else
                    //todo what scenario is bening handled here
                    if (!simupsert.iscreated()) {
                        MobileRestUtility.statusCode = 200;
                        MobileRestUtility.status = '00';
                        MobileRestUtility.message = '00';
                    }
            }
            //todo what scenario is bening handled here
            else {
                MobileRestUtility.statusCode = 400;
                //query data base
                //MobileRestUtility.setExceptionResponse(e, 'MobileRetailAPIHelper.retailPostSIMHelper', jsonData);
                Database.error error = simupsert.getErrors()[0];
                errorCode = error.getMessage();
                MobileRestUtility.message = mapError.get(errorCode).Error_Message__c + 'Error Fields:' + error.getFields() + ' Error Code:' + error.getStatusCode();
                //Any DML error will throw error from trigger which is the code
                MobileRestUtility.status = error.getMessage();
            }
            //send the currenct status of record firm database
            simData = (Sim_Data__c) GlobalUtil.getSObjectRecords('Sim_Data__c', NULL, NULL, 'Card_number__c=\'' + simData.Card_number__c + '\'')[0];
            //database.upsert(simData,false);
            //set success code as 00 for black hawk. Anything success is success
            //Ideally this statusCode field should capture the error code for black hawk, but due to 00 in as success we cant use it
            /*  MobileRestUtility.statusCode = 200;
                MobileRestUtility.status = '00';
                MobileRestUtility.message = '00' + ' Card value ' + sCardNumber;*/
        }
        //Girish changes
        catch (System.DmlException e) {
            //set Error message as 400, as it is bad data
            //Actual error message will be returned from addError via trigger logic
            //added to catch DML messages
            /*   MobileRestUtility.setExceptionResponse(e, 'MobileRetailAPIHelper.retailPostSIMHelper', jsonData);
                errorCode = e.getDmlMessage(0);
                MobileRestUtility.message = mapError.get(errorCode).Error_Message__c + '. Card value: ' + sCardNumber;
                //Any DML error will throw error from trigger which is the code
                MobileRestUtility.status = e.getDmlMessage(0);
                MobileRestUtility.statusCode = 400;*/
        }
        //catch any other generic exception here
        catch (Exception e) {
            MobileRestUtility.setExceptionResponse(e, 'MobileRetailAPIHelper.retailPostSIMHelper', jsonData);
        }
        return RestUtility.getResponse(MobileRestUtility.status, MobileRestUtility.message, MobileRestUtility.statuscode, new List<Sim_Data__c> {simData}, MobileRestUtility.logs);
    }
}