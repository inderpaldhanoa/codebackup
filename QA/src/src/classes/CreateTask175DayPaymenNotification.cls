public with sharing class CreateTask175DayPaymenNotification {

    @InvocableMethod(label = 'createNotification' description = 'createNotification task for 157 day of unpaid')
    public static void createNotification (List<string> listID) {
        String  recordTypeId = (Id)GlobalUtil.getRecordTypeByName('Task').get('Mobile Notification').getRecordTypeId();
        String  recordTypeIdEmail = (Id)GlobalUtil.getRecordTypeByName('Task').get('Mobile Notification').getRecordTypeId();
        set<String> setUnpaidID = new Set<String>();
        //IF customer pays, Day 11-179 paid task will be created
        String taskType = SYSTEM.Label.MOBILE_PAYMENT_PAID_TASKTYPE;
        //Fetch the tasks that execute the PaymentNotification task for 175 day
        //get the order id for all such tasks
        //fetch the tasks that were with Type =Day11-175 Paid
        //if any such task is found do not create 75 task notification, else create one
        List<String> listPaymentOrderIDs = new List<String>();
        For(Task task: [Select id, Payment_OrderID__c
                        from Task
                        where Id in :listID]) {
            listPaymentOrderIDs.add(task.Payment_OrderID__c);
        }
        String listPaymentIDs = String.join(listPaymentOrderIDs, ',');
        listPaymentIDs = GlobalUtil.createDynamicInWhereClause(listPaymentIDs);
        System.debug(listPaymentIDs);
        //This where clause will fetch all the tasks that were created for customers who has NOT PAID
        String sWhereClause = 'Payment_OrderID__c in ' + listPaymentIDs + ' AND Type=\'Notification\' AND Task_Sub_Type__c!=\'' + taskType + '\' AND recordTypeId=\'' + recordTypeId + '\'';
        Map<id, task> mapInsert175Task = new Map<id, task>();
        //on 175 day check if there are any task which are created as Day 11-179 upaid task for payment order Id
        Map<Id, Task> unpaidTasks = new Map<Id, Task>((List<Task>)GlobalUtil.getSObjectRecords('Task', NULL, NULL, sWhereClause));
        For(Task eachtask: unpaidTasks.values()) {
            if (!mapInsert175Task.containsKey(eachtask.WhoId)) {
                Task newtask = eachtask;
                eachtask.id = null;
                eachtask.Subject = 'Day 11-179 - Unpaid: Email Notification';
                eachtask.Task_Sub_Type__c = 'Day 11-179 - unpaid';
                eachtask.Task_Sub_Type__c = 'Day 175';
                eachtask.RecordtypeId = recordTypeIdEmail;
                mapInsert175Task.put(eachtask.WhoId, eachtask);
            }
        }
        System.debug('unpaidTasks*********' + mapInsert175Task);
        insert mapInsert175Task.values();
    }
}