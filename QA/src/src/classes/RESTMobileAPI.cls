/************************************************************************************************************
    Apex Class Name   : RESTMobileAPI.cls
    Version           : 1.0
    Created Date      : 18 April 2017
    Function          : API endpoints for all Mobile needs.
    Modification Log  :
    Developer             	Date                Description
    -----------------------------------------------------------------------------------------------------------
    Tom Clayton           	18/04/2017          Created Class to merge in work from each seperate API.
    Tom Clayton / Girish P    01/05/2017          Ongoing changes and build.
	Vijay                   11/09/2017          Implemented Sim validation req
************************************************************************************************************/
@RestResource(urlMapping = '/RESTMobileAPI/*')
global without sharing class RESTMobileAPI {
	Public static Final String MOBILE_CASE_RECORD_TYPE = 'Mobile Support';
	public static Final String API_IVR = 'ivr';
	public static Final String API_SIM_VALIDATION = 'validateSim';
	public static Final String API_CREATE_NOTIFICATIONS = 'createNotification';
	public static Final String API_GET_NOTIFICATIONS = 'getNotification';
	public static Final String API_DISMISS_NOTIFICATIONS = 'dismissNotification';
	public static Final String API_CHANGE_USER_PASSWORD = 'changePassword';

	public static Set<String> allowedApiCalls = new Set<String> {
		'getPublicKnowledgeTopics',
		'getCases',
		'getDataCategory',
		'commentCase',
		'createCase',
		'upsertServiceTransaction',
		'getServiceTransactions',
		'sendVerificationCode',
		'checkVerificationCode',
		API_SIM_VALIDATION,
		API_IVR,
		API_CREATE_NOTIFICATIONS,
		API_GET_NOTIFICATIONS,
		API_DISMISS_NOTIFICATIONS,
		API_CHANGE_USER_PASSWORD
	};
	public static String restResponse;
	public static String apiCall = '';
	public static final String STATUS_EXCEPTION = 'Exception';
	public static final String HTTP_ERROR = 'Error';

	/*
	    == Get Cases ==
	    Returns list of Cases sorted by status ("Open" first), then open date.
	    Expecting to recieve parameters in URL e.g.
	    URL: /services/apexrest/RESTMobileAPI?APICall=getCases&octaneId=90000012&startOffset=0&maxNumOfResults=10

	    == Get Public Knowledge Topics ==
	    Returns list of Public Knowledge Topics
	    Expecting to recieve call as URL e.g.
	    URL: /services/apexrest/RESTMobileAPI?APICall=getPublicKnowledgeTopics

	    == Get Customer Mapping ==
	    Returns list of Customer Mapping/s for the Account which matches the provided Octane Id
	    Expecting to recieve parameters in URL e.g.
	    URL: /services/apexrest/RESTMobileAPI?APICall=getCustomerMapping&octaneId=00909012

	    == Get Service Transactions ==
	    Returns list of Service Transactions sorted by creation date.
	    Expecting to recieve parameters in URL e.g.
	    URL: /services/apexrest/RESTMobileAPI?APICall=getServiceTransactions&octaneId=90000012&startOffset=0&maxNumOfResults=10&mobileNo=0467%20289%20161&status=In%20Progress&subStatus=Progressing&startDate=2017-05-10&endDate=2017-05-12
		
		 == Get Sim Validations ==
	    Returns result of validation on Digital/retail SIM.
	    Expecting to recieve parameters in URL e.g.
	    URL: /services/apexrest/RESTMobileAPI?APICall=validateSim&simno=1223555&email=example@gmail.com

    */
	@HttpGet
	global static void getMobile() {
		Savepoint sp = Database.setSavepoint();
		MobileRestUtility.logs = new List<Log__c>();
		RestRequest req = RestContext.request;
		RestResponse res = Restcontext.response;
		Map<String, String> mapParameters = req.params;
		// Set header as JSON
		res.addHeader('Content-Type', 'application/json');
		// Call post helper class to process the POST request
		// Append the response to request body
		if (mapParameters.containsKey('APICall')) {
			apiCall = mapParameters.get('APICall');
		}
		else {
			restResponse = 'Invalid Api Call or parameter missing';
			restResponse = JSON.serialize(RestUtility.getResponse('Error', restResponse, 404, NULL, NULL));
			res.responseBody = Blob.valueOf(restResponse);
			return;
		}
		if (!allowedApiCalls.contains(apiCall)) {
			restResponse = RestUtility.getResponse('Error',  'This is not a valid Api Call', 404, NULL, NULL).status;
		}
		else
			if (apiCall.equalsIgnoreCase( 'getPublicKnowledgeTopics') ) {
				restResponse = MobileRestHelper.getPublicKnowledgeTopicsHelper().status;
			}
			else
				if (apiCall.equalsIgnoreCase('getCases')) {
					restResponse = MobileRestCaseAPIHelper.getCasesHelper(req.params, MOBILE_CASE_RECORD_TYPE + ',Closed Mobile Support').status;
					// The addition to the record types above is because getCases must also return Cases which have been resolved and set to "Closed Mobile Support" record type
				}
				else
					if (apiCall.equalsIgnoreCase('getDataCategory')) {
						restResponse = JSON.serializePretty(MobileRestHelper.getDataCategory());
					}
					else
						if (apiCall.equalsIgnoreCase('getServiceTransactions')) {
							restResponse = MobileRestServiceTransactionAPIHelper.getServiceTransactionHelper(req.params).status;
						}
						else
							if (apiCall.equalsIgnoreCase(API_SIM_VALIDATION)) {
								restResponse =MobileRestCustomerApiHelper.validateSim(mapParameters.get('simno'),mapParameters.get('email')).status;
							}
						else
							if (apiCall.equalsIgnoreCase(API_GET_NOTIFICATIONS)) {
								restResponse = MobileRestHelper.getNotificationsHelper(req.params).status;
							}
							else {
								restResponse = HTTP_ERROR;
							}
		System.debug('restResponse****' + restResponse);
		if (restResponse.equalsIgnoreCase(STATUS_EXCEPTION)
		        || restResponse.equalsIgnoreCase(HTTP_ERROR)) {
			Database.rollback(sp);
		}
		if (!MobileRestUtility.logs.isEmpty()) {
			insert MobileRestUtility.logs;
		}
	}

	/*
	    == Re-open Case ==
	    Returns confirmation that the specified Case has been re-opened and given a comment
	    Expecting to receive parameters in JSON e.g.
	    URL: /services/apexrest/RESTMobileAPI?APICall=commentCase
	    JSON example (out of date):
	    {
	    "caseId": "500O000000B3APZIA3",
	    "octaneId": "12345623",
	    "comment": "Test comment from API call to re-open and/or comment on the Case. This comment comes from the user."
	    }

	    == Create Case ==
	    Returns confirmation that a Case has been created
	    Expecting to receive parameters in JSON e.g.
	    URL: /services/apexrest/RESTMobileAPI?APICall=createCase
	    JSON example (out of date):
	    Authenticated:
	    {
	    "octaneId": "ATest2",
	    "email": "thespicypancake@hotmail.com",
	    "knowledgeArticle": "kA0O00000004ocLKAQ",
	    "subject": "Mouse is melting",
	    "description": "My mouse has white slime coming down the sides of it. Please send help.",
	    "fileData": [
	                    {"fileName": "exampleOfErrorsISee.csv",
	                    "fileBlob":"T3JkZXIgSUQsIEVtYWlsIEFkZHJlc3MsIFByb2R1Y3QgQ29kZSwgRGF0ZSBEaXNwYXRjaGVkLCBTdGF0dXMsIFJlYXNvbiwgU0lNIE51bWJlciwgVHJhY2tpbmcgTnVtYmVyCiIwMmlPMDAwMDAwMG1wTk5JQVkiLCAibW91c2UyQHRlc3QuY29tIiwgIk1vYmlsZSBTSU0iLCAyMDE3LTAzLTIyLCAiRGlzcGF0Y2hlZCIsICIiLCAiMTIzMTIzNDU2Nzg5MDEyMzQ1NjciLCAiUlAzODA5OTczOCIKIjAyaU8wMDAwMDAwbXFUd0lBSSIsICJtaWtlaGFzZml4ZWQyQHRlc3QuY29tLmF1IiwgIk1vYmlsZSBTSU0iLCAyMDE3LTAzLTIyLCAiRGlzcGF0Y2hlZCIsICIiLCAiMTIzMTIzNDU2Nzg5MDEyMzQ1NjgiLCAiUlAzODA5OTczOSIKIjAyaU8wMDAwMDAwbXFUcklBSSIsICJtaWtlbm9maXhlZDJAdGVzdC5jb20uYXUiLCAiTW9iaWxlIFNJTSIsIDIwMTctMDMtMjIsICJEaXNwYXRjaGVkIiwgIiIsICIxMjMxMjM0NTY3ODkwMTIzNDU2OSIsICJSUDM4MDk5NzMwIgo="
	                    }
	                ]
	    }
	    Unauthenticated:
	    {
	    "email": "thespicypancake@hotmail.com",
	    "knowledgeArticle": "kA0O00000004ocLKAQ",
	    "subject": "Mouse is melting",
	    "description": "My mouse has white slime coming down the sides of it. Please send help.",
	    "fileData": [
	                    {"fileName": "exampleOfErrorsISee.csv",
	                    "fileBlob":"T3JkZXIgSUQsIEVtYWlsIEFkZHJlc3MsIFByb2R1Y3QgQ29kZSwgRGF0ZSBEaXNwYXRjaGVkLCBTdGF0dXMsIFJlYXNvbiwgU0lNIE51bWJlciwgVHJhY2tpbmcgTnVtYmVyCiIwMmlPMDAwMDAwMG1wTk5JQVkiLCAibW91c2UyQHRlc3QuY29tIiwgIk1vYmlsZSBTSU0iLCAyMDE3LTAzLTIyLCAiRGlzcGF0Y2hlZCIsICIiLCAiMTIzMTIzNDU2Nzg5MDEyMzQ1NjciLCAiUlAzODA5OTczOCIKIjAyaU8wMDAwMDAwbXFUd0lBSSIsICJtaWtlaGFzZml4ZWQyQHRlc3QuY29tLmF1IiwgIk1vYmlsZSBTSU0iLCAyMDE3LTAzLTIyLCAiRGlzcGF0Y2hlZCIsICIiLCAiMTIzMTIzNDU2Nzg5MDEyMzQ1NjgiLCAiUlAzODA5OTczOSIKIjAyaU8wMDAwMDAwbXFUcklBSSIsICJtaWtlbm9maXhlZDJAdGVzdC5jb20uYXUiLCAiTW9iaWxlIFNJTSIsIDIwMTctMDMtMjIsICJEaXNwYXRjaGVkIiwgIiIsICIxMjMxMjM0NTY3ODkwMTIzNDU2OSIsICJSUDM4MDk5NzMwIgo="
	                    }
	                ]
	    }
	    // HOW TO GET KNOWLEDGE ARTICLE VERSION DATA:
	    // SELECT Id,KnowledgeArticleId,Title,Summary FROM FAQ_Mobile__kav WHERE PublishStatus ='Online' AND Language = 'en_US' AND IsLatestVersion = true
	*/
	// POSSIBLE PROBLEM: This will pick the first Lead with matching email found. There is no management of duplicates.
	@HttpPost
	global static void postMobile() {
		MobileRestUtility.logs = new List<Log__c>();
		Savepoint sp = Database.setSavepoint();
		RestRequest req = RestContext.request;
		RestResponse res = Restcontext.response;
		Map<String, String> mapParameters = req.params;
		// Set header as JSON
		res.addHeader('Content-Type', 'application/json');
		// Call post helper class to process the POST request
		// Append the response to request body
		if (mapParameters.containsKey('APICall')) {
			apiCall = mapParameters.get('APICall');
		}
		else {
			restResponse = 'Invalid Api Call or parameter missing....';
			restResponse = JSON.serialize(RestUtility.getResponse('Error', restResponse, 404, NULL, NULL));
			res.responseBody = Blob.valueOf(restResponse);
			return;
		}
		System.debug(' req.requestBody********' + req.requestBody.toString());
		if (String.isEmpty(req.requestBody.toString())) {
			restResponse = 'JSON Body is required for Post requests.';
			restResponse = RestUtility.getResponse('Error', restResponse, 404, NULL, NULL).status;
		}
		else
			if (!allowedApiCalls.contains(apiCall)) {
				restResponse = RestUtility.getResponse('Error',  'Invalid Api Call or parameter missing', 404, NULL, NULL).status;
			}
			else
				if (apiCall.equalsIgnoreCase('commentCase')) {
					restResponse = MobileRestCaseAPIHelper.commentCaseHelper(req.requestBody.toString()).status;
				}
				else
					if (apiCall.equalsIgnoreCase('createCase')) {
						restResponse = MobileRestCaseAPIHelper.createCaseHelper(req.requestBody.toString(), MOBILE_CASE_RECORD_TYPE).status;
					}
					else
						if (apiCall.equalsIgnoreCase(API_IVR)) {
							restResponse = MobileRestHelper.ivrApiHelper(req.requestBody.toString()).status;
						}
						else
							if (apiCall.equalsIgnoreCase(API_CREATE_NOTIFICATIONS)) {
								restResponse = MobileRestHelper.notificationTasksHelper(req.requestBody.toString()).status;
							}
							else
								if (apiCall.equalsIgnoreCase('sendVerificationCode')) {
									restResponse = MobileRestHelper.sendVerificationCodeHelper(req.requestBody.toString()).status;
								}
								else
									if (apiCall.equalsIgnoreCase('checkVerificationCode')) {
										restResponse = MobileRestHelper.checkVerificationCodeHelper(req.requestBody.toString()).status;
									}
									else {
										restResponse = HTTP_ERROR;
									}
		if (restResponse.equalsIgnoreCase(STATUS_EXCEPTION) || restResponse.equalsIgnoreCase(HTTP_ERROR)) {
			Database.rollback(sp);
		}
		if (!MobileRestUtility.logs.isEmpty()) {
			insert MobileRestUtility.logs;
		}
		//return null;
	}

	/*
	    == Upsert Service Transaction ==
	    Returns confirmation that a Case has been created
	    Expecting to receive parameters in JSON e.g.
	    URL: /services/apexrest/RESTMobileAPI?APICall=upsertServiceTransaction
	    JSON example (out of date):
	    Create:
	    {
	    "telFlowId": "1234",
	    "octaneId": "90000012",
	    "action": "Activate",
	    "agentId": "1234",
	    "billingCycle": "2017-03-10",
	    "completionDate": "2017-05-18T11:12:13.123+10:00",
	    "gifterName": "Bobby Jaytron",
	    "gifterMobileNo": "0463 728 192",
	    "mobileNo": "0467 289 161",
	    "offerContractTerm": "12 months",
	    "offerDescriptions": "Good deal package",
	    "offerIds": "6789, 8767",
	    "offerType": "Package 5R",
	    "offerVersionDateValidFrom": "2017-02-14",
	    "offerVersionDateValidTo": "2017-05-01",
	    "offerVersionNumber": "v0.02",
	    "offerPrice": "72.00, 8.00",
	    "totalPrice": "80.00",
	    "processDate": "2017-05-17T17:01:01.123+10:00",
	    "receiverName": "Janet Blacktown",
	    "receiverMobileNo": "0472 877 455",
	    "rejectCode": "Not Rejected 111",
	    "rejectReason": "N/A",
	    "dataGiftSize": "2",
	    "source": "Website",
	    "status": "In Progress",
	    "subStatus": "Progressing",
	    "subType": "Port",
	    "submittedDate": "2017-05-17T13:22:02.123+10:00",
	    "type": "Activate"
	    }
	*/

	@HttpPatch
	global static void patchMobile() {
		MobileRestUtility.logs = new List<Log__c>();
		Savepoint sp = Database.setSavepoint();
		RestRequest req = RestContext.request;
		RestResponse res = Restcontext.response;
		Map<String, String> mapParameters = req.params;
		// Set header as JSON
		res.addHeader('Content-Type', 'application/json');
		// Call post helper class to process the POST request
		if (mapParameters.containsKey('APICall')) {
			apiCall = mapParameters.get('APICall');
		}
		else {
			restResponse = 'Invalid Api Call or parameter missing';
			restResponse = JSON.serialize(RestUtility.getResponse('Error', restResponse, 404, NULL, NULL));
			res.responseBody = Blob.valueOf(restResponse);
			return;
		}
		if (String.isEmpty(req.requestBody.toString())) {
			restResponse = 'JSON Body is required for Patch requests.';
			restResponse = RestUtility.getResponse('Error', restResponse, 404, NULL, NULL).status;
		}
		else
			if (!allowedApiCalls.contains(apiCall)) {
				restResponse = 'Invalid Api Call or parameter missing';
				restResponse = RestUtility.getResponse('Error', restResponse, 404, NULL, NULL).status;
				return;
			}
			else
				if (apiCall.equalsIgnoreCase('upsertServiceTransaction')) {
					restResponse = MobileRestServiceTransactionAPIHelper.upsertServiceTransactionHelper(req.requestBody.toString()).status;
				}
				else
					if (apiCall.equalsIgnoreCase(API_DISMISS_NOTIFICATIONS)) {
						restResponse = MobileRestHelper.notificationDismissHelper(req.requestBody.toString()).status;
					}
					else
						if (apiCall.equalsIgnoreCase(API_CHANGE_USER_PASSWORD)) {
							restResponse = MobileRestHelper.changePassword((Map<String, String>)JSON.deserialize(req.requestBody.toString(), Map<String, String>.class)).status;
						}
						else {
							restResponse = JSON.serialize(RestUtility.getResponse('Error', 'APICall parameter is invalid or missing.', 404, NULL, NULL));
						}
		if (restResponse.equalsIgnoreCase(STATUS_EXCEPTION) || restResponse.equalsIgnoreCase(HTTP_ERROR)) {
			Database.rollback(sp);
		}
		if (!MobileRestUtility.logs.isEmpty()) {
			insert MobileRestUtility.logs;
		}
	}
}