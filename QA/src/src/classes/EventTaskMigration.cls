global class EventTaskMigration implements Database.Batchable<sObject>, Database.Stateful
{
	// ************************************************************************************************
	// *** Static globals
	
	// ************************************************************************************************
	// *** Globals
	
	// ************************************************************************************************
	// **** Executing Methods
	global EventTaskMigration()
	{
		
	}
	
	// ************************************************************************************************
	// **** Batch Methods
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		List<String> filters = new List<String>{'%SMS%', '%EMAIL%'};
		List<String> idList = new List<String>{'00U2800000B295v', '00U2800000B295R', '00U2800000B1tq7', '00U28000001L7NG', '00U28000001LUJd'};
		String Query = 'SELECT AccountId, ActivityDate, ActivityDateTime, Contact_Sub_Type__c, Contact_Type__c, DB_Activity_Type__c, Description';
		Query += ', Octane_ContactLog_ID__c, Octane_ContactLog_Username__c, Subject, WhatId, WhoId';
	 	Query += ' FROM Event WHERE Subject LIKE: filters AND TBD__c = False LIMIT 49500';
		
		//Query += ' FROM Event WHERE Subject LIKE: filters AND Id IN: idList LIMIT 50000';
		
		System.debug('EventTaskMigration Query ' + Query);
		return Database.getQueryLocator(Query);
	}
	
	global void execute(Database.BatchableContext BC, List<SObject> scope)
	{
		List<Task> newTaskList = new List<Task>();
		List<Event> newEventList = new List<Event>();
		List<String> keyBillingList = new List<String>();
		Set<Id> whoIdList = new Set<Id>();
		Map<id, sObject> contactMap = new Map<id, sObject>(); 
		
		Map<String,Schema.RecordTypeInfo> taskRecordTypeInfoName;
		Map<Id,Schema.RecordTypeInfo> taskRecordTypeInfoId;
        
        taskRecordTypeInfoId		= GlobalUtil.getRecordTypeById('Task'); //getting all Recordtype for the Sobject
	    taskRecordTypeInfoName		= GlobalUtil.getRecordTypeByName('Task');//getting all Recordtype for the Sobject

        Id smsId = taskRecordTypeInfoName.get('SMS').getrecordTypeId();
        Id emailId = taskRecordTypeInfoName.get('Email').getrecordTypeId();
		
		keyBillingList.add('We haven\'t been able to debit');
		keyBillingList.add('You currently have a charge of');
		keyBillingList.add('As we did not receive your monthly payment');
		keyBillingList.add('your Belong service will be re-activated');
		keyBillingList.add('Your Belong service has been cancelled as you haven\'t paid');
		
		for (SObject obj: scope)
		{
			Event eventSObj = (Event)obj;
			
			if(String.isNotBlank(eventSObj.WhoId))
				whoIdList.add(eventSObj.WhoId);
		}
		
		contactMap = Cache.getRecordMap(whoIdList);
		
		for (SObject obj: scope)
		{
			Event eventSObj = (Event)obj;
			Task taskObj = new Task();
			
			taskObj.ActivityDate = eventSObj.ActivityDate;
			taskObj.Contact_Sub_Type__c = eventSObj.Contact_Sub_Type__c;
			taskObj.Contact_Type__c = eventSObj.Contact_Type__c;
			taskObj.Description = eventSObj.Description;
			taskObj.Octane_ContactLog_ID__c = eventSObj.Octane_ContactLog_ID__c;
			taskObj.Octane_ContactLog_Username__c = eventSObj.Octane_ContactLog_Username__c;
			taskObj.Subject = eventSObj.Subject;
			taskObj.WhatId = eventSObj.WhatId;
			taskObj.WhoId = eventSObj.WhoId;
			taskObj.Status = 'Completed';
			taskObj.Notification_Date_Time__c = eventSObj.ActivityDateTime;
			
			if(eventSObj.Subject.containsIgnoreCase('SMS'))
			{
				taskObj.RecordTypeId = smsId;
				taskObj.SMS_Send_Status__c = 'Success';
				taskObj.SMS_EVE_Response__c = 'Migration from Event to Task';
				
				System.debug(logginglevel.Error, 'MAP ' + contactMap);
				System.debug(logginglevel.Error, 'eventSObj.WhoId ' + eventSObj.WhoId);
				if(String.isNotBlank(eventSObj.WhoId))
				{
					Contact conObj = (Contact) contactMap.get(eventSObj.WhoId);
					System.debug(logginglevel.Error, 'conObj ' + conObj);
					taskObj.SMS_Recipient__c = String.valueof(conObj.Phone);
				}
				
				for (String strKey: keyBillingList) 
				{
    				if(eventSObj.Description.contains(strKey))
    				{
    					taskObj.Billing__c = true;
    				}
				}
			}
			else if(eventSObj.Subject.containsIgnoreCase('Email'))
			{
				taskObj.RecordTypeId = emailId;
			}
			
			newTaskList.add(taskObj);
			
			newEventList.add(New Event(id =eventSObj.id, TBD__c = true));
		}
		
		if(!newTaskList.isEmpty())
			insert newTaskList;
		
		if(!newEventList.isEmpty())
			update newEventList;
		
	}
	global void finish(Database.BatchableContext BC)
	{
		
	}
}