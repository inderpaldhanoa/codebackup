@isTest
private class ViewSurveyControllerTest
{

	static testmethod void testViewSurveyController()
    {
    	SurveyTestingUtil tu = new SurveyTestingUtil();
        
        Apexpages.currentPage().getParameters().put('id',tu.surveyId);  
        Apexpages.Standardcontroller stc;   

        ViewSurveyController vsc = new ViewSurveyController(stc);

        vsc.init();
        
        System.assert(vsc.allQuestionsSize == 4);
        System.assert(tu.surveyId != null);
        
        
        vsc.submitResults();
        
        for (ViewSurveyController.question q : vsc.allQuestions)
        {
            q.selectedAnswer= String.valueof(2);            
            vsc.submitResults();            
        }
    }

    static Testmethod void testUpdateSurveyName() 
    {

      SurveyTestingUtil tu = new SurveyTestingUtil();
      Apexpages.currentPage().getParameters().put('id',tu.surveyId);  
      Apexpages.Standardcontroller stc; 
      ViewSurveyController vsc = new ViewSurveyController(stc);
      vsc.surveyName = 'new name';
      system.assert(vsc.updateSurveyName() == null);
      
    }

    static Testmethod void testupdateSurveyThankYouAndLink() 
    {
        SurveyTestingUtil tu = new SurveyTestingUtil();
        Apexpages.currentPage().getParameters().put('id',tu.surveyId);  
        Apexpages.Standardcontroller stc; 
        ViewSurveyController vsc = new ViewSurveyController(stc);
        vsc.surveyThankYouText = 'new stuff';
        vsc.surveyThankYouURL = 'more new stff';
        system.assert(vsc.updateSurveyThankYouAndLink()==null);
    }
}