/**
* An apex page controller that exposes the site login functionality
*/
global without sharing class CustomSiteLoginController {
    
    global CustomSiteLoginController () {
        String username = System.currentPageReference().getParameters().get('username');
    } 
    @RemoteAction
    global static string login(String username,String password, String startURL) {
        //Check for migrating users old pwd
        List<User> oldUser = [SELECT id, AEMPassword__c
                              FROM User
                              WHERE username = :username
                              AND AEMPassword__c != NULL
                              LIMIT 1];
        if(!oldUser.isempty()){
            // Migrated User from AEM
            Boolean result = passwordDecoder.checkPaswordValidity(password, oldUser[0].AEMPassword__c);
            if(result){
                // update AEMPassword user to NULL
                system.setPassword(oldUser[0].Id, password);
                oldUser[0].AEMPassword__c = '';
                update oldUser;
                // Allow Login
                if(!test.isRunningTest())
                    return Site.login(username, password, startUrl).getURL().unescapeHtml4();
                return null;
            } else {
                // Migrated User Incorrect password
                return null;
            }
        }else {
            // New user after Migration
            return  Site.login(username, password, startUrl).getURL().unescapeHtml4();  
        }
    }    
}