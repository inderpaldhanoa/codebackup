/* Controller associated with pages rendering the survey.
 * Used by SurveyPage, ResultsPage, TakeSurvey
 */
global virtual without sharing class ViewSurveyController 
{
    public List<SelectOption> singleOptions     {get; set;} 
    public List<SelectOption> anonymousOrUser   {get; set;}
    public List<question> allQuestions          {get; set;}
    public List<String> newOrder                {get; set;}
    public List<String> responses               {get; set;}
    public String   qQuestion                   {get; set;}
    public Boolean  qRequired                   {get; set;}
    public String   qChoices                    {get; set;}
    public String   surveyName                  {get; set;}
    public String   surveyHeader                {get; set;}
    public String   pageErrors                  {get; set;}
    public String   surveyId                    {get; 
        set{
            this.surveyId = value;
            init();
        }
    }
    public String   renderSurveyPreview         {get; set;}  
    public String   questionName                {get; set;}  
    public String   questionType                {get; set;}
    public Boolean  questionRequired            {get; set;}
    public Integer  allQuestionsSize            {get; set;}
    public String   templateURL                 {get; set;}
    public String   surveyThankYouText          {get; set;}
    public String   surveyContainerCss          {get; set;}
    public String   surveyThankYouURL           {get; set;}
    public String   caseId                      {get; set;}
    public String   contactId                   {get; set;}
    public String   anonymousAnswer             {get; set;}
    public Boolean  isInternal                  {get; set;}
    public String   baseURL                     {get; set;}
    public String   userId                      {get; set;}
    public String   orderId                     {get; set;}
    public String   userName                    {get; set;}
    public String   surveyTakerId               {get; set;}
    public String   chatKey;
    
    public Boolean  thankYouRendered            {get; set;}
    public Boolean  surveyTaken                 {get; set;}
    public Id  transcriptId                     {get; set;}
    
    //public Id  transcript                       {get; set;}
    public Boolean isSite { get { return String.isNotBlank(Site.getName()); } }
    LiveChatTranscript oLiveChatTranscript;
    
    /* Retrieves the list of questions, the survey name, after retrieving the 
       necessary parameters from the url.
    */
    //------------------------------------------------------------------------------// 
    public ViewSurveyController(ApexPages.StandardController stdController) 
    {
        // Get url parameters
        surveyId = Apexpages.currentPage().getParameters().get('id');
        caseId   = Apexpages.currentPage().getParameters().get('caId');
        contactId = Apexpages.currentPage().getParameters().get('cId'); 
        orderId = Apexpages.currentPage().getParameters().get('ordId');
        chatKey = Apexpages.currentPage().getParameters().get('chatKey');
        transcriptId = ApexPages.currentPage().getParameters().get('tId');
        
        surveyTaken = false;
        thankYouRendered = false;
        
        system.debug(logginglevel.error, 'ViewSurveyController = > surveyId' + surveyId);
        system.debug(logginglevel.error, 'ViewSurveyController = > caseId' + caseId);
        system.debug(logginglevel.error, 'ViewSurveyController = > contactId' + contactId);
        system.debug(logginglevel.error, 'ViewSurveyController = > orderId' + orderId);
    
        if(caseId ==null || caseId.length()<5){
            caseId = 'none';
        }
        if(contactId ==null || contactId.length()<5){
            contactId = 'none';
        }    
               
        // By default the preview is not showing up
        renderSurveyPreview = 'false';

        init();
        
        if(contactId != null || caseId != null || orderId != null)
        {
            List<SurveyTaker__c> check = [SELECT Contact__c, Survey__c, Case__c, User__c 
                                            FROM SurveyTaker__c 
                                            WHERE Contact__c =: contactId AND 
                                            Survey__c =: surveyId AND 
                                            Order__c =: orderId];
            
            if(check != null && check.size()>0)
            {
                surveyHeader = System.Label.LABS_SF_You_have_already_taken_this_survey;
                surveyTaken = true;
            }
        }
        
    } 
    
    public ViewSurveyController(viewShareSurveyComponentController controller)
    {
        surveyId = Apexpages.currentPage().getParameters().get('id');
        caseId   = Apexpages.currentPage().getParameters().get('caId');
        contactId = Apexpages.currentPage().getParameters().get('cId'); 
        if(caseId ==null || caseId.length()<5){
            caseId = 'none';
        }
        if(contactId ==null || contactId.length()<5){
            contactId = 'none';
        }       
        // By default the preview is not showing up
        renderSurveyPreview = 'false';
        init();
    }
    
    
    public void init()
    {
        if (surveyId != null)
        { 
            // Retrieve all necessary information to be displayed on the page
            allQuestions = new List<question>();
            setupQuestionList();
            setSurveyNameAndThankYou(surveyId);
            anonymousOrUser = new List<SelectOption>();
            anonymousOrUser.add(new SelectOption('Anonymous',System.Label.LABS_SF_Anonymous));
            anonymousOrUser.add(new SelectOption('User','User ' + UserInfo.getFirstName() + ' ' + UserInfo.getLastName()));
            anonymousAnswer = 'Anonymous';
            isInternal =true;
            newOrder = new List<String>();
            String urlBase = URL.getSalesforceBaseUrl().toExternalForm();
            baseURL = urlBase;
            
            userId = UserInfo.getUserId();
            userName = UserInfo.getName();
            
            String profileId = UserInfo.getProfileId();
            try
            {
                Profile p = [select Id, UserType from Profile where Id=:profileId];
                if (p.UserType == 'Guest')
                {
                    isInternal = false;
                }
                else
                {
                    isInternal = true;
                }
            }
            catch (Exception e){
                isInternal = false;
            }
            thankYouRendered=false;
        }       
    }
      
    /* Called during the setup of the page. 
       Retrieve questions and responses from DB and inserts them in 2 lists. */
    public Integer setupQuestionList()
    {
        getAQuestion();
        return allQuestions.size();
    }
    
    
   /** Sets the survey's name variable
    *  param: sID   The survey ID as specified in the DB
    */
    public void setSurveyNameAndThankYou(String sId)
    {
        Survey__c s = [SELECT Name, Id, URL__c, Thank_You_Text__c, thankYouText__c, thankYouLink__c, Survey_Header__c, Survey_Container_CSS__c 
                        FROM Survey__c 
                        WHERE Id =:sId];
        surveyName = s.Name;
        if(String.IsNotBlank(s.Survey_Header__c))
        {
            surveyHeader = s.Survey_Header__c.replaceAll('<[^>]*>', '').trim();
        }
        
        templateURL = s.URL__c+'id='+sId;//+'&cId={!Contact.Id}'+'&caId='+'{!Case.id}';
        if(String.IsNotBlank(s.Survey_Header__c))
        {
            surveyThankYouText = s.Thank_You_Text__c.replaceAll('<[^>]*>', '').trim();
        }
        
        if (surveyThankYouText == null)
        {
            surveyThankYouText = System.Label.LABS_SF_Survey_Submitted_Thank_you;
        }
        surveyThankYouURL = s.thankYouLink__c;
        surveyContainerCss = s.Survey_Container_CSS__c;
    }
    
    //------------------------------------------------------------------------------//   
    public Pagereference updateSurveyName()
    {
        Survey__c s = [SELECT Name, Id, URL__c, thankYouText__c, thankYouLink__c FROM Survey__c WHERE Id =:surveyId];
        s.Name = surveyName;
        try
        {
            update s;
        }catch (Exception e)
        {
        Apexpages.addMessages(e);
        }
        return null;
    } 
    //------------------------------------------------------------------------------//      
    public Pagereference updateSurveyThankYouAndLink()
    {
        Survey__c s = [SELECT Name, Id, URL__c, thankYouText__c, thankYouLink__c FROM Survey__c WHERE Id =:surveyId];
        s.thankYouText__c = surveyThankYouText;
        s.thankYouLink__c = surveyThankYouURL;
        try{
            update s;
        }catch(Exception e){
            Apexpages.addMessages(e);
        }
        return null;
    }
  //------------------------------------------------------------------------------//    
  /** When requested from the page - when the user clicks on 'Update Order' -
      this function will reorganize the list so that it is displayed in the new order
   */
    public Pagereference refreshQuestionList()
    {
        setupQuestionList();
        return null;
    }
 
    //------------------------------------------------------------------------------//      
    //------------------------------------------------------------------------------//    
    private static boolean checkRequired(String response, Survey_Question__c question)
    {
        if(question.Required__c == true)
        {
            if(response == null || response =='NO RESPONSE')
                return false;
        }
        return true;
    } 

    /** Redirects the page that displays the detailed results of the survey, 
       from all users who took the survey.
    */
    public PageReference resultPage() 
    {
        return new PageReference('/apex/ResultsPage?id='+surveyId);
    }

    //------------------------------------------------------------------------------//  
    //------------------------------------------------------------------------------//  

    /** 
    */
    public List<String> getResponses() 
    {
        List<SurveyQuestionResponse__c> qr = [Select Survey_Question__c, SurveyTaker__c, Response__c, Name From SurveyQuestionResponse__c limit 100];
        List<String> resp = new List<String>();
        for (SurveyQuestionResponse__c r : qr) 
        {
            resp.add(r.Response__c);
        }
        
        return resp;
    }  

   /** Class: question
    *  Retrieves the question information and puts it in the question object
    */      
    public class question
    {
        public List<String> selectedOptions     {get;set;}
        public List<SelectOption> singleOptions {get; set;}
        public List<SelectOption> multiOptions  {get; set;}
        public List<String> responses           {get; set;} 
        public List<String> strList             {get; set;} // The question's option as a list of string
        public List<Integer> resultsCounts      {get; set;} // The count of each response to a question's choices
        public List<SelectOption> rowOptions    {get; set;}     
        public String   name                    {get; set;}
        public String   id                      {get; set;}
        public String   question                {get; set;}
        public String   orderNumber             {get; set;}
        public String   choices                 {get; set;}
        public String   selectedOption          {get;set;}
        public Boolean  required                {get; set;}
        public String   questionType            {get; set;}    
        public String   surveyName              {get; set;}
        public String   renderFreeText          {get; set;}
        public String   renderSelectRadio       {get; set;}
        public String   renderSelectCheckboxes  {get; set;} 
        public String   renderSelectRow         {get; set;}
        public String   singleOptionsForChart   {get; set;}
        public String   qResultsForChart        {get; set;} 
        public boolean  noData                  {get; set;}
        public String   selectedAnswer          {get;set;}
      
      /** Fills up the question object
       *  param:    Survey_Question__c 
       */
        public question(Survey_Question__c sq) 
        {
            name = sq.Name;
            id = sq.Id;
            question = sq.Question__c;
            orderNumber = String.valueOf(sq.OrderNumber__c+1);
            choices = sq.Choices__c;
            required = sq.Required__c;
            questionType = sq.Type__c;
            singleOptionsForChart = ' ';
            selectedOption = '';
            selectedOptions = new List<String>();
            if (sq.Type__c=='Single Select--Vertical')
            {
                renderSelectRadio='true';
                singleOptions = stringToSelectOptions(choices);
            
                renderSelectCheckboxes='false';
                renderFreeText='false';
                renderSelectRow = 'false';
                selectedOption = '';
                selectedOptions = new List<String>();                               
            }
            else if (sq.Type__c=='Multi-Select--Vertical')
            {        
                renderSelectCheckboxes='true';
                multiOptions = stringToSelectOptions(choices);
                renderSelectRadio='false';
                renderFreeText='false';
                renderSelectRow = 'false';
                selectedOption = '';
                selectedOptions = new List<String>();
            }
            else if (sq.Type__c=='Single Select--Horizontal')
            {   
                renderSelectCheckboxes='false';
                rowOptions = stringToSelectOptions(choices);
                renderSelectRadio='false';
                renderFreeText='false';
                renderSelectRow = 'true';
                selectedOption = '';
                selectedOptions = new List<String>();     
            }
            else if (sq.Type__c=='Free Text')
            {
                renderFreeText='true';
                renderSelectRadio='false';
                renderSelectCheckboxes='false';
              renderSelectRow = 'false';
              choices='';
            }
            //responses= getResponses();
        }
      
       /** Splits up the string as given by the user and adds each option
        *  to a list to be displayed as option on the Visualforce page
        *  param: str   String as submitted by the user
        *  returns the List of SelectOption for the visualforce page
        */  
        private List<SelectOption> stringToSelectOptions(String str)
        {
            if (str == '')
            {
                return new List<SelectOption>();
            }
            strList = str.split('\n');
          
            List<SelectOption> returnVal = new List<SelectOption>();
            Integer i = 0;
            for(String s: strList)
            {
                if (s!='') 
                {    
                    if (s != 'null' && s!= null) 
                    {
                        String sBis = s.replace(' ', '%20');
                        singleOptionsForChart += s.trim()+'|';
                        
                        /*RSC2012-02-20
                        String st = s.replace (' ', '&nbsp;');
                        returnVal.add(new SelectOption(String.valueOf(i),st));
                        */
                        returnVal.add(new SelectOption(String.valueOf(i),s));
                        System.debug('*****VALUES: ' + s);
                        i++;
                    }
                }
            }
            singleOptionsForChart = singleOptionsForChart.substring(0, singleOptionsForChart.length()-1);
            return returnVal;
        }
    }
    
  /** Fills up the List of questions to be displayed on the Visualforce page
   */   
    public List<question> getAQuestion() 
    {
        qQuestion = '';
        qChoices ='';
        
        List<Survey_Question__c> allQuestionsObject = [Select s.Type__c, s.Id, s.Survey__c, s.Required__c, s.Question__c, 
                                                            s.OrderNumber__c, s.Name, s.Choices__c 
                                                        From Survey_Question__c s 
                                                        WHERE s.Survey__c =: surveyId 
                                                        ORDER BY s.OrderNumber__c];
        allQuestions = new List<question>();
        
        Double old_OrderNumber = 0;
        Double new_OrderNumber;
        Double difference = 0;
        /* Make sure that the order number follow each other (after deleting a question, orders might not do so) */
        for (Survey_Question__c q : allQuestionsObject)
        { 
            new_OrderNumber = q.OrderNumber__c;
            difference = new_OrderNumber - old_OrderNumber - 1;
            if (difference > 0) 
            {
                Double dd = double.valueOf(difference);
                Integer newOrderInt = dd.intValue();
                q.OrderNumber__c -= Integer.valueOf(newOrderInt); 
            }
            old_OrderNumber = q.OrderNumber__c;
            question theQ = new question(q);
            allQuestions.add(theQ);
        }
        allQuestionsSize = allQuestions.size();
        return allQuestions;
    }   
    
    public PageReference submitResults()
    {
        List <SurveyQuestionResponse__c> sqrList = new List<SurveyQuestionResponse__c>();
        pageErrors = '';
        
       
        
        //transcriptId = Id.valueOf(transcript);
        
        for (question q : allQuestions)
        {
            SurveyQuestionResponse__c sqr = new SurveyQuestionResponse__c();
            system.debug(logginglevel.error, 'ViewSurveyController @Survey Question ' + q);
            if (q.renderSelectRadio == 'true') //single answer
            {
                
                if (q.required &&  String.isBlank(q.selectedAnswer))
                {
                    pageErrors += 'Please fill out all required fields';
                    return null;
                }
                
                sqr.Response__c = string.valueof(q.selectedAnswer);
                system.debug('q.selectedAnswer:: ' + q.selectedAnswer);
                system.debug('q.selectedAnswer is numeric:: ' + q.selectedAnswer.isNumeric());
                system.debug('q.selectedAnswer numeric val:: ' + Integer.valueOf(q.selectedAnswer));
                sqr.Survey_Question__c = q.Id;
                sqrList.add(sqr);
            }
            else if (q.renderFreeText == 'true') // FREE TEXT
            {
                if (q.required && String.isBlank(q.selectedAnswer))
                {
                    pageErrors += 'Please fill out all required fields';
                    return null;
                }
                System.debug('*****Select Radio ' + q.choices);
                
                //sqr.Response__c = q.choices;
                sqr.Response__c = string.valueof(q.selectedAnswer);
                sqr.Survey_Question__c = q.Id;
                sqrList.add(sqr);
            }
            else if (q.renderSelectCheckboxes == 'true') //Multiple answers
            {
                if (q.required && String.isBlank(q.selectedAnswer))
                {
                    Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill out all required fields'));
                    return null;
                }
                
                List<String> selectedAnswers = new List<String>();
                
                selectedAnswers = q.selectedAnswer.Split(';');
                
                for (String opt : selectedAnswers)
                {
                    sqr = new SurveyQuestionResponse__c();
                    sqr.Response__c = string.valueof(q.selectedAnswer);
                    sqr.Survey_Question__c = q.Id;
                    sqrList.add(sqr);
                }
            }
            else if (q.renderSelectRow == 'true') // SINGLE ANSWER
            {
                if (q.required && String.isBlank(q.selectedAnswer))
                {
                    pageErrors += 'Please fill out all required fields';
                    return null;
                }
                
                sqr.Response__c = string.valueof(q.selectedAnswer);
                sqr.NPS_Score__c = String.isNotBlank(q.selectedAnswer) && q.selectedAnswer.isNumeric() ? Integer.valueOf(q.selectedAnswer) : null; 
                sqr.Survey_Question__c = q.Id;
                sqrList.add(sqr);
            }
        }
        if(AddSurveyTaker())
        {
            for (SurveyQuestionResponse__c sqr : sqrList)
            {
                sqr.SurveyTaker__c = surveyTakerId;
            }
            insert sqrList;
            thankYouRendered=true;      
        }
        /*if(isSite)
        {
            PageReference pageInternal;
            system.debug(logginglevel.error, 'BelongSupportHomeController @initCheckDomain redirecting ' );
            pageInternal = new PageReference('http://www.belong.com.au');
            pageInternal.setRedirect(true);
            return pageInternal;
        }*/
        return null;
    }
    
    private Boolean AddSurveyTaker()
    {
        String userId;
        
        if (surveyId == null)
        {
            return false;
        }
        
        if(caseId.toUpperCase() =='NONE'|| caseId.length()<5)
          caseId = null;
        
        if(contactId.toUpperCase() =='NONE'|| contactId.length()<5)
          contactId = null;
        
        if (anonymousAnswer != 'Anonymous')
        {
            userId = UserInfo.getUserId();
        }
        else
        {
            userId = null;
        }
        
        SurveyTaker__c st = new SurveyTaker__c();      
      
        st.Contact__c = contactId;        
        st.Survey__c = surveyId;
        st.Taken__c = 'false';
        st.Case__c = caseId;
        st.User__c = userId;
        st.Order__c = orderId;
        st.Live_Chat_Key__c = chatKey;
        st.Live_Chat_Transcript__c = transcriptId;
        insert st;  
        surveyTakerId = st.Id;
        
        return true;
    }
}