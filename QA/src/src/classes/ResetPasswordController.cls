global class ResetPasswordController{
    global static List<User> checkUser(string userEmail, string DoB) {
        date DateofBirth = date.parse(DoB);
        list<User> validUser = [SELECT id, Birthdate__c 
                                FROM user 
                                WHERE username = :userEMail 
                                AND birthdate__c = :DateofBirth
                                AND isActive = True LIMIT 1];
        if(!validUser.isempty()){
            return validUser; 
        }else {
            return NULL;
        }
    }
    @RemoteAction
    global static string resetUserPwd(String userEmail, String DOB){
        List<User> correctUser;
        string returnValue = 'failure'; 
        // validate the Email 
        String emailRegex = '([a-zA-Z0-9_\\-\\.]+)@((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})';
        Pattern MyPattern = Pattern.compile(emailRegex);
        Matcher MyMatcher = MyPattern.matcher(userEmail);
        if (!MyMatcher.matches()) {
            return 'failure';
        }
        try{ 
            correctUser = checkUser(userEmail, DoB);   
            if(NULL!=correctUser && !correctUser.isEmpty()){
                system.resetPassword(correctUser.get(0).Id, True);
                returnValue ='success';         
            }
            else{
                returnValue = 'WrongCredentials';         
            }
        } 
        catch(Exception e) {
            system.debug(e.getMessage());
            returnValue = e.getMessage();
        }
        return returnValue;
    }
}