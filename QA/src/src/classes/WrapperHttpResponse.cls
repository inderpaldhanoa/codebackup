public class WrapperHttpResponse {
	public string status;
	public string message;
	public Error[] errors;
	public Object[] results;
	public Integer statusCode;
	public WrapperHttpResponse(String status,
	                           String message,
	                           Integer statusCode,
	                           Object[] results,
	                           Error[] errors) {
		this.status = status;
		this.message = message;
		this.statusCode = statusCode;
		this.errors = errors;
		this.results = results;
	}
	public class Error {
		Integer errorCode;
		string errorDescription;
		Object logRecord;
		public Error(Integer errorCode, String errorDescription, Object logRecord) {
			this.errorCode = errorCode;
			this.errorDescription = errorDescription;
			this.logRecord = logRecord;
		}
	}
	public class Result {
		Object result;
		public Result(Object result) {
			this.result = result;
		}
	}
}