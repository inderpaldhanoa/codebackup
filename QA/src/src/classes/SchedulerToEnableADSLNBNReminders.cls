/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  @created : 22/03/2016
  @Description : Scheduler class for the bacth class EnableADSLNBNRemindersBatch
 */
global class SchedulerToEnableADSLNBNReminders implements Schedulable
{
    global void execute(SchedulableContext SC) 
    {
       try 
       { Database.executeBatch(new EnableADSLNBNRemindersBatch(), 200);} catch (Exception e){}         
    }
}