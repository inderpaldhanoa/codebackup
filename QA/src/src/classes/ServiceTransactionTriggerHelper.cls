/************************************************************************************************************
* Apex Class Name   : ServiceTransactionTriggerHelper.cls
* Version           : 1.0
* Created Date      : 20 June 2017
* Function          : Trigger helper for Service Transactions
* Modification Log  :
* Developer                 Date                Description
* -----------------------------------------------------------------------------------------------------------
* Tom Clayton               20/06/2017          Created Class.
* Tom Clayton               28/06/2017          Ongoing updates.
************************************************************************************************************/

public with sharing class ServiceTransactionTriggerHelper
{
    public static Final String TYPES_CHANGEMSN = 'Support-Change MSN';
    public static Final String TYPES_REPLACESIM = 'Support-Replace SIM';
    public static Final String MOBILE_CASE_RECORD_TYPE = 'Mobile Support';
    public static Final String MOBILE_ASSET_RECORD_TYPE = 'Mobile';
    public static Final String ACTIVATE_FAILURE_CASE_SUBJECT = 'New Activate failed';
    public static Final String PORT_FAILURE_CASE_SUBJECT = 'Maximum port failures reached';
    public static Final String REPLACESIM_FAILURE_CASE_SUBJECT = 'Replace SIM failed';
    public static Final List<String> FAILURE_CASE_OPEN_STATUSES = new List<String> {'Open','Re-Opened','Unresolved','In Progress'};
    private static map<Id, Asset> mapServTransToAsset;
    private static map<Id, Contact> mapServTransToContact;
    private static List<Contact> lstContactsToUpdate;
    private static List<Asset> lstAssetsToUpdate;
    private static List<Asset> lstAssetsToInsert;
    private static List<Case> lstCasesToInsert;
    private static List<Service_Transactions__c> lstServiceTransactionsToUpdate;
    private static map<String, Porting_Failure_Settings__c> portFailureSettingMapping;
    private static Boolean isInitialized = false;
    private static Id caseRecTypeId;
    private static Id assetRecTypeId;
    private static Set<String> setFinalServiceTranStatus = new Set<String> {
        'Complete',
        'Rejected'
    };
    private static Map<Id, Mobile_Asset_Status_Mapping__c> mapAssetStatusMapping;
    
    public static void initializeProperties()
    {
        if(isInitialized) {
            return;
        }
        
        mapServTransToAsset             = new map<Id, Asset>();
        mapServTransToContact           = new map<Id, Contact>();
        lstContactsToUpdate             = new List<Contact>();
        lstAssetsToUpdate               = new List<Asset>();
        lstAssetsToInsert               = new List<Asset>();
        lstCasesToInsert                = new List<Case>();
        lstServiceTransactionsToUpdate  = new List<Service_Transactions__c>();
        mapAssetStatusMapping           = new map<Id, Mobile_Asset_Status_Mapping__c>();
        portFailureSettingMapping       = Porting_Failure_Settings__c.getAll();
        isInitialized                   = true;
        caseRecTypeId                   = (Id)GlobalUtil.getRecordTypeByName('Case').get(MOBILE_CASE_RECORD_TYPE).getRecordTypeId();
    }
    
    public static void collectAssetsAndContacts()
    {
        system.debug('-- collectAssetsAndContacts is running with lstNewServTrans: ' + trigger.new);
        List<Service_Transactions__c> lstNewServTrans = (List<Service_Transactions__c>) trigger.new;
        Set<String> setServiceTransStatus = new Set<String>();
        List<Asset> lstRelatedAssets;
        List<Contact> lstRelatedContacts;
        mapServTransToAsset = new map<Id, Asset>();
        mapServTransToContact = new map<Id, Contact>();
        map<Id, Id> mapContactIdToServTransId = new map<Id, Id>();
        map<Id, Id> mapAssetIdToServTransId = new map<Id, Id>();
        
        for(Service_Transactions__c newServTrans : lstNewServTrans) {
            // Get all the Ids for Assets which are already linked to the Service Transactions
            if(newServTrans.Asset__c != null) {
                mapAssetIdToServTransId.put(newServTrans.Asset__c, newServTrans.Id);
            }
            // Get all the Contact Ids
            if(newServTrans.Primary_Contact__c != null) {
                mapContactIdToServTransId.put(newServTrans.Primary_Contact__c, newServTrans.Id);
            }
            // For asset mapping, note which service transaction statuses are actually present
            // The contains check essentially asks if the Service Transaction Status__c is "Complete" or "Rejected"
            if (setFinalServiceTranStatus.contains(newServTrans.Status__c)) {
                // Add it to setServiceTransStatus
                setServiceTransStatus.add(newServTrans.Status__c);
            }
        }
        
        // Convert the set of Asset Ids into a comma separated string of Ids (for use in the GlobalUtil getRecords)
        List<Id> theIds = new List<Id>(mapAssetIdToServTransId.keySet());
        String idsToQuery = String.join(theIds, ',');
        // Query all the Assets with IDs in the map of existing linked Assets
        lstRelatedAssets = (List<Asset>) GlobalUtil.getSObjectRecords('Asset', 'Id', idsToQuery, NULL);
        for(Asset currentAsset : lstRelatedAssets) {
            // Put those assets in a map by Service Transaction Id
            mapServTransToAsset.put(mapAssetIdToServTransId.get(currentAsset.Id), currentAsset);
        }
        
        // Convert the set of Contact Ids into a comma separated string of Ids (for use in the GlobalUtil getRecords)
        theIds = new List<Id>(mapContactIdToServTransId.keySet());
        idsToQuery = String.join(theIds, ',');
        // Query all the Contacts which match Service Transactions
        lstRelatedContacts = (List<Contact>) GlobalUtil.getSObjectRecords('Contact', 'Id', idsToQuery, NULL);
        for(Contact currentContact : lstRelatedContacts) {
            // Put those Contacts in a map by Service Transaction Id
            mapServTransToContact.put(mapContactIdToServTransId.get(currentContact.Id), currentContact);
        }
        
        // Fetch only the rows of the Asset Mapping custom setting which we need
        mapAssetStatusMapping = new Map<id, Mobile_Asset_Status_Mapping__c>([SELECT id, Asset_Status__c, Service_Transaction_Status__c, Final_Status__c
                                                                        FROM Mobile_Asset_Status_Mapping__c
                                                                        WHERE Final_Status__c in :setServiceTransStatus]);
    }
    
    public static void fixEmptyAssetRelationships() {
        system.debug('-- fixEmptyAssetRelationships is running with lstNewServTrans: ' + trigger.new);
        // Try to add Assets for Service Transactions which were missing them
        List<Service_Transactions__c> lstNewServTrans = (List<Service_Transactions__c>) trigger.new;
        map<Id, Id> mapContactIdToServTransId = new map<Id, Id>();
        List<Asset> lstMissingAssets;
        
        for(Service_Transactions__c newServTrans : lstNewServTrans) {
            if(newServTrans.Asset__c == null && newServTrans.Primary_Contact__c != null) {
                mapContactIdToServTransId.put(newServTrans.Primary_Contact__c, newServTrans.Id);
            }
        }
        List<String> lstInServiceStatuses = Label.MOBILE_IN_SERVICE_ASSET_STATUS.split(',');
        Set<String> setInserviceStatuses = new Set<String>(lstInServiceStatuses);
        if(!mapContactIdToServTransId.isEmpty()) {
            lstMissingAssets = [SELECT Id, ContactId
                                FROM Asset
                                WHERE ContactId IN: mapContactIdToServTransId.keySet()
                                AND Status IN: setInserviceStatuses];
            
            if(!lstMissingAssets.isEmpty()) {
                for(Asset a : lstMissingAssets) {
                    // Update the values in the map so the Service Transactions have Assets when they are saved
                    trigger.newMap.get(mapContactIdToServTransId.get(a.ContactId)).put('Asset__c', a.Id);
                }
            }
        }
    }
    
    // Update Assets as appropriate when Service Transactions have certain status
    public static void processServTransAndAsset(sObject oldSObj, sObject newSObj)
    {
        system.debug('-- processServTransAndAsset is running with newSObj: ' + newSObj);
        Service_Transactions__c newServiceTrans = (Service_Transactions__c) newSObj;
        Service_Transactions__c oldServiceTrans = oldSObj != null ? (Service_Transactions__c) oldSObj : null;
        List<Asset> lstRelatedAssets;
        Contact relatedContact;
        Asset relatedAsset;
        Service_Transactions__c servTransToUpdate;
        
        // Hold the Contact we found
        if( mapServTransToContact.containsKey(newServiceTrans.Id) ) {
            relatedContact = mapServTransToContact.get(newServiceTrans.Id);
        }
        // Hold the Asset we found
        if( mapServTransToAsset.containsKey(newServiceTrans.Id) ) {
            relatedAsset = mapServTransToAsset.get(newServiceTrans.Id);
        }
        servTransToUpdate = new Service_Transactions__c(Id = newServiceTrans.Id);
        
        // Find the and update the Assets which will need update based on their Service Transactions
        system.debug('-- Got here, the related asset was: ' + relatedAsset);
        if(relatedAsset != null) {
            String serviceTransactionTypes =  newServiceTrans.Type__c + '-' + newServiceTrans.Sub_Type__c;
            // The contains check essentially asks if the Service Transaction Status__c is "Complete" or "Rejected"
            system.debug('-- Got here, will compare with set: ' + setFinalServiceTranStatus);
            if (setFinalServiceTranStatus.contains(newServiceTrans.Status__c)) {
                For(Mobile_Asset_Status_Mapping__c eachMapping: mapAssetStatusMapping.values()) {
                    // Using "contains" because the custom setting field can't fit the whole length of the type concatenation
                    if (serviceTransactionTypes.containsIgnoreCase(eachMapping.Service_Transaction_Status__c)) {
                        relatedAsset.Status = eachMapping.Asset_Status__c;
                        break;
                    }
                }
                // Special case: If change MSN - we will update Asset and Contact with new MobileNo
                system.debug('-- Got here but types: ' + serviceTransactionTypes + ', and status: ' + newServiceTrans.Status__c);
                if(serviceTransactionTypes == TYPES_CHANGEMSN && newServiceTrans.Status__c == 'Complete' && newServiceTrans.Previous_Mobile_No__c != null && newServiceTrans.Mobile_No__c != null) {
                    // Update Contact with new Number if needed
                    if(relatedContact != null && relatedContact.MobilePhone == String.valueOf(newServiceTrans.Previous_Mobile_No__c).deleteWhiteSpace())
                    {
                        relatedContact.MobilePhone = String.valueOf(newServiceTrans.Mobile_No__c).deleteWhiteSpace();
                        lstContactsToUpdate.add(relatedContact);
                    }
                    // Update Asset with new Numbers
                    relatedAsset.Mobile_Number__c = String.valueOf(newServiceTrans.Mobile_No__c).deleteWhiteSpace();
                    servTransToUpdate.Previous_Mobile_No__c = newServiceTrans.Mobile_No__c;
                    lstServiceTransactionsToUpdate.add(servTransToUpdate);
                }
                // Special case: If replace SIM - we will make existing Asset as Replaced, and Activate new Asset
                else if(serviceTransactionTypes == TYPES_REPLACESIM && newServiceTrans.Status__c == 'Complete') {
                    relatedAsset.Status = 'Replaced';
                    createOrActivateNewAsset(relatedContact, newServiceTrans);
                }
                lstAssetsToUpdate.add(relatedAsset);
            }
        }
    }
    
    public static void createOrActivateNewAsset(Contact relatedContact, Service_Transactions__c newServiceTrans)
    {
        system.debug('-- createOrActivateNewAsset is running with newServiceTrans: ' + newServiceTrans);
        List<Asset> lstFoundNewAssets = new List<Asset>();
        Asset newAsset = new Asset();
        if(assetRecTypeId == null) {
            assetRecTypeId  = (Id)GlobalUtil.getRecordTypeByName('Asset').get(MOBILE_ASSET_RECORD_TYPE).getRecordTypeId();
        }
        
        lstFoundNewAssets = [SELECT Id, Name
                            FROM Asset
                            WHERE ContactId =: relatedContact.Id
                            AND SIM__c =: newServiceTrans.New_SIM_Number__c];
                            
        if(!lstFoundNewAssets.isEmpty()) {
            newAsset = lstFoundNewAssets[0];
            newAsset.Status = 'Active';
            lstAssetsToUpdate.add(newAsset);
        }
        else {
            newAsset.Status = 'Active';
            newAsset.Name = 'Mobile';
            newAsset.ContactId = relatedContact.Id;
            newAsset.AccountId = relatedContact.AccountId;
            newAsset.RecordTypeId = assetRecTypeId;
            newAsset.SIM__c = newServiceTrans.New_SIM_Number__c;
            newAsset.Mobile_Number__c = newServiceTrans.Mobile_No__c;
            newAsset.Primary_Mobile__c = true;
            newAsset.Port_Activation_Date__c = newServiceTrans.Completion_Date__c;
            lstAssetsToInsert.add(newAsset);
        }
    }
    
    // Update failure counts and create Assets if necessary
    public static void processFailures(sObject newSObj)
    {
        system.debug('-- processFailures is running with newSObj: ' + newSObj);
        Service_Transactions__c newServiceTrans = (Service_Transactions__c) newSObj;
        Service_Transactions__c servTransToUpdate;
        
        if((newServiceTrans.Type__c == 'Activate' || newServiceTrans.Type__c == 'Support') && (newServiceTrans.Status__c == 'Rejected' || newServiceTrans.Status__c == 'Cancelled'))
        {
            if(newServiceTrans.Sub_Type__c == 'New')
            {
            	List <Service_Transactions__c> lstRecentFailures = findRecentFailures(newServiceTrans.Primary_Contact__c, newServiceTrans.Type__c, newServiceTrans.Sub_Type__c);
                
                // If there are failure (Rejected) Service Transactions in the last 30 days
                if( !lstRecentFailures.isEmpty() ) {
                	// Store the count of failures
                	servTransToUpdate = new Service_Transactions__c(Id = newServiceTrans.Id, New_Fail_Count__c = lstRecentFailures.size() );
                	lstServiceTransactionsToUpdate.add(servTransToUpdate);
                }
                
                // FOR REVIEW: Using this query within the trigger may not be bulkified (Not a serious concern because there will not be bulk Service Transactions, let alone with these failure criteria)
                List <Case> lstOtherCases = findRecentCases(newServiceTrans.Primary_Contact__c, ACTIVATE_FAILURE_CASE_SUBJECT);
                
                // If there are no other Cases logged
                if( lstOtherCases.isEmpty() )
                {
                    // Log a Case for this as it is the first Activate-New Failure for this customer
                    createFailureCase(newServiceTrans, ACTIVATE_FAILURE_CASE_SUBJECT);
                }
            }
            else if (newServiceTrans.Sub_Type__c == 'Port')
            {
                // FOR REVIEW: Using this query in "findRecentFailures()" within the trigger may not be bulkified (Not a serious concern because there will not be bulk Service Transactions, let alone with these failure criteria)
                List <Service_Transactions__c> lstRecentFailures = findRecentFailures(newServiceTrans.Primary_Contact__c, newServiceTrans.Type__c, newServiceTrans.Sub_Type__c);
                
                // If there are failure (Rejected) Service Transactions in the last 30 days
                if( !lstRecentFailures.isEmpty() ) {
                	// Store the count of failures
                	servTransToUpdate = new Service_Transactions__c(Id = newServiceTrans.Id, Port_Fail_Count__c = lstRecentFailures.size() );
                	lstServiceTransactionsToUpdate.add(servTransToUpdate);
                    // If the one inserted just now has a reject code in the custom setting 
                    if( portFailureSettingMapping.keySet().contains(newServiceTrans.Reject_code__c) ) {
                        // Use the custom setting to decide whether the number of failures requires creation of a Case
                        if( lstRecentFailures.size() >= integer.valueOf(portFailureSettingMapping.get(newServiceTrans.Reject_code__c).Num_Failure_To_Raise_Case__c) ) {
                        	// Check if there is already a Case open for this
            				List <Case> lstOtherCases = findRecentCases(newServiceTrans.Primary_Contact__c, PORT_FAILURE_CASE_SUBJECT);
                            if( lstOtherCases.isEmpty() ) {
                            	createFailureCase(newServiceTrans, PORT_FAILURE_CASE_SUBJECT);
                            }
                        }
                    }
                    // Otherwise we just create a case at the first failure
                    else if( lstRecentFailures.size() == 1 )
                    {
                        createFailureCase(newServiceTrans, PORT_FAILURE_CASE_SUBJECT);
                    }
                }
            }
            else if (newServiceTrans.Sub_Type__c == 'Replace SIM')
            {
            	// Check if there is already a Case open for this
            	List <Case> lstOtherCases = findRecentCases(newServiceTrans.Primary_Contact__c, REPLACESIM_FAILURE_CASE_SUBJECT);
                
                // If there are no other Cases logged
                if( lstOtherCases.isEmpty() )
                {
                    // Log a Case for this as it is the first Support-Replace SIM Failure for this customer
                    createFailureCase(newServiceTrans, REPLACESIM_FAILURE_CASE_SUBJECT);
                }
            }
        }
    }
    
    public static List<Service_Transactions__c> findRecentFailures(Id primaryContact, String type, String subType)
    {
        system.debug('-- findRecentFailures is running with primaryContact: ' + primaryContact);
        List<Service_Transactions__c> lstRecentFailures = new List<Service_Transactions__c>();
        
        lstRecentFailures = [SELECT Id, Type__c, Sub_Type__c, Status__c, Primary_Contact__c, CreatedDate, Reject_code__c, Reject_Reason__c
                            FROM Service_Transactions__c
                            WHERE Type__c =: type
                            AND Sub_Type__c =: subType
                            AND (Status__c = 'Rejected' OR Status__c = 'Cancelled')
                            AND Primary_Contact__c =: primaryContact
                            AND CreatedDate = LAST_N_DAYS:30];
        
        return lstRecentFailures;
    }
    
    public static List<Case> findRecentCases(Id primaryContact, String subject)
    {
        List<Case> lstRecentCases = new List<Case>();
        
        lstRecentCases = [SELECT Id, RecordTypeId, Subject
	                        FROM Case
	                        WHERE RecordTypeId =: caseRecTypeId
	                        AND ContactId =: primaryContact
	                        AND Status IN: FAILURE_CASE_OPEN_STATUSES
	                        AND Subject =: subject];
        
        return lstRecentCases;
    }
    
    public static void createFailureCase(Service_Transactions__c newServiceTrans, String subject)
    {
        system.debug('-- createFailureCase is running with newServiceTrans: ' + newServiceTrans);
        Case caseToCreate = new Case();
        
        caseToCreate.ContactId = newServiceTrans.Primary_Contact__c;
        caseToCreate.AccountId = newServiceTrans.Account__c;
        // Using different values here depending on whether it is for Port, New or ReplaceSIM
        caseToCreate.Subject = subject;
        caseToCreate.Description = newServiceTrans.Sub_Type__c.equals('Port') ? 'A reject code of ' +  newServiceTrans.Reject_code__c + ' (Reason: ' + newServiceTrans.Reject_Reason__c + ') was recorded.' : 'An Activation or Replacement for the service failed.';
        caseToCreate.RecordTypeId = caseRecTypeId;
        caseToCreate.Status = MobileConstants.NEW_CASE_STATUS;
        caseToCreate.Origin = MobileConstants.NEW_CASE_ORIGIN;
        caseToCreate.Category__c = 'Other support';
        caseToCreate.Parent_Category__c = 'Fix a Problem';
        caseToCreate.Assigned_Queue__c = 'Troubleshooting';
        
        lstCasesToInsert.add(caseToCreate);
    }
    
    public static void insertCases()
    {
        if(!lstCasesToInsert.isEmpty())
        {
            insert lstCasesToInsert;
        }
    }
    
    public static void updateContacts()
    {
        if(!lstContactsToUpdate.isEmpty())
        {
            update lstContactsToUpdate;
        }
    }
    
    public static void updateAssets()
    {
        if(!lstAssetsToUpdate.isEmpty())
        {
            update lstAssetsToUpdate;
        }
    }
    
    public static void insertAssets()
    {
        if(!lstAssetsToInsert.isEmpty())
        {
            insert lstAssetsToInsert;
        }
    }
    
    public static void updateServiceTransactions()
    {
        if(!lstServiceTransactionsToUpdate.isEmpty())
        {
            update lstServiceTransactionsToUpdate;
        }
    }
}