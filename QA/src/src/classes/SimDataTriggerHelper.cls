public class SimDataTriggerHelper{
    public static map<string,GD_Card_Value__c> cardValueMap;
    
    public static void cacheBefore(){
        cardValueMap=GD_Card_Value__c.getAll(); 
    }
    
    public static void mapResponseCode(sObject simData){
        sim_data__c rt = (sim_Data__c)simData;
        if((null != rt.Product_Code_SKU__c && cardValueMap.containsKey(rt.Product_Code_SKU__c)) && rt.Transaction_Type__c!='Reverse Activation'){
            rt.Card_Value__c=cardValueMap.get(rt.Product_Code_SKU__c).Card_Value__c;
            rt.Miscellaneous_Credit_Type__c=cardValueMap.get(rt.Product_Code_SKU__c).Miscellaneous_Credit_Type__c;
            rt.Retail_Plan_Id__c=cardValueMap.get(rt.Product_Code_SKU__c).Retail_Plan_Id__c;
            
        }  
    }
    //Girish changes, read comments
   public static void blackhawknull(sObject simData, sObject simDataold){
  // insert new Account();
        sim_data__c rt = (sim_Data__c)simData;
        sim_data__c rt_old = (sim_Data__c)simDataold;
        if(rt.Transaction_Type__c=='Reverse Activation'){
             if(rt_old.Sim_Status__c=='Available for purchase'){
                                      //Just return Error codes, put custom setting to map error code with erro message. At this moment they are not concerned
               //about proper message but they may come back to have proper error message against each error code.
               //card_value__c is already coming in API Call.
                rt.Transaction_Type__c.addError('16');
             }
                //else if(null!=rt_old.Transaction_Amount__c && rt.Transaction_Amount__c!=rt.Card_Value__c) {}
               else if(rt.Transaction_Amount__c!=rt.Card_Value__c || rt.Transaction_Currency_Code__c !='036') {
               //Just return Error codes, put custom setting to map error code with erro message. At this moment they are not concerned
               //about proper message but they may come back to have proper error message against each error code.
               //card_value__c is already coming in API Call.
                         rt.addError('13');
                 }
             else if((rt_old.Retrieval_Reference_Number__c!= rt.Retrieval_Reference_Number__c || 
                   (rt_old.Acquiring_Institution_Identifier__c !=rt.Acquiring_Institution_Identifier__c ))||
                     (rt_old.Merchant_Identifier__c !=rt.Merchant_Identifier__c || 
                        rt_old.Merchant_Terminal_Id__c != rt.Merchant_Terminal_Id__c ) || 
                        (rt_old.Processing_Code__c!= rt.Processing_Code__c || 
                        rt_old.Local_Transaction_DateTime__c !=rt.Local_Transaction_DateTime__c )) {
                         //Just return Error codes, put custom setting to map error code with erro message. At this moment they are not concerned
               //about proper message but they may come back to have proper error message against each error code.
               //card_value__c is already coming in API Call.
                            rt.adderror('12');    
                   } 
                 else{
                  if((rt.Sim_Status__c != rt_old.Sim_Status__c) && (rt.Sim_Status__c=='Available for purchase')&& (rt.Transaction_Type__c == 'Reverse Activation')){
                        rt.Processing_Code__c='';
                        rt.Retrieval_Reference_Number__c='';
                        rt.Point_Of_Service_Entry_Mode__c='';
                        rt.Network_Management_Code__c='';
                        rt.Transaction_Currency_Code__c='';
                        rt.Transaction_Amount__c='000000000000';
                        rt.Transmission_DateTime__c=Null;
                        rt.Purchase_DateTime__c=Null;
                        rt.Product_Id__c='';
                        rt.Acquiring_Institution_Identifier__c='';
                        rt.Merchant_Terminal_Id__c='';
                        rt.Merchant_Category_Code__c='';
                        rt.Merchant_Location__c='';
                        rt.Merchant_Identifier__c='';
                        rt.System_Trace_Audit_Number__c='';
                        rt.Local_Transaction_DateTime__c=Null;
                        rt.Reference_Transaction_Id__c='';
                    } 
                    
            }
        }
        else if(rt.Transaction_Type__c=='Activation'){
            
            if(rt.Transaction_Amount__c!=rt.Card_Value__c || rt.Transaction_Currency_Code__c !='036'){
            rt.addError('13');  
            }
            else if(rt_old.Sim_Status__c=='Purchased'){
            rt.addError('21');  
            }
        }
        else if(rt.Transaction_Type__c=='SAF Activation'){
            
            if(rt.Transaction_Amount__c!=rt.Card_Value__c || rt.Transaction_Currency_Code__c !='036'){
            rt.addError('13');  
            }
            else if(((rt_old.Sim_Status__c=='Void'|| rt_old.Sim_Status__c=='Redeemed') && rt_old.Transaction_Type__c!='SAF Activation')||
                      (rt_old.Sim_Status__c!='Available for purchase' && rt_old.Transaction_Type__c=='SAF Activation'))
            {
            rt.addError('21');  
            }
        }
        else if(rt.Transaction_Type__c=='Void Activation'){
            
            if(rt.Transaction_Amount__c!=rt.Card_Value__c || rt.Transaction_Currency_Code__c !='036'){
            rt.addError('13');  
            }
            else if(rt_old.Sim_Status__c=='Redeemed'){
            rt.addError('04');  
            }
            else if((rt_old.Sim_Status__c=='Void'|| rt_old.Sim_Status__c=='Available for purchase')){
            rt.addError('16');  
            }
             else if((rt_old.Transaction_Unique_Id__c!= rt.Reference_Transaction_Id__c))
                    {
                     
                            rt.adderror('12');    
                   } 
            else{
                  if((rt.Sim_Status__c != rt_old.Sim_Status__c) && (rt.Sim_Status__c=='Void')&& (rt.Transaction_Type__c == 'Void Activation')){
                        
                        rt.Transaction_Amount__c='000000000000';
                        
                    } 
                    
            }
        }
        else if(rt.Transaction_Type__c=='Reverse void activation'){
            
            if(rt.Transaction_Amount__c!=rt.Card_Value__c || rt.Transaction_Currency_Code__c !='036'){
            rt.addError('13');  
            }
            else if(rt_old.Sim_Status__c!='Void'){
            rt.addError('16');  
            }
            else if((rt_old.Retrieval_Reference_Number__c!= rt.Retrieval_Reference_Number__c || 
                   (rt_old.Acquiring_Institution_Identifier__c !=rt.Acquiring_Institution_Identifier__c ))||
                     (rt_old.Merchant_Identifier__c !=rt.Merchant_Identifier__c || 
                        rt_old.Merchant_Terminal_Id__c != rt.Merchant_Terminal_Id__c ) ||  
                        (rt_old.Local_Transaction_DateTime__c !=rt.Local_Transaction_DateTime__c )) {
                         //Just return Error codes, put custom setting to map error code with erro message. At this moment they are not concerned
               //about proper message but they may come back to have proper error message against each error code.
               //card_value__c is already coming in API Call.
                            rt.adderror('12');    
                   } 
        }
    }
}