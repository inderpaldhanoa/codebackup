public with sharing class MobileRestCaseAPIHelper
{
    //All constants
    Public static Final String HTTP_SUCCESS = 'Success';
    Public static Final String HTTP_ERROR = 'Error';
    Public static Final String STATUS_EXCEPTION = 'Exception';
    Public static Lead mobileLead;
    Public static WrapperServiceTransaction wrapperServTrans;
    Public static WrapperCaseWithAttachment wrapperInput;
    Public static WrapperCaseCommentWithAttachment wrapperCommentInput;
    Public static List<sObject> listSObjectUpdate = new List<sObject>();
    public static Object[] results ;
    public static final Integer STATUS_BAD = RestUtility.STATUS_BAD;
    public static final Integer STATUS_OK = RestUtility.STATUS_OK;
    public static final Integer STATUS_ISE = RestUtility.STATUS_ISE;
    public static final String IVR_UNKNOWN_CONTACT = 'Mobile Unknown';
    public static Final List<String> CASE_OPEN_STATUSES = new List<String> {'Open','Re-Opened','Unresolved','In Progress'};

    /*
    @input  : GET request with Octane Id, recordTypeNames, Maximum Number of Results, Starting Offset
                The recordTypeNames is a string with comma separated values so there can be multiple record types.
    @output : A list of Cases for the provided Octane Id, range and record type
    @Description : Helper method for the HTTP GET method for RESTMobileAPI.cls
    */
    public static WrapperHTTPResponse getCasesHelper(Map<String, String> mapParameters, String recordTypeNames) {
        // Declare variables
        List<Case> lstCases = new List<Case>();
        integer intMaxNumOfResults, intStartOffset;
        String strOctaneId, whereClause, strRecTypeIds;
        list<String> lstRecordTypeNames = new list<String>();
        list<Id> lstRecTypeIds = new list<Id>();
        
        try {
            // Get the Record Type Ids
            if ( recordTypeNames != null ) {
                lstRecordTypeNames = recordTypeNames.split(',');
                // Make a list of record type Ids by getting them from the Names
                for(string recTypeName : lstRecordTypeNames) {
                    lstRecTypeIds.add( (Id)GlobalUtil.getRecordTypeByName('Case').get(recTypeName).getRecordTypeId() );
                }
                // Make a string which can be used in the query's IN clause
                strRecTypeIds = '(\'' +
                                + String.join(lstRecTypeIds, '\', \'') +
                                + '\')';
            }
            // If the parameters include the required fields
            if (mapParameters.containsKey('octaneId') && !String.isEmpty(mapParameters.get('octaneId')) && mapParameters.containsKey('maxNumOfResults')
                    && mapParameters.containsKey('startOffset') && !lstRecTypeIds.isEmpty()) {
                // Get the Octane Id
                strOctaneId = (String) mapParameters.get('octaneId');
                // Get the numbers
                intMaxNumOfResults = integer.valueOf(mapParameters.get('maxNumOfResults'));
                intStartOffset = integer.valueOf(mapParameters.get('startOffset'));
                // Find the Cases for the provided Octane Id
                if (!string.isEmpty(strRecTypeIds)) {
                    // Octane_Customer_Number__c works for Mobile because it is a formula which chooses either Mobile or Fixed from Account, based on the Case record type
                    whereClause = ' Octane_Customer_Number__c = \'' + strOctaneId + '\'' +
                                  ' AND RecordTypeId IN ' + strRecTypeIds +
                                  ' ORDER BY Status, System_Last_Modified__c' +
                                  ' LIMIT ' + intMaxNumOfResults +
                                  ' OFFSET ' + intStartOffset;
                    lstCases = (List<Case>)GlobalUtil.getSObjectRecords('Case', '', '', whereClause);
                }
                // If we have found relevant records
                MobileRestUtility.status = HTTP_SUCCESS; MobileRestUtility.statuscode = STATUS_OK;
                if (!lstCases.isEmpty()) {
                    MobileRestUtility.message = 'Successfully found ' + lstCases.size() + ' Case(s) for the Octane Id (in the requested range and Record Type).';
                }
                else {
                    MobileRestUtility.message = 'There are no further Cases in Salesforce for the Octane Id (in the requested range and Record Type).';
                }
            }
            else {
                MobileRestUtility.message = 'Error: Invalid parameters provided. Make sure all parameters are correct (octaneId, maxNumOfResults, startOffset).';
                MobileRestUtility.returnWithError('getCasesHelper', JSON.serialize(mapParameters));
                //MobileRestUtility.status = HTTP_ERROR;
                //MobileRestUtility.statuscode = STATUS_BAD;
            }
        }
        catch (Exception e) {
            System.debug('Exception!' + e);
            MobileRestUtility.setExceptionResponse(e, 'getCasesHelper', NULL);
        }
        return RestUtility.getResponse(MobileRestUtility.status, MobileRestUtility.message, MobileRestUtility.statuscode, lstCases, MobileRestUtility.logs);
    }

    /*
    @input  : POST request with Case Id, Comment
    @output : Confirmation that the specified Case has been re-opened and given a comment
    @Description : Helper method for the HTTP POST method for RESTMobileAPI.cls
    */
    public static WrapperHTTPResponse commentCaseHelper(String jsonInput) {
        // Declare variables
        List<Case> lstCaseToUpdate = new List<Case>();
        List<User> lstUserWithOctaneId = new List<User>();
        List<WrapperCaseCommentWithAttachment.fileData> lstAttachedFiles = new List<WrapperCaseCommentWithAttachment.fileData>();
        Savepoint sp = Database.setSavepoint();
        try {
            // Get the values out of the JSON string into a map
            wrapperCommentInput = WrapperCaseCommentWithAttachment.parseComment(jsonInput);
            // Get list of attachments
            if (wrapperCommentInput.fileData != null) {
                for (WrapperCaseCommentWithAttachment.fileData currentAttachment : wrapperCommentInput.fileData) {
                    if (currentAttachment.fileName != null) {
                        lstAttachedFiles.add(new WrapperCaseCommentWithAttachment.fileData(currentAttachment.fileName, currentAttachment.fileBlob));
                    }
                }
            }
            // If the JSON has the required fields
            if (wrapperCommentInput.caseId != null && wrapperCommentInput.comment != null) {
                // Validate the ID: If Case ID is in the right format and points to the right object
                if (GlobalUtil.isValidId(wrapperCommentInput.caseId) && GlobalUtil.getObjectNameFromId((Id) wrapperCommentInput.caseId) == 'Case') {
                    // Find the Cases for the provided Octane Id
                    lstCaseToUpdate = (List<Case>) GlobalUtil.getSObjectRecords('Case', 'Id', wrapperCommentInput.caseId, NULL);
                    if (!string.isEmpty(wrapperCommentInput.octaneId)) {
                        lstUserWithOctaneId = [SELECT Id, Name FROM User WHERE Mobile_Octane_Id__c = : String.valueOf(wrapperCommentInput.octaneId)];
                    }
                    // If we have found a record
                    if (!lstCaseToUpdate.isEmpty()) {
                        // Update case
                        if (lstCaseToUpdate[0].Status == 'Closed' || lstCaseToUpdate[0].Status == 'Resolved') {
                            lstCaseToUpdate[0].Status = 'Re-Opened';
                        }
                        else
                            if (lstCaseToUpdate[0].Status == 'More Information Required') {
                                lstCaseToUpdate[0].Status = 'Information Received';
                            }
                            else {
                                lstCaseToUpdate[0].New_Comments_From_Customer__c = true;
                            }
                        // Add the feed item on its own
                        FeedItem mainFeedItem = new FeedItem();
                        mainFeedItem.Body = wrapperCommentInput.comment;
                        mainFeedItem.ParentId = lstCaseToUpdate[0].Id;
                        mainFeedItem.Title = 'Auto Feed Item';
                        mainFeedItem.visibility = 'AllUsers';
                        if (!lstUserWithOctaneId.isEmpty()) {
                            mainFeedItem.CreatedById = lstUserWithOctaneId[0].Id;
                        }
                        insert mainFeedItem;
                        // Update the Case with any status changes requried
                        update lstCaseToUpdate;
                        // If attachments then add each attachment with a content version
                        list<ContentVersion> lstContentVersions = new list<ContentVersion>();
                        list<FeedAttachment> lstFeedAttachments = new list<FeedAttachment>();
                        if ( !lstAttachedFiles.isEmpty() ) {
                            for (WrapperCaseCommentWithAttachment.fileData currentFile : lstAttachedFiles) {
                                ContentVersion contentVers = new ContentVersion();
                                contentVers.Title = currentFile.fileName;
                                contentVers.VersionData = currentFile.fileBlob;
                                contentVers.PathOnClient = currentFile.fileName;
                                lstContentVersions.add(contentVers);
                            }
                            insert lstContentVersions;
                            integer i = 0;
                            for (WrapperCaseCommentWithAttachment.fileData currentFile : lstAttachedFiles) {
                                FeedAttachment feedAttachment = new FeedAttachment();
                                feedAttachment.FeedEntityId = mainFeedItem.Id;
                                feedAttachment.RecordId = lstContentVersions[i].Id;
                                feedAttachment.Title = currentFile.fileName;
                                feedAttachment.Type = 'CONTENT';
                                lstFeedAttachments.add(feedAttachment);
                                i++;
                            }
                            insert lstFeedAttachments;
                        }
                        MobileRestUtility.status = HTTP_SUCCESS;
                        MobileRestUtility.statuscode = STATUS_OK;
                        MobileRestUtility.message = 'Successfully updated Case ' + wrapperCommentInput.caseId + ' and added a Comment.';
                    }
                    else {
                        MobileRestUtility.message = 'Error: Could not find Case ' + wrapperCommentInput.caseId;
                        MobileRestUtility.returnWithError('commentCaseHelper', jsonInput);
                        //MobileRestUtility.status = HTTP_ERROR;
                        //MobileRestUtility.statuscode = STATUS_ISE;
                    }
                }
                else {
                    MobileRestUtility.message = 'Error: Invalid Case ID provided.';
                    MobileRestUtility.returnWithError('commentCaseHelper', jsonInput);
                    //MobileRestUtility.status = HTTP_ERROR;
                    //MobileRestUtility.statuscode = STATUS_BAD;
                }
            }
            else {
                MobileRestUtility.message = 'Error: Invalid JSON provided. Make sure all parameters are correct.';
                MobileRestUtility.returnWithError('commentCaseHelper', jsonInput);
                //MobileRestUtility.status = HTTP_ERROR;
                //MobileRestUtility.statuscode = STATUS_BAD;
            }
        }
        catch (Exception e) {
            MobileRestUtility.setExceptionResponse(e, 'commentCaseHelper', jsonInput);
        }
        return RestUtility.getResponse(MobileRestUtility.status, MobileRestUtility.message, MobileRestUtility.statuscode, lstCaseToUpdate, MobileRestUtility.logs);
    }

    /*
    @input  : POST request with OctaneId or email, Subject, Description, RecordType
    @output : Confirmation a case has been created
    @Description : Helper method for the HTTP POST method for RESTMobileAPI.cls
    */
    public static WrapperHTTPResponse createCaseHelper(String jsonInput, String recordType) {
        // Declare variables
        Case caseToCreate = new Case();
        Id recTypeId;
        List<WrapperCaseWithAttachment.fileData> lstAttachedFiles = new List<WrapperCaseWithAttachment.fileData>();
        List<Contact> lstRelatedContact = new List<Contact>();
        List<User> lstUserWithOctaneId = new List<User>();
        List<Lead> lstRelatedLead = new List<lead>();
        List<Case> lstCaseToCreate = new List<Case>();
        List<Case> lstExistingCases = new List<Case>();
        String strTroubleshootQuestions, strKnowledgeArticleVersionId, strKnowledgeArticleId, strKnowledgeCategory;
        Savepoint sp = Database.setSavepoint();
        try {
            // Get the data from the JSON into the wrapper class
            wrapperInput = WrapperCaseWithAttachment.parseCase(jsonInput);
            // Get list of attachments
            if (wrapperInput.fileData != null) {
                for (WrapperCaseWithAttachment.fileData currentAttachment : wrapperInput.fileData) {
                    if (currentAttachment.fileName != null) {
                        lstAttachedFiles.add(new WrapperCaseWithAttachment.fileData(currentAttachment.fileName, currentAttachment.fileBlob));
                    }
                }
            }
            //  Get the record type Id
            if ( recordType != null ) {
                recTypeId = (Id)GlobalUtil.getRecordTypeByName('Case').get(recordType).getRecordTypeId();
            }
            // Get the Contact and / or the Lead which should be related
            if (wrapperInput.octaneId != null) {
                lstRelatedContact = GlobalUtil.getContactByOctane(wrapperInput.octaneId);
                lstUserWithOctaneId = [SELECT Id, Name FROM User WHERE Mobile_Octane_Id__c = : String.valueOf(wrapperInput.octaneId)];
            }
            if (wrapperInput.email != null && lstRelatedContact.isEmpty()) {
            	// Try to find Contact with email first, then look for Lead
                lstRelatedContact = (List<Contact>)GlobalUtil.getSObjectRecords('Contact', 'email', wrapperInput.email, NULL);
                if(lstRelatedContact.isEmpty()) {
                	lstRelatedLead = (List<Lead>)GlobalUtil.getSObjectRecords('Lead', 'email', wrapperInput.email, NULL);
                }
            }
            // If we've been provided a leadId then this is an IVR authenticated case
            if (wrapperInput.leadId != null && lstRelatedContact.isEmpty()) {
            	lstRelatedLead = (List<Lead>)GlobalUtil.getSObjectRecords('Lead', 'Id', wrapperInput.leadId, NULL);
            }
            // If unauthenticaed then either use IVR Unknown or make a new lead
            if (lstRelatedContact.isEmpty() && lstRelatedLead.isEmpty()) {
            	if (wrapperInput.isIVRCase != null && wrapperInput.isIVRCase == true ) {
            		lstRelatedContact = (List<Contact>)GlobalUtil.getSObjectRecords('Contact', 'Name', IVR_UNKNOWN_CONTACT, NULL);
            	}
            	else {
	                if (String.isEmpty(wrapperInput.lastName)) {
	                    return RestUtility.getResponse('Error', 'LastName is mandatory for Unauthenticated user', 400, NULL, MobileRestUtility.logs);
	                }
	                mobileLead = new Lead(
	                    LastName = wrapperInput.lastName,
	                    FirstName = wrapperInput.firstName,
	                    Email = wrapperInput.email,
	                    Phone = wrapperInput.phoneNumber,
	                    Lead_Type__c = MobileConstants.UNAUTH_CASE_LEAD_TYPE,
	                    Product_Type__c = MobileConstants.LEAD_PRODUCT_TYPE
	                );
	                mobileLead.Company = wrapperInput.firstName + ' ' + wrapperInput.lastName;
	                insert mobileLead;
	                lstRelatedLead.add(mobileLead);
            	}
            }
            // Get the questions from the Knowledge Article
            if (wrapperInput.knowledgeArticle != null && GlobalUtil.getObjectNameFromId((Id) wrapperInput.knowledgeArticle) == MobileConstants.KNOWLEDGE_ARTICLE_VERSION_NAME) {
                List<FAQ__KAV> lstFAQKnowledgeArticles = new List<FAQ__KAV>();
                lstFAQKnowledgeArticles = (List<FAQ__KAV>)GlobalUtil.getSObjectRecords(MobileConstants.KNOWLEDGE_ARTICLE_VERSION_NAME, 'Id', wrapperInput.knowledgeArticle, NULL);
                if (!lstFAQKnowledgeArticles.isEmpty()) {
                    strTroubleshootQuestions = lstFAQKnowledgeArticles[0].Troubleshooting_Questions__c;
                    strKnowledgeArticleVersionId = lstFAQKnowledgeArticles[0].Id;
                    strKnowledgeArticleId = lstFAQKnowledgeArticles[0].KnowledgeArticleId;
                }
            }
            // Get a map of all parents of data categories
            map<string, string> mapCategoryToParentCategory = getMapChildDataCategory();
            // Get the Data Category from the Knowledge Article
            if (wrapperInput.knowledgeArticle != null && !String.IsEmpty(strKnowledgeArticleVersionId)) {
                List <FAQ__DataCategorySelection> lstKnowledgeCategory = new List<FAQ__DataCategorySelection>();
                lstKnowledgeCategory = [SELECT DataCategoryName FROM FAQ__DataCategorySelection WHERE ParentId = : strKnowledgeArticleVersionId];
                strKnowledgeCategory = lstKnowledgeCategory[0].DataCategoryName;
            }
            // Make sure we're not being asked to create a duplicate IVR Case
            if (wrapperInput.isIVRCase != null || wrapperInput.isIVRCase == true) {
            	// If we've been given a Contact, check there aren't already open Cases for it
            	if (wrapperInput.octaneId != null) {
	            	lstExistingCases = [SELECT Id
	            						FROM Case
	            						WHERE ContactId =: lstRelatedContact[0].Id
	            						AND Parent_Category__c =: wrapperInput.parentCategory
	            						AND Category__c =: wrapperInput.Category
	            						AND Status IN: CASE_OPEN_STATUSES];
            	}
            	// If we've been given a Lead, check there aren't already open Cases for it
            	else if (wrapperInput.leadId != null) {
            		lstExistingCases = [SELECT Id
	            						FROM Case
	            						WHERE Lead__c =: lstRelatedLead[0].Id
	            						AND Parent_Category__c =: wrapperInput.parentCategory
	            						AND Category__c =: wrapperInput.Category
	            						AND Status IN: CASE_OPEN_STATUSES];
            	}
            }
            if (wrapperInput.isIVRCase == null || wrapperInput.isIVRCase == false || lstExistingCases.isEmpty()) {
	            // If the record type has been found
	            if (recTypeId != null) {
	                // Octane ID, leadId, Unknown-catch-all or email have successfully matched a Contact or Lead
	                if (!lstRelatedContact.isEmpty() || !lstRelatedLead.isEmpty()) {
	                    // If the JSON has the required fields
	                    if ((wrapperInput.octaneId != null || wrapperInput.email != null || wrapperInput.isIVRCase == true) 
	                    		&& wrapperInput.subject != null && wrapperInput.description != null 
	                    		&& !(wrapperInput.isIVRCase == true && wrapperInput.category == null)) {
	                        // If we have Id to link the Case to a Contact (authenticated)
	                        if (!lstRelatedContact.isEmpty()) {
	                            caseToCreate.ContactId = lstRelatedContact[0].Id;
	                            caseToCreate.AccountId = lstRelatedContact[0].AccountId;
	                        }
	                        // If we have the Id to link the Case to a Lead
	                        if (!lstRelatedLead.isEmpty()) {
	                            caseToCreate.Lead__c = lstRelatedLead[0].Id;
	                        }
	                        caseToCreate.Subject = wrapperInput.subject;
	                        caseToCreate.Description = strTroubleshootQuestions != null ? strTroubleshootQuestions + '\n-----------\n\n' + wrapperInput.description : wrapperInput.description;
	                        caseToCreate.RecordTypeId = recTypeId;
	                        caseToCreate.Status = MobileConstants.NEW_CASE_STATUS;
	                        caseToCreate.Origin = wrapperInput.isIVRCase != null && wrapperInput.isIVRCase == true ? MobileConstants.NEW_IVR_CASE_ORIGIN : MobileConstants.NEW_CASE_ORIGIN;
	                        // If we're dealing with an IVR case then the categories get set from input. Otherwise we look up category information from the Knowledge Article
	                        if( wrapperInput.isIVRCase != null && wrapperInput.isIVRCase == true ) {
	                        	caseToCreate.Category__c = wrapperInput.category;
	                        	caseToCreate.Parent_Category__c = wrapperInput.parentCategory;
	                        	if(wrapperInput.phoneNumber != null) {
	                        		caseToCreate.Description += ' Phone number was: ' + wrapperInput.phoneNumber;
	                        	}
	                        }
	                        else {
		                        caseToCreate.Category__c = !String.IsEmpty(strKnowledgeCategory) ? strKnowledgeCategory.replace('_', ' ') : null;
		                        system.debug('-- ================ ');
		                        for (String current : mapCategoryToParentCategory.keyset()) {
		                            system.debug('-- mapCategoryToParentCategory contains: ' + current + ' - ' + mapCategoryToParentCategory.get(current));
		                        }
		                        system.debug('-- ================ ');
		                        system.debug('-- Will be searching in mapCategoryToParentCategory for: ' + strKnowledgeCategory);
		                        caseToCreate.Parent_Category__c = mapCategoryToParentCategory.containsKey(strKnowledgeCategory) ? mapCategoryToParentCategory.get(strKnowledgeCategory) : null;
	                        }
	                        insert caseToCreate;
	                        // Add a custom feed item to describe the new Case
	                        FeedItem mainFeedItem = new FeedItem();
	                        mainFeedItem.Body = strTroubleshootQuestions != null ? 'Case created with description:\n ' + strTroubleshootQuestions + '\n-----------\n\n' + wrapperInput.description : wrapperInput.description;
	                        mainFeedItem.ParentId = caseToCreate.Id;
	                        mainFeedItem.Title = 'See below for attachments.';
	                        mainFeedItem.visibility = 'AllUsers';
	                        if (!lstUserWithOctaneId.isEmpty()) {
	                            mainFeedItem.CreatedById = lstUserWithOctaneId[0].Id;
	                        }
	                        insert mainFeedItem;
	                        // If attachments then add each attachment with a content version
	                        list<ContentVersion> lstContentVersions = new list<ContentVersion>();
	                        list<FeedAttachment> lstFeedAttachments = new list<FeedAttachment>();
	                        if (!lstAttachedFiles.isEmpty()) {
	                            for (WrapperCaseWithAttachment.fileData currentFile : lstAttachedFiles) {
	                                ContentVersion contentVers = new ContentVersion();
	                                contentVers.Title = currentFile.fileName;
	                                contentVers.VersionData = currentFile.fileBlob;
	                                contentVers.PathOnClient = currentFile.fileName;
	                                lstContentVersions.add(contentVers);
	                            }
	                            insert lstContentVersions;
	                            system.debug('-- lstAttachedFiles: ' + lstAttachedFiles);
	                            integer i = 0;
	                            for (WrapperCaseWithAttachment.fileData currentFile : lstAttachedFiles) {
	                                system.debug('-- currentFile.fileName: ' + currentFile.fileName);
	                                FeedAttachment feedAttachment = new FeedAttachment();
	                                feedAttachment.FeedEntityId = mainFeedItem.Id;
	                                feedAttachment.RecordId = lstContentVersions[i].Id;
	                                feedAttachment.Title = currentFile.fileName;
	                                feedAttachment.Type = 'CONTENT';
	                                lstFeedAttachments.add(feedAttachment);
	                                i++;
	                            }
	                            system.debug('-- lstFeedAttachments: ' + lstFeedAttachments);
	                            insert lstFeedAttachments;
	                        }
	                        // If a valid Knowledge Article ID has been provided
	                        if (!String.IsEmpty(strKnowledgeArticleId) != null && GlobalUtil.getObjectNameFromId((Id) strKnowledgeArticleId) == MobileConstants.KNOWLEDGE_ARTICLE_NAME) {
	                            CaseArticle newCaseArticle = new CaseArticle();
	                            newCaseArticle.ArticleLanguage = 'en_US';
	                            newCaseArticle.CaseId = caseToCreate.Id;
	                            newCaseArticle.KnowledgeArticleId = strKnowledgeArticleId;
	                            insert newCaseArticle;
	                        }
	                        MobileRestUtility.status = HTTP_SUCCESS;
	                        MobileRestUtility.statuscode = STATUS_OK;
	                        MobileRestUtility.message = 'Successfully created new Case: ' + caseToCreate.Id;
	                    }
	                    else {
	                        MobileRestUtility.message = 'Error: Invalid JSON provided. Make sure all parameters are correct and required parameters are not null.';
	                        MobileRestUtility.returnWithError('createCaseHelper', jsonInput);
	                    }
	                }
	                else {
	                    MobileRestUtility.message = 'Error: The provided Octane ID, leadId, Unknown-catch-all or email could not be used to match a Contact or Lead.';
	                    MobileRestUtility.returnWithError('createCaseHelper', jsonInput);
	                }
	            }
	            else {
	                MobileRestUtility.message = 'Error: Invalid Record Type provided.';
	                MobileRestUtility.returnWithError('createCaseHelper', jsonInput);
	            }
            }
	    	else {
	            MobileRestUtility.message = 'Not creating duplicate IVR Case: the same Parent Category and Category is already open for that Customer.';
	            MobileRestUtility.returnWithError('createCaseHelper', jsonInput);
	        }
            lstCaseToCreate.add(caseToCreate);
        }
        catch (Exception e) {
            MobileRestUtility.setExceptionResponse(e, 'createCaseHelper', jsonInput);
        }
        return RestUtility.getResponse(MobileRestUtility.status, MobileRestUtility.message, MobileRestUtility.statuscode, lstCaseToCreate, MobileRestUtility.logs);
    }

    public static map<String, String> mapParentChilds = new map<String, String>();
    public static Map<String, String> getMapChildDataCategory() {
        List<Schema.DataCategory> knowledge = (List<Schema.DataCategory>)GlobalUtil.getDataCategory('KnowledgeArticleVersion', 'Mobile');
        List<RecursiveParser.childCategories> dd = RecursiveParser.parse(JSON.serialize(knowledge[0].childCategories));
        Set<String> setChilds = new Set<String>();
        for (RecursiveParser.childCategories d : dd) {
            System.debug('**dddd****' + d.Name);
            System.debug('**dddd****' + d.Label);
            getchilds(d.childCategories, d.Label);
        }
        return mapParentChilds;
    }
    public static void getChilds(List<RecursiveParser.childCategories> child, String parentLabel) {
        for (RecursiveParser.childCategories d : child) {
            if (NULL != d.childCategories) {
                mapParentChilds.put(d.Name, parentLabel);
                // setChilds.add(d.Name);
                getChilds(d.childCategories, parentLabel);
            }
        }
    }
}