@isTest
private class OrderTriggersTest
{
	public static Map<String,Schema.RecordTypeInfo> accRecordTypeInfoName;
	public static Account sAccount;
    public static Order sOrder;

	@testSetup static void setupTestData()
    {
    	accRecordTypeInfoName	= GlobalUtil.getRecordTypeByName('Account');

    	sAccount                            = new Account();
        sAccount.RecordTypeId               = accRecordTypeInfoName.get('Residential').getRecordTypeId();
        sAccount.Name                       = 'Test Account';
        sAccount.Phone                      = '0456123789';
        sAccount.Customer_Status__c      = 'Active';
        insert sAccount;

        sOrder = new Order();
        sOrder.AccountId = sAccount.Id;
        sOrder.effectiveDate = Date.today();
        sOrder.status = 'New';
        insert sOrder;    
    }

    static testmethod void testOrderTrigger()
    {
        Test.startTest();
        sOrder = [Select id,Id_Hashcode__c FROM Order];        
        System.assert(String.isNotBlank(sOrder.Id_Hashcode__c));
        Test.stopTest();
    }
}