@istest
public class FutureMethodsTest {
   
    public static TestMethod void CheckFunction (){
        list<Id> userCons = new list<Id>();
        Test.startTest();
        Account Acc = new Account();
        Acc.name = 'test and Acc 33 ';
        Acc.octane_customer_number__c = '985654';
        insert Acc;
        
        contact con = new contact();
        con.firstname = 'first'; 
        con.lastname = 'last1333';
        con.accountid = Acc.id;
        con.MobilePhone = '0412345678';
        con.Phone = '0412345679';
        con.email = Acc.id+'4456@mailinator.com';
        insert con;
        
        //con.Email = Acc.id+'14456@mailinator.com';
        //update con;
               
        userCons.add(con.Id);
        FutureMethods.updateUsernamesFromContact(userCons);
        Test.stopTest();
    }
}