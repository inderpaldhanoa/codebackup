/*
@author  : Shrinivas Kalkote (shrinivas_kalkote@infosys.com)
@created : 22/11/2016.
@Description : This is ININ services class. All services which neds to be consumed from ININ are will be catered from this class. 
@modified : 21/03/2017 
@ Changes - Added extra attribute values to be consumed from ININ

Modified: 23/06/2017       by Anuja Pandey 
Changes: Modifications made to accomodate the requirements for Mobiles IVR. Changes made to methods to extend functionality for Mobiles.
*/

public class ININServices{
    
    public static final String CONST_EXTERNAL = 'External';
    public static final String CONST_COMPLETED = 'Completed';
    static String RELATIVE_PATH = '/oauth/token';
    public static AuthProperties__c oAuthProperties = GlobalUtil.getAuthPropertiesByName('ININ_OAUTH');
    public static AuthProperties__c oAuthProperties1 = GlobalUtil.getAuthPropertiesByName('ININ_API'); 
    Public static id unknownrecordId= Schema.SObjectType.Task.getRecordTypeInfosByName().get('Unknown Product Call').getRecordTypeId();

    
    public static void findOrphanInteractions(set<String> setConvId, String OAUT_TOKEN)
    {      
        Map<String, List<ININConversationWrapper>> attrbiMap = getAllConversation(setConvId, OAUT_TOKEN);
        Map<String, Contact> aniMap = new Map<String, Contact>();
        for(Contact oContact : [SELECT Id, LastName, Phone, MobilePhone, AssistantPhone, HomePhone, OtherPhone, AccountId FROM Contact WHERE Phone IN :attrbiMap.keySet() OR MobilePhone IN :attrbiMap.keySet() OR AssistantPhone IN :attrbiMap.keySet() OR HomePhone IN :attrbiMap.keySet() OR OtherPhone IN :attrbiMap.keySet()]){
            if(attrbiMap.containsKey(oContact.Phone)){
                aniMap.put(oContact.Phone,oContact); 
            }
            else if(attrbiMap.containsKey(oContact.MobilePhone)){
                aniMap.put(oContact.MobilePhone,oContact);
            }
            else if(attrbiMap.containsKey(oContact.AssistantPhone)){
                aniMap.put(oContact.AssistantPhone,oContact);
            }
            else if(attrbiMap.containsKey(oContact.HomePhone)){
                aniMap.put(oContact.HomePhone,oContact);
            }
            else if(attrbiMap.containsKey(oContact.OtherPhone)){
                aniMap.put(oContact.OtherPhone,oContact);
            }
        }
        
        List<Task> taskInsertList = new List<Task>();
        Contact oUnknownContact = [SELECT Id, AccountId FROM Contact WHERE Id = :Label.Unknown_Unknown_Contact_Id]; 
        List<ININConversationWrapper> lstININConversationWrapper;
        Contact oContact;
        Task oTask;
        for(String sPhoneNumber: attrbiMap.keySet())
        {   
            lstININConversationWrapper = attrbiMap.get(sPhoneNumber);
            oContact = aniMap.containsKey(sPhoneNumber) ? aniMap.get(sPhoneNumber):oUnknownContact;
            if(lstININConversationWrapper != null)
            {
                for(ININConversationWrapper oININConversationWrapper: lstININConversationWrapper)
                {
                    oTask = createTaskFromWrapper(oININConversationWrapper.Id, (Attributes)((ParticipantWrapper)oININConversationWrapper.participants[0]).attributes, oContact);
                    taskInsertList.add(oTask);
                }    
            }
        }
        system.debug('taskInsertList:: ' + taskInsertList.size());
        insert taskInsertList;
    }
    /*Mobile IVR: Edited this method to sdd more Mobile specific attributes
    Changed By: Anuja     Date: 07/07/2017*/
    public static Task createTaskFromWrapper(String interactionId, Attributes att, Contact oContact)
    {
        Task oTask = new Task();
        oTask.Status = CONST_COMPLETED;
        oTask.Interaction_Id__c = interactionId;
        oTask.CallObject = interactionId;
       
        oTask.WhoId = oContact.Id;
        oTask.WhatId = oContact.accountId;
        oTask.Call_ANI__c = att.Customer_ANI;
        oTask.CLI_Match__c = att.CLI_Match;
        oTask.Manual_CLI_Match__c = att.Manual_CLI_Match;
        oTask.Manual_CLI_Time__c = att.Manual_CLI_Time;
        oTask.Avalanche_DD__c = att.Avalanche_DD;
        oTask.Avalanche_time__c = att.Avalanche_time;
        oTask.Avalanche_Continue__c = att.Avalanche_Continue;
        oTask.DOB_Match__c = att.DOB_Match;
        oTask.AccountDetails_DD__c = att.AccountDetails_DD;
        oTask.AccountDetails_DD_Time__c = att.AccountDetails_DD_Time;
        oTask.Preactivation__c = att.Preactivation;
        oTask.Hear_Balance__c = att.Hear_Balance;
        oTask.Acc_Suspended__c = att.Acc_Suspended;
        oTask.Payment_Result__c = att.Payment_Result;
        oTask.BlackHawk_DD_Time__c = att.BlackHawk_DD_Time;
        oTask.Call_In__c = att.Call_In;
        oTask.Transfer_Q_or_Menu__c = att.Transfer_Q_or_Menu;
        oTask.Plan_Type__c = att.Plan_Type;
        oTask.Prepay_Result__c = att.Prepay_Result;
        oTask.Fetch_Prepay__c = att.Fetch_Prepay;
        oTask.Outage__c = att.Outage;
        oTask.Inactive_Customer__c = att.Inactive_Customer;
        oTask.CLI_Time__c = att.CLI_Time;
        oTask.Avalanche_Status__c = att.Avalanche_Status;
        oTask.Mobile_Email_Task__c = att.Mobile_Email_Task;
        oTask.Block_Number_Input__c= att.Block_Number_Input;
        oTask.Contact_DD_BlockSIM__c= att.Contact_DD_BlockSIM;
        oTask.DOB_Match_BlockSIM__c= att.DOB_Match_BlockSIM;
        oTask.Block_SIM__c= att.Block_SIM;
        oTask.Return_To_Main_Menu__c= att.Return_To_Main_Menu;
        oTask.Disconnect__c= att.Disconnect;
        oTask.Mobile_Sales__c= att.MENU;
        if(att.SF_Product!= Null){
        oTask.RecordTypeId = att.SF_Product;
        }        
        else{
        oTask.RecordTypeId = unknownrecordId;
        }
        
        return oTask;
    }
    public static void getFailedInteractions (String OAUT_TOKEN, integer pageNumb)
    {
        RestUtility rRestUtility = new RestUtility();
        Set<String> conversationsSet = new Set<String>();
        Map<String, String> mapRequestHeader = new Map<String, String>();
        if(String.isBlank(OAUT_TOKEN))
            OAUT_TOKEN = getOAuthToken();
        mapRequestHeader.put('Content-Type', 'application/json');
        mapRequestHeader.put('Authorization', 'Bearer ' + OAUT_TOKEN);
        //hardcoded Json body just for testing. should be chsnged before Go live
         String sBody = '{\"interval\": \"' + (Datetime.now().addminutes(-90).format('yyyy-MM-dd\'T\'HH:mm:ss.SSS'+ 'Z')) + '/'  +(Datetime.now().addminutes(-75).format('yyyy-MM-dd\'T\'HH:mm:ss.SSS'+ 'Z')) + '\", \"order\": \"asc\",\"orderBy\": \"conversationStart\",\"paging\": {\"pageSize\": 80,\"pageNumber\": '+pageNumb+'}}';
        //String sBody= '{\"interval\": \"' + (Datetime.now().addminutes(-90).format('yyyy-MM-dd\'T\'HH:mm:ss.SSS'+ 'Z')) + '/'  +(Datetime.now().addminutes(-75).format('yyyy-MM-dd\'T\'HH:mm:ss.SSS'+ 'Z')) + '\", \"order\": \"asc\",\"orderBy\": \"conversationStart\",\"paging\": {\"pageSize\": 80,\"pageNumber\": '+pageNumb+'},"segmentFilters": [{"type": "or","predicates": [{"type": "dimension","dimension": "dnis","operator": "matches","value": "+61399296262"}]}]}';


        String endPointURL = oAuthProperties1.EndPoint__c + '/api/v2/analytics/conversations/details/query';
        HttpRequest rResquest = rRestUtility.createHttpRequest(sBody, '', endPointURL, 'POST', mapRequestHeader);
        system.debug('get body'+  rResquest.getBody());
        HttpResponse rResponse = rRestUtility.createHttpResponse(rResquest, new Http()); 
        system.debug('rResponse:: ' + rResponse.getBody());  
        ININConversationWrapper2 rININConversationWrapper;
        if(rResponse != NULL && rResponse.getBody().trim() != '{}' && rResponse.getStatusCode() == 200)
        {
            rININConversationWrapper = (ININConversationWrapper2)JSON.deserialize(rResponse.getBody(), ININConversationWrapper2.class);                    
            system.debug('rININConversationWrapper:: ' + rININConversationWrapper);
        }else if (rResponse.getBody().trim() == '{}' )
        {
            return;
        }
        else{
            Log__c rLog = GlobalUtil.createLog('ERROR', 'Unable to get data', 'getOrphanInteractions()', rResponse.toString());
            rININConversationWrapper = new ININConversationWrapper2();
            rININConversationWrapper.rLog = rLog;
            
        }
        Set<String> callObjSet = new Set<String>();
        for(Task oTask : [SELECT Id, CallObject FROM Task WHERE CreatedDate = Today OR CreatedDate = Yesterday]){
            callObjSet.add(oTask.CallObject);
        }
        system.debug('callObjSet' + callObjSet);
        for(ConversationWrapper3 conv : rININConversationWrapper.conversations){
            conversationsSet.add(conv.conversationId);
            system.debug('conversationSet' + conversationsSet);
        }
        for(String objId: callObjSet){
            if(conversationsSet.contains(objId))
                conversationsSet.remove(objId);
        }
        findOrphanInteractions(conversationsSet, OAUT_TOKEN);
    }
    public class ININConversationWrapper2{
        public List<ConversationWrapper3> conversations{get; set;}
        public Log__c rLog;
    }
    public class ConversationWrapper3{
        public String conversationId{get; set;}
    }
    public static ININConversationWrapper getININConversationDetails(String sConversationID, String OAUT_TOKEN)
    {
        RestUtility oRestUtility = new RestUtility();
        Map<String, String> mapRequestHeader = new Map<String, String>();
        if(String.isBlank(OAUT_TOKEN))
            OAUT_TOKEN = getOAuthToken();
        mapRequestHeader.put('Content-Type', 'application/json');
        mapRequestHeader.put('Authorization', 'Bearer ' + OAUT_TOKEN);
        String endPointURL = oAuthProperties1.EndPoint__c + '/api/v2/conversations/' + sConversationID;
        HttpRequest oResquest = oRestUtility.createHttpRequest('', '', endPointURL, 'GET', mapRequestHeader);
        system.debug('request:: ' + Limits.getCallouts());
        HttpResponse oResponse = oRestUtility.createHttpResponse(oResquest, new Http()); 
        system.debug('oResponse:: ' + oResponse.getBody());
        ININConversationWrapper oININConversationWrapper;
        if(oResponse != NULL && oResponse.getStatusCode() == 200 && oResponse.getBody().trim() != '{}' )
        {
            oININConversationWrapper = (ININConversationWrapper)JSON.deserialize(oResponse.getBody(), ININConversationWrapper.class);                    
            system.debug('oININConversationWrapper:: ' + oININConversationWrapper);
            for(ParticipantWrapper oParticipantWrapper: oININConversationWrapper.participants )
            {
                if(String.isNotBlank(oParticipantWrapper.participantType) && CONST_EXTERNAL.equalsIgnoreCase(oParticipantWrapper.participantType))
                {
                    oININConversationWrapper.participants[0] = oParticipantWrapper;
                     return oININConversationWrapper;
                }
               
            }
        }
        else{
            Log__c oLog = GlobalUtil.createLog('ERROR', 'Unable to get NPS data from ININ', 'getININConversationDetails()', oResponse.toString());
            oININConversationWrapper = new ININConversationWrapper();
            oININConversationWrapper.oLog = oLog;
        }
        return oININConversationWrapper;
    }  
    public static String getOAuthToken()
    {
        RestUtility oRestUtility = new RestUtility();
        String requestStaticBody = 'grant_type=client_credentials';
        String endPointURL = oAuthProperties.EndPoint__c + RELATIVE_PATH;
        Map<String, String> mapRequestHeader = new Map<String, String>();
        String postCred = oAuthProperties.Client_Id__c + ':' + oAuthProperties.Client_Secret__c;
        String base64String = EncodingUtil.base64Encode(Blob.valueOf(postCred));
        String token;
        mapRequestHeader.put('Content-Type', 'application/x-www-form-urlencoded');
        mapRequestHeader.put('Authorization', 'Basic ' + base64String);
        HttpRequest oResquest = oRestUtility.createHttpRequest(requestStaticBody, '', endPointURL, 'POST', mapRequestHeader);
        HttpResponse oResponse = oRestUtility.createHttpResponse(oResquest, new Http()); 
        if(oResponse != null && oResponse.getStatusCode() == 200)
        {
            String sBody = oResponse.getBody();
            if(String.isNotBlank(sBody))
            {
                AuthClass sAuthClass = (AuthClass)JSON.deserialize(sBody, AuthClass.class);            
                token = sAuthClass.access_token;
                system.debug('OAUT_TOKEN :: ' + token );
                return token;
            }
        }
        else{
            //log the error
            Log__c oLog = GlobalUtil.createLog('ERROR', 'Authentication failed while contacting to ININ', 'getOAuthToken()', oResponse.toString());
            insert oLog;
            return null;
        }
        return null;
    }
    
    public class AuthClass{
        public String access_token{get; set;}
        public String expires_in{get; set;}
    }
    public class ININConversationWrapper{
        public String id{get; set;}
        public List<ParticipantWrapper> participants{get; set;}
        public Log__c oLog;
    }
    
    public class ParticipantWrapper{
        public Attributes attributes{get; set;}
        public String participantType{get; set;}
    }
   // Mobile IVR: Modified by Anuja for Mobile IVR changes 
    public class Attributes{
        public String NPS_Score{get; set;}
        public String NPS_Resolution{get; set;}
        public String NPS_Verbatim{get; set;}
        public String Customer_ANI{get; set;}
        public String CLI_Match{get;set;}
        public String CLI_Time{get;set;}
        public String Manual_CLI_Match{get;set;}
        public String Manual_CLI_Time{get;set;}
        public String Avalanche_DD{get;set;}
        public String Avalanche_time{get;set;}
        public String Avalanche_Continue{get;set;}
        public String DOB_Match{get;set;}
        public String AccountDetails_DD{get;set;}
        public String AccountDetails_DD_Time{get;set;}
        public String Preactivation{get;set;}
        public String Hear_Balance{get;set;}
        public String Acc_Suspended{get;set;}
        public String Payment_Result{get;set;}
        public String BlackHawk_DD{get;set;}
        public String BlackHawk_DD_Time{get;set;}
        public String Call_In{get;set;}
        public String Transfer_Q_or_Menu{get;set;}
        public String Plan_Type{get;set;}
        public String Prepay_Result{get;set;}
        public String Fetch_Prepay{get;set;}
        public String Inactive_Customer{get;set;}
        public String Outage{get;set;}
        public String Avalanche_Status{get;set;}
        public String SF_SearchValue{get;set;}
        public String Disconnect{get;set;}
        public String SF_Product{get;set;}
        public String Mobile_Email_Task{get;set;}
        public String Block_Number_Input{get;set;}
        public String Contact_DD_BlockSIM{get;set;}
        public String DOB_Match_BlockSIM{get;set;}
        public String Block_SIM{get;set;}
        public Integer Return_To_Main_Menu{get;set;}
        public String MENU{get;set;}
       
    }
    
    public static Map<String, List<ININConversationWrapper>> getAllConversation (Set<String> convIds, String token) {
        Map<String,ININWebservice__c> incMap = ININWebservice__c.getAll();
        Map<String, List<ININConversationWrapper>> attrbiMap = new Map<String, List<ININConversationWrapper>>();
        for(String convID : convIDs){
            ININConversationWrapper oININConversationWrapper = getININConversationDetails(convID, token);
            Attributes oAttributes = (Attributes)((ParticipantWrapper)oININConversationWrapper.participants[0]).attributes;
            String sPhone = oAttributes.SF_SearchValue;
            system.debug('attrbiMap: '+attrbiMap);
            if(String.isNotBlank(sPhone) && oININConversationWrapper != null){
                if(attrbiMap.containsKey(sPhone) && attrbiMap.get(sPhone).add(oININConversationWrapper) != null){
                    system.debug('sPhone:'+sPhone + ' value:: '  + attrbiMap.get(sPhone).add(oININConversationWrapper));
                    attrbiMap.put(sPhone, (List<ININConversationWrapper>)attrbiMap.get(sPhone).add(oININConversationWrapper));
                }
                else{
                    system.debug('sPhone:: ' + sPhone + ' wrapper: ' + oININConversationWrapper );
                    attrbiMap.put(sPhone, new List<ININConversationWrapper>{oININConversationWrapper});
                }
            }
        }
        return attrbiMap;
    }
}