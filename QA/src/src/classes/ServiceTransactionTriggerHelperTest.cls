/************************************************************************************************************
* Apex Class Name	: ServiceTransactionTriggerHelperTest.cls
* Version 			: 1.0 
* Created Date  	: 27 July 2017
* Function 			: Test class for ServiceTransactionTriggerHelper.
* Modification Log	:
* Developer				Date				Description
* -----------------------------------------------------------------------------------------------------------
* Tom Clayton			27/07/2017 			Created Class.
************************************************************************************************************/

@isTest
private class ServiceTransactionTriggerHelperTest {
	public static Account account1;
	public static Contact contact1, contact2;
	public static Case case1, case2;
	public static Asset asset1;
	public static Final String PORT_FAILURE_CASE_SUBJECT = 'Maximum port failures reached';
	public static Final String PORT_TYPE = 'Activate';
	public static Final String PORT_SUBTYPE = 'Port';
	
	@testSetup static void setupTestData()
	{
		// Create test data 
		account1 = TestDataFactoryMobile.createAccounts(1)[0];
		system.debug('--Created an Account: ' + account1);
		insert account1;
		
		list <Contact> lstContacts = TestDataFactoryMobile.createContacts(2);
		lstContacts[0].AccountId = account1.Id;
		lstContacts[0].MobilePhone = '0400444000';
		lstContacts[1].AccountId = account1.Id;
		lstContacts[1].MobilePhone = '0400444001';
		system.debug('--Created Contacts: ' + lstContacts);
		insert lstContacts;
		
		/*Public_Knowledge_Topic__c publicKnowledgeTopic = TestDataFactoryMobile.createPublicKnowledgeTopics(1)[0];
		insert publicKnowledgeTopic;*/
		
		/*list <Case> lstCases = TestDataFactoryMobile.createCases(2);
		lstCases[0].AccountId = account1.Id;
		lstCases[0].ContactId = lstContacts[0].Id;
		lstCases[1].AccountId = account1.Id;
		lstCases[1].ContactId = lstContacts[1].Id;
		insert lstCases;*/
		
		list<Asset> lstAssets = TestDataFactoryMobile.createAssets(2);
		lstAssets[0].ContactId = lstContacts[0].Id;
		lstAssets[0].Mobile_Number__c = lstContacts[0].MobilePhone;
		lstAssets[0].Status = 'Active';
		lstAssets[1].ContactId = lstContacts[1].Id;
		lstAssets[1].Mobile_Number__c = lstContacts[1].MobilePhone;
		lstAssets[1].Status = 'Active';
		system.debug('--Created Assets: ' + lstAssets);
		insert lstAssets;
		
		list <Service_Transactions__c> lstServTrans = TestDataFactoryMobile.createServiceTransactions(2);
        lstServTrans[0].Status__c = 'Complete';
        lstServTrans[0].Primary_Contact__c = lstContacts[0].Id;
        lstServTrans[0].Type__c = PORT_TYPE;
        lstServTrans[0].Sub_Type__c = PORT_SUBTYPE;
        lstServTrans[1].Status__c = 'Complete';
        lstServTrans[1].Primary_Contact__c = lstContacts[1].Id;
        lstServTrans[1].Type__c = PORT_TYPE;
        lstServTrans[1].Sub_Type__c = PORT_SUBTYPE;
        system.debug('--Created Service Transactions: ' + lstServTrans);
		insert lstServTrans;
	}
	
	static testMethod void insertServiceTransactionTest() {
        account1 = [SELECT Id, Name, Mobile_Octane_Customer_Number__c FROM Account][0];
        contact1 = [SELECT Id, Name FROM Contact][0];
        
        Test.startTest();
	        Service_Transactions__c servTrans = TestDataFactoryMobile.createServiceTransactions(1)[0];
	        servTrans.ID__c = 'NUM999';
	        servTrans.Primary_Contact__c = contact1.Id;
	        servTrans.Account__c = account1.Id;
	        servTrans.Status__c = 'Complete';
			insert servTrans;
        Test.stopTest();
        
        //system.assert(someTest, 'The insertUpdateServiceTransactionTest didnt work because someReason.');
    }
    
    static testMethod void updateServiceTransactionTest() {
        account1 = [SELECT Id, Name, Mobile_Octane_Customer_Number__c FROM Account][0];
        contact1 = [SELECT Id, Name FROM Contact][0];
        Service_Transactions__c servTrans1 = [SELECT Id, Status__c FROM Service_Transactions__c][0];
        
        Test.startTest();
	        servTrans1.Sub_Status__c = 'aaa123';
			update servTrans1;
        Test.stopTest();
        
        //system.assert(someTest, 'The insertUpdateServiceTransactionTest didnt work because someReason.');
    }
    
    static testMethod void insertServTransChangeMSN() {
        account1 = [SELECT Id, Name, Mobile_Octane_Customer_Number__c FROM Account][0];
        contact1 = [SELECT Id, Name, MobilePhone FROM Contact][0];
        asset1 = [SELECT Id, Name, ContactId FROM Asset WHERE ContactId =: contact1.Id][0];
        
        Test.startTest();
	        Service_Transactions__c servTrans = TestDataFactoryMobile.createServiceTransactions(1)[0];
	        servTrans.ID__c = 'NUM998';
	        //servTrans.Primary_Contact__c = contact1.Id;
	        servTrans.Account__c = account1.Id;
	        servTrans.Status__c = 'Complete';
	        servTrans.Type__c = 'Support';
	        servTrans.Sub_Type__c = 'Change MSN';
	        servTrans.Mobile_No__c = '0400222000';
	        servTrans.Previous_Mobile_No__c = contact1.MobilePhone;
	        servTrans.Asset__c = asset1.Id;
	        system.debug('-- Inserting changeMSN test: ' + servTrans);
			insert servTrans;
			system.debug('-- Inserted changeMSN test: ' + servTrans);
        Test.stopTest();
        
        //system.assert(someTest, 'The insertServTransChangeMSN didnt work because someReason.');
    }
    
    static testMethod void insertServTransReplaceSIM() {
        account1 = [SELECT Id, Name, Mobile_Octane_Customer_Number__c FROM Account][0];
        contact1 = [SELECT Id, Name FROM Contact][0];
        
        Test.startTest();
	        Service_Transactions__c servTrans = TestDataFactoryMobile.createServiceTransactions(1)[0];
	        servTrans.ID__c = 'NUM997';
	        servTrans.Primary_Contact__c = contact1.Id;
	        servTrans.Account__c = account1.Id;
	        servTrans.Status__c = 'Complete';
	        servTrans.Type__c = 'Support';
	        servTrans.Sub_Type__c = 'Replace SIM';
	        system.debug('-- Inserting replaceSIM test: ' + servTrans);
			insert servTrans;
			system.debug('-- Inserted replaceSIM test: ' + servTrans);
        Test.stopTest();
        
        //system.assert(someTest, 'The insertServTransReplaceSIM didnt work because someReason.');
    }
    
    static testMethod void insertServTransRejected() {
        account1 = [SELECT Id, Name, Mobile_Octane_Customer_Number__c FROM Account][0];
        contact1 = [SELECT Id, Name FROM Contact][0];
        
        Test.startTest();
	        Service_Transactions__c servTrans = TestDataFactoryMobile.createServiceTransactions(1)[0];
	        servTrans.ID__c = 'NUM996';
	        servTrans.Primary_Contact__c = contact1.Id;
	        servTrans.Account__c = account1.Id;
	        servTrans.Status__c = 'Rejected';
	        servTrans.Type__c = 'Activate';
	        servTrans.Sub_Type__c = 'Port';
	        system.debug('-- Inserting rejected test: ' + servTrans);
			insert servTrans;
			system.debug('-- Inserted rejected test: ' + servTrans);
        Test.stopTest();
        
        //system.assert(someTest, 'The insertServTransRejected didnt work because someReason.');
    }
    
    static testMethod void createOrActivateAssetTest() {
        account1 = [SELECT Id, Name, Mobile_Octane_Customer_Number__c FROM Account][0];
        contact1 = [SELECT Id, Name, AccountId FROM Contact][0];
        Service_Transactions__c servTrans1 = [SELECT Id, Status__c, New_SIM_Number__c, Mobile_No__c, Completion_Date__c FROM Service_Transactions__c][0];
        servTrans1.New_SIM_Number__c = '12345689';
        asset1 = [SELECT Id, Name, Status, Mobile_Number__c, ContactId FROM Asset][0];
        asset1.ContactId = contact1.Id;
        asset1.SIM__c = servTrans1.New_SIM_Number__c;
        update asset1;
        
        Test.startTest();
        	ServiceTransactionTriggerHelper.initializeProperties();
        	ServiceTransactionTriggerHelper.createOrActivateNewAsset(contact1, servTrans1);
        Test.stopTest();
        
        //system.assert(someTest, 'The createOrActivateAssetTest didnt work because someReason.');
    }
    
    static testMethod void findRecentFailuresTest() {
        account1 = [SELECT Id, Name, Mobile_Octane_Customer_Number__c FROM Account][0];
        contact1 = [SELECT Id, Name FROM Contact][0];
        
        Test.startTest();
        	ServiceTransactionTriggerHelper.findRecentFailures(contact1.Id, PORT_TYPE, PORT_SUBTYPE);
        Test.stopTest();
        
        //system.assert(someTest, 'The findRecentFailuresTest didnt work because someReason.');
    }
    
    static testMethod void createFailureCaseTest() {
        Service_Transactions__c servTrans1 = [SELECT Id, Status__c, New_SIM_Number__c, Primary_Contact__c, Mobile_No__c, Completion_Date__c, Account__c, Reject_code__c, Reject_Reason__c, Sub_Type__c FROM Service_Transactions__c][0];
        
        Test.startTest();
        	ServiceTransactionTriggerHelper.initializeProperties();
        	ServiceTransactionTriggerHelper.createFailureCase(servTrans1, PORT_FAILURE_CASE_SUBJECT);
        Test.stopTest();
        
        //system.assert(someTest, 'The createFailureCaseTest didnt work because someReason.');
    }
}