/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  @created : 16/05/2016
  @Description : transtale the public knowledge articles URL.
*/
global class KnowledgeSiteURLRewriter implements Site.UrlRewriter 
{
    //Custom settings, Knowledge basic configuration
    public Boolean isSite { get { return String.isNotBlank(Site.getName()); } }
    
    public Map<String,String> categoryKeySiteURL
        {
            set;
            get
            {
                if(categoryKeySiteURL == null)
                {
                    categoryKeySiteURL = new Map<String,String>();
                    String prodCat;
                    for(String keyMap : knowledgeProducts.keySet())
                    {
                        prodCat = knowledgeProducts.get(keyMap)[0].Product_Category__c;
                        categoryKeySiteURL.put(keyMap,  prodCat + '/' + knowledgeProducts.get(keyMap)[0].Article_Category__c);
                        for(Public_Knowledge_Product__c knowProd : knowledgeProducts.get(keyMap))
                        {
                            if(!categoryKeySiteURL.containsKey(knowProd.Name))
                            {
                                prodCat = knowProd.Product_Category__c;
                                categoryKeySiteURL.put(knowProd.Name, prodCat + '/' + knowProd.Article_Category__c);
                            }
                            
                            if(!categoryKeySiteURL.containsKey(knowProd.Data_Category__c))
                            {
                                categoryKeySiteURL.put(knowProd.Data_Category__c, knowProd.Product_Category__c + '/' + knowProd.Article_Category__c);
                            }
                        }
                    }
                }
                return categoryKeySiteURL;
            }
        }
    public Map<String,List<Public_Knowledge_Product__c>> knowledgeProducts
        {
            set;
            get 
            {
                if (knowledgeProducts == null) 
                {
                    knowledgeProducts = KnowledgeSiteUtil.buildProductDisplay();
                }
                return knowledgeProducts;
            }
        }
    
    public Map<String,Public_Knowledge_URL_Mapping__c> knowledgeURL
        {
            set;
            get 
            {
                if (knowledgeURL == null) 
                {
                    knowledgeURL = KnowledgeSiteUtil.buildURLMapping();
                }
                return knowledgeURL;
            }
        }
    
    public String currentSiteUrl 
    {
        set;
        get 
        {
            if (currentSiteUrl == null) 
            {
                currentSiteUrl = Site.getBaseUrl()+'/';//Site.getCurrentSiteUrl();
            }
            return currentSiteUrl;
        }
        
    }
        
     // The first global method for mapping external URL to an internal one
    global PageReference mapRequestUrl(PageReference BelongSupportUrl) 
    {
        PageReference pageInternal;
        
        String tempURL;
        String internalURL = '';
        
        if(BelongSupportUrl.getUrl().startsWith('/') && BelongSupportUrl.getUrl().length() > 1)
            tempURL = BelongSupportUrl.getUrl().substring(1);
        
        if(BelongSupportUrl.getUrl().contains('Belong_Support_Home'))
        {
            tempURL='';
            internalURL = BelongSupportUrl.getUrl();
        }
        
        try
        {
        
            if(String.isNotBlank(tempURL))
            {
                List<String> URLStructure = tempURL.split('/');
                List<String> URLSearch;
                String productSearch;
                 
                system.debug(logginglevel.error, 'KnowledgeSiteURLRewriter @mapRequestUrl => URLStructure ' + URLStructure);
                system.debug(logginglevel.error, 'KnowledgeSiteURLRewriter @mapRequestUrl => knowledgeProducts ' + knowledgeProducts);
                if(URLStructure.size() == 1)
                {
                    URLSearch = URLStructure[0].split('\\?');
                    system.debug(logginglevel.error, 'KnowledgeSiteURLRewriter @mapRequestUrl => URLSearch ' + URLSearch);
                    if(URLSearch.size()<2)
                    {
                        productSearch = URLSearch[0];
                            
                        if(knowledgeProducts.containsKey(productSearch))
                        {
                            internalURL += knowledgeURL.get('Product').Visualforce_Page_Name__c + '?cat=' + knowledgeProducts.get(productSearch)[0].Name;
                        }
                        else
                        {
                            internalURL +=  knowledgeURL.get('Error').Visualforce_Page_Name__c;
                        }
                    }
                    /*else if(URLSearch[0] == 'TakeSurvey')
                    {
                        system.debug(logginglevel.error, 'KnowledgeSiteURLRewriter @mapRequestUrl => RETURNING' + ViewSurveyController.siteURL);
                        return BelongSupportUrl;
                    }*/
                    else
                    {
                        internalURL += knowledgeURL.get(URLSearch[0]).Visualforce_Page_Name__c + '?' + URLSearch[1];
                    }
                    system.debug(logginglevel.error, 'KnowledgeSiteURLRewriter @mapRequestUrl => URLStructure#1 internalURL ' + internalURL);
                }
                else if(URLStructure.size() == 2)
                {
                    productSearch = URLStructure[0];
                    if(!knowledgeProducts.containsKey(productSearch))
                    {
                        boolean isSurveyValid = false;
                        if(productSearch == 'Survey')
                        {
                            system.debug(logginglevel.error, 'KnowledgeSiteURLRewriter @Survey ');
                            String surveyId, contactId, orderId, surveyName, contactHashCode, orderHashCode;
                            List<String> surveyStructure;// = URLStructure[1].split('?');
                            
                            try
                            {
                                surveyStructure = URLStructure[1].split('\\?');
                                surveyName = surveyStructure[0];
                                contactHashCode = BelongSupportUrl.getParameters().get('id');
                                orderHashCode = BelongSupportUrl.getParameters().get('srv');
                            }
                            catch (Exception e)
                            {
                                system.debug(logginglevel.error, 'KnowledgeSiteURLRewriter @catch ' + e);
                            }
                            
                            for(Survey__c surveyObj : [SELECT id, Name 
                                                        FROM Survey__c 
                                                        WHERE Name =: surveyName LIMIT 1])
                            {
                                surveyId = surveyObj.Id;
                            }
                            
                            for(Contact conObj : [SELECT Id 
                                                    FROM Contact 
                                                    WHERE Id_Hashcode__c =: contactHashCode AND 
                                                    Id_Hashcode__c != NULL LIMIT 1])
                            {
                                contactId = conObj.Id;
                            }
                            for(Order orderObj : [SELECT Id 
                                                    FROM Order 
                                                    WHERE Id_Hashcode__c =: orderHashCode AND 
                                                    Id_Hashcode__c != NULL LIMIT 1])
                            {
                                orderId = orderObj.Id;
                            }
                            
                            if(String.IsNotBlank(surveyId) && String.IsNotBlank(contactId) && String.IsNotBlank(orderId))
                            {
                                isSurveyValid = true;
                                String surveyParams = '?id=' + surveyId + '&cId=' + contactId + '&caId=none' +'&ordId='+orderId;
                                internalURL += knowledgeURL.get('Survey').Visualforce_Page_Name__c + surveyParams;
                            }else if(String.IsNotBlank(surveyId)){
                            /*Survey URL's don't have any paramter so added below else condition*/
                                isSurveyValid = true;
                                String surveyParams = '?id=' + surveyId;
                                internalURL += knowledgeURL.get('Survey').Visualforce_Page_Name__c + surveyParams;
                            }
                        }
    
                        if(!isSurveyValid)
                        {
                            internalURL += knowledgeURL.get('Error').Visualforce_Page_Name__c;
                        }
                    }
                    else
                    {
                        internalURL += knowledgeURL.get('Product').Visualforce_Page_Name__c;
                        
                        for(Public_Knowledge_Product__c pKnow : knowledgeProducts.get(productSearch))
                        {
                            if(pKnow.Article_Category__c == URLStructure[1])
                            {
                                internalURL += '?cat=' + pKnow.Name;
                                break;
                            }
                        }   
                    }
                    system.debug(logginglevel.error, 'KnowledgeSiteURLRewriter @mapRequestUrl => URLStructure#2 internalURL ' + internalURL);
                }
                else if(URLStructure.size() == 3)
                {
                    system.debug(logginglevel.error, 'KnowledgeSiteURLRewriter @mapRequestUrl => URLStructure#3 knowledgeProducts ' + knowledgeProducts);
                    productSearch = URLStructure[0];
                    
                    if(!knowledgeProducts.containsKey(productSearch))
                    {
                        internalURL += knowledgeURL.get('Error').Visualforce_Page_Name__c;
                    }
                    else
                    {
                        String catSearch = URLStructure[1];
                        String catSearchAPI;
                        String ArticleName = URLStructure[2];
                        ArticleName = ArticleName.toLowerCase().substringBefore('?');
                        boolean foundArticle = false;
                        
                        for(Public_Knowledge_Product__c pProd : knowledgeProducts.get(productSearch))
                        {
                            if(pProd.Article_Category__c == catSearch)
                            {
                                catSearchAPI = pProd.Data_Category__c.removeEndIgnoreCase('__c');
                            }
                        }
                        
                        for(FAQ__DataCategorySelection faq: [SELECT Id, ParentId, DataCategoryGroupName, DataCategoryName, Parent.URLNAME, Parent.Normalize_URL_Name__c 
                                                        FROM FAQ__DataCategorySelection 
                                                        WHERE Parent.Normalize_URL_Name__c =: ArticleName])
                        {
                            if(faq.DataCategoryName == catSearchAPI)
                            {
                                internalURL += knowledgeURL.get('Article').Visualforce_Page_Name__c;
                                internalURL += '?artName=' + ArticleName;
                                foundArticle = true;
                                break;
                            }
                        }
                        
                        if(!foundArticle)
                        {
                            internalURL += knowledgeURL.get('Error').Visualforce_Page_Name__c;
                        }
                    }
                    system.debug(logginglevel.error, 'KnowledgeSiteURLRewriter @mapRequestUrl => URLStructure#3 internalURL ' + internalURL);
                }
            }
             
            pageInternal = new PageReference(internalURL);
        }catch(Exception oException)
        {
            pageInternal = null;
        }
        return pageInternal;
    }

    global List<PageReference> generateUrlFor(List<PageReference> BelongSupportUrls) 
    {
        List<PageReference> finalUrls = new List<PageReference>();
        List<String> articleUrls = new List<String>();
        Map<String,String> artNameArtUrl = new Map<String,String>();
        String externalURL, internalPage, pageURL;
        system.debug(logginglevel.error, 'KnowledgeSiteURLRewriter @generateUrlFor => BelongSupportUrls ' + BelongSupportUrls);
        system.debug(logginglevel.error, 'KnowledgeSiteURLRewriter @generateUrlFor => categoryKeySiteURL ' + categoryKeySiteURL);
        system.debug(logginglevel.error, 'KnowledgeSiteURLRewriter @generateUrlFor => knowledgeURL ' + knowledgeURL);
        for(PageReference blngSupURL : BelongSupportUrls)
        {
            pageURL = blngSupURL.getUrl();
            internalPage = knowledgeURL.get('Article').Visualforce_Page_Name__c;
            system.debug(logginglevel.error, 'KnowledgeSiteURLRewriter @generateUrlFor => find articles ');
            if(pageURL.contains(internalPage) && String.IsNotBlank(pageURL.substringAfterLast('=')))
            {
                String articleName = pageURL.substringAfterLast('=');
                articleUrls.add(articleName);
            }
        }
        
        if(!articleUrls.isEmpty())
        {
            for(FAQ__DataCategorySelection faq: [SELECT Id, ParentId, DataCategoryGroupName,DataCategoryName, Parent.URLNAME, Parent.Normalize_URL_Name__c
                                                    FROM FAQ__DataCategorySelection 
                                                    WHERE Parent.Normalize_URL_Name__c IN: articleUrls])
            {
                String DataCategoryAPI = faq.DataCategoryName + '__c';
                if(categoryKeySiteURL.containsKey(DataCategoryAPI))
                {
                    String artURL = categoryKeySiteURL.get(DataCategoryAPI) + '/' + faq.Parent.Normalize_URL_Name__c;
                    artNameArtUrl.put(faq.Parent.Normalize_URL_Name__c, artURL);
                }
            }
        }
        
        system.debug(logginglevel.error, 'KnowledgeSiteURLRewriter @generateUrlFor => artNameArtUrl ' + artNameArtUrl);
        
        for(PageReference blngSupURL : BelongSupportUrls)
        {
            pageURL = blngSupURL.getUrl();
            externalURL = '';
            system.debug(logginglevel.error, 'KnowledgeSiteURLRewriter @generateUrlFor => pageURL ' + pageURL);
            for(String keyURL : knowledgeURL.keySet())
            {
                internalPage = knowledgeURL.get(keyURL).Visualforce_Page_Name__c;
                system.debug(logginglevel.error, 'KnowledgeSiteURLRewriter @generateUrlFor => keyURL ' + keyURL);
                if(pageURL.contains(internalPage))
                {
                    if(keyURL == 'Article')
                    {
                        String articleName;
                        if(String.IsNotBlank(pageURL.substringAfterLast('=')))
                        {
                            articleName = pageURL.substringAfterLast('=');
                            articleName = articleName.toLowerCase();
                            system.debug(logginglevel.error, 'KnowledgeSiteURLRewriter @generateUrlFor => articleName ' + articleName);
                            if(artNameArtUrl.containsKey(articleName))
                            {
                                externalURL = artNameArtUrl.get(articleName);
                                system.debug(logginglevel.error, 'KnowledgeSiteURLRewriter @generateUrlFor => Article externalURL ' + externalURL);
                                break;
                            }
                        }
                    }
                    else if(keyURL == 'Product')
                    {
                        String keySelected;
                        if(String.IsNotBlank(pageURL.substringAfterLast('=')))
                        {
                            keySelected = pageURL.substringAfterLast('=');
                            if(categoryKeySiteURL.containsKey(keySelected))
                            {
                                externalURL = categoryKeySiteURL.get(keySelected);
                                system.debug(logginglevel.error, 'KnowledgeSiteURLRewriter @generateUrlFor => PRODUCT externalURL ' + externalURL);
                                break;
                            }
                        }
                    }
                    else if(keyURL == 'Search')
                    {
                        String searchTerm;
                        if(String.IsNotBlank(pageURL.substringAfterLast('?')))
                        {
                            searchTerm = pageURL.substringAfterLast('?');
                            externalURL =   keyURL + '?search=' + searchTerm;
                        }
                        else
                        {
                            externalURL = keyURL + '?search=';
                        }
                        system.debug(logginglevel.error, 'KnowledgeSiteURLRewriter @generateUrlFor => SEARCH externalURL ' + externalURL);
                    }
                }
            }
            
            finalUrls.add(new PageReference(externalURL));
        }
        
        return finalUrls;
    }
}