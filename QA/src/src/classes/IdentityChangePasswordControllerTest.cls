@istest
public class IdentityChangePasswordControllerTest{
    @testSetup
    public static void CheckFunction () {    
        Account Acc = new Account();
        Acc.name = 'test and Acc 33 ';
        Acc.octane_customer_number__c = '985654';
        insert Acc;
        
        contact con = new contact();
        con.firstname = 'first'; 
        con.lastname = 'last1333';
        con.accountid = acc.id;
        con.Birthdate = date.valueOf('1995-01-01');
        con.MobilePhone = '1234567890';
        con.email = 't234hjsss4456@mailinator.com';
        insert con;
    }
    
    public static TestMethod void checkPwdChange(){
        IdentityChangePasswordController.changeUserPwd('t234hjsss4456@mailinator.com', 'Test@1234', 'Test@1234');
        IdentityChangePasswordController.changeUserPwd('t234hjsss44561@mailinator.com', 'Test@1234', 'Test@1234');
        IdentityChangePasswordController.changeUserPwd('t234hjsss44561.com', 'Test@1234', 'Test@1234');
        IdentityChangePasswordController.changeUserPwd('t234hjsss44561@mailinator.com', 'Test@1234', 'Test@12345');
        IdentityChangePasswordController.changeUserPwd('t234hjsss44561@mailinator.com', 'Test', 'Test');
    }
}