/************************************************************************************************************
    Apex Class Name  : MobileRetailRestHelperTest.cls
    Version       : 1.0
    Created Date    : 06 sep 2017
    Function       : Test class for MobileRetailRestHelper .
    Modification Log  : Covers MobileRetailRestHelper and MobileRetailRest
    Developer        Date        Description
    -----------------------------------------------------------------------------------------------------------
    Anuja Pandey       06 sep 2017    Created Test Class for Mobile Retail Rest Helper
************************************************************************************************************/

@isTest
private class MobileRetailRestHelperTest {
  @testSetup
  static void setupTestData() {
    UserRole ur = [SELECT Id FROM UserRole Where name = 'Belong MOBILE'];
    // Setup test data
    // This code runs as the system user
    Profile p  = [Select id from Profile where name = 'Belong Integration API'];
    User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
                      EmailEncodingKey = 'UTF-8',
                      LastName = 'Testing',
                      LanguageLocaleKey = 'en_US',
                      LocaleSidKey = 'en_US',
                      ProfileId = p.Id,
                      UserRoleId = ur.id,
                      TimeZoneSidKey = 'America/Los_Angeles',
                      UserName = 'belongapiuser@testorg.com');
    System.runAs(u) {
      // The following code runs as user 'u'
      
      List<Sim_data__c> listSimData = TestDataFactoryMobile.createSimData(201);
      insert listSimData;
    }
  }
  static testMethod void CreateApiResponses() {
    // Create test request
    String jsonInput = '{'
                        + '  "Retrieval_Reference_Number__c": null,'
                        + '  "Sim_Status__c": "Available for purchase",'
                        + '  "Transaction_Type__c": "Activation",'
                        + '  "Purchase_DateTime__C": null,'
                        + '  "Processing_Code__c": "725400",'
                        + '  "Transaction_Amount__c": "000000002500",'
                        + '  "Transmission_DateTime__c": "2013-11-21T10:54:14.000+10",'
                        + '  "System_Trace_Audit_Number__c": "000110",'
                        + '  "Merchant_Category_Code__c": "5411",'
                        + '  "Local_Transaction_DateTime__c": "2013-11-21T10:54:14.000+10",'
                        + '  "Point_Of_Service_Entry_Mode__c": "011",'
                        + '  "Acquiring_Institution_Identifier__c": "60300000063",'
                        + '  "Merchant_Terminal_Id__c": "06220@@@@@600@@@",'
                        + '  "Merchant_Identifier__c": "60300000063@@@@",'
                        + '  "Merchant_Location__c": "BLACKHAWK@SIMULATOR@@@PLEASANTON@@@CA@US",'
                        + '  "Transaction_Currency_Code__c": "036",'
                        + '  "Product_Id__c": "07675004390",'
                        + '  "Reference_Transaction_Id__c": null,'
                        + '  "Blackhawk_code__c": null,'
                        + '  "Card_Value__c": "2500",'
                        + '  "Card_Number__c": "4653480400004"'
                        +'}';
    RestRequest req = new RestRequest();
    RestResponse res = new RestResponse();
    req.requestURI = '/services/apexrest/RESTMobileRetail/V1';
    req.params.put('APICall', 'upsertSimdata');
    
    // Fake the passed call
    req.requestBody = Blob.valueOf(jsonInput);
    req.httpMethod = 'POST';
    RestContext.request = req;
    RestContext.response = res;
    Test.startTest();
    //Test Create notification API
    MobileRestRetail.postMobileRetail();
    Test.stopTest();
    }
    //Testing Sim data validation rules when inserted
 static testMethod void UpdateSimData_validation() { 
 
 list<Mobile_Retail_Error_Mapping__c> errmap = new list<Mobile_Retail_Error_Mapping__c>();
        Mobile_Retail_Error_Mapping__c testerr = new Mobile_Retail_Error_Mapping__c();
        testerr.Error_Code__c = '04';
        testerr.Name = '04';
        testerr.Error_Message__c = 'Void error';
        errmap.add(testerr);
      
        insert errmap;
   
      
    Sim_Data__c sim = new Sim_Data__c ();
        sim.Product_Code_SKU__c = 'MOB-SIM-RETAIL-001-25';
        sim.Sim_Status__c='Available for Purchase';
        sim.Card_number__c='100004444444';
        insert sim;
       
    
    String jsonInput1 = '{'
                        + '  "Retrieval_Reference_Number__c": null,'
                        + '  "Sim_Status__c": "Purchased",'
                        + '  "Transaction_Type__c": "Activation",'
                        + '  "Purchase_DateTime__C": null,'
                        + '  "Processing_Code__c": "725400",'
                        + '  "Transaction_Amount__c": "000000002500",'
                        + '  "Transmission_DateTime__c": "2013-11-21T10:54:14.000+10",'
                        + '  "System_Trace_Audit_Number__c": "000110",'
                        + '  "Merchant_Category_Code__c": "5411",'
                        + '  "Local_Transaction_DateTime__c": "2013-11-21T10:54:14.000+10",'
                        + '  "Point_Of_Service_Entry_Mode__c": "011",'
                        + '  "Acquiring_Institution_Identifier__c": "60300000063",'
                        + '  "Merchant_Terminal_Id__c": "06220@@@@@600@@@",'
                        + '  "Merchant_Identifier__c": "60300000063@@@@",'
                        + '  "Merchant_Location__c": "BLACKHAWK@SIMULATOR@@@PLEASANTON@@@CA@US",'
                        + '  "Transaction_Currency_Code__c": "036",'
                        + '  "Product_Id__c": "07675004390",'
                        + '  "Reference_Transaction_Id__c": null,'
                        + '  "Blackhawk_code__c": null,'
                        + '  "Card_Number__c": "100004444444"'
                        +'}';
  RestRequest req = new RestRequest();
    RestResponse res = new RestResponse();
  req.requestURI = '/services/apexrest/RESTMobileRetail/V1';
    req.params.put('APICall', 'upsertSimdata');
    
    // Fake the passed call
    req.requestBody = Blob.valueOf(jsonInput1);
    req.httpMethod = 'POST';
    RestContext.request = req;
    RestContext.response = res;
    Test.startTest();
    //Test Create notification API
    MobileRestRetail.postMobileRetail();
    Test.stopTest();
  
  }
    // testing Sim Data validation rules when updated
    static testMethod void UpdateSimData_no_validations() { 
 
      list<Mobile_Retail_Error_Mapping__c> errmap = new list<Mobile_Retail_Error_Mapping__c>();
        Mobile_Retail_Error_Mapping__c testerr = new Mobile_Retail_Error_Mapping__c();
        testerr.Error_Code__c = '04';
        testerr.Name = '04';
        testerr.Error_Message__c = 'Void error';
        errmap.add(testerr);
      
        insert errmap;
   
      
    Sim_Data__c sim = new Sim_Data__c ();
        sim.Product_Code_SKU__c = 'MOB-SIM-RETAIL-001-25';
        sim.Sim_Status__c='Available for Purchase';
        sim.Card_number__c='100004444475';
        insert sim;
       
    
    String jsonInput2 = '{'
                        + '  "Retrieval_Reference_Number__c": null,'
                        + '  "Sim_Status__c": "Purchased",'
                        + '  "Transaction_Type__c": "Activation",'
                        + '  "Purchase_DateTime__C": null,'
                        + '  "Processing_Code__c": "725400",'
                        + '  "Transaction_Amount__c": "000000002500",'
                        + '  "Transmission_DateTime__c": "2013-11-21T10:54:14.000+10",'
                        + '  "System_Trace_Audit_Number__c": "000110",'
                        + '  "Merchant_Category_Code__c": "5411",'
                        + '  "Local_Transaction_DateTime__c": "2013-11-21T10:54:14.000+10",'
                        + '  "Point_Of_Service_Entry_Mode__c": "011",'
                        + '  "Acquiring_Institution_Identifier__c": "60300000063",'
                        + '  "Merchant_Terminal_Id__c": "06220@@@@@600@@@",'
                        + '  "Merchant_Identifier__c": "60300000063@@@@",'
                        + '  "Merchant_Location__c": "BLACKHAWK@SIMULATOR@@@PLEASANTON@@@CA@US",'
                        + '  "Transaction_Currency_Code__c": "036",'
                        + '  "Product_Id__c": "07675004390",'
                        + '  "Reference_Transaction_Id__c": null,'
                        + '  "Blackhawk_code__c": null,'
                        + '  "Card_Number__c": "100004444475"'
                        +'}';
  RestRequest req = new RestRequest();
    RestResponse res = new RestResponse();
  req.requestURI = '/services/apexrest/RESTMobileRetail/V1';
    req.params.put('APICall', 'upsertSimdata');
    
    // Fake the passed call
    req.requestBody = Blob.valueOf(jsonInput2);
    req.httpMethod = 'POST';
    RestContext.request = req;
    RestContext.response = res;
    Test.startTest();
    //Test Create notification API
    MobileRestRetail.postMobileRetail();
    Test.stopTest();
  
  }
    // testing negative scenarios
    static testMethod void Negativetest_nocardnumber() { 

    String jsonInputwrong = '{'
                        + '  "Retrieval_Reference_Number__c": null,'
                        + '  "Sim_Status__c": "Purchased",'
                        + '  "Transaction_Type__c": "Activation",'
                        + '  "Purchase_DateTime__C": null,'
                        + '  "Processing_Code__c": "725400",'
                        + '  "Transaction_Amount__c": "000000002500",'
                        + '  "Transmission_DateTime__c": "2013-11-21T10:54:14.000+10",'
                        + '  "System_Trace_Audit_Number__c": "000110",'
                        + '  "Merchant_Category_Code__c": "5411",'
                        + '  "Local_Transaction_DateTime__c": "2013-11-21T10:54:14.000+10",'
                        + '  "Point_Of_Service_Entry_Mode__c": "011",'
                        + '  "Acquiring_Institution_Identifier__c": "60300000063",'
                        + '  "Merchant_Terminal_Id__c": "06220@@@@@600@@@",'
                        + '  "Merchant_Identifier__c": "60300000063@@@@",'
                        + '  "Merchant_Location__c": "BLACKHAWK@SIMULATOR@@@PLEASANTON@@@CA@US",'
                        + '  "Transaction_Currency_Code__c": "036",'
                        + '  "Product_Id__c": "07675004390",'
                        + '  "Reference_Transaction_Id__c": null,'
                        + '  "Blackhawk_code__c": null'
                        +'}';
  RestRequest req = new RestRequest();
    RestResponse res = new RestResponse();
  req.requestURI = '/services/apexrest/RESTMobileRetail/V1';
    req.params.put('APICall', 'upsertSimdata');
    
    // Fake the passed call
    req.requestBody = Blob.valueOf(jsonInputwrong);
    req.httpMethod = 'POST';
    RestContext.request = req;
    RestContext.response = res;
    Test.startTest();
    //Test Create notification API
    MobileRestRetail.postMobileRetail();
    Test.stopTest();
  
  }
     static testMethod void Negativetest_API() { 

    String jsonInput = '{'
                        + '  "Retrieval_Reference_Number__c": null,'
                        + '  "Sim_Status__c": "Purchased",'
                        + '  "Transaction_Type__c": "Activation",'
                        + '  "Purchase_DateTime__C": null,'
                        + '  "Processing_Code__c": "725400",'
                        + '  "Transaction_Amount__c": "000000002500",'
                        + '  "Transmission_DateTime__c": "2013-11-21T10:54:14.000+10",'
                        + '  "System_Trace_Audit_Number__c": "000110",'
                        + '  "Merchant_Category_Code__c": "5411",'
                        + '  "Local_Transaction_DateTime__c": "2013-11-21T10:54:14.000+10",'
                        + '  "Point_Of_Service_Entry_Mode__c": "011",'
                        + '  "Acquiring_Institution_Identifier__c": "60300000063",'
                        + '  "Merchant_Terminal_Id__c": "06220@@@@@600@@@",'
                        + '  "Merchant_Identifier__c": "60300000063@@@@",'
                        + '  "Merchant_Location__c": "BLACKHAWK@SIMULATOR@@@PLEASANTON@@@CA@US",'
                        + '  "Transaction_Currency_Code__c": "036",'
                        + '  "Product_Id__c": "07675004390",'
                        + '  "Reference_Transaction_Id__c": null,'
                        + '  "Blackhawk_code__c": null'
                        +'}';
  RestRequest req = new RestRequest();
    RestResponse res = new RestResponse();
  req.requestURI = '/services/apexrest/RESTMobileRetail/V1';
    
    // Fake the passed call
    req.requestBody = Blob.valueOf(jsonInput);
    req.httpMethod = 'POST';
    RestContext.request = req;
    RestContext.response = res;
    Test.startTest();
    //Test Create notification API
    MobileRestRetail.postMobileRetail();
    Test.stopTest();
  
  }
      static testMethod void Negativetest_Jsonbody() { 

    String jsonInput = '';
  RestRequest req = new RestRequest();
    RestResponse res = new RestResponse();
  req.requestURI = '/services/apexrest/RESTMobileRetail/V1';
   req.params.put('APICall', 'upsertSimdata'); 
    // Fake the passed call
    req.requestBody = Blob.valueOf(jsonInput);
    req.httpMethod = 'POST';
    RestContext.request = req;
    RestContext.response = res;
    Test.startTest();
    //Test Create notification API
    MobileRestRetail.postMobileRetail();
    Test.stopTest();
  
  }
 }