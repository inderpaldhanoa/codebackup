@istest
public class SimdataTriggerHandlerTest {
    public static TestMethod void CheckFunction() {
        list<GD_Card_Value__c> testGCCard = new list<GD_Card_Value__c>();
        GD_Card_Value__c testGCCard1 = new GD_Card_Value__c();
        testGCCard1.Card_Value__c = '4000';
        testGCCard1.Name = 'MOB-SIM-RETAIL-001-40';
        testGCCard.add(testGCCard1);
        
        GD_Card_Value__c testGCCard2 = new GD_Card_Value__c();
        testGCCard2.Card_Value__c = '2500';
        testGCCard2.Name = 'MOB-SIM-RETAIL-001-25';
        testGCCard.add(testGCCard2);
        insert testGCCard;
        
        Sim_Data__c sim = new Sim_Data__c ();
        sim.Product_Code_SKU__c = 'MOB-SIM-RETAIL-001-25';
        insert sim;
        
        sim.Product_Code_SKU__c = 'MOB-SIM-RETAIL-001-40';
        sim.Transaction_Amount__c='4000';
        update sim;        
    }
}