/************************************************************************************************************
* Apex Class Name   : MobileRestUtility.cls
* Version           : 1.0
* Created Date      : 22 March 2017
* Function          : Utility class for Mobile Development
* Modification Log  :
* Developer         :        Date                Description
* -----------------------------------------------------------------------------------------------------------
* Girish P                  18/04/2017          Created Class.
************************************************************************************************************/
public with sharing class MobileRestUtility {
	public static final Integer STATUS_BAD = RestUtility.STATUS_BAD;
	public static final Integer STATUS_OK = RestUtility.STATUS_OK;
	public static final Integer STATUS_ISE = RestUtility.STATUS_ISE;
	public static Integer statusCode = STATUS_OK;
	public static Final String HTTP_SUCCESS = 'Success';
	public static Final String HTTP_ERROR = 'Error';
	public static Final String STATUS_EXCEPTION = 'Exception';
	public static List<Log__c> logs;
	public static String message, status = HTTP_SUCCESS;
	/***********************************************************************************************************
	@input  : Exception, SourceMethod: Method for which log has to be, jsonRequest: request received in API call
	@output : add the logs to log object and insert later once API call is finished.
	@Description : xxx
	************************************************************************************************************/
	public static void setExceptionResponse(Exception e,
	                                        String sSourceMethod,
	                                        String jsonRequest) {
		message = 'Exception Occured:' +  e.getMessage();
		status = STATUS_EXCEPTION;
		statusCode = STATUS_ISE;
		System.debug('Exception**************' + e.getStackTraceString());
		logs.add(GlobalUtil.createLog(
		             'ERROR', e.getMessage().abbreviate(225),
		             sSourceMethod + ':  API Request Error',
		             'JSON request:' + '\n' + jsonRequest + '\n' + '****getStackTraceString: ' + '\n' + e.getMessage() + '\n' + e.getStackTraceString())
		        );
	}
	/***********************************************************************************************************
	@input  : SourceMethod: Method for which log has to be, jsonRequest: request received in API call
	@output : add the logs to log object and insert later once API call is finished.
	************************************************************************************************************/
	public static void createDebugLog(String sSourceMethod, String jsonRequest ) {
		status = HTTP_ERROR;
		statusCode = STATUS_BAD;
		logs.add(GlobalUtil.createLog(
		             'ERROR', message,
		             sSourceMethod + ':  API Data Error',
		             'JSON request:' + '\n' + jsonRequest + '\n' )
		        );
	}
	/***********************************************************************************************************
	@input  : SourceMethod: Method for which log has to be, jsonRequest: request received in API call
	@output : add the logs to log object and insert later once API call is finished.
	Descripton: returns the HTTP call with error response
	************************************************************************************************************/
	public static WrapperHTTPResponse returnWithError(String sourceMethod, String sJsonData) {
		MobileRestUtility.createDebugLog(sourceMethod, sJsonData);
		return RestUtility.getResponse(HTTP_ERROR, message, 404, NULL, logs);
	}

}