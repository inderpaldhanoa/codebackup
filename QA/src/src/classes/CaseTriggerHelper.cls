/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  @created : 22/03/2016
  @Description : Helper class for the case trigger process
*/
public with sharing class CaseTriggerHelper 
{
    private static Map<String,Schema.RecordTypeInfo> caseRecordTypeInfoName;
    private static Map<Id,Schema.RecordTypeInfo> caseRecordTypeInfoId;
    private static Map<Id, List<Case>> mapContactIdCases;
    private static Map<Id, Contact> mapAccountIdPrimaryContact;
    private static Map<Id, sObject> closedCases;
    private static Map<Id, Id> accountIdIFBOTCaseId;
    private static Map<String, Id> mapBelongIdAccountID;
    private static List<Id> caseAssigmentRule;
    private static List<Account> updateIFBOTAccount;
    private static Notification_Settings__c notifSetting;
    private static Boolean isInitialized = false;
        
    
    public static void initializeProperties()
    {
        if(isInitialized)
            return;
            
        caseRecordTypeInfoId        = GlobalUtil.getRecordTypeById('Case'); //getting all Recordtype for the Sobject
        caseRecordTypeInfoName      = GlobalUtil.getRecordTypeByName('Case');//getting all Recordtype for the Sobject
        mapContactIdCases           = new Map<Id,List<Case>>();
        mapAccountIdPrimaryContact  = new Map<Id, Contact>();
        closedCases                 = new Map<Id, sObject>();
        mapBelongIdAccountID        = new Map<String, Id>();
        caseAssigmentRule           = new List<Id>();
        updateIFBOTAccount          = new List<Account>();
        accountIdIFBOTCaseId        = new Map<Id, Id>();
        notifSetting                = Notification_Settings__c.getInstance();
        isInitialized               = true;
    }
    
    public static void collectContantCasesList()
    {
        Set<id> setContactIds = new Set<Id>();
        for(sObject caseObj : trigger.new)
        {
            if(String.IsNotEmpty((String)caseObj.get('ContactId')))
            {
                setContactIds.add((Id)caseObj.get('ContactId'));
            }
        }
        
        for(Case c: [SELECT Id, Subject, AccountId, ContactId, ParentId 
                        FROM Case 
                        WHERE ContactId IN: setContactIds AND
                        ContactId != NULL 
                        order by CreatedDate asc])
        {
            if(!mapContactIdCases.containsKey(c.ContactId))
                mapContactIdCases.put(c.ContactId, new List<Case>());
                
            mapContactIdCases.get(c.ContactId).add(c);
        }
    }
    
    public static void collectBelongIdAccountReference()
    {
        Set<String> belongID = new Set<String>();
        //gathering all records id or belong id for query
        for(sObject caseObj : trigger.new)
        {
            if(caseObj.get('AccountId') == NULL && String.IsNotEmpty((String)caseObj.get('Belong_Id__c')))
            //if((boolean)caseObj.get('Is_NBN_Transition__c') && )
                belongID.add((String)caseObj.get('Belong_Id__c'));
            
            if(caseObj.get('AccountId') != NULL)//(boolean)caseObj.get('Is_NBN_Transition__c') &&
                belongID.add((String)caseObj.get('AccountId'));
        }
        
        //Put all related accounts in a map for future reference
        String IFBOTRecordType = caseRecordTypeInfoName.get('IFBOT').getrecordTypeId();
        for(Account acc: [SELECT Id, Octane_Customer_Number__c, (SELECT id, CaseNumber
                                                                    FROM Cases 
                                                                    WHERE RecordtypeId =: IFBOTRecordType 
                                                                    ORDER BY createdDate LIMIT 1)
                        FROM Account 
                        WHERE Octane_Customer_Number__c != NULL AND
                            ID != NULL AND
                            (Octane_Customer_Number__c IN: belongID OR
                                ID IN: belongID)])
        {
            if(!mapBelongIdAccountID.containsKey(acc.Octane_Customer_Number__c))
                mapBelongIdAccountID.put(acc.Octane_Customer_Number__c, acc.Id);
        
            if(!acc.Cases.isEmpty())
            {
                Case iFBOTCase = acc.Cases;
                accountIdIFBOTCaseId.put(acc.Id, iFBOTCase.Id);
            }
        }
        collectContactData();
    }
    
    private static void collectContactData()
    {
        for(Contact cont : [Select Id, Email, AccountId
                            FROM Contact 
                            WHERE Contact_Role__c = 'Primary' 
                            AND AccountId IN: mapBelongIdAccountID.values()])
        {
            if(!mapAccountIdPrimaryContact.containsKey(cont.AccountId))
                mapAccountIdPrimaryContact.put(cont.AccountId, cont);
        }
    }
    
    public static void setRecordType(sObject newSObj)
    {
        if((boolean)newSObj.get('Is_NBN_Transition__c'))
        {
            newSObj.put('RecordTypeId', caseRecordTypeInfoName.get('ADSL to NBN Transitions').getrecordTypeId());
            newSObj.put('Origin', 'Recovery Team (Load)');
        }
    }
    
    public static void setAccountRef(sObject newSObj)
    {   
        if(newSObj.get('AccountId') == NULL && String.IsNotEmpty((String)newSObj.get('Belong_Id__c')))
        {
            String caseRecordType = caseRecordTypeInfoId.get((Id)newSObj.get('RecordTypeId')).getName();
            if(mapBelongIdAccountID.containsKey((String)newSObj.get('Belong_Id__c')))
            {
                String extId = mapBelongIdAccountID.get((String)newSObj.get('Belong_Id__c'));
                if(String.isNotBlank(extId))
                {
                    newSObj.put('AccountId', extId);
                }
            }
            else if((boolean)newSObj.get('Is_NBN_Transition__c') || caseRecordType == 'Customer Call Back')
            {
                newSObj.addError('Case cannot be opened due to there is not an account with ID:' +(String)newSObj.get('Belong_Id__c') +' to relate the record.');
            }
        }
        else if((boolean)newSObj.get('Is_NBN_Transition__c') && newSObj.get('AccountId') == NULL && String.IsEmpty((String)newSObj.get('Belong_Id__c')))
        {
            newSObj.addError('Case cannot be opened due to the account Id is not specified');
        }
    }
    
    public static void setContactData(sObject newSObj)
    {
        if((Id)newSObj.get('ContactId') == NULL && newSObj.get('AccountId') != NULL)
        {
            Id accId = (Id)newSObj.get('AccountId');
            if(mapAccountIdPrimaryContact.containsKey(accId))
            {
                newSObj.put('ContactId' , mapAccountIdPrimaryContact.get(accId).Id);
            }
        }
    }
    
    public static void setCallBackCaseIFBOTCase(sObject newSObj)
    {
        String caseRecordType = caseRecordTypeInfoId.get((Id)newSObj.get('RecordTypeId')).getName();
        if(caseRecordType == 'Customer Call Back')
        {
            newSObj.put('Follow_Up_Date__c', GlobalUtil.AddBusinessDays(system.Now(), 2));
            if(accountIdIFBOTCaseId.containsKey((Id)newSObj.get('AccountId')))
                newSObj.put('ParentId', accountIdIFBOTCaseId.get((Id)newSObj.get('AccountId')));
        }
    }
    
    public static void setExpectedDates(sObject newSObj, boolean checkResolutionAgreed)
    {
        if(newSObj.get('recordtypeId')!= NULL && caseRecordTypeInfoId.get((Id)newSObj.get('recordtypeId')).getName() != 'Complaints')
            return;
        
        if((String)newSObj.get('Complaint_Category__c') == 'General')
        {
            // Adding 5 days for General
            // The date conversion in this will drop the time details
            newSObj.put('Expected_Resolution_Agreed_Date__c', GlobalUtil.addBusinessDays(system.now().date(), 5));
        }
        else if((String)newSObj.get('Complaint_Category__c') == 'TIO' || (String)newSObj.get('Complaint_Category__c') == 'High Risk')
        {
            // Adding 3 days for TIO or High Risk
            newSObj.put('Expected_Resolution_Agreed_Date__c', GlobalUtil.addBusinessDays(system.now().date(), 3));
        }
        
        if(checkResolutionAgreed)
        {
            // Set the exp res exec date only if "Resolution Agreed" has been ticked
            if((String)newSObj.get('Complaint_Category__c') == 'General' || (String)newSObj.get('Complaint_Category__c') == 'TIO' || 
                (String)newSObj.get('Complaint_Category__c') == 'High Risk' && (boolean)newSObj.get('Resolution_Agreed__c'))
            {
                // Adding 10 days
                newSObj.put('Expected_Resolution_Execution_Date__c', GlobalUtil.addBusinessDays(system.now().date(), 10));
            }
        }
    }
    
    public static void relateParentCase(sObject newSObj)
    {
        if(mapContactIdCases.containsKey((Id)newSObj.get('ContactId')))
        {
            for(Case cs: mapContactIdCases.get((Id)newSObj.get('ContactId')))
            {
                if(String.isNotBlank(cs.Subject) && String.isNotBlank((String)newSObj.get('Subject')) && ((String)newSObj.get('Subject')).contains(cs.Subject))
                {
                    if(cs.ParentId != NULL)
                    {
                        newSObj.put('ParentId', cs.ParentId);
                    } 
                    else
                    {
                        newSObj.put('ParentId', cs.Id);
                    }
                    break;
                }
            }
        }
    }
    
    //updates the "expected" date fields if the Complaint Category or Resolution Agreed has been changed
    public static void verifyExpectedDates(sObject oldSObj, sObject newSObj)
    {
        if((String)newSObj.get('Complaint_Category__c') != (String)oldSObj.get('Complaint_Category__c'))
        {
            // If the Resolution Agreed has been changed to true
            boolean checkRAgreed = false;
            if((boolean)newSObj.get('Resolution_Agreed__c') && !(boolean)oldSObj.get('Resolution_Agreed__c') && 
                newSObj.get('Expected_Resolution_Execution_Date__c') != NULL)
                checkRAgreed = true;
            
            setExpectedDates(newSObj, checkRAgreed);
        }
    }
    
    public static void checkCaseClose(sObject oldSObj, sObject newSObj)
    {
        if((String)oldSObj.get('Status') != (String)newSObj.get('Status') && (String)newSObj.get('Status') == 'Closed')
        {
            closedCases.put((Id)newSObj.get('Id'), newSObj);
        }
    }
    
    public static void closeChildCases()
    {
        List<Case> updateChildCases = new List<Case>();
        for(Case childObj: [SELECT Id, Subject, AccountId, Reason, ContactId, ParentId, RecordtypeId, Area__c, Product_and_Service__c 
                            FROM Case 
                            WHERE ParentId IN: closedCases.keySet()])
        {
            String rootCause = 'Closed by Parent';
            String subRootCause = 'Closed by Parent';
            String childProductAndService;
            
            if(String.isEmpty(childObj.Product_and_Service__c))
            {
                childProductAndService=(String)closedCases.get(childObj.ParentId).get('Product_and_Service__c');
            }
            else
            {
                childProductAndService = childObj.Product_and_Service__c;
            }

            updateChildCases.add(new Case(Id = childObj.Id, 
                Reason = (String)closedCases.get(childObj.ParentId).get('Reason'),
                Root_Cause__c = rootCause,
                Sub_Root_Cause__c = subRootCause,
                Product_and_Service__c = childProductAndService,
                Status='Closed'));
        }
        
        //Make sure the current cases will not be use on further iterations of the trigger
        closedCases.clear();
        if(!updateChildCases.isEmpty())
            update updateChildCases;
    }
    
    //If case is no complian when create, Reminder and stage fields need to be adjusted accordingly with the ADSL NBN process  
    public static void adjustNoCompliantADSLNBNCase(sObject newSObj)
    {
        if(caseRecordTypeInfoId.get((Id)newSObj.get('recordtypeId')).getName() != 'ADSL to NBN Transitions')
            return;
            
        if((boolean)newSObj.get('NBN_No_Compliant__c') && (String)newSObj.get('Reminder_Stage__c') == 'Initial Notification')
        {
            Date disconectionDate = (Date)newSObj.get('HFL_Disconnection_Date__c');
            integer notifDays = integer.valueof(notifSetting.Second_Reminder_Before_Disconnection__c) * -1;
            Date reminderDue = disconectionDate.addDays(notifDays);
                        
            if(reminderDue > system.today())
            {
                newSObj.put('Reminder_Date__c', reminderDue);
                newSObj.put('Reminder_Stage__c', '1st Reminder (Email)');
                return;
            }
            
            notifDays = integer.valueof(notifSetting.Third_Reminder_Before_Disconnection__c) * -1;
            reminderDue = disconectionDate.addDays(notifDays);
            if(reminderDue > system.today())
            {
                newSObj.put('Reminder_Date__c',  reminderDue);
                newSObj.put('Reminder_Stage__c', '2nd Reminder (Email)');
                return;
            }
            
            notifDays = integer.valueof(notifSetting.Fourth_Reminder_Before_Disconnection_del__c) * -1;
            reminderDue = disconectionDate.addDays(notifDays);
            if(reminderDue > system.today())
            {
                newSObj.put('Reminder_Date__c',  reminderDue);
                newSObj.put('Reminder_Stage__c', 'Final Notice (Email)');
                return;
            }
            
            if(disconectionDate < system.today())
            {
                newSObj.addError('Case cannot be opened due to the disconnection date has already passed.');
                return;
            }
        }
    }
    
    public static void followUpMoveCase(sObject newSObj)
    {
        if(caseRecordTypeInfoId.get((Id)newSObj.get('recordtypeId')).getName() != 'Move Request')
            return;
        
        newSObj.put('Follow_Up_Date__c', GlobalUtil.AddBusinessDays(system.Now(), 1));
    }
    
    //Update IFBOT field on parent Account (Marketing cloud purposes)
    public static void setAccountIFBOTfield(sObject newSObj)
    {
        if(caseRecordTypeInfoId.get((Id)newSObj.get('recordtypeId')).getName() != 'IFBOT' || 
            newSObj.get('AccountId') == NULL)
            return;
        Id accID = (Id)newSObj.get('AccountId');
        Account accUpdate = new Account(Id = accID, Case_IFBOT__c = (Id)newSObj.get('Id'));
        for(integer i=0; i< updateIFBOTAccount.size() ;i++)
        {
            if(updateIFBOTAccount[i].Id == accUpdate.Id)
            {
                updateIFBOTAccount[i].Case_IFBOT__c = accUpdate.Case_IFBOT__c;
                return;
            }
        }
        updateIFBOTAccount.add(accUpdate);
    }
    
    public static void updateAccountIFBOTcaseField()
    {
        if(!updateIFBOTAccount.isEmpty())
        {
            update updateIFBOTAccount;
        }
    }
    
    // Assign Case to Queue if "Assigned Queue" field has been set
    public static void assignToQueue(sObject oldSObj, sObject newSObj)
    {
        // Only work with Mobile cases
        if(caseRecordTypeInfoId.get((Id)newSObj.get('recordtypeId')).getName() != 'Mobile Support')
            return;
        
        // Only update if Category has been changed or this is a new case creation
        if( oldSObj == null || (oldSobj.get('Category__c') != newSObj.get('Category__c')) ) {
            
            // Get queue name for the selected Category
            map<string, Knowledge_Category_to_Queue__c> mapAllCatCustomSettings = Knowledge_Category_to_Queue__c.getAll();
            map<string, string> mapCategoryToQueue = new map <string, string>();
            for(Knowledge_Category_to_Queue__c currentEntry : mapAllCatCustomSettings.values()) {
                mapCategoryToQueue.put(currentEntry.Name.toLowerCase(), currentEntry.Queue__c);
            }
            
            // Set the Assigned Queue field so the rules can decide how to set Owner
            if( newSObj.get('Category__c') != null && mapCategoryToQueue.containsKey(((string)newSObj.get('Category__c')).toLowerCase()) ) {
            	newSObj.put('Assigned_Queue__c', mapCategoryToQueue.get(((string)newSObj.get('Category__c')).toLowerCase()) );
            }
        }
        return;
    }
}