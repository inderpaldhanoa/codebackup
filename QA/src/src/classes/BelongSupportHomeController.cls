/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  @created : 26/04/2016
  @Description : Controller Class for Belong Support Home page .
*/
public with sharing class BelongSupportHomeController 
{
	/* ***** HANDLE TO CURRENT INSTANCE OF CONTROLLER (to be passed to rendered VF components, avoids re-instantiation of controller) ***** */
	public BelongSupportHomeController pkbCon { get { return this; } }
	
	/* ***** KAV EXTENSION, used in VF pages when need to derive article type name from whole object name ***** */
	//private final static String KAV_EXTENSION = '__kav';
	/* ***** STANDARD CONTROLLER AND RECORD PROPS AND COLLECTIONS HELLO ***** */
	public List<Public_Knowledge_Topic__c> knowledgeTopics 							{ get; set; }
	public Map<String,List<Public_Knowledge_Product__c>> knowledgeProducts 			{ get; set; }
	public Map<String,Public_Knowledge_URL_Mapping__c> knowledgeURL 				{ get; set; }
	public List<Public_Knowledge_Product_Display_Setting__c> knowledgeSettingProd 	{ get; set; }
	public Public_Knowledge_URL_Mapping__c knowledgeMetaSetting						{ get; set; }
	
  	public String publishStatus { get { return DEFAULT_PUBLISH_STATUS; } }
	
	public static String DEFAULT_PUBLISH_STATUS = 'online';
	public static String DEFAULT_SITE_NAME = Site.getName();
	public String siteName { get { return DEFAULT_SITE_NAME; } }
	public Boolean isSite { get { return String.isNotBlank(Site.getName()); } }
	
	public String categorySelected 	{ get; set; }
	public String URLButton	 		{ get; set; }
	public String redirectURL		{ get; set; }
	
	public String keywordsMeta 			{ get; set; }
	public String descriptionMeta 		{ get; set; }
	public String titleMeta 			{ get; set; }
	public String urlMetaOG 			{ get; set; }
	public String keywordsMetaOG 		{ get; set; }
	public String descriptionMetaOG 	{ get; set; }
	public String titleMetaOG 			{ get; set; }
	public String imageMetaOG 			{ get; set; }
	
	
	// these fields must be queried when instantiating a KAD wrapper object, so this set is
	// used in the sosl and soql queries to ensure that all queries get the right fields
  	public String pageTitle 
	{
    	get 
    	{
      		String t = 'Belong Support - Home';
      		return t;
    	}
  	}
  	
  	public String currentSiteUrl 
  	{
    	set;
    	get 
    	{
      		if (currentSiteUrl == null) 
      		{
        		currentSiteUrl = Site.getBaseUrl()+'/';//Site.getCurrentSiteUrl();
        		if (!isSite) 
        		{
        			currentSiteUrl = Page.Belong_Support_Home.getUrl() + '/';
        		}
      		}
      		return currentSiteUrl;
    	}
      	
	}
	
	public Map<String,String> urlRedirect = new Map<String,String>
		{
			'http://help.adsl.belong.com.au/' 	=> 'http://support.belong.com.au/adsl', 
			'http://help.nbn.belong.com.au/'		=> 'http://support.belong.com.au/nbn'
		};
  	
	/* ***** CONTROLLER EXTENSION CONSTRUCTOR ***** */
	public BelongSupportHomeController(){}
	public BelongSupportHomeController(ApexPages.StandardController sc) 
	{
		knowledgeTopics = KnowledgeSiteUtil.buildTopicDisplay();
		knowledgeProducts = KnowledgeSiteUtil.buildProductDisplay();
		knowledgeURL = KnowledgeSiteUtil.buildURLMapping();
		knowledgeSettingProd =  KnowledgeSiteUtil.buildSettingsDisplay();
		system.debug(logginglevel.error, 'knowledgeSettingProd => ' + knowledgeSettingProd);
		system.debug(logginglevel.error, 'knowledgeProducts => ' + knowledgeProducts);
		
		constructMetadata();
	}
	
	public void constructMetadata()
	{
		knowledgeMetaSetting = knowledgeURL.get('Home');
		
		integer	sizeMetaTitle = 70;
		String normTitleMeta = '';
		String normDescMeta = '';
		if(String.IsNotBlank(knowledgeMetaSetting.SERP_Title__c))
		{
			sizeMetaTitle -= knowledgeMetaSetting.SERP_Title__c.length() + 1;
		}
		
		if(String.IsNotBlank(knowledgeMetaSetting.Meta_Title__c))
		{
			normTitleMeta = knowledgeMetaSetting.Meta_Title__c;
			if(normTitleMeta.length() > sizeMetaTitle)
			{
				normTitleMeta = normTitleMeta.substring(0, sizeMetaTitle);
			}
		}
		
		if(String.IsNotBlank(knowledgeMetaSetting.SERP_Title__c))
		{
			normTitleMeta +=  ' ' + knowledgeMetaSetting.SERP_Title__c;
		}
		
		if(String.IsNotBlank(knowledgeMetaSetting.Meta_Description__c))
		{
			normDescMeta  = knowledgeMetaSetting.Meta_Description__c;
			if(normDescMeta.length() > 155)
			{
				normDescMeta 		 +=  normDescMeta.substring(0, 155);
			}
		}
		
		titleMeta 			= normTitleMeta;
		descriptionMeta		= normDescMeta;
		
		sizeMetaTitle = 70;
		normTitleMeta = '';
		normDescMeta = '';
		
		if(String.IsNotBlank(knowledgeMetaSetting.Meta_OG_Title__c))
		{
			normTitleMeta = knowledgeMetaSetting.Meta_OG_Title__c;
			if(normTitleMeta.length() > sizeMetaTitle)
			{
				normTitleMeta = normTitleMeta.substring(0, sizeMetaTitle);
			}
		}
		
		if(String.IsNotBlank(knowledgeMetaSetting.SERP_Title__c))
		{
			normTitleMeta +=  ' ' + knowledgeMetaSetting.SERP_Title__c;
		}
		
		if(String.IsNotBlank(knowledgeMetaSetting.Meta_OG_Description__c))
		{
			normDescMeta  = knowledgeMetaSetting.Meta_OG_Description__c;
			if(normDescMeta.length() > 155)
			{
				normDescMeta 		 +=  normDescMeta.substring(0, 155);
			}
		}
		
    	titleMetaOG 		= normTitleMeta;
    	descriptionMetaOG	= normDescMeta;
    	
    	if(String.IsNotBlank(knowledgeMetaSetting.Meta_Keyword__c))
		{
			keywordsMeta = knowledgeMetaSetting.Meta_Keyword__c;	
		}

		if(String.IsNotBlank(knowledgeMetaSetting.Meta_OG_Keywords__c))
		{
			keywordsMetaOG = knowledgeMetaSetting.Meta_OG_Keywords__c;	
		}
		
		if(String.IsNotBlank(knowledgeMetaSetting.Meta_OG_URL__c))
		{
			urlMetaOG = knowledgeMetaSetting.Meta_OG_URL__c;	
		}
		
		if(String.IsNotBlank(knowledgeMetaSetting.Meta_OG_Description__c))
		{
			descriptionMetaOG = knowledgeMetaSetting.Meta_OG_Description__c;	
		}
		
		if(String.IsNotBlank(knowledgeMetaSetting.Meta_OG_Image__c))
		{
			imageMetaOG	= knowledgeMetaSetting.Meta_OG_Image__c;	
		}
	}
	
	public PageReference initCheckDomain()
	{
	 	system.debug(logginglevel.error, 'BelongSupportHomeController @initCheckDomain redirecting currentSiteUrl'+ currentSiteUrl);
	 	
		if(urlRedirect.containsKey(currentSiteUrl))
		{
			PageReference pageInternal;
			system.debug(logginglevel.error, 'BelongSupportHomeController @initCheckDomain redirecting ' );
			pageInternal = new PageReference(urlRedirect.get(currentSiteUrl));
			pageInternal.setRedirect(true);
			return pageInternal;
		}
		Return Null;
	}
}