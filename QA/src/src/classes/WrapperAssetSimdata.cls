public class WrapperAssetSimdata {
  public string errorCode;
  public String simtype;
  public Asset asset;
  public Sim_Data__c simdata;
  public WrapperAssetSimdata(String simtype, Asset asset, Sim_Data__c simdata,string errorCode) {
    this.simtype = simtype;
    this.asset = asset;
    this.simdata = simdata;
    this.errorCode=errorCode;
  }
}