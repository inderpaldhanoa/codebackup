@isTest
public class TaskTriggerHelperTest {
    @testSetup public static void setupData(){
        Survey__c s = new Survey__c(Name='PostCallSurvey',Submit_Response__c='Thank you',URL__c='www.something.com');
        insert s;
        insert new Survey_Question__c(Name='how likely are you to recommend Belong to your family and friends?',Survey__c=s.Id,Question__c='Some Question',OrderNumber__c=1);
        insert new Survey_Question__c(Name='Did you get the help you needed?',Survey__c=s.Id,Question__c='Some Question',OrderNumber__c=2);
        Map<String,Schema.RecordTypeInfo>accRecordTypeInfoName = GlobalUtil.getRecordTypeByName('Account');
        
        Account sAccount = new Account();
        sAccount.RecordTypeId = accRecordTypeInfoName.get('Residential').getRecordTypeId();
        sAccount.Name = 'Test Account';
        sAccount.Phone = '0412345678';
        sAccount.Customer_Status__c = 'Active';
        sAccount.Octane_Customer_Number__c = '123456';
        insert sAccount;
        
        Contact sContact = new Contact();
        sContact.FirstName = 'Joe';
        sContact.LastName = 'Belong';
        sContact.AccountId = sAccount.Id;
        sContact.Contact_Role__c = 'Primary';
        insert sContact;
        
        Lead ld = TestUtil.creatTestLead(sAccount.id);    
        insert ld;
    }
    
    public static testMethod void testOne(){
        Contact c = [SELECT Id FROM Contact LIMIT 1];
        Account a = [SELECT Id FROM Account LIMIT 1];
        Task task1 = new Task(WhoId = c.Id, WhatId = a.Id,Call_ANI__c='+6132432443');
        Task task2 = new Task(WhoId = c.Id, WhatId = a.Id,Call_ANI__c='+6132432443');
        Test.startTest();
        insert new Task[]{task1,task2};
        task1.NPS_Resolution__c = 'Some Resolution';
        task2.NPS_Score__c = 'Some Score';
        update new Task[]{task1,task2};
        Test.stopTest();
        List<SurveyQuestionResponse__c> responseList = [SELECT Id,Response__c,Survey_Date__c,Survey_Question__c,SurveyTaker__c FROM SurveyQuestionResponse__c];
        System.assert(responseList.size() == 4);
        List<task> createdTasks = [SELECT Id,Call_ANI__c FROM Task];
       // system.assert(!createdTasks[0].Call_ANI__c.startsWith('+61') && createdTasks[0].Call_ANI__c.startsWith('0'));
    }
    
    public static testMethod void testNegative(){
        Contact c = [SELECT Id FROM Contact LIMIT 1];
        Account a = [SELECT Id FROM Account LIMIT 1];
        Task task1 = new Task(WhoId = c.Id, WhatId = a.Id,Call_ANI__c='+6132432443');
        Task task2 = new Task(WhoId = c.Id, WhatId = a.Id,Call_ANI__c='+6132432443');
        Test.startTest();
        insert new Task[]{task1,task2};
        update new Task[]{task1,task2};
        Test.stopTest();
        List<SurveyQuestionResponse__c> responseList = [SELECT Id,Response__c,Survey_Date__c,Survey_Question__c,SurveyTaker__c FROM SurveyQuestionResponse__c];
        System.assert(responseList.size() != 4);
    }
    
    public static testMethod void testBulkInsert(){
        Contact c = [SELECT Id FROM Contact LIMIT 1];
        Account a = [SELECT Id FROM Account LIMIT 1];
        List<Task> taskList = new List<Task>();
        for(integer i = 0; i < 2000; i++){
            taskList.add(new Task(WhoId = c.Id, WhatId = a.Id,Call_ANI__c='+6132432443'));
        }
        Test.startTest();
        insert taskList;
        Test.stopTest();
        Task createdTask = [SELECT Id,Call_ANI__c FROM Task LIMIT 1];
        system.assert(!createdTask.Call_ANI__c.startsWith('+61') && createdTask.Call_ANI__c.startsWith('0'));
    }
    
    public static testMethod void testBulkUpdate(){
        Contact c = [SELECT Id FROM Contact LIMIT 1];
        Account a = [SELECT Id FROM Account LIMIT 1];
        List<Task> taskList = new List<Task>();
        for(integer i = 0; i < 2000; i++){
            taskList.add(new Task(WhoId = c.Id, WhatId = a.Id));
        }
        insert taskList;
        Test.startTest();
        for(integer i = 0; i < 2000; i++){
            taskList[i].NPS_Resolution__c = 'Some Resolution';
        }
        update taskList;
        Test.stopTest();
        system.assert([SELECT Id,Contact__c FROM SurveyTaker__c].size() == 2000);
        system.assert([SELECT Id,Response__c,Survey_Date__c,Survey_Question__c,SurveyTaker__c FROM SurveyQuestionResponse__c].size() == 4000);
    }
}