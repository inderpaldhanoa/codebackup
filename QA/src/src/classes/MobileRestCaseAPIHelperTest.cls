/************************************************************************************************************
* Apex Class Name	: MobileRestCaseAPIHelperTest.cls
* Version 			: 1.0 
* Created Date  	: 26 July 2017
* Function 			: Test class for MobileRestCaseAPIHelper.
* Modification Log	:
* Developer				Date				Description
* -----------------------------------------------------------------------------------------------------------
* Tom Clayton			26/07/2017 			Created Class.
************************************************************************************************************/

@isTest
public with sharing class MobileRestCaseAPIHelperTest {
    
	public static Account account1;
	public static Contact contact1, contact2;
	public static Case case1, case2;
	public static Lead lead1;
	
	@testSetup static void setupTestData()
	{
		// Create test data 
		account1 = TestDataFactoryMobile.createAccounts(1)[0];
		system.debug('--Created an account: ' + account1);
		insert account1;
		
		list <Contact> lstContacts = TestDataFactoryMobile.createContacts(2);
		system.debug('--Created a list of contacts: ' + lstContacts);
		lstContacts[0].AccountId = account1.Id;
		lstContacts[1].AccountId = account1.Id;
		insert lstContacts;
		
		Public_Knowledge_Topic__c publicKnowledgeTopic = TestDataFactoryMobile.createPublicKnowledgeTopics(1)[0];
		insert publicKnowledgeTopic;
		
		list <Case> lstCases = TestDataFactoryMobile.createCases(2);
		lstCases[0].AccountId = account1.Id;
		lstCases[0].ContactId = lstContacts[0].Id;
		lstCases[1].AccountId = account1.Id;
		lstCases[1].ContactId = lstContacts[1].Id;
		insert lstCases;
		
		lead1 = new Lead(FirstName='Test', LastName='Lead', Company='Test Company');
		insert lead1;
	}
	
	static testMethod void GetCasesTest() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        account1 = [SELECT Id, Name, Mobile_Octane_Customer_Number__c FROM Account][0];
        
        req.requestURI = '/services/apexrest/RESTMobileAPI?APICall=getCases&octaneId=' + account1.Mobile_Octane_Customer_Number__c + '&startOffset=0&maxNumOfResults=10';
        req.params.put('APICall', 'getCases');
        req.params.put('octaneId', account1.Mobile_Octane_Customer_Number__c);
        req.params.put('startOffset', '0');
        req.params.put('maxNumOfResults', '10');
        
        // Fake the passed call
        req.requestBody = Blob.valueOf('Test Body');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        	RESTMobileAPI.getMobile();
        Test.stopTest();
        
        String testResponseBody = res.responseBody.toString();
        
        system.debug('--The GetCasesTest responseBody was: ' + testResponseBody);
        system.assert(testResponseBody.contains('trouble with an xyz'), 'The GetCasesTest responseBody did not contain the Description in the test data Case.');
    }
	
	static testMethod void commentCaseTest() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        account1 = [SELECT Id, Name, Mobile_Octane_Customer_Number__c FROM Account][0];
        list <Case> lstCases = [SELECT Id FROM Case];
        
        // Could get more coverage here by setting up an "External Identity User" which also matches the Octane Id
        string jsonInput = '{ "caseId": "' + lstCases[0].Id + '", '
        					+ '"octaneId": "' + account1.Mobile_Octane_Customer_Number__c + '",'
        					+ '"comment": "Magical walnut paste with orange peel sprinkles.",'
        					+ '"fileData": [{"fileName": "testFile.csv", "fileBlob": "p398rhaidhfsdkjfh"},{"fileName": "testFile2.csv", "fileBlob": "p398rhaidhfsdkjfh2"}]}';
        
        req.requestURI = '/services/apexrest/RESTMobileAPI?APICall=commentCase';
        req.params.put('APICall', 'commentCase');
        
        // Fake the passed call
        req.requestBody = Blob.valueOf(jsonInput);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        	RESTMobileAPI.postMobile();
        Test.stopTest();
        
        String testResponseBody = res.responseBody.toString();
        
        system.debug('--The commentCaseTest responseBody was: ' + testResponseBody);
        system.assert(testResponseBody.contains('Successfully updated Case'), 'The commentCaseTest responseBody did not include the success message.');
    }
    
    static testMethod void createCaseAuthenticatedTest() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        account1 = [SELECT Id, Name, Mobile_Octane_Customer_Number__c FROM Account][0];
        // Problem here - TestDataFactoryMobile doesn't yet have the right data to insert a FAQ__kav
        //FAQ__kav kav = TestDataFactoryMobile.createKnowledgeArticle();
        //system.debug('--Made this kav: ' + kav);
        //KbManagement.PublishingService.publishArticle(kav.Id, true);
        
        string jsonInput = '{ "octaneId": "' + account1.Mobile_Octane_Customer_Number__c + '",'
						    + '"email": "testerdffsy@hotmail.com",'
						    + '"knowledgeArticle": "ka0O0000000936FIAQ",'
						    + '"subject": "This is a Case to log a problem",'
						    + '"description": "My SIM card is cooking my phone. There is steam coming out. Ahhhh",'
						    + '"fileData": [{"fileName": "testFile.csv", "fileBlob": "p398rhaidhfsdkjfh"},{"fileName": "testFile2.csv", "fileBlob": "p398rhaidhfsdkjfh2"}]}';
        
        req.requestURI = '/services/apexrest/RESTMobileAPI?APICall=createCase';
        req.params.put('APICall', 'createCase');
        
        // Fake the passed call
        req.requestBody = Blob.valueOf(jsonInput);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        	RESTMobileAPI.postMobile();
        Test.stopTest();
        
        String testResponseBody = res.responseBody.toString();
        
        system.debug('--The createCaseAuthenticatedTest responseBody was: ' + testResponseBody);
        //system.assert(testResponseBody.contains('Successfully'), 'The responseBody did not include the success message.');
    }
    
    static testMethod void createCaseUnauthenticatedTest() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        account1 = [SELECT Id, Name, Mobile_Octane_Customer_Number__c FROM Account][0];
        // Problem here - TestDataFactoryMobile doesn't yet have the right data to insert a FAQ__kav
        //FAQ__kav kav = TestDataFactoryMobile.createKnowledgeArticle();
        //system.debug('--Made this kav: ' + kav);
        //KbManagement.PublishingService.publishArticle(kav.Id, true);
        
        string jsonInput = '{ "email": "qwetqerwerwer@hotmail.com",'
					        + '"firstName": "TestX",'
					        + '"lastNAme": "TestY",'
						    + '"knowledgeArticle": "ka0O0000000936FIAQ",'
						    + '"subject": "This is a Case to log a problem unauthenticated",'
						    + '"description": "My SIM card is cooking my phone. There is steam coming out. Ahhhh",'
						    + '"fileData": [{"fileName": "testFile.csv", "fileBlob": "p398rhaidhfsdkjfh"},{"fileName": "testFile2.csv", "fileBlob": "p398rhaidhfsdkjfh2"}]}';
        
        req.requestURI = '/services/apexrest/RESTMobileAPI?APICall=createCase';
        req.params.put('APICall', 'createCase');
        
        // Fake the passed call
        req.requestBody = Blob.valueOf(jsonInput);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        	RESTMobileAPI.postMobile();
        Test.stopTest();
        
        String testResponseBody = res.responseBody.toString();
        
        system.debug('--The createCaseUnauthenticatedTest responseBody was: ' + testResponseBody);
        //system.assert(testResponseBody.contains('Successfully'), 'The responseBody did not include the success message.');
    }
    
    static testMethod void createCaseInvalidJSONTest() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        account1 = [SELECT Id, Name, Mobile_Octane_Customer_Number__c FROM Account][0];
        // Problem here - TestDataFactoryMobile doesn't yet have the right data to insert a FAQ__kav
        //FAQ__kav kav = TestDataFactoryMobile.createKnowledgeArticle();
        //system.debug('--Made this kav: ' + kav);
        //KbManagement.PublishingService.publishArticle(kav.Id, true);
        
        string jsonInput = '{ "xxxy": "zzzzz",'
					        + '"xxx": "TestX",'
						    + '"fileData": [{"fileName": "testFile.csv", "fileBlob": "p398rhaidhfsdkjfh"},{"fileName": "testFile2.csv", "fileBlob": "p398rhaidhfsdkjfh2"}]}';
        
        req.requestURI = '/services/apexrest/RESTMobileAPI?APICall=createCase';
        req.params.put('APICall', 'createCase');
        
        // Fake the passed call
        req.requestBody = Blob.valueOf(jsonInput);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        	RESTMobileAPI.postMobile();
        Test.stopTest();
        
        String testResponseBody = res.responseBody.toString();
        
        system.debug('--The createCaseInvalidJSON responseBody was: ' + testResponseBody);
        //system.assert(testResponseBody.contains('Successfully'), 'The responseBody did not include the success message.');
    }
    
    static testMethod void createIVRCaseAuthenticatedTest() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        account1 = [SELECT Id, Name, Mobile_Octane_Customer_Number__c FROM Account][0];
        
        string jsonInput = '{ "octaneId": "' + account1.Mobile_Octane_Customer_Number__c + '",'
        					+ '"isIVRCase": true,'
						    + '"phoneNumber": "9448 7821",'
						    + '"parentCategory": "Fix a Problem",'
						    + '"category": "IVR Authenticated",'
						    + '"subject": "Still need help after IVR call",'
						    + '"description": "The user selected options related to SIM card fault. Please follow up."}';
        
        req.requestURI = '/services/apexrest/RESTMobileAPI?APICall=createCase';
        req.params.put('APICall', 'createCase');
        
        // Fake the passed call
        req.requestBody = Blob.valueOf(jsonInput);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        	RESTMobileAPI.postMobile();
        Test.stopTest();
        
        String testResponseBody = res.responseBody.toString();
        
        system.debug('--The createIVRCaseAuthenticatedTest responseBody was: ' + testResponseBody);
        //system.assert(testResponseBody.contains('Successfully'), 'The responseBody did not include the success message.');
    }
    
    static testMethod void createIVRCaseAuthenticatedInvalidTest() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        account1 = [SELECT Id, Name, Mobile_Octane_Customer_Number__c FROM Account][0];
        
        // This test does not have all the required fields so will be invalid JSON
        string jsonInput = '{ "octaneId": "' + account1.Mobile_Octane_Customer_Number__c + '",'
        					+ '"isIVRCase": true,'
						    + '"phoneNumber": "9448 7821",'
						    + '"description": "The user selected options related to SIM card fault. Please follow up."}';
        
        req.requestURI = '/services/apexrest/RESTMobileAPI?APICall=createCase';
        req.params.put('APICall', 'createCase');
        
        // Fake the passed call
        req.requestBody = Blob.valueOf(jsonInput);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        	RESTMobileAPI.postMobile();
        Test.stopTest();
        
        String testResponseBody = res.responseBody.toString();
        
        system.debug('--The createIVRCaseAuthenticatedInvalidTest responseBody was: ' + testResponseBody);
        //system.assert(testResponseBody.contains('Successfully'), 'The responseBody did not include the success message.');
    }
    
    static testMethod void createIVRCaseAuthenticatedLeadTest() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        lead1 = [SELECT Id, Name FROM Lead][0];
        
        string jsonInput = '{ "leadId": "' + lead1.Id + '",'
        					+ '"isIVRCase": true,'
						    + '"phoneNumber": "9448 7821",'
						    + '"parentCategory": "Fix a Problem",'
						    + '"category": "IVR Authenticated",'
						    + '"subject": "Still need help after IVR call",'
						    + '"description": "The user selected options related to SIM card fault. Please follow up."}';
        
        req.requestURI = '/services/apexrest/RESTMobileAPI?APICall=createCase';
        req.params.put('APICall', 'createCase');
        
        // Fake the passed call
        req.requestBody = Blob.valueOf(jsonInput);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        	RESTMobileAPI.postMobile();
        Test.stopTest();
        
        String testResponseBody = res.responseBody.toString();
        
        system.debug('--The createIVRCaseAuthenticatedLeadTest responseBody was: ' + testResponseBody);
        //system.assert(testResponseBody.contains('Successfully'), 'The responseBody did not include the success message.');
    }
    
    static testMethod void createIVRCaseAuthenticatedNoDOBTest() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        account1 = [SELECT Id, Name, Mobile_Octane_Customer_Number__c FROM Account][0];
        
        string jsonInput = '{ "octaneId": "' + account1.Mobile_Octane_Customer_Number__c + '",'
        					+ '"isIVRCase": true,'
						    + '"phoneNumber": "9448 7821",'
						    + '"parentCategory": "Fix a Problem",'
						    + '"category": "IVR Unauthenticated",'
						    + '"subject": "Still need help after IVR call",'
						    + '"description": "The user selected options related to SIM card fault. Please follow up."}';
        
        req.requestURI = '/services/apexrest/RESTMobileAPI?APICall=createCase';
        req.params.put('APICall', 'createCase');
        
        // Fake the passed call
        req.requestBody = Blob.valueOf(jsonInput);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        	RESTMobileAPI.postMobile();
        Test.stopTest();
        
        String testResponseBody = res.responseBody.toString();
        
        system.debug('--The createIVRCaseAuthenticatedNoDOBTest responseBody was: ' + testResponseBody);
        //system.assert(testResponseBody.contains('Successfully'), 'The responseBody did not include the success message.');
    }
    
    static testMethod void createIVRCaseUnauthenticatedTest() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        account1 = [SELECT Id, Name, Mobile_Octane_Customer_Number__c FROM Account][0];
        
        string jsonInput = '{ "isIVRCase": true,'
						    + '"phoneNumber": "9448 7821",'
						    + '"parentCategory": "Fix a Problem",'
						    + '"category": "IVR Unauthenticated",'
						    + '"subject": "Still need help after IVR call",'
						    + '"description": "The user selected options related to SIM card fault. Please follow up."}';
        
        req.requestURI = '/services/apexrest/RESTMobileAPI?APICall=createCase';
        req.params.put('APICall', 'createCase');
        
        // Fake the passed call
        req.requestBody = Blob.valueOf(jsonInput);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        	RESTMobileAPI.postMobile();
        Test.stopTest();
        
        String testResponseBody = res.responseBody.toString();
        
        system.debug('--The createIVRCaseUnauthenticatedTest responseBody was: ' + testResponseBody);
        //system.assert(testResponseBody.contains('Successfully'), 'The responseBody did not include the success message.');
    }
}