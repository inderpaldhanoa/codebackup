/************************************************************************************************************
* Apex Class Name	: WrapperCaseWithAttachment.cls
* Version 			: 1.0 
* Created Date  	: 24 April 2017
* Function 			: Wrapper class for Mobile API Case generation.
* Modification Log	:
* Developer				Date				Description
* -----------------------------------------------------------------------------------------------------------
* Tom Clayton			24/04/2017 			Created Class.
* Tom Clayton			26/04/2017 			Added constructor for fileData.
* Tom Clayton			01/05/2017 			Changed to use octane Id (authenticated) or email (unauthenticated).
* Tom Clayton			03/05/2017 			Added Knowledge Article field.
************************************************************************************************************/

public class WrapperCaseWithAttachment
{
    public string octaneId;
    public string knowledgeArticle;
    public string email;
    public string subject;
    public string description;
    public string recordType;
    public fileData[] fileData;
    public string firstName;
    public string lastName;
    public string phoneNumber;
    public boolean isComplaint;
    public boolean isIVRCase;
    public string parentCategory;
    public string category;
    public string leadId;
    public class fileData {
    	public string fileName;
    	public blob fileBlob;
    	public fileData(string inFileName, blob inFileBlob){
    		this.fileName = inFileName;
    		this.fileBlob = inFileBlob;
    	}
    }
    public static WrapperCaseWithAttachment parseCase(String json) {
    	return (WrapperCaseWithAttachment) System.JSON.deserialize(json, WrapperCaseWithAttachment.class);
    }
}