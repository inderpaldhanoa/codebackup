/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  @created : 17/05/2016
  @Description : Util class for the public knowledge site.
*/
public with sharing class KnowledgeSiteUtil
{
   //builds the Product structure and its categories
    public static Map<String,List<Public_Knowledge_Product__c>> buildProductDisplay()
    {
        
        Map<String,List<Public_Knowledge_Product__c>> knowledgeProducts = new Map<String,List<Public_Knowledge_Product__c>>();
        List<Public_Knowledge_Product__c> pbkProducts = [SELECT id, name, Article_Category__c, Data_Category__c, Meta_Description__c, Meta_Keyword__c, Meta_Name__c, Product_Category__c, Title__c, Weighted_Score__c from Public_Knowledge_Product__c order by Weighted_Score__c ASC NULLS LAST];
        
        List<Public_Knowledge_Product_Display_Setting__c> knowledgeSettingProd =  KnowledgeSiteUtil.buildSettingsDisplay();
        Set<String> availableProds = new Set<String>();
        
        
        for(Public_Knowledge_Product_Display_Setting__c prodSet : knowledgeSettingProd)
        {
            availableProds.add(prodSet.Name);
        }
        
        for(Public_Knowledge_Product__c pkbProd : pbkProducts)
        {
            if(!pkbProd.Name.right(2).IsNumeric() || !availableProds.contains(pkbProd.Product_Category__c))
                continue;
            
            if(!knowledgeProducts.containsKey(pkbProd.Product_Category__c))
            {
                knowledgeProducts.put(pkbProd.Product_Category__c, new List<Public_Knowledge_Product__c>());
            }
            
            knowledgeProducts.get(pkbProd.Product_Category__c).add(pkbProd);
            
        }
            
        return knowledgeProducts; 
    }
    
    public static Map<String,Public_Knowledge_URL_Mapping__c> buildURLMapping()
    {
        Map<String, Public_Knowledge_URL_Mapping__c> URLMap = new Map<String, Public_Knowledge_URL_Mapping__c>();
        List<Public_Knowledge_URL_Mapping__c> pbkURLs = Public_Knowledge_URL_Mapping__c.getall().values();
        
        for(Public_Knowledge_URL_Mapping__c pkbUrl : pbkURLs)
        {
            URLMap.put(pkbUrl.Name, pkbUrl);
        }
        
        return URLMap;
    }
    
    public static List<Public_Knowledge_Topic__c> buildTopicDisplay()
    {
        List<Public_Knowledge_Topic__c> knowledgeTopics = new List<Public_Knowledge_Topic__c>();
        
		//for(Public_Knowledge_Topic__c pKwTopic : Public_Knowledge_Topic__c.getall().values())
		for(Public_Knowledge_Topic__c pKwTopic : [SELECT Id, Active__c, Details__c, Product__c, Title__c, Page_Name__c FROM Public_Knowledge_Topic__c ORDER BY Display_Order__c ASC])
        {
            if(pKwTopic.Active__c)
                knowledgeTopics.add(pKwTopic);
        }
        return knowledgeTopics;
    }
    
    public static List<Public_Knowledge_Product_Display_Setting__c> buildSettingsDisplay()
    {
        List<Public_Knowledge_Product_Display_Setting__c> knowledgeProducts = new  List<Public_Knowledge_Product_Display_Setting__c> ();
        
        for(Public_Knowledge_Product_Display_Setting__c pKwDispProd : [SELECT Name, Product_Label__c 
                                                                        FROM Public_Knowledge_Product_Display_Setting__c 
                                                                        WHERE Active__c = true 
                                                                        ORDER BY Display_Order__c ASC])
        {
                knowledgeProducts.add(pKwDispProd);
        }
        
        return knowledgeProducts;
    }
    
    public static String join(List<String> strings, String c) 
    {
        if (strings == null || strings.isEmpty()) return null;

        String sep = (String.isBlank(c) ? ',' : c);
        Integer i, size;
        String s = strings[0];

        size = strings.size();
        for (i = 1; i < size; i++) 
        {
            s += (sep + strings[i]);
        }
        return s;
    }

    // version of join method that takes a set of strings
    public static String join(Set<String> strings, String c) 
    {
        return join(new List<String>(strings), c);
    }
}