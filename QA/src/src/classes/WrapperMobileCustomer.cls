/*
    @author  : Girish P(Girish.pauriyal@infosys.com)
    @created : 22/03/2017
    @Description : Warepper class for Mobile API for lead and customer generation
*/
public class WrapperMobileCustomer {
	public String firstName;	//Gc
	public String lastName;	//string
	public String email;	//string@gmail.com
	public String phoneNumber;	//string
	public String braintreeCustId;	//string
	public address address;
	public identities[] identities;
	public String mobileNo;	//string@gmail.com
	public String dateOfBirth;	//string
	public String userPassword;	//string
	public String dateTimePortTerms;	//1990-12-31T23:59:60Z
	public String simCardNo;
	public String currentMobileProvider;	//string@gmail.com
	public String acnCustomerNo;	//string
	public String portServiceType;
	public String termsAndConditions;
	public String portDateOfBirth;
	public class address {
		public String additionalAddress;	//string
		public String subType;	//string
		public String subNo;	//string
		public String streetNo;	//string
		public String streetName;	//string//this will have po box
		public String streetType;	//string
		public String streetAddress;
		public String suburb;	//string
		public String state;	//string
		public String postcode;	//string
		public String gnafId;	//string
		public String addrText;	//string
		public String country;	//string
	}
	public class identities {
		public String utilibillId;	//string
		public String accountType;	//mobile
	}
	public static WrapperMobileCustomer parseCustomer(String json) {
		return (WrapperMobileCustomer) System.JSON.deserialize(json, WrapperMobileCustomer.class);
	}

}