@isTest(seeAlldata = True)
public class SequenceFAQWeightedScoreTest {
    public static TestMethod void CheckFunction () {
        Public_Knowledge_Product__c testPKP = new Public_Knowledge_Product__c();
        testPKP.Product_Category__c = 'nbn';
        testPKP.title__c = 'Looking to join Belong';
        testPKP.Data_Category__c = 'NBN_Join__c';
        testPKP.Name = 'NBN930';
        insert testPKP;
        SequenceFAQWeightedScore SFAQWS = new SequenceFAQWeightedScore();
        FAQ__kav FAQ = [select id, ArticleNumber, Question__c, VersionNumber, Weighted_Score__c FROM FAQ__kav 
                        WHERE Language = 'en_US' AND PublishStatus = 'Online' 
                        WITH DATA CATEGORY Category__c at NBN_Join__c ORDER by Weighted_Score__c nulls last limit 1];
        List<Sobject> sobj = new List<Sobject>();
        sobj.add(FAQ);
        SFAQWS.OldAllRecords = sobj;
        
        SFAQWS.SelectedProductTitle();
        SFAQWS.SelectedPrdtTitle = 'Looking to join Belong';
        SFAQWS.selected_product_title.put('Looking to join Belong','NBN_Join__c');
        SFAQWS.getrecords();
        FAQ.Weighted_Score__c = 19;
        List<Sobject> sobjnew = new List<Sobject>();
        sobjnew.add(FAQ);
        SFAQWS.AllRecords = sobjnew;    
        SFAQWS.SaveReorderSequence();
        SFAQWS.cancel();
    }
}