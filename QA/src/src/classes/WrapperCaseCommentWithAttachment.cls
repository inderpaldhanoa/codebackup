/************************************************************************************************************
* Apex Class Name	: WrapperCaseCommentWithAttachment.cls
* Version 			: 1.0 
* Created Date  	: 19 June 2017
* Function 			: Wrapper class for Mobile API Case Comment.
* Modification Log	:
* Developer				Date				Description
* -----------------------------------------------------------------------------------------------------------
* Tom Clayton			19/06/2017 			Created Class.
************************************************************************************************************/

public class WrapperCaseCommentWithAttachment
{
    public string caseId;
    public string octaneId;
    public string comment;
    public fileData[] fileData;
    public class fileData {
    	public string fileName;
    	public blob fileBlob;
    	public fileData(string inFileName, blob inFileBlob){
    		this.fileName = inFileName;
    		this.fileBlob = inFileBlob;
    	}
    	/*public fileData(){
    		this.fileName = null;
    		this.fileBlob = null;
    	}*/
    }
    public static WrapperCaseCommentWithAttachment parseComment(String json) {
    	return (WrapperCaseCommentWithAttachment) System.JSON.deserialize(json, WrapperCaseCommentWithAttachment.class);
    }
}