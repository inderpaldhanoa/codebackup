<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>Getting_Started_With_Survey_Force</defaultLandingTab>
    <formFactors>Large</formFactors>
    <label>Survey Force</label>
    <tab>Getting_Started_With_Survey_Force</tab>
    <tab>Survey__c</tab>
    <tab>SurveyTaker__c</tab>
    <tab>standard-report</tab>
    <tab>SurveyQuestionResponse__c</tab>
    <tab>Service_Outage_Message__c</tab>
</CustomApplication>
