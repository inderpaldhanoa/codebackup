<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ADSL_to_NBN_Customer_Disconection_Proceed</fullName>
        <ccEmails>hello@belong.com.au</ccEmails>
        <description>ADSL to NBN Customer Disconection Proceed</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>hello@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Email_Templates/ADSL_TO_NBN_Disconnection</template>
    </alerts>
    <alerts>
        <fullName>ADSL_to_NBN_Customer_Final_Notice</fullName>
        <ccEmails>hello@belong.com.au</ccEmails>
        <description>ADSL to NBN Customer Final Notice</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>hello@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Email_Templates/ADSL_TO_NBN_Final</template>
    </alerts>
    <alerts>
        <fullName>ADSL_to_NBN_Customer_Notice</fullName>
        <ccEmails>hello@belong.com.au</ccEmails>
        <description>ADSL to NBN Customer Notice</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>hello@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Email_Templates/ADSL_TO_NBN_Standard</template>
    </alerts>
    <alerts>
        <fullName>Complaint_Agreed_Resolution_Execution_violation_reminder</fullName>
        <description>Complaint Agreed Resolution Execution violation reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Email_Templates/Complaint_Resolution_Agreement_Execution_SLA_violated</template>
    </alerts>
    <alerts>
        <fullName>Complaint_EscalatioReminder</fullName>
        <description>Complaint Escalation Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Email_Templates/Complaint_Escalation_reminder</template>
    </alerts>
    <alerts>
        <fullName>Complaint_Escalation_Notification</fullName>
        <description>Complaint Escalation Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Email_Templates/COMPLAINT_General_and_High_Risk_Escalation</template>
    </alerts>
    <alerts>
        <fullName>Complaint_Escalation_Reminder</fullName>
        <description>Complaint Escalation Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Email_Templates/Complaint_Escalation_reminder</template>
    </alerts>
    <alerts>
        <fullName>Complaint_Resolutioagreement_violation_reminder</fullName>
        <description>Complaint Resolution agreement violation reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Email_Templates/Complaint_Resolution_Agreement_SLA_violated</template>
    </alerts>
    <alerts>
        <fullName>Complaint_Resolution_agreement_violation_reminder</fullName>
        <description>Complaint Resolution agreement violation reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Email_Templates/Complaint_Resolution_Agreement_SLA_violated</template>
    </alerts>
    <alerts>
        <fullName>Complaint_compensation_request_has_been_approved</fullName>
        <description>Complaint compensation request has been approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Email_Templates/COMPLAINT_Compensation_Request_Approved</template>
    </alerts>
    <alerts>
        <fullName>Complaint_compensation_request_has_been_rejected</fullName>
        <description>Complaint compensation request has been rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Email_Templates/COMPLAINT_Compensation_Request_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Complaint_copensation_request_has_been_rejected</fullName>
        <description>Complaint compensation request has been rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Email_Templates/COMPLAINT_Compensation_Request_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Complait_compensation_request_has_been_approved</fullName>
        <description>Complaint compensation request has been approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Email_Templates/COMPLAINT_Compensation_Request_Approved</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_NBN_FTTB_SC_11_12</fullName>
        <description>Email Alert NBN FTTB SC 11 12</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Email_Templates/NBN_FTTB_SC_11_12_Confirmation_Email</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_NBN_FTTN_SC_11_12</fullName>
        <description>Email Alert NBN FTTN SC 11 12</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Email_Templates/NBN_FTTN_SC_11_12_Confirmation_Email</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_NBN_Switch_SC1_2_HFC_SC21_23</fullName>
        <description>Email Alert NBN Switch SC1 2 HFC SC21 23</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Email_Templates/NBN_FTTP_SC1_2_and_21_23_Confirmation_Email</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_for_NBN_SC_3_13_and_HFC_SC_24</fullName>
        <description>Email Alert for NBN SC 3 13 and HFC SC 24</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Email_Templates/NBN_SC_3_13_HFC_SC_24_Confirmation_Email</template>
    </alerts>
    <alerts>
        <fullName>Email_sent_to_a_customer_when_they_submit_a_moves_order_online</fullName>
        <description>Email sent to a customer when they submit a moves order online</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Email_Templates/Moves_Confirmation_Email</template>
    </alerts>
    <alerts>
        <fullName>New_Complaint_Creation_Notification</fullName>
        <description>New Complaint Creation Notification</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Email_Templates/Complaint_Case_creation_notification_email</template>
    </alerts>
    <alerts>
        <fullName>POD_Confirmation_email</fullName>
        <ccEmails>yourorder@2s9k5zyfit73we8b124xmzo5qiyx99fgna2jri0x0ouxf6wfxn.28-12ocheay.ap2.case.salesforce.com</ccEmails>
        <description>POD Confirmation email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>POD_Templates/POD_confirmation_email</template>
    </alerts>
    <alerts>
        <fullName>POD_Documents_required</fullName>
        <description>POD Documents required</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>yourorder@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>POD_Templates/Proof_of_Occupancy_Required</template>
    </alerts>
    <alerts>
        <fullName>POD_doc_final_reminder</fullName>
        <ccEmails>yourorder@2s9k5zyfit73we8b124xmzo5qiyx99fgna2jri0x0ouxf6wfxn.28-12ocheay.ap2.case.salesforce.com</ccEmails>
        <description>POD doc final reminder</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>POD_Templates/POD_Last_Reminder</template>
    </alerts>
    <alerts>
        <fullName>POD_doc_reminder_1</fullName>
        <ccEmails>yourorder@2s9k5zyfit73we8b124xmzo5qiyx99fgna2jri0x0ouxf6wfxn.28-12ocheay.ap2.case.salesforce.com</ccEmails>
        <description>POD doc reminder 1</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>POD_Templates/POD_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Support_Case_creation_notification_email</fullName>
        <description>An Email sent to customers request for a support agent to get back to them with troubleshooting issues</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Email_Templates/Support_Case_creation_notification_email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Agreed_ResolutiExecuted_SLA_Violated</fullName>
        <description>This is when a complaint has an agreed resolution but has not been executed after 10 business days of agreement.</description>
        <field>Agreed_Resolution_Executed_SLA_Violated__c</field>
        <literalValue>1</literalValue>
        <name>Agreed Resolution Executed SLA Violated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Agreed_Resolution_Executed_SLA_Violated</fullName>
        <description>This is when a complaint has an agreed resolution but has not been executed after 10 business days of agreement.</description>
        <field>Agreed_Resolution_Executed_SLA_Violated__c</field>
        <literalValue>1</literalValue>
        <name>Agreed Resolution Executed SLA Violated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Apply_Holiday_Plan_Queue_Assignment</fullName>
        <description>This is to assign new holiday plan request to apply holiday plan queue.</description>
        <field>OwnerId</field>
        <lookupValue>Apply_Holiday_Plan_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Apply Holiday Plan Queue Assignment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_Case_Contact_Address</fullName>
        <field>Asset_Shipping_Address__c</field>
        <formula>Contact.OtherStreet +&apos; &apos;+ Contact.OtherCity +&apos; &apos;+ Contact.OtherState +&apos; &apos;+ Contact.OtherPostalCode +&apos; &apos;+ Contact.OtherCountry</formula>
        <name>Asset Case Contact Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_case_to_Remove_holiday_queue</fullName>
        <description>This is to assign holiday plan request that have been applied to the &quot;Remove Holiday Plan Queue&quot;.</description>
        <field>OwnerId</field>
        <lookupValue>Remove_Holiday_Plan_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign case to Remove holiday queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CASE_Follow_up_Date_3_days_after</fullName>
        <description>Update the follow up field to 3 days after current date</description>
        <field>Follow_Up_Date__c</field>
        <formula>NOW() + 3</formula>
        <name>CASE Follow up Date 3 days after</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Owner_Held_queue</fullName>
        <description>Assigns the ownership of the case to the Held queue</description>
        <field>OwnerId</field>
        <lookupValue>Held_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case Owner to Held queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Owner_Update_to_Belong_Integration</fullName>
        <field>OwnerId</field>
        <lookupValue>belongadmin@belong.com.au</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Case Owner Update to Belong Integration</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Owner_Update_to_FOH_Team_Lead_Queue</fullName>
        <description>This is to update case owner to GM queue for cases escalated to FOH Team Lead Queue.</description>
        <field>OwnerId</field>
        <lookupValue>Belong_FOH_Team_Leaders_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case Owner Update to FOH Team Lead Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Owner_Update_to_Moves_Queue</fullName>
        <description>This is to assign the case to the Moves queue</description>
        <field>OwnerId</field>
        <lookupValue>Move_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case Owner Update to Moves Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Owner_Update_to_Operations_Queue</fullName>
        <description>This is to update a case owner to Operations queue when escalated to Operations team</description>
        <field>OwnerId</field>
        <lookupValue>Belong_Operations_Team_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case Owner Update to Operations Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Owner_is_Belong_BOH_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Belong_BOH_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case Owner is Belong BOH Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Owner_update_to_Belong_FOH_queue</fullName>
        <description>This is to update the case owner field to the Belong FOH Queue</description>
        <field>OwnerId</field>
        <lookupValue>Belong_FOH_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case Owner update to Belong FOH queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Owner_update_to_Notification_Queue</fullName>
        <description>This is to update the case owner to the Notification queue</description>
        <field>OwnerId</field>
        <lookupValue>Notifications_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case Owner update to Notification Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Owner_update_to_Received_queue</fullName>
        <description>This is to update the case owner to Received queue</description>
        <field>OwnerId</field>
        <lookupValue>Received_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case Owner update to Received queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_field_update</fullName>
        <description>Update case status field to completed</description>
        <field>Status</field>
        <literalValue>Completed</literalValue>
        <name>Case Status field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_update</fullName>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Case Status update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Changed_appointment_date</fullName>
        <field>Follow_Up_Date__c</field>
        <formula>NOW() - (TODAY()-Appointment_Reminder_Date__c)</formula>
        <name>Changed appointment date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Changed_follow_up</fullName>
        <description>change follow up date when queue status updates from &quot;NBN Transition/Transfer in Progress&quot;  to &quot;Submit_Confirm_Connect&quot;</description>
        <field>Follow_Up_Date__c</field>
        <formula>(NOW() + X1_Business_day__c )</formula>
        <name>Changed follow up date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Complaint_Resoluti_Agreed_SLA_violated</fullName>
        <field>Resolution_Agreed_SLA_Violated__c</field>
        <literalValue>1</literalValue>
        <name>Complaint Resolution Agreed SLA violated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Complaint_ResolutionAgreed_SLA_violated</fullName>
        <field>Resolution_Agreed_SLA_Violated__c</field>
        <literalValue>1</literalValue>
        <name>Complaint Resolution Agreed SLA violated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Complaint_Resolution_Agreed_SLA_violated</fullName>
        <field>Resolution_Agreed_SLA_Violated__c</field>
        <literalValue>1</literalValue>
        <name>Complaint Resolution Agreed SLA violated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Escalated_field_TRUE</fullName>
        <field>IsEscalated</field>
        <literalValue>1</literalValue>
        <name>Escalated field TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Escalated_field_update</fullName>
        <field>IsEscalated</field>
        <literalValue>0</literalValue>
        <name>Escalated field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Expected_Resolution_Agreed_Date_Update</fullName>
        <description>This is to update the Expected Resolution Agreed Date field</description>
        <field>Expected_Resolution_Execution_Date_Sys__c</field>
        <formula>CASE( 
MOD( TODAY() - DATE( 1900, 1, 7 ), 7 ), 
0,TODAY() + 12, 
1,TODAY() + 14, 
2,TODAY() + 14, 
3,TODAY() + 14,
4,TODAY() + 14, 
5,TODAY() + 14, 
TODAY() + 13 
)</formula>
        <name>Expected Resolution Agreed Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Follow_Up_Date_For_Call_Reminder</fullName>
        <description>This is used to update the &quot;Follow Up Date&quot; after 3 business days when &quot;Proof Of Doweling Status&quot; became &quot;Ist Reminder sent&quot; for Call reminders.</description>
        <field>Follow_Up_Date__c</field>
        <formula>NOW()</formula>
        <name>Follow Up Date For Call Reminder</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Follow_Up_Date_For_Doc_Verification</fullName>
        <description>This is used to update the &quot;Follow Up Date&quot;  when &quot;Proof Of Doweling Status&quot; became &quot;Pending for document verification&quot;.</description>
        <field>Follow_Up_Date__c</field>
        <formula>NOW()+1</formula>
        <name>Follow Up Date For Doc Verification</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Follow_Up_date</fullName>
        <field>Follow_Up_Date__c</field>
        <formula>NOW()</formula>
        <name>Follow Up date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Follow_up_date_change_to_today</fullName>
        <field>Follow_Up_Date__c</field>
        <formula>Now()</formula>
        <name>Follow up date change to today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IFBOT_Case_Owner_Update</fullName>
        <description>This to update IFBOT case owner to POD Queue</description>
        <field>OwnerId</field>
        <lookupValue>POD_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>IFBOT Case Owner Update</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IFBOT_Case_owner_update_to_In_Progress</fullName>
        <description>This is to update the case owner to the In Progress queue.</description>
        <field>OwnerId</field>
        <lookupValue>In_Progress_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>IFBOT Case owner update to &quot;In Progress&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IFBOT_Case_owner_update_to_Submitted</fullName>
        <description>This is to update the case owner to the Submitted queue.</description>
        <field>OwnerId</field>
        <lookupValue>Submitted_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>IFBOT Case owner update to &quot;Submitted&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Key_Update_one</fullName>
        <field>DuplicateKey__c</field>
        <formula>Contact.Id + RecordType.Id + text(Asset_Case_Reason__c)+ text(Product_Code__c)+text( Status)+ text( ClosedDate )</formula>
        <name>Key Update one</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NBN_Queue_Change</fullName>
        <field>OwnerId</field>
        <lookupValue>NBN_Pending_Submission</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>NBN Queue Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>POD_1st_reminder_sent</fullName>
        <description>This is to update the &quot;Proof Of Dwelling Status&quot; value to &quot;1st Reminder sent&quot; after 2 business days from the POD status &quot;Documented Required&quot;.</description>
        <field>Proof_Of_Dwelling_Status__c</field>
        <literalValue>1st Reminder sent</literalValue>
        <name>POD 1st reminder sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>POD_Call_Complete_Date</fullName>
        <description>This is used to update the &quot;Call Complete Date&quot; when &quot;Proof Of Doweling Status&quot; became &quot;Call Reminder Complete&quot;</description>
        <field>Call_Complete_Date__c</field>
        <formula>TODAY()</formula>
        <name>POD Call Complete Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>POD_Call_Reminder_Required</fullName>
        <description>This is to update the &quot;Proof Of Dwelling Status&quot; value to &quot;Call Reminder Required&quot; after 3 business days from the POD status &quot;1st Reminder Sent&quot;.</description>
        <field>Proof_Of_Dwelling_Status__c</field>
        <literalValue>Call reminder required</literalValue>
        <name>POD Call Reminder Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>POD_Complete_Date</fullName>
        <description>This is for capturing the date when POD process completes</description>
        <field>POD_Complete_Date__c</field>
        <formula>TODAY()</formula>
        <name>POD Complete Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>POD_Document_Required_Date</fullName>
        <description>This is used to update the &quot;Document Required Date&quot; when &quot;Proof Of Doweling Status&quot; became &quot;Document Required&quot;</description>
        <field>Document_Required_Date__c</field>
        <formula>TODAY()</formula>
        <name>POD Document Required Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>POD_Final_Reminder_Sent</fullName>
        <description>This is to update the &quot;Proof Of Dwelling Status&quot; value to &quot;Final Reminder sent&quot; after 2 business days from the POD status &quot;Call Reminder Complete&quot;.</description>
        <field>Proof_Of_Dwelling_Status__c</field>
        <literalValue>Final reminder sent</literalValue>
        <name>POD Final Reminder Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>POD_Follow_Up_Date_For_Order_Withdraw</fullName>
        <description>This is used to update the &quot;Follow Up Date&quot; after 2 business days when &quot;Proof Of Doweling Status&quot; became &quot;Call Reminder Complete&quot; for order withdraw.</description>
        <field>Follow_Up_Date__c</field>
        <formula>NOW()</formula>
        <name>POD Follow Up Date For Order Withdraw</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetSyncToSent</fullName>
        <description>Change the Sync Status field to &quot;Sent&quot;</description>
        <field>Sync_Status__c</field>
        <literalValue>Sent</literalValue>
        <name>SetSyncToSent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Change</fullName>
        <field>Status</field>
        <literalValue>Submitted - Confirm Connect</literalValue>
        <name>Status Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Record_Type_To_Social</fullName>
        <description>Update case record type to social.</description>
        <field>RecordTypeId</field>
        <lookupValue>Social_Cases</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Case Record Type To Social</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_Recd</fullName>
        <field>Date_Received__c</field>
        <formula>now()</formula>
        <name>Update Date Recd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Description</fullName>
        <field>Description</field>
        <formula>&quot;Move Request&quot;</formula>
        <name>Update Description</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Duplicate_check_key</fullName>
        <field>DuplicateKey__c</field>
        <formula>Contact.Id  +  RecordType.Id + text(Product_Code__c)</formula>
        <name>Update Duplicate check key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Follow_Up_Date_for_NBN</fullName>
        <description>For NBN orders with Telflow POST event attributes  where    status = &quot;Rejected&quot; and sub_status = &quot;RQUTP&quot;  and product_type = &apos;NBN&apos;, 

Update FOD = Today</description>
        <field>Follow_Up_Date__c</field>
        <formula>Now()</formula>
        <name>Update Follow Up Date for NBN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Followup_date</fullName>
        <field>Follow_Up_Date__c</field>
        <formula>CreatedDate + 2</formula>
        <name>Update Followup date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_POD_Status</fullName>
        <field>Proof_Of_Dwelling_Status__c</field>
        <literalValue>Document required</literalValue>
        <name>Update POD Status to Document Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_as_Closed</fullName>
        <description>Auto Close case</description>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Update Status as Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Subject</fullName>
        <field>Subject</field>
        <formula>&quot;Move Request&quot;</formula>
        <name>Update Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_case_status_NBN_cases</fullName>
        <description>For NBN orders with Telflow POST event attributes  where    status = &quot;Rejected&quot; and sub_status = &quot;RQUTP&quot;  and product_type = &apos;NBN&apos;, 

move order to Queue = &quot;Received&quot; and Case status = &quot;Received&quot;.</description>
        <field>Status</field>
        <literalValue>Received</literalValue>
        <name>Update case status NBN cases</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updtte_SC1_2_and_11_order_status</fullName>
        <description>Whenever the order is of SC 1, 2 and 11; update the order status to submit confirm appointment</description>
        <field>Status</field>
        <literalValue>Submitted – Confirm Appointment</literalValue>
        <name>Updtte SC1,2 and 11 order status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Asset_case_outbound</fullName>
        <apiVersion>35.0</apiVersion>
        <endpointUrl>https://qa6-elb.belongtest.com.au/project-eve/api/services/salesforce/asset/notification</endpointUrl>
        <fields>AssetId</fields>
        <fields>Asset_Case_Description__c</fields>
        <fields>Asset_Case_Reason_Code__c</fields>
        <fields>Asset_Case_Reason__c</fields>
        <fields>CaseNumber</fields>
        <fields>ContactId</fields>
        <fields>Id</fields>
        <fields>Octane_Customer_Number__c</fields>
        <fields>Product_Code__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>devops@belong.com.au</integrationUser>
        <name>Asset case outbound</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Asset Case Contact Address</fullName>
        <actions>
            <name>Asset_Case_Contact_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Asset</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Asset Case assignment to Belong FOH Queue</fullName>
        <actions>
            <name>Case_Owner_update_to_Belong_FOH_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Asset</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>This is to assign an asset case to the Belong FOH queue.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Asset case outbound</fullName>
        <actions>
            <name>SetSyncToSent</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_case_outbound</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Send outbound message to service layer to create modem/accessories replacement/return orders, can also re-triggered by administrator by update sync status to send</description>
        <formula>(ISNEW() &amp;&amp; (ISPICKVAL(Asset_Case_Reason__c, &apos;Return&apos;)|| ISPICKVAL(Asset_Case_Reason__c, &apos;Replacement&apos;))) || (ISCHANGED( Sync_Status__c ) &amp;&amp; ISPICKVAL(Sync_Status__c, &apos;Send&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CALL BACK Case Assignment to Queue</fullName>
        <actions>
            <name>Case_Owner_Held_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Customer Call Back</value>
        </criteriaItems>
        <description>Assigns an Call Back Request case the Held Queue when created.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CASE Online Moves Case</fullName>
        <actions>
            <name>Email_sent_to_a_customer_when_they_submit_a_moves_order_online</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Move Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Move_Type__c</field>
            <operation>notEqual</operation>
            <value>NBN (Transition)</value>
        </criteriaItems>
        <description>Email triggered to a customer once a moves case has been created from the online moves process</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case %3A Close Speed Boost Cases</fullName>
        <actions>
            <name>Update_Status_as_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Speed Boost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Service_Type__c</field>
            <operation>equals</operation>
            <value>FTTP</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Close Speed Bost cases when Service Type - FTTP</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case %3A Move Request</fullName>
        <actions>
            <name>Update_Description</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Subject</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Move Request</value>
        </criteriaItems>
        <description>Auto Populate Subject and Description when new case with Type - Move Request is created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case %3A Update Date Received when Status is %27Received%27</fullName>
        <actions>
            <name>Update_Date_Recd</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Date field when Case Status - &apos;Received&apos;</description>
        <formula>and(ischanged(Status), ISPICKVAL(Status, &quot;Received&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case %3A Update Followup date for ADSL IFBOT</fullName>
        <actions>
            <name>Update_Followup_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>equals</operation>
            <value>IFBOT ADSL PORT_ORDER</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Duplication Exception on Closed cases</fullName>
        <actions>
            <name>Key_Update_one</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Asset</value>
        </criteriaItems>
        <description>This workflow rule has been created as a part to avoid case Duplication. Once the original case is closed system should allow users to create a case with same combination.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Status field udate</fullName>
        <actions>
            <name>Case_Owner_Update_to_Belong_Integration</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Status_field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>NBN</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>IFBOT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Request_Status__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Segment_Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <description>Wheneven an SMS is being sent from salesforce, we need to update the case status field to completed.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case Updates for SC 1%2C2 and 11 NBN orders</fullName>
        <actions>
            <name>Changed_appointment_date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Updtte_SC1_2_and_11_order_status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Case status updates for sc 1 , 2 and 11 NBN orders</description>
        <formula>AND(  OR( ISPICKVAL (NBN_Service_Class__c, &quot;NBN_SERVICE_CLASS_TWO&quot;), ISPICKVAL (NBN_Service_Class__c, &quot;NBN_SERVICE_CLASS_ONE&quot;),  ISPICKVAL (NBN_Service_Class__c, &quot;NBN_SERVICE_CLASS_ELEVEN&quot;)),   ISPICKVAl( Request_Status__c ,&quot;In Progress&quot;),  ISPICKVAl(  Segment_Status__c  ,&quot;Active&quot;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Complaint Escalation to FOH Team Lead Queue</fullName>
        <actions>
            <name>Case_Owner_Update_to_FOH_Team_Lead_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Escalated_field_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Area__c</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Escalation_Step__c</field>
            <operation>equals</operation>
            <value>Escalate to FOH Team Leads</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>This is to escalate a complaint to the FOH Team Lead queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Complaint Escalation to Operations Team Queue</fullName>
        <actions>
            <name>Case_Owner_Update_to_Operations_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Escalated_field_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Area__c</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Escalation_Step__c</field>
            <operation>equals</operation>
            <value>Escalate to Operations</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>This is to escalate a complaint to the Operations Team queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Complaint Initial Contact</fullName>
        <actions>
            <name>New_Complaint_Creation_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Hour_Contact</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Complaints</value>
        </criteriaItems>
        <description>This workflow is to create tasks for case owner for general and high risk complaints that have been submitted by customers.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Complaints - Resolution Agreed Execution Date</fullName>
        <actions>
            <name>Expected_Resolution_Agreed_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Complaints</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Resolution_Agreed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This is to calculate the Resolution Agreed Execution Date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>De-escalation of Cases</fullName>
        <actions>
            <name>Escalated_field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Escalation_Step__c</field>
            <operation>equals</operation>
            <value>Escalation Resolved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsEscalated</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This is to de-escalate cases once the escalation has been resolved.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Duplicate case management</fullName>
        <actions>
            <name>Update_Duplicate_check_key</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Asset</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CreatedById</field>
            <operation>notEqual</operation>
            <value>Belong Integration</value>
        </criteriaItems>
        <description>This workflow rule is created to prevent duplicate case creation.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Escalate Step is Escalate to BOH</fullName>
        <actions>
            <name>Case_Owner_is_Belong_BOH_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Escalated_field_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Escalation_Step__c</field>
            <operation>equals</operation>
            <value>Escalate to Level 2 Technical Support</value>
        </criteriaItems>
        <description>This workflow is for Escalation Step is equal Escalate to Back of House</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Follow up date change</fullName>
        <actions>
            <name>Changed_appointment_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>For NBN orders, follow up date should be automatically updated with changed Appointment Date -2 Business Days</description>
        <formula>AND(   NOT(ISPICKVAL( NBN_Service_Class__c,&quot;NBN_SERVICE_CLASS_THREE&quot;)), NOT(ISPICKVAL( NBN_Service_Class__c,&quot;NBN_SERVICE_CLASS_THIRTEEN&quot;)), NOT(ISBLANK(PRIORVALUE(Appointment_Date__c))),  ISCHANGED( Appointment_Date__c ),  RecordType.DeveloperName = &apos;IFBOT&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IFBOT Case Assignment to In Progress Queue</fullName>
        <actions>
            <name>IFBOT_Case_owner_update_to_In_Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>IFBOT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <description>This is to assign an IFBOT case to the In Progress Queue once the status is updated to &quot;In Progress&quot;.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IFBOT Case Assignment to Submitted Queue</fullName>
        <actions>
            <name>IFBOT_Case_owner_update_to_Submitted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>IFBOT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Submitted - Confirm Connect,Submitted – Confirm Appointment</value>
        </criteriaItems>
        <description>This is to assign an IFBOT case to the Submitted Queue once the status is updated to &quot;Submitted&quot;.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IFBOT and YourOrder Case Assignment to Received Queue</fullName>
        <actions>
            <name>Case_Owner_update_to_Received_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 3 ) AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>IFBOT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>YourOrder</value>
        </criteriaItems>
        <description>This is to assign an IFBOT case or YourOrder cases to the Received Queue when created.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>MoveRequest Follow up Date</fullName>
        <actions>
            <name>CASE_Follow_up_Date_3_days_after</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Move Request</value>
        </criteriaItems>
        <description>update the follow up date to 3 days after a move request case is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Moves Cases Assignment</fullName>
        <actions>
            <name>Case_Owner_Update_to_Moves_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Move Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>To Complete</value>
        </criteriaItems>
        <description>This is to assign moves cases to the moves queue when status is &quot;To complete&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NBN Inprogess Cases</fullName>
        <actions>
            <name>Case_Status_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Follow_up_date_change_to_today</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IFBOT_Case_owner_update_to_In_Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 OR 5) AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.Segment_Status__c</field>
            <operation>equals</operation>
            <value>Initial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>NBN</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Request_Status__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Telflow_Alarm__c</field>
            <operation>equals</operation>
            <value>Stuck in Submission</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Segment_Status__c</field>
            <operation>equals</operation>
            <value>Transmitted</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>NBN Rejected Orders</fullName>
        <actions>
            <name>Update_Follow_Up_Date_for_NBN</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_case_status_NBN_cases</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>NBN</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Segment_Status__c</field>
            <operation>equals</operation>
            <value>Unable to Process</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Request_Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <description>For NBN orders with Telflow POST event attributes  where    status = &quot;Rejected&quot; and sub_status = &quot;RQUTP&quot;  and product_type = &apos;NBN&apos;, 

move order to Queue = &quot;Received&quot; and Case status = &quot;Received&quot;.
Update FOD = Today</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New Holiday Plan Assignment</fullName>
        <actions>
            <name>Apply_Holiday_Plan_Queue_Assignment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Holiday Plan Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sub_Status__c</field>
            <operation>equals</operation>
            <value>Holiday Plan - Request Recorded</value>
        </criteriaItems>
        <description>This is to assign new holiday plan request to the &quot;apply holiday plan queue&quot;.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>POD Complete Date</fullName>
        <actions>
            <name>POD_Complete_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Proof_Of_Dwelling_Status__c</field>
            <operation>equals</operation>
            <value>POD Complete</value>
        </criteriaItems>
        <description>This is for capturing the date when POD process completes</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>POD Confirmation email</fullName>
        <actions>
            <name>POD_Confirmation_email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Proof_Of_Dwelling_Status__c</field>
            <operation>equals</operation>
            <value>Sent to Global Connect Outstanding team</value>
        </criteriaItems>
        <description>This is used to send POD confirmation email to customers if POD is approved.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>POD First Customer Email on one day before move in date</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Move Request,IFBOT,Reconnecting Customer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>POD</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sub_Status__c</field>
            <operation>equals</operation>
            <value>Send Documents</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Move_In_Date__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>POD_Documents_required</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_POD_Status</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.minus_one_bd__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>POD Follow Up Date For POD Verification</fullName>
        <actions>
            <name>Follow_Up_Date_For_Doc_Verification</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Proof_Of_Dwelling_Status__c</field>
            <operation>equals</operation>
            <value>Pending for document verification</value>
        </criteriaItems>
        <description>This is used to update the &quot;Follow Up Date&quot; when the &quot;Proof Of Dwelling&quot; status became &quot;POD Complete&quot;.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>POD Queue Assignment</fullName>
        <actions>
            <name>IFBOT_Case_Owner_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>IFBOT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>POD</value>
        </criteriaItems>
        <description>This is to assign IFBOT case to POD Queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>POD doc final reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Proof_Of_Dwelling_Status__c</field>
            <operation>equals</operation>
            <value>Call reminder complete</value>
        </criteriaItems>
        <description>This is used to send the Final email reminder and update the Proof Of Dwelling status to &quot;Final Reminder Sent&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>POD_doc_final_reminder</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>POD_Final_Reminder_Sent</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>POD_Follow_Up_Date_For_Order_Withdraw</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.Final_Reminder_Send_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>POD doc reminder 1</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Proof_Of_Dwelling_Status__c</field>
            <operation>equals</operation>
            <value>Document required</value>
        </criteriaItems>
        <description>This is used to send the 1st email reminder and update the Proof Of Dwelling status to &quot;1st  Reminder Sent&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>POD_doc_reminder_1</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>POD_1st_reminder_sent</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.X1st_Reminder_Send_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>POD-2 Business Days After Call Complete</fullName>
        <actions>
            <name>POD_Call_Complete_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Proof_Of_Dwelling_Status__c</field>
            <operation>equals</operation>
            <value>Call reminder complete</value>
        </criteriaItems>
        <description>This is used to update the &quot;Call Complete Date&quot; when &quot;Proof Of Doweling Status&quot; became &quot;Call Reminder Complete&quot;.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>POD-2 Business Days After POD Request</fullName>
        <actions>
            <name>POD_Document_Required_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Proof_Of_Dwelling_Status__c</field>
            <operation>equals</operation>
            <value>Document required</value>
        </criteriaItems>
        <description>This is used to update the &quot;Document Required Date&quot; when &quot;Proof Of Doweling Status&quot; became &quot;Document Required&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>POD_Call Reminder Required</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Proof_Of_Dwelling_Status__c</field>
            <operation>equals</operation>
            <value>1st Reminder sent</value>
        </criteriaItems>
        <description>This is used to update the &quot;Proof Of Dwelling Status&quot; to &quot;Call Reminder Required&quot; after 3 business days from 1st reminder sent</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Follow_Up_Date_For_Call_Reminder</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>POD_Call_Reminder_Required</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.Call_Reminder_Required__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Remove Holiday Plan Assignment</fullName>
        <actions>
            <name>Assign_case_to_Remove_holiday_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Holiday Plan Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sub_Status__c</field>
            <operation>equals</operation>
            <value>Holiday Plan - Applied</value>
        </criteriaItems>
        <description>This is to assign applied holiday plan request to the &quot;Remove holiday plan queue&quot;.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Speed Boost Request and NBN appointment assignment</fullName>
        <actions>
            <name>Case_Owner_update_to_Notification_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Speed Boost,NBN Appointment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New,In Progress</value>
        </criteriaItems>
        <description>This is to assign all speed boost request or NBN appointment changes from the web layer to the Notification queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Technical Support Assignment</fullName>
        <actions>
            <name>Case_Owner_is_Belong_BOH_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Technical Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>This is to assign new Technical Support  cases to the &quot;Belong BOH queue&quot;.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Case Record Type to Social</fullName>
        <actions>
            <name>Update_Case_Record_Type_To_Social</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This workflow rule updates case record type to social based on case origin</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update for delayed assessment orders</fullName>
        <actions>
            <name>Follow_Up_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Segment_Status__c</field>
            <operation>equals</operation>
            <value>Under Assessment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Request_Status__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Telflow_Alarm__c</field>
            <operation>equals</operation>
            <value>Under Assessment Delay</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>NBN</value>
        </criteriaItems>
        <description>Update of follow up date for delayed NBN orders</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>caseFollowUpDateUpdate</fullName>
        <actions>
            <name>Follow_Up_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Segment_Status__c</field>
            <operation>equals</operation>
            <value>Withdrawn</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Request_Status__c</field>
            <operation>equals</operation>
            <value>Cancelled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>NBN</value>
        </criteriaItems>
        <description>follow up date update for the NBN cancelled orders</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>caseStatusChangeNBNTransitionTransferinProgressToSubmitConfirmConnect</fullName>
        <actions>
            <name>Changed_follow_up</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NBN_Queue_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>change of case status from NBN Pending Submission&quot;  to &quot;Submit_Confirm_Connect&quot; fro SC3 orders for SC3 orders</description>
        <formula>AND(  ISPICKVAL (NBN_Service_Class__c, &quot;NBN_SERVICE_CLASS_THREE&quot;), ISPICKVAL((Status),&quot;NBN Pending Submission&quot;)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>caseStatusChangeNBNTransitionTransferinProgressToSubmitConfirmConnect1</fullName>
        <actions>
            <name>Status_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>change of case status from NBN Pending Submission&quot;  to &quot;Submit_Confirm_Connect&quot;  for SC3 orders</description>
        <formula>AND(  ISPICKVAL (NBN_Service_Class__c, &quot;NBN_SERVICE_CLASS_THREE&quot;), ISPICKVAL((Status),&quot;NBN Pending Submission&quot;), ISPICKVAl( Segment_Status__c ,&quot;Active&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Hour_Contact</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>New</status>
        <subject>24 Hour Contact</subject>
    </tasks>
    <tasks>
        <fullName>X24_Hour_Contact</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>New</status>
        <subject>24 Hour Contact</subject>
    </tasks>
</Workflow>
