<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approval_Email_Alert_sent_to_initial_Submitter</fullName>
        <description>Approval Email Alert sent to initial Submitter</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>KM_Templates/APPROVAL_Approved</template>
    </alerts>
    <alerts>
        <fullName>Approval_email</fullName>
        <description>Approval email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>KM_Templates/APPROVAL_Approved</template>
    </alerts>
    <alerts>
        <fullName>Approval_email_to_KM_user</fullName>
        <description>Approval email to KM user</description>
        <protected>false</protected>
        <recipients>
            <recipient>carly.donnellan@team.telstra.com.agent</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>diane.kacinari@team.telstra.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>michelle.rajalingam@team.telstra.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nimit.jain@team.telstra.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rachid.souccane@team.belong.com.au.agent</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shitiz.brutta@team.belong.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>todd.anderson@team.belong.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>KM_Templates/APPROVAL_Request_for_Approval</template>
    </alerts>
    <alerts>
        <fullName>Email_advising_record_Submitter_of_Rejection_and_rework_of_article_required</fullName>
        <description>Email advising record Submitter of Rejection and rework of article required</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>KM_Templates/APPROVAL_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Rejected</fullName>
        <description>Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>KM_Templates/REJECTION_Review_article</template>
    </alerts>
    <alerts>
        <fullName>Rejection</fullName>
        <description>Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>KM_Templates/APPROVAL_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Lifecycle_Review_Status</fullName>
        <field>Lifecycle_Review_Status__c</field>
        <literalValue>Due for Review</literalValue>
        <name>Update Lifecycle Review Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Review_Date</fullName>
        <field>Review_Date__c</field>
        <formula>LastPublishedDate + 180</formula>
        <name>Update Review Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status</fullName>
        <field>Lifecycle_Review_Status__c</field>
        <literalValue>Due for Review</literalValue>
        <name>Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Validated_Status</fullName>
        <field>ValidationStatus</field>
        <literalValue>Validated</literalValue>
        <name>Update Validated Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Validated_Status_1</fullName>
        <field>ValidationStatus</field>
        <literalValue>Validated</literalValue>
        <name>Update Validated Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Validated_Status_2</fullName>
        <field>ValidationStatus</field>
        <literalValue>Not Validated</literalValue>
        <name>Update Validated Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Validation_Status</fullName>
        <field>ValidationStatus</field>
        <literalValue>Not Validated</literalValue>
        <name>Validation Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <knowledgePublishes>
        <fullName>Publish_Article</fullName>
        <action>Publish</action>
        <label>Publish Article</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <rules>
        <fullName>Alert %3A Update Lifecycle Review Status</fullName>
        <actions>
            <name>Update_Lifecycle_Review_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Review_Date__c &gt; today()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Alert %3A Update Review Date</fullName>
        <actions>
            <name>Update_Review_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Alert__kav.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update Review date = Last modified date + 6 months</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
