<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <articleTypeChannelDisplay>
        <articleTypeTemplates>
            <channel>App</channel>
            <template>Tab</template>
        </articleTypeTemplates>
        <articleTypeTemplates>
            <channel>Prm</channel>
            <template>Tab</template>
        </articleTypeTemplates>
        <articleTypeTemplates>
            <channel>Csp</channel>
            <template>Tab</template>
        </articleTypeTemplates>
        <articleTypeTemplates>
            <channel>Pkb</channel>
            <template>Toc</template>
        </articleTypeTemplates>
    </articleTypeChannelDisplay>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Document an answer to a single question or explain how a simple task, activity or process is performed.  Document a detailed step in a broader installation activity.</description>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <fields>
        <fullName>Answer__c</fullName>
        <externalId>false</externalId>
        <label>Answer</label>
        <length>131072</length>
        <trackHistory>false</trackHistory>
        <type>Html</type>
        <visibleLines>25</visibleLines>
    </fields>
    <fields>
        <fullName>Button_Label__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Label is passed to mobile app when the a button label is to be used.</inlineHelpText>
        <label>Button Label</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Case_Subject__c</fullName>
        <externalId>false</externalId>
        <label>Case Subject</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Keywords_Public_Knowledge__c</fullName>
        <description>Define the keywords used for SEO. ONLY FOR Public Knowledge</description>
        <externalId>false</externalId>
        <inlineHelpText>Keywords for the Public Knowledge Site</inlineHelpText>
        <label>Keywords Public Knowledge</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Leave_it_with_us__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <inlineHelpText>Choose this option first and then you can determine what login state is required for the LIWU to work.</inlineHelpText>
        <label>Leave it with us?</label>
        <trackHistory>false</trackHistory>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Lifecycle_Review_Status__c</fullName>
        <externalId>false</externalId>
        <label>Lifecycle Review Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Due for Review</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Reviewed</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Overdue for Review</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Login_State__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>If this value is set to null, no button will display, if set to - login required, only authenticated users can LIWU, other wise if value is - login not required, both auth and non-auth users can LIWU.</inlineHelpText>
        <label>Login State</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
        <valueSet>
            <controllingField>Leave_it_with_us__c</controllingField>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Login required</fullName>
                    <default>true</default>
                </value>
                <value>
                    <fullName>Login not required</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
            <valueSettings>
                <controllingFieldValue>checked</controllingFieldValue>
                <valueName>Login required</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>checked</controllingFieldValue>
                <valueName>Login not required</valueName>
            </valueSettings>
        </valueSet>
    </fields>
    <fields>
        <fullName>Normalize_URL_Name__c</fullName>
        <externalId>false</externalId>
        <formula>LOWER(UrlName)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Normalize URL Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Question__c</fullName>
        <externalId>false</externalId>
        <label>Question</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <type>Html</type>
        <visibleLines>25</visibleLines>
    </fields>
    <fields>
        <fullName>Related_Topic_Ids__c</fullName>
        <externalId>false</externalId>
        <label>Related Topic Ids</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Related_Topics__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>All related topics associated with the FAQ, these must be Smart Links in knowledge Refer to -
 https://help.salesforce.com/articleView?id=knowledge_custom_field_rta_smartlink.htm&amp;language=en_US&amp;type=0</inlineHelpText>
        <label>Related Topics</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <type>Html</type>
        <visibleLines>25</visibleLines>
    </fields>
    <fields>
        <fullName>Review_Date__c</fullName>
        <externalId>false</externalId>
        <label>Review Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Script_Details__c</fullName>
        <description>Scripting details</description>
        <externalId>false</externalId>
        <label>Script Details</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <type>Html</type>
        <visibleLines>25</visibleLines>
    </fields>
    <fields>
        <fullName>Troubleshooting_Questions__c</fullName>
        <externalId>false</externalId>
        <label>Troubleshooting Questions</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>URL_Link__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Used for mobile app URL links so that the user can go straight from the knowledge article to the link in the app which answers the question.</inlineHelpText>
        <label>URL Link</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Url</type>
    </fields>
    <fields>
        <fullName>Weighted_Score__c</fullName>
        <externalId>false</externalId>
        <label>Weighted_Score</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>FAQ</label>
    <pluralLabel>FAQs</pluralLabel>
</CustomObject>
