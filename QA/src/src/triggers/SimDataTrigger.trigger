trigger SimDataTrigger on Sim_Data__c (before insert,before update,after insert,after update,after delete,before delete) {
    
    if(SimdataTriggerHandler.executeTrigger)
        TriggerBuilder.handle(SimdataTriggerHandler.class);
}