/************************************************************************************************************
* Apex Class Name   : AttachmentTrigger.trigger
* Version           : 1.0 
* Created Date      : 29 March 2017
* Function          : Trigger on Attachment object.
* Modification Log  :
* Developer             Date                Description
* -----------------------------------------------------------------------------------------------------------
* Tom Clayton           29/03/2017          Created Trigger.
************************************************************************************************************/

trigger AttachmentTrigger on Attachment (after delete, after insert, after undelete, after update, before delete, before insert, before update)
{
    if(AttachmentTriggerHandler.executeTrigger)
    {
        TriggerBuilder.handle(AttachmentTriggerHandler.class);
    }
}