<messaging:emailTemplate subject="Belong Service - Proof of Occupancy Document 'Reminder'" recipientType="Contact" relatedToType="Account" replyTo="Hello@belong.com.au">
<messaging:htmlEmailBody >
    <html>
      <title>Belong</title>
        <style>
            body {
              background: #f4f4f4;
              font-size: 16px;
              line-height: 24px;
            }

            #main {
              max-width: 600px;
              width: 100%;
              height: auto;
              margin: 0 auto;
              font-family: Arial,"Helvetica Neue",Helvetica,sans-serif;
              color: #000;
              background: #ffffff;
            }

            .content-block {
              padding: 0 35px;
              position: relative;
            }

            .banner {
              width: 100%; 
              max-width:600px;
              height: 58px;
              background: #222222;
              padding: 0 0 0 20px;
            }

            .main-text {
              width: 100%; 
              max-width:600px;
              padding-top: 35px;
              font-family: Arial,"Helvetica Neue",Helvetica,sans-serif;
            }

            .body-content {
              width: 100%;
              max-width:600px;
              padding-left: 20px;
              padding-top: 25px;
            }


            .footer {
              font-family: Arial,"Helvetica Neue",Helvetica,sans-serif;
              width: 100%;
              max-width:600px;
              height: auto;
              background: #222;
              color: #ffffff;
              font-size: 12px;
            }

            .footer a {
              color: #ffffff;
              text-decoration: none;
              font-family: Arial,"Helvetica Neue",Helvetica,sans-serif;
            }

            .footer p {
              color: #ffffff;
              font-family: Arial,"Helvetica Neue",Helvetica,sans-serif;
            }
            
            .mvp {
              padding: 10px 0 10px 0;
              font-size: 16px;
              line-height: 24px;
              font-family: Arial,"Helvetica Neue",Helvetica,sans-serif;
            }
        </style>
        <div id="main" style="width: 100%; max-width:600px;height: auto;margin: 0 auto;color: #000;font-family: Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;background: #ffffff;">

            <table class="banner" style="width: 100%; max-width:600px;height: 58px;background: #222222;padding: 0 0 0 20px;">
                <tbody>
                    <tr>
                        <td width="50%">
                            <a href="https://www.belong.com.au" target="_top">
                                <img src="https://www.belong.com.au/content/dam/belong/assets/img/belong-logo-sm.png" alt="Belong Logo" style="border:0; outline:none; text-decoration:none; display:block;" height="" border="0" width=""/>
                            </a>
                        </td>
                        <td style="padding-right:20px;" width="30%">
                            <a href="https://www.belong.com.au/" target="_top">
                                <img src="https://www.belong.com.au/content/dam/belong/assets/img/we-belong-tag.png" alt="We Belong Together" style="border:0; outline:none; text-decoration:none; display:block; max-width:150px; min-width: 120px;" border="0" width="100%"/>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table class="body-content" style="width: 100%; max-width:600px; padding: 25px 20px 0 20px;">
                <tbody contentEditable="true">
                      <tr height="90">
                        <td style="text-align: left; vertical-align: top; padding: 10px 0px 10px 20px; line-height: 22px;" class="mvp" valign="top">
                            <p style="line-height: 16.5pt;">
                                <font face="sans-serif" size="3" color="#000000">
                                    Hello {!recipient.name},<br/><br/>
                                    
                                    Thanks for your interest with a Belong product. Following the previous email sent, it was identified that in order to progress your connection we will require proof of occupancy at the address you are wishing to connect. Once we have received and verified the valid document(s) your service will be provisioned within 10 business days.<br/><br/>

                                    If you do not supply the document(s) within 2 business days we will withdraw your order.<br/><br/>

                                    <span style="font-size: 14.6667px; line-height: 16.8667px; font-weight: bold; color: rgb(57, 132, 198); font-family: Helvetica;">What POD documents do I need to provide?</span><br/><br/>

                                    The following documents are accepted, please provide on from the following categories:

                                    <ul>
                                        <li style="line-height: 22.2222px;"><span style="color: rgb(25, 25, 25); font-family: Helvetica; font-size: 14.6667px; line-height: 16.8667px;">Lease/Rental Agreement*; or</span></li>
                                        <li style="line-height: 22.2222px;"><span style="color: rgb(25, 25, 25); font-size: 14.6667px; line-height: 16.8667px; font-family: Helvetica;">Contract of Sale*; or</span></li>
                                        <li style="line-height: 22.2222px;"><span style="color: rgb(25, 25, 25); font-size: 14.6667px; line-height: 16.8667px; font-family: Helvetica;">
                                            Statutory declaration^ from yourself and a utility bill (eg rates, power, water) ; or</span></li>
                                        <li style="line-height: 22.2222px;"><span style="color: rgb(25, 25, 25); font-size: 14.6667px; line-height: 16.8667px; font-family: Helvetica;">
                                            Statutory declaration^ from the property owner and the rent receipt from yourself; or</span></li>
                                        <li style="line-height: 22.2222px;"><span style="color: rgb(25, 25, 25); font-size: 14.6667px; line-height: 16.8667px; font-family: Helvetica;">
                                            Statutory declaration^ from the property owner and the bond payment receipt; or</span></li>
                                        <li style="line-height: 22.2222px;"><span style="color: rgb(25, 25, 25); font-size: 14.6667px; line-height: 16.8667px; font-family: Helvetica;">
                                            Letter from the Real Estate Agent and the rent receipt from yourself; or</span></li>
                                        <li style="line-height: 22.2222px;"><span style="color: rgb(25, 25, 25); font-size: 14.6667px; line-height: 16.8667px; font-family: Helvetica;">
                                            Letter from the Real Estate Agent and the bond payment receipt</span></li>
                                    </ul>

                                    Please only send through pages which consist of following details:
                                    
                                    <ul>

                                        <li style="line-height: 22.2222px;"><span style="color: rgb(25, 25, 25); font-family: Helvetica; font-size: 14.6667px; line-height: 16.8667px;">Full Name</span></li>

                                        <li style="line-height: 22.2222px;"><span style="color: rgb(25, 25, 25); font-family: Helvetica; font-size: 14.6667px; line-height: 16.8667px;">Service Address</span></li>

                                        <li style="line-height: 22.2222px;"><span style="color: rgb(25, 25, 25); font-family: Helvetica; font-size: 14.6667px; line-height: 16.8667px;">Lease/Rental Commencement date or Settlement date</span></li>
                                    </ul>

                                    If you are attaching a photo, please ensure the image is taken directly over the entire document to include all of the above information and is clear and legible. This will minimise delays with progressing with your connection otherwise resubmission will be required.<br/><br/>

                                    <b>Note: </b><br/>

                                    <p style="line-height: 10pt; font-family: Tahoma; font-size: 10px;">* If your name is different to your account details please include a statutory declaration to explain the difference with supporting documentation such as a drivers license or marriage certificate.<br/>

                                    ^ A statutory declaration must be signed by an appropriate witness eg. Justice of the Peace, Police Officer, Pharmacist, Medical Practitioner etc.</p><br/>

                                    <span style="font-size: 14.6667px; line-height: 16.8667px; font-weight: bold; color: rgb(57, 132, 198); font-family: Helvetica;">When will my service be connected?</span><br/><br/>


                                    Once the documents have been received and validated, your service will generally be connected within 10 business days, or on your Requested Connection Date, whichever is later.<br/><br/>

                                    Please note that if POD hasn't been received within 14 business days of the Requested Connection Date the order will be withdrawn.<br/><br/>

                                    If you have questions don't hesitate to reply to this email or give us a call on 1300 BELONG (235 664).
                                    
                                </font>
                            </p>
                        </td>
                    </tr>
                    <tr height="90">
                        <td style="text-align: left; vertical-align: top; padding: 10px 0px 10px 20px; line-height: 22px;" class="mvp" valign="top">
                            <span style="line-height: 22px; text-align: left; color: rgb(0, 0, 0);">
                                <font face="sans-serif" size="3">
                                    Cheers, <br/>
                                    The Belong Team
                                </font>
                            </span><br/>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table class="footer" style="background: #222;" height="auto" bgcolor="#000000" border="0" cellpadding="0" cellspacing="0" width="600">
                <tbody>
                    <tr style="background: #1cb9de;">
                        <td style="background: #1cb9de; text-align: left; vertical-align:middle; font-size: 18px; font-family: Arial; line-height:22px; padding: 5px 42px 5px 42px;" valign="top" width="200">
                        </td>
                        <td style="text-align: right; vertical-align:top; font-size: 11px; font-family: Arial; line-height:22px;" valign="top" width="400"><br/>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: auto;font-size: 12px;font-family: Arial; -webkit-text-size-adjust: none; text-size-adjust: none; -ms-text-size-adjust: none; color: #ffffff; padding: 5px 30px 5px 30px;" colspan="2">
                            <br/>
                            <a class="white-text" href="https://www.belong.com.au/privacy" style="color: #ffffff; text-decoration: none; font-size:12px;" target="_top">
                                <span style="color: #ffffff; text-decoration: none; font-size:12px; padding: 0px 5px;">
                                    Privacy
                                </span>
                            </a>
                            <span style="color: #666666;">|</span>
                            <a class="white-text" href="https://www.belong.com.au/customer-terms" style="color: #ffffff; text-decoration: none; font-size:12px;" target="_top">
                                <span style="color: #ffffff; text-decoration: none; font-size:12px; padding: 0px 5px;">
                                    Customer Terms
                                </span>
                            </a>
                            <span style="color: #666666;">|</span>
                            <a class="white-text" href="https://support.belong.com.au/" style="color: #ffffff; text-decoration: none; font-size:12px;" target="_top">
                                <span style="color: #ffffff; text-decoration: none; font-size:12px; padding: 0px 5px;">
                                    Contact Us
                                </span>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px 5px 5px 42px;">
                            <br/>
                            <a href="https://www.belong.com.au/" style="color: #ffffff;text-decoration: none;font-family: Arial;" target="_top">
                                <img alt="Belong logo" src="https://www.belong.com.au/content/dam/belong/assets/images/belong-logo.png" style="width:140px; height: 50px;" height="50" border="0" />
                            </a>
                            <br/><br/>
                        </td>
                        <td style="width: auto;font-size: 12px;font-family: Arial; -webkit-text-size-adjust: none; text-size-adjust: none; -ms-text-size-adjust: none; color: #ffffff; padding: 5px 10px 5px 45px;">
                            <br/>
                            <span style="color: #999999;font-family: Arial; font-size:12px;">
                                Belong is a division of Telstra Corporation Limited
                            </span>
                            <br/><br/>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
     </html>
</messaging:htmlEmailBody>
</messaging:emailTemplate>