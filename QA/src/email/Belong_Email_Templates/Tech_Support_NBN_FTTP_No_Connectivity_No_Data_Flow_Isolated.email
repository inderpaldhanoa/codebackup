Hi {!Contact.FirstName},

Thanks for contacting us about your connection issue. We will look into this for you and have it resolved as soon as possible. To assist us with identifying potential network or provisioning issues, we would like to get some further information from you about the connection.
If you are experiencing limited connection or no data flow issues, please complete the following steps on your Windows PC, and reply to this email with the details.

Please ensure your laptop or computer is connected directly to your NBN Co NTD via the ethernet cable when you perform these tests. 
We have activated <<UNI-D Port X>>

Hardware Status
1)	Start by completing a Factory Reset of the NBN NTD by pressing in the Reset button between the UNI-V and UNI-D ports. You will see the lights turn off, and then start to come back on. Once on the Power and Optic light should be on and green. 

If the Optic light is RED, this indicates damaged fibre. Stop troubleshooting and notify us if your Optic light is red immediately. 

The Optic may be solid green or may flicker if data is being transferred. If the Power and Optic are green continue troubleshooting.

Service Status
2)	Connect to the NTD via Ethernet, and confirm the UNI-D Port lights up orange or green. 
If the UNI-D Port light does not illuminate, please stop troubleshooting and notify us immediately. We will require the NTD Serial Number (it starts with ALCL….) located on the back of the NTD.

IPConfig and Trace Route
3)	If the UNI-D Port light comes on, you can open Command Prompt (you can find this in the Start Menu Accessories), and enter the following commands
•	ipconfig /all
•	ping 8.8.8.8
•	tracert google.com.au
•	tracert abc.com.au

If the trace route (tracert) to google.com.au fails you may have to change your DNS settings. If trace route to google.com.au returns all timeouts, you may need to temporarily disable your Anti-Virus and/or Firewall to see return requests. Once these are complete, please cut (right click Select All then Ctrl+X) and paste into Notepad, or printscreen and attach in your reply.

Device Configuration
4)	Open the Task Manager (right click the Taskbar or use Alt+Ctrl+Del) and click on the Networking tab. Once there, you will need to identify the connected adapter, and provide the Link Speed and Network Utilisation percentage.

5)	From the Task Manager, click the Performance tab and then select Resource Monitor. From here, select the Network tab to show all of the Processes with Network Activity. Once displayed, use printscreen and attach in your reply.


If you could complete these steps and reply to techsupport@belong.com.au with the details, that will assist us in identifying the cause of your issues.