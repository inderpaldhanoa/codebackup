trigger SubscribeTokenTrigger on Contact (before insert) {
  
    // Before Insert Logic
    if(Trigger.isInsert && Trigger.isBefore)
    {
        SubscribeTokenTriggerHandler.OnBeforeInsert(Trigger.new);
    }
    
}