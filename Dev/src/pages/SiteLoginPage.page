<apex:page sidebar="false" showHeader="false" applyBodyTag="false" applyHtmlTag="false" standardStylesheets="false" Controller="CustomSiteLoginController" docType="html-5.0">
    <link rel="shortcut icon" href="{!URLFOR($Resource.BelongIcon)}" type="image/x-icon" title = "Welcome to Belong"/>
    <head>
        <link href="https://www.belong.com.au/account/" rel="canonical" />
        <apex:stylesheet value="https://belong.com.au/assets/css/style.css"/>
        <apex:stylesheet value="https://belong.com.au/assets/img/svgs/data-svg.css" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="description" content="Sign in to your Belong account. Check your usage, manage your details and sort out payments all in one easy area from your computer, tablet or phone." />
        <meta name="keywords" content="belong, belong internet, belong broadband, pay bill, check usage, check internet usage, change service, change internet plan, update details, update information, pay internet bill, update payment details" />
    </head>
    <body data-action="" data-controller="login">
        <div id="lpButtonDiv"></div>
        <noscript>&lt;div class="page-warning"&gt; &lt;div class="l-padding"&gt;Please enable JavaScript in order to get the best experience when using this site.&lt;/div&gt; &lt;/div&gt;</noscript>
        <div class="header" data-page-type="styleguide" id="header">
            <div class="top cf" itemtype="http://schema.org/Organization">
                <!-- START Accessibility Links - NOTE: these will change per template as not all areas will be on the page at once -->
                <ul class="accessibility-links">
                    <li>
                        <a href="#nav">Site Navigation</a>
                    </li>
                    <li>
                        <a href="#main">Page Content</a>
                    </li>
                </ul>
                <!-- END Accessibility Links -->
                <a class="nav-toggle" href="#" id="nav-toggle"><span class="vh">Open navigation</span></a> <a class="call" href="tel:1300%20235%20664" id="header-call" itemprop="telephone"><span class="vh">Call</span></a>
                <div class="nav" id="nav">
                    <ul class="main-navigation navigation-list horizontal">
                        <li class="">
                            <a class="" data-agent="true" data-agent-mobo="true" data-auth="true" data-customer="true" data-join="false" data-unauth="true" href="/adsl.html" id="nav-adsl"><span class="c-target">ADSL2+</span></a>
                        </li>
                        <li class="">
                            <a class="" data-agent="true" data-agent-mobo="true" data-auth="true" data-customer="true" data-join="false" data-unauth="true" href="/nbn.html" id="nav-nbn"><span class="c-target">NBN</span></a>
                        </li>
                    </ul>
                    <div class="hidden authenticated-menu">
                        <a data-agent="true" data-agent-mobo="true" data-auth="true" data-customer="true" data-join="true" data-unauth="false" href="#">Hi <span class="user-first-name"></span></a>
                        <ul class="authenticated-navigation navigation-list hidden">
                            <li class="hidden">
                                <a class="" data-agent="true" data-agent-mobo="false" data-auth="true" data-customer="true" data-join="true" data-unauth="false" href="#" id="nav-logout">Sign out</a>
                            </li>
                            <li class="hidden">
                                <a class="" data-agent="true" data-agent-mobo="false" data-auth="true" data-customer="false" data-join="true" data-unauth="false" href="#" id="nav-change-password">Change password</a>
                            </li>
                        </ul>
                    </div>
                    <ul class="secondary-navigation navigation-list horizontal">
                        <li class="desktop-call desktop-phone-reveal">
                            <a class="call" href="#" id="header-call-icon"><span class="vh">Call</span></a> <span class="desktop-phone hidden"><a href="#" id="header-call" onclick="return false;">1300 235 664</a></span>
                        </li>
                        <li class="">
                            <a class="bc-blue" data-agent="true" data-agent-mobo="true" data-auth="true" data-customer="true" data-join="true" data-unauth="true" href="/join.html" id="nav-join">Join</a>
                        </li>
                        <li class="">
                            <a class="" data-agent="true" data-agent-mobo="true" data-auth="true" data-customer="true" data-join="true" data-unauth="true" href="#" id="nav-support">Support</a>
                        </li>
                        <li class="hidden">
                            <a class="" data-agent="true" data-agent-mobo="true" data-auth="true" data-customer="true" data-join="false" data-unauth="false" href="#" id="nav-belong-here">Belong Here</a>
                        </li>
                        <li class="">
                            <a class="" data-agent="false" data-agent-mobo="false" data-auth="false" data-customer="true" data-join="true" data-unauth="true" href="#" id="nav-login">Sign in</a>
                        </li>
                    </ul>
                    <div class="mobile-menu hide">
                        <ul>
                            <li>
                                <a href="/adsl.html" id="mobile-nav-adsl">ADSL2+</a>
                            </li>
                            <li>
                                <a href="/nbn.html" id="mobile-nav-nbn">NBN</a>
                            </li>
                            <li>
                                <a class="bc-blue hidden" href="/join.html" id="mobile-nav-join">Join</a>
                            </li>
                            <li>
                                <a class=" hidden" href="#" id="mobile-nav-support">Support</a>
                            </li>
                            <li>
                                <a class=" hidden" href="#" id="mobile-nav-belong-here">Belong Here</a>
                            </li>
                            <li>
                                <a class=" hidden" href="#" id="mobile-nav-login">Sign in</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="error-banner rest-error hidden">
                    There was an issue connecting to Belong. Please try again later.
                </div>
                <div class="error-banner private-browser-error hidden">
                    Private (incognito) browsing mode detected. Functionality will be limited. Please turn off private browsing mode to access full functionality of the site.
                </div>
            </div>
        </div><span class="hidden success" id="notification"></span>
        <div class="content-container cf" style="min-height: initial;">
            <!-- Code to remove current header -->
            <style>
            #header {
                display: none;
            }
            
            .content-container {
                padding-top: 0;
            }
            </style>
            <div class="main -page account-log-in">
                <div class="mboxDefault" style="visibility: visible; display: block;">
                    <div class="campaign-header">
                        <div class="l-padding campaign-header__logo">
                            <a href="{!$label.Belong_Logo}"><img alt="Belong" src="https://belong.com.au/assets/img/belong-logo-white.png" />
                                <!-- Update this path in JSP -->
                            </a>
                        </div>
                        <div class="l-content campaign-header__container">
                            <h1 class="campaign-header__heading--desktop">Sign in to your Belong account</h1>
                            <h1 class="campaign-header__heading--mobile">Sign in to your Belong account</h1>
                        </div>
                    </div>
                    <form id="log-in-block" class="content-column cf log-in-block" novalidate="novalidate">
                        <div class="l-content">
                            <div class="log-in-block__form">
                                <span id="notify-user" class="notify-user hidden -success">
                                            Your login failed. Please check your username and password, and try again.
                                            </span>
                                <span id="notify-user-logout" class="notify-user hidden -success">
                                                You have been logged out.
                                                </span>
                                <a href="https://belong.com.au/account" class="return-to-login btn -size-small -text-small -color-primary hidden" id="login_button">Login to Belong</a>
                                <a href="/account" class="return-to-login btn -size-small -text-small -color-primary hidden">Login to Belong</a>
                                <div class="ctrl-holders" id="login-form-fields">
                                    <div class="ctrl-holder shift-label">
                                        <label class="input-label" for="IDToken1">Your email address</label>
                                        <input name="IDToken1" id="IDToken1" class="text medium" type="text" data-bind="textInput: username"></input>
                                    </div>
                                    <div class="ctrl-holder shift-label">
                                        <label class="input-label" for="IDToken2">Password</label>
                                        <input name="IDToken2" id="IDToken2" class="text medium" type="password" data-bind="textInput: password"></input>
                                        <a class="log-in-block__password-forgot" href="{!$Label.Forgot_Password_URL}">Forgot password?</a>
                                        <br/>
                                    </div>
                                    <div class="ctrl-holder">
                                        <input id="loubmit" name="Login.Submit" type="button" class="btn -size-small -text-small -color-primary log-in-submit" data-state="" value="Login"></input>
                                    </div>
                                </div>
                                <div class="hidden response-messages" id="log-in-block-response-messages">
                                    <span class="fed_val-IDToken1-required">Please enter your username.</span>
                                    <span class="fed_val-IDToken1-backendEmail">Please check your email.</span>
                                    <span class="fed_val-IDToken2-required">Please enter your password.</span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div id="mboxMarker-default-belong-testPage-login-0" style="visibility:hidden;display:none">&nbsp;</div>
                <script src="https://telstracorporationlt.tt.omtrdc.net/m2/telstracorporationlt/mbox/standard?mboxHost=www.belong.com.au&mboxPage=1493172206455-784871&screenHeight=768&screenWidth=1366&browserWidth=1366&browserHeight=362&browserTimeOffset=600&colorDepth=24&mboxSession=1493172187713-471636&mboxPC=1488241209115-55435.27_5&mboxCount=1&mboxTime=1493208206516&mboxMCGVID=63809755149305193744602400807008750496&mboxMCGLH=8&mboxAAMB=NRX38WO0n5BH8Th-nqAG_A&mboxMCAVID=2BF503FE852C316E-400000C1E0009904&mboxMCSDID=4B512D1C60BEB358-46F6EE2C4894E0A5&mbox=belong-testPage-login&mboxId=0&mboxURL=https%3A%2F%2Fwww.belong.com.au%2Fam%2FUI%2FLogin%3Frealm%3D%2FExternal%26goto%3Dhttp%253A%252F%252Fwww.belong.com.au%253A80%252Faccount&mboxReferrer=&mboxVersion=57">
                </script>
                <div id="mboxMarker-default-belong-testPage-login-0" style="visibility:hidden;display:none">&nbsp;</div>
                <img alt="loading" class="loader-circle hidden" src="https://belong.com.au/assets/img/loader-on-grey.gif" />
                <div id="things-to-do">
                    <div class="content-column cf pad-40">
                        <div class="l-content">
                            <h2>Save time. Manage your account online.</h2>
                            <div class="things-to-do-cards">
                                <div class="things-to-do-cards__item">
                                    <h3>Manage your Belong account</h3>
                                    <span class="things-to-do-cards__item-icon svg-account-change-service"></span>
                                    <p>You're in control! View your current plan, upgrade your data allowance, or add/change your Belong Voice options.</p>
                                </div>
                                <div class="things-to-do-cards__item">
                                    <h3>Pay for your<br/> service</h3>
                                    <span class="things-to-do-cards__item-icon svg-account-payment"></span>
                                    <p>In just a few clicks, you can change your payment method, prepay for your service, download your current or past invoices and make an overdue payment.</p>
                                </div>
                                <div class="things-to-do-cards__item">
                                    <h3>Track your Belong order</h3>
                                    <span class="things-to-do-cards__item-icon svg-track-your-modem-blue"></span>
                                    <p>Just signed up to Belong and want to know where your order’s at? Check its progress and the status of your modem delivery right here.</p>
                                </div>
                                <div class="things-to-do-cards__item">
                                    <h3>Update your personal details</h3>
                                    <span class="things-to-do-cards__item-icon svg-view-update-details-blue"></span>
                                    <p>Need to update some of your personal details? You can change your mobile number, account password or email address here, in your own time.</p>
                                </div>
                                <div class="things-to-do-cards__item">
                                    <h3>Support and network updates</h3>
                                    <span class="things-to-do-cards__item-icon svg-support"></span>
                                    <p>Get step by step broadband help for common troubleshooting issues, plus up to the minute network status for service updates in your area.</p>
                                </div>
                                <div class="things-to-do-cards__item">
                                    <h3>Check your data <br/> usage</h3>
                                    <span class="things-to-do-cards__item-icon svg-account-usage"></span>
                                    <p>Stay on top of your data usage online, so you know how you’re tracking. Need more data? You can upgrade your data allowance once a month.</p>
                                </div>
                                <div class="things-to-do-cards__item">
                                    <h3>Move with Belong <br/> <br/></h3>
                                    <span class="things-to-do-cards__item-icon svg-moves-blue"></span>
                                    <p>On the move to your new home? In a few easy steps, you can take your Belong service with you to your new place. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="more-and-contact">
                    <div class="content-column cf pad-40 more-info">
                        <center>
                            <div class="l-content">
                                <h2>Belong on your mobile</h2>
                                <p>If you're always on the move, download the Belong app for iPhone or Android to access all of your account details at your fingertips.</p>
                                <a class="badge" href="https://itunes.apple.com/us/app/belong/id929905619?ls=1&amp;#38;mt=8" target="_blank"><img height="auto" src="https://www.belong.com.au/content/dam/belong/assets//images/badges/app-store.png" width="160px" /></a>
                                <a class="badge" href="https://play.google.com/store/apps/details?id=com.belong" target="_blank"><img height="auto" src="https://www.belong.com.au/content/dam/belong/assets/images/badges/google-play.png" width="160px" /></a>
                                <br/>
                                <br/>
                                <h3>Need help?</h3>
                                <a class="btn -size-small -text-small -color-secondary-dark" href="{!$Label.Contact_Us}">Contact us</a>
                            </div>
                        </center>
                    </div>
                </div>
            </div>
            <div class="footer cf" itemtype="http://schema.org/Organization">
                <div class="refreshglobalfooter refreshpagefooter">
                    <div class="l-padding">
                        <div class="image-links cf">
                        </div>
                        <a href="{!$Label.Belong_Logo}" id="footer-logo"><img alt="Belong Internet Logo" class="logo" src="https://www.belong.com.au/content/dam/belong/assets/images/belong-logo.png" /></a>
                        <ul class="social horizontal f-right">
                            <li>
                                <a class="facebook" href="https://www.facebook.com/LetsBelong/" itemprop="sameAs" rel="nofollow" target="_blank">Facebook</a>
                            </li>
                            <li>
                                <a class="twitter" href="https://twitter.com/letsbelong" itemprop="sameAs" rel="nofollow" target="_blank">Twitter</a>
                            </li>
                        </ul>
                    </div>
                    <div class="links cf">
                        <ul class="horizontal">
                            <li>
                                <a href="https://www.belong.com.au/critical-information-summaries">Critical Information Summaries</a>
                            </li>
                            <li>
                                <a href="https://www.belong.com.au/customer-terms">Customer terms</a>
                            </li>
                            <li>
                                <a href="https://www.belong.com.au/privacy">Privacy</a>
                            </li>
                            <li>
                                <a href="https://www.belong.com.au/terms-of-use">Terms of use</a>
                            </li>
                            <li>
                                <a href="https://www.belong.com.au/contact-us">Contact us</a>
                            </li>
                        </ul>
                        <p class="legal">Belong is not available in all areas or to all homes. NBN is a trademark of NBN Co Limited and is used under license from NBN Co Limited.
                            <br/> Belong is a division of Telstra Corporation Limited ABN 33 051 775 556</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="hidden" data-is-agent="false" data-is-customer="false" data-is-logged-in="false" data-is-mobo="false" data-login-email="" data-login-firstname="" data-login-lastname="" data-mobo-email="" data-mobo-firstname="" data-mobo-lastname="" data-utilibillid="" id="GlobalData"></div>
        
    </body>
   <script type="text/javascript">
var livePersonSettingsData = {
    isDisabled: 'false'
};
</script>
<script type="text/javascript">
var lpSection = "service,authenticate,login";
</script>
<script>
window.BELONG = window.BELONG || {};
</script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://use.typekit.net/ohz2heb.js" type="text/javascript"/>
            <script src="https://www.belong.com.au/assets/js/head.js" />
                <!--<script src="https://www.belong.com.au/assets/lib/jquery-ui-1.10.4.custom.min.js" />
                    <script src="https://www.belong.com.au/assets/lib/jquery.jsonp-2.4.0.min.js" />-->
                        <script src="https://www.belong.com.au/assets/js/plugins.js" />
                            <script src="https://www.belong.com.au/assets/js/script.js" />
                                <!--<script src="https://www.belong.com.au/assets/js/cookieInvalidate.js" />-->
                                    
    <apex:includeScript value="{!URLFOR($Resource.BelongIdentityJS, '/BelongIdentityJS.js')}" />
   
                                
                                  
    <script>
    window.onload = function() {
        $('#loubmit').on('click', function() {
            var startURL = window.location.href;
            if (startURL.indexOf('/Portal/')>0) {
                console.log('This is portal');
            } else {
                startURL = '{!$CurrentPage.parameters.startURL}';
            }
            login(startURL);
            return false;
        });
    };
    </script>
    
   <script>

          var goto_url = getUrlParameter('retUrl');
          var logout= getUrlParameter('logout');
          
          if(goto_url){
               $('#login_button').attr('href',goto_url);
          }
          
          if(logout){
                
                $('#login-form-fields').addClass("hidden");
                $('#notify-user-logout').removeClass("hidden");
                $('#login_button').removeClass("hidden");
               
                $.ajax({
                  url: '/secur/logout.jsp',
                  type: 'GET',
                  success: function(){
                            console.log("Logged out successfully");
                          },
                  error: function(err){console.log("Error logging out" + err);}
            });
           
          }
                  
        function getUrlParameter(sParam) {
           var sPageURL = decodeURIComponent(window.location.search.substring(1)),
           sURLVariables = sPageURL.split('&'), sParameterName, i;

           for (i = 0; i < sURLVariables.length; i++) {
              sParameterName = sURLVariables[i].split('=');

              if (sParameterName[0] === sParam) {
                  return sParameterName[1] === undefined ? true : sParameterName[1];
              }
           }
        }
    </script>
</apex:page>