<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Service Transactions</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Service Transactions</value>
    </caseValues>
    <fields>
        <label><!-- API Request JSON --></label>
        <name>API_Request_JSON__c</name>
    </fields>
    <fields>
        <label><!-- Account Balance --></label>
        <name>Account_Balance__c</name>
    </fields>
    <fields>
        <label><!-- Account --></label>
        <name>Account__c</name>
        <relationshipLabel><!-- Service Transactions --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Action --></label>
        <name>Action__c</name>
        <picklistValues>
            <masterLabel>Activate</masterLabel>
            <translation><!-- Activate --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Add</masterLabel>
            <translation><!-- Add --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Block</masterLabel>
            <translation><!-- Block --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Change MSN</masterLabel>
            <translation><!-- Change MSN --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Disconnect</masterLabel>
            <translation><!-- Disconnect --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Downgrade</masterLabel>
            <translation><!-- Downgrade --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Give</masterLabel>
            <translation><!-- Give --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>New</masterLabel>
            <translation><!-- New --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Receive</masterLabel>
            <translation><!-- Receive --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Remove</masterLabel>
            <translation><!-- Remove --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Replace</masterLabel>
            <translation><!-- Replace --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Shape</masterLabel>
            <translation><!-- Shape --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Suspend</masterLabel>
            <translation><!-- Suspend --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Upgrade</masterLabel>
            <translation><!-- Upgrade --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Used to indicate active services i.e. current plan and bolt-ons --></help>
        <label><!-- Active Service --></label>
        <name>Active_Service__c</name>
    </fields>
    <fields>
        <help><!-- If the source was agent, who was the agent that performed the service transaction --></help>
        <label><!-- Agent ID --></label>
        <name>Agent_ID__c</name>
    </fields>
    <fields>
        <label><!-- Asset --></label>
        <name>Asset__c</name>
        <relationshipLabel><!-- Service Transactions --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- The billing date for the customer&apos;s service --></help>
        <label><!-- Billing cycle --></label>
        <name>Billing_cycle__c</name>
    </fields>
    <fields>
        <label><!-- Completion Date --></label>
        <name>Completion_Date__c</name>
    </fields>
    <fields>
        <label><!-- Gifter Mobile No --></label>
        <name>Gifter_Mobile_No__c</name>
    </fields>
    <fields>
        <label><!-- Gifter Name --></label>
        <name>Gifter_Name__c</name>
    </fields>
    <fields>
        <help><!-- ID is from eternal system providing transactions, but this should be a joined value of Octane Customer + TelflowID
i.e. 12345678-123123 --></help>
        <label><!-- Telflow ID --></label>
        <name>ID__c</name>
    </fields>
    <fields>
        <help><!-- This is the person who initiated the Service Transaction (likely a Belong agent). --></help>
        <label><!-- Initiating User Id --></label>
        <name>Initiating_User_Id__c</name>
    </fields>
    <fields>
        <help><!-- Lookup is for any Service transactions which are generated prior to the customer being activated. --></help>
        <label><!-- Lead --></label>
        <name>Lead__c</name>
        <relationshipLabel><!-- Service Transactions --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Used to identify transctions per mobile phone number, allowing for many SIMs per customer. --></help>
        <label><!-- Mobile No --></label>
        <name>Mobile_No__c</name>
    </fields>
    <fields>
        <label><!-- New SIM Number --></label>
        <name>New_SIM_Number__c</name>
    </fields>
    <fields>
        <label><!-- Mobile Octane Customer Number --></label>
        <name>Octane_Customer_Number__c</name>
    </fields>
    <fields>
        <label><!-- Offer Description --></label>
        <name>Offer_Description__c</name>
    </fields>
    <fields>
        <help><!-- The offer the customer has selected (if applicable). How to handle multiple offers as one transaction? --></help>
        <label><!-- Offer ID --></label>
        <name>Offer_ID__c</name>
    </fields>
    <fields>
        <label><!-- Offer Price --></label>
        <name>Offer_Price__c</name>
    </fields>
    <fields>
        <label><!-- Offer Type --></label>
        <name>Offer_Type__c</name>
    </fields>
    <fields>
        <help><!-- The offer contract term - for marketing purposes --></help>
        <label><!-- Offer contract term --></label>
        <name>Offer_contract_term__c</name>
    </fields>
    <fields>
        <help><!-- The offer version start date - for marketing purposes --></help>
        <label><!-- Offer version date valid from --></label>
        <name>Offer_version_date_valid_from__c</name>
    </fields>
    <fields>
        <help><!-- The offer version end date - for marketing purposes --></help>
        <label><!-- Offer version date valid to --></label>
        <name>Offer_version_date_valid_to__c</name>
    </fields>
    <fields>
        <help><!-- The offer version - for marketing purposes --></help>
        <label><!-- Offer version number --></label>
        <name>Offer_version_number__c</name>
    </fields>
    <fields>
        <label><!-- Payment Due Date --></label>
        <name>Payment_Due_Date__c</name>
    </fields>
    <fields>
        <help><!-- Will display an increment on the port failing. --></help>
        <label><!-- Port Fail Count --></label>
        <name>Port_Fail_Count__c</name>
    </fields>
    <fields>
        <label><!-- Previous Mobile No --></label>
        <name>Previous_Mobile_No__c</name>
    </fields>
    <fields>
        <label><!-- Total Price --></label>
        <name>Price__c</name>
    </fields>
    <fields>
        <help><!-- Primary Contact used for contacting the customer on particular service transaction trigger points, i.e. Service port failure. --></help>
        <label><!-- Primary Contact --></label>
        <name>Primary_Contact__c</name>
        <relationshipLabel><!-- Service Transactions --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Process Date --></label>
        <name>Process_Date__c</name>
    </fields>
    <fields>
        <label><!-- Receiver Mobile No --></label>
        <name>Receiver_Mobile_No__c</name>
    </fields>
    <fields>
        <label><!-- Receiver Name --></label>
        <name>Receiver_Name__c</name>
    </fields>
    <fields>
        <label><!-- Reject Reason --></label>
        <name>Reject_Reason__c</name>
    </fields>
    <fields>
        <help><!-- Failure / reject reasons --></help>
        <label><!-- Reject code --></label>
        <name>Reject_code__c</name>
    </fields>
    <fields>
        <help><!-- The data gift size in gigabytes. --></help>
        <label><!-- Gift Amount (GB) --></label>
        <name>Size__c</name>
    </fields>
    <fields>
        <help><!-- Who performed the initial service transaction: Customer, system, agent --></help>
        <label><!-- Source --></label>
        <name>Source__c</name>
    </fields>
    <fields>
        <label><!-- Status --></label>
        <name>Status__c</name>
        <picklistValues>
            <masterLabel>Acknowledged</masterLabel>
            <translation><!-- Acknowledged --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Complete</masterLabel>
            <translation><!-- Complete --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>In progress</masterLabel>
            <translation><!-- In progress --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rejected</masterLabel>
            <translation><!-- Rejected --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Sub-Status --></label>
        <name>Sub_Status__c</name>
    </fields>
    <fields>
        <label><!-- Sub Type --></label>
        <name>Sub_Type__c</name>
        <picklistValues>
            <masterLabel>Add</masterLabel>
            <translation><!-- Add --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Block</masterLabel>
            <translation><!-- Block --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Call forward</masterLabel>
            <translation><!-- Call forward --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Call waiting</masterLabel>
            <translation><!-- Call waiting --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Caller ID</masterLabel>
            <translation><!-- Caller ID --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Cancel</masterLabel>
            <translation><!-- Cancel --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Change</masterLabel>
            <translation><!-- Change --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Change MSN</masterLabel>
            <translation><!-- Change MSN --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>DataFlex</masterLabel>
            <translation><!-- DataFlex --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Day 1 - Payment Failed Notification</masterLabel>
            <translation><!-- Day 1 - Payment Failed Notification --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Day 11</masterLabel>
            <translation><!-- Day 11 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Day 11 - shape data / suspend add-ons</masterLabel>
            <translation><!-- Day 11 - shape data / suspend add-ons --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Day 180 non-payment - disconnect</masterLabel>
            <translation><!-- Day 180 non-payment - disconnect --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Day 25</masterLabel>
            <translation><!-- Day 25 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Day 25 non-payment - suspend service</masterLabel>
            <translation><!-- Day 25 non-payment - suspend service --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Downgrade</masterLabel>
            <translation><!-- Downgrade --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Give</masterLabel>
            <translation><!-- Give --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>National &amp; International Voice &amp; SMS</masterLabel>
            <translation><!-- National &amp; International Voice &amp; SMS --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>National Voice &amp; SMS</masterLabel>
            <translation><!-- National Voice &amp; SMS --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>New</masterLabel>
            <translation><!-- New --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Non-payment</masterLabel>
            <translation><!-- Non-payment --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Payment successful - unshape data /unsuspend add-ons</masterLabel>
            <translation><!-- Payment successful - unshape data /unsuspend add-ons --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Port</masterLabel>
            <translation><!-- Port --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Port-out</masterLabel>
            <translation><!-- Port-out --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Reactivate service</masterLabel>
            <translation><!-- Reactivate service --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Receive</masterLabel>
            <translation><!-- Receive --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Remove</masterLabel>
            <translation><!-- Remove --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Replace SIM</masterLabel>
            <translation><!-- Replace SIM --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Unblock</masterLabel>
            <translation><!-- Unblock --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Upgrade</masterLabel>
            <translation><!-- Upgrade --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Submitted Date --></label>
        <name>Submitted_Date__c</name>
    </fields>
    <fields>
        <label><!-- Type --></label>
        <name>Type__c</name>
        <picklistValues>
            <masterLabel>Activate</masterLabel>
            <translation><!-- Activate --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Add-on</masterLabel>
            <translation><!-- Add-on --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Data gifting</masterLabel>
            <translation><!-- Data gifting --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Disconnect</masterLabel>
            <translation><!-- Disconnect --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Network features</masterLabel>
            <translation><!-- Network features --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Outstanding payment</masterLabel>
            <translation><!-- Outstanding payment --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Plan Change</masterLabel>
            <translation><!-- Plan Change --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Support</masterLabel>
            <translation><!-- Support --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Top-up</masterLabel>
            <translation><!-- Top-up --></translation>
        </picklistValues>
    </fields>
    <layouts>
        <layout>Service Activation</layout>
        <sections>
            <label><!-- Custom Links --></label>
            <section>Custom Links</section>
        </sections>
        <sections>
            <label><!-- Status Information --></label>
            <section>Status Information</section>
        </sections>
        <sections>
            <label><!-- Transaction Information --></label>
            <section>Transaction Information</section>
        </sections>
    </layouts>
    <recordTypes>
        <label><!-- Mobile Service Transaction --></label>
        <name>Mobile_Service_Transaction</name>
    </recordTypes>
    <startsWith>Consonant</startsWith>
</CustomObjectTranslation>
