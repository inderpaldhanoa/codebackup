<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object will store all the Services related information</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>ADBoR_ID__c</fullName>
        <description>Address ID</description>
        <externalId>false</externalId>
        <label>ADBoR ID</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AccountID__c</fullName>
        <description>Master-Detail relationship between Services and Account</description>
        <externalId>false</externalId>
        <label>Account Name</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Services</relationshipLabel>
        <relationshipName>Services</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Address__c</fullName>
        <description>Stores the Address coming from Octane</description>
        <externalId>false</externalId>
        <label>Address</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Belong_Customer_Stage__c</fullName>
        <description>Stores the Customer Stage. Source is Prospect Database</description>
        <externalId>false</externalId>
        <label>Belong Customer Stage</label>
        <length>100</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Belong_Network_Technology_Current__c</fullName>
        <externalId>false</externalId>
        <label>Belong Network Technology Current</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Belong_Network_Technology_Type_Current__c</fullName>
        <description>Belong Network Technology Type Current value is stored in this field.Data Source is Octane</description>
        <externalId>false</externalId>
        <label>Belong Network Technology Type Current</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Case__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Relationship created as a part of Prospect Database Initiative</description>
        <externalId>false</externalId>
        <label>Case</label>
        <referenceTo>Case</referenceTo>
        <relationshipLabel>Services</relationshipLabel>
        <relationshipName>Services</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Lookup to Contact</description>
        <externalId>false</externalId>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Services</relationshipLabel>
        <relationshipName>Services</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Contract_End_Date__c</fullName>
        <description>Stores the Contract End Date. Source is Experian</description>
        <externalId>false</externalId>
        <label>Contract End Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Contract_Start_Date__c</fullName>
        <description>Stores the Contract Start Date. Source is Experian</description>
        <externalId>false</externalId>
        <label>Contract Start Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Contract_Term__c</fullName>
        <description>Stores the Contract Term. Source is Experian</description>
        <externalId>false</externalId>
        <label>Contract Term</label>
        <length>100</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Copper_Disconnection_Date__c</fullName>
        <description>Disconnection Date will be captured in this field. Source is HFL.</description>
        <externalId>false</externalId>
        <label>Copper Disconnection Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Current_Plan__c</fullName>
        <description>Displays current plan customer is associated with.</description>
        <externalId>false</externalId>
        <label>Current Plan</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Disconnected_date__c</fullName>
        <description>Disconnected date field captures Services disconnection date for a household</description>
        <externalId>false</externalId>
        <label>Disconnected date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>EXPERIAN_ADSL_Exchange_Name__c</fullName>
        <description>Field created for PDB. ADSL Exchange value will be stored.</description>
        <externalId>false</externalId>
        <label>EXPERIAN ADSL Exchange Name</label>
        <length>100</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Estimated_Time_at_Address__c</fullName>
        <description>Displays the Length of Residence , for a household</description>
        <externalId>false</externalId>
        <label>Estimated Time at Address</label>
        <precision>5</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>External_Account_ID__c</fullName>
        <description>Only for PDB Load Purposes. Not must not be used or visibile</description>
        <externalId>true</externalId>
        <label>External Account ID</label>
        <length>30</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GNAF_PID__c</fullName>
        <externalId>true</externalId>
        <label>GNAF PID</label>
        <length>100</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>House_Sale_List_Date__c</fullName>
        <description>House Sale List Date</description>
        <externalId>false</externalId>
        <label>House Sale List Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Mosaic_Classification__c</fullName>
        <externalId>false</externalId>
        <label>Mosaic Classification</label>
        <length>10</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>NBN_Tech_Type_Available__c</fullName>
        <description>Field has Tech Type information available for the Service. Data Source is HFL.</description>
        <externalId>false</externalId>
        <label>NBN Tech Type Available</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PDB_ADSL_Exchange_Network_Type__c</fullName>
        <description>PDB Field</description>
        <externalId>false</externalId>
        <label>PDB ADSL Exchange Network Type</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PDB_LastModified__c</fullName>
        <description>Field used for PDB Source date validation</description>
        <externalId>false</externalId>
        <label>PDB LastModified</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>PDB_Serviceable_Tech_Type__c</fullName>
        <description>Field created for PDB. Stores the Serviceable Tech Type for a prospect</description>
        <externalId>false</externalId>
        <label>PDB Serviceable Tech Type</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>RFS_Date_Actual__c</fullName>
        <description>Information related to NBN Ready for service- actual date will be captured. Source is Experian</description>
        <externalId>false</externalId>
        <label>RFS Date Actual</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>RFS_date_Forecast__c</fullName>
        <description>Information related to NBN Ready for service- forecasted date will be captured. Source is Experian</description>
        <externalId>false</externalId>
        <label>RFS date Forecast</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Rental_Active__c</fullName>
        <description>PDB Rental_Active Boolean value will be stored.</description>
        <externalId>false</externalId>
        <label>Rental Active</label>
        <length>5</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Rental_DateFirstSeen__c</fullName>
        <description>PDB Date first seen for Rent</description>
        <externalId>false</externalId>
        <label>Rental DateFirstSeen</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Rental_DateLastSeen__c</fullName>
        <description>PDB Date last seen for Rent</description>
        <externalId>false</externalId>
        <label>Rental DateLastSeen</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Rental_Date__c</fullName>
        <description>Created to capture Rental date. Source is Experian</description>
        <externalId>false</externalId>
        <label>Rental Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Rental_List_Date__c</fullName>
        <description>Rental List Date. Data Source Experian/AusPost</description>
        <externalId>false</externalId>
        <label>Rental List Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Sale_Active__c</fullName>
        <description>PDB Sale Active Y/N/Null</description>
        <externalId>false</externalId>
        <label>Sale Active</label>
        <length>5</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Sale_DateFirstSeen__c</fullName>
        <description>PDB Sale DateFirstSeen</description>
        <externalId>false</externalId>
        <label>Sale DateFirstSeen</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Sale_DateLastSeen__c</fullName>
        <description>PDB Sale DateLastSeen</description>
        <externalId>false</externalId>
        <label>Sale DateLastSeen</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Sale_Date__c</fullName>
        <description>Created to capture Sale date. Source is Experian</description>
        <externalId>false</externalId>
        <label>Sale Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Service_class_Available_del__c</fullName>
        <description>Describes which Service class a customer is associated with</description>
        <externalId>false</externalId>
        <label>Service class Available</label>
        <length>100</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Service</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>AccountID__c</columns>
        <columns>Contact__c</columns>
        <columns>RFS_Date_Actual__c</columns>
        <columns>LAST_UPDATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Service Number</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Services</pluralLabel>
    <searchLayouts>
        <searchResultsAdditionalFields>Contact__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Address__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ADBoR_ID__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>GNAF_PID__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Service_Validation_Rule</fullName>
        <active>true</active>
        <description>PDB Service Validation Rule</description>
        <errorConditionFormula>if($Setup.IntegrationUser__c.Source__c &lt;&gt; &apos;SKIP&apos;, if( $Setup.IntegrationUser__c.Source__c =&apos;Validate&apos;,if( PDB_LastModified__c &lt; PRIORVALUE( LastModifiedDate ),true,false),false),false )</errorConditionFormula>
        <errorMessage>Source has more recent data.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Validate_Source_Data</fullName>
        <active>true</active>
        <errorConditionFormula>if($Setup.IntegrationUser__c.Source__c &lt;&gt; &apos;SKIP&apos;, if( $Setup.IntegrationUser__c.Source__c =&apos;Validate&apos;,if( PDB_LastModified__c &lt; PRIORVALUE( LastModifiedDate ),true,false),false),false )</errorConditionFormula>
        <errorMessage>Source has more recent data</errorMessage>
    </validationRules>
</CustomObject>
