trigger ServiceTransactionTrigger on Service_Transactions__c (after delete, after insert, after undelete, after update, before delete, before insert, before update)
{
	if(ServiceTransactionTriggerHandler.executeTrigger)
		TriggerBuilder.handle(ServiceTransactionTriggerHandler.class);
 	if(true)
	 	return;
}