trigger TaskTrigger on Task (before insert, after update, before update) 
{
    if(Trigger.isUpdate)
    {
        if(Trigger.isBefore )
        {
            //method to update whatId(related to) field of task. Associated contacts account will be considered as related to account. 
            TaskTriggerHelper.updateWhatId(Trigger.new);
                
        }else{
           //method create survey objects when ININ updates few NPS fields on Task.
            TaskTriggerHelper.createPostCallSurveys(Trigger.newMap, Trigger.OldMap);  
         }
            
    }
    
    if(Trigger.isInsert && Trigger.isBefore)
    {
        TaskTriggerHelper.updateTaskCallANIToAUSFormat(Trigger.new);
    }
    
}