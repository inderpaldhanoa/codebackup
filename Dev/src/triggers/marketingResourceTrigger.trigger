trigger marketingResourceTrigger on Marketing_Resource__c (before insert, before update) {
    if(trigger.isInsert){
        marketingResourceTriggerhandler.handleValidation(trigger.new);
        marketingResourceTriggerhandler.mapPrimaryContact(trigger.new);
    }
    
    if(trigger.isUpdate){
        marketingResourceTriggerhandler.handleValidation(trigger.new);
        list<Marketing_Resource__c> mkR=new list<Marketing_Resource__c>();
        for(Marketing_Resource__c mk:trigger.new){
            if(mk.account__c!=trigger.oldMap.get(mk.id).account__c)
               mkR.add(mk); 
        }
        if(!mkR.isEmpty())
            marketingResourceTriggerhandler.mapPrimaryContact(mkR);
    }
}