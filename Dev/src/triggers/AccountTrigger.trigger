trigger AccountTrigger on Account (after delete, after insert, after undelete, after update, before delete, before insert, before update) 
{
    //Trigger too small to implement handler and helper. review concept "Handler - Helper"i.e Case, if this becomes big

    if(trigger.isBefore && trigger.isInsert)
    {
        AccountTriggerHelper.createEncodeKey(trigger.New);
    }
    
    if(trigger.isAfter && trigger.isUpdate){
        // Octane number updated should trigger the creation  of the new User for Change of lease , Lead Merge join
        
        // Mobile/Fixed Customer joins as Fixed/Mobile then the Octane number is updated to the User object      
        
        // Octane number updated should trigger the creation of the new User for Change of lease , Lead Merge join
        // Mobile/Fixed Customer joins as Fixed/Mobile then the Octane number is updated to the User object
        AccountTriggerHelper.updateOctaneNumberCreateOrUpdateUser(trigger.NewMap, trigger.OldMap);     
    }    
}