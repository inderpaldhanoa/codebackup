/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  @created : 07/07/2016
  @Description : Order Trigger
*/
trigger OrderTrigger on Order (after delete, after insert, after undelete, after update, before delete, before insert, before update) 
{
	if(OrderTriggerHandler.executeTrigger)
		TriggerBuilder.handle(OrderTriggerHandler.class);    
}