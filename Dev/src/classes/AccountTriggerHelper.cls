/************************************************************************************************************
* Apex Class Name   : AccountTriggerHelper.cls
* Version           : 1.0 
* Created Date      : 24 JUN 2016
* Function          : Handler class for Account Object Trigger
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Mitali Telang               24/06/2016              Created Helper Class
************************************************************************************************************/

public with sharing class AccountTriggerHelper 
{
    
    /******************************************************************************
Method Name: createEncodeKey
Parameters: List of Accounts (Trigger.New)
Return: Void
Description: It will create an encoding key for the account to be used in surveys by the related contacts.
Developer Name: Mitali Telang
*****************************************************************************/
    public static void createEncodeKey(List<Account> newAccountList)
    {
        Blob key;
        for(Account acc: newAccountList)
        {
            if(acc.Encryption_Key__c == null || acc.Encryption_Key__c.length() <= 0)
            {
                key = Crypto.generateAesKey(128);
                String s = EncodingUtil.base64Encode(key);
                acc.Encryption_Key__c = s;
            }
        }
    }
    
    public static void updateOctaneNumberCreateOrUpdateUser(Map<id, Sobject> accountNewMap, Map<id, Sobject> accountOldMap){
        system.debug('updateOctaneNumberCreateOrUpdateUser Started :::::::::::::');
        // Mobile Code
        List<Account> accountOctaneNumbers = new List<Account>();
        List<User> updateUserOctaneNumber = new List<User>();
        // The user is a new customer who joins fixed and is not a Mobile customer
        // For Change of lease
        List<account> accountUpdatedOctaneNumbers = new List<account>();
        List<user> newUsersAndExistingUserList = new List<user>();
        Map<Id, User> existingUser = new Map<Id, User>();
        List<User> passwordResetUserList = new List<User>();
        List<Database.UpsertResult> srList;
        List<Log__c> logs = new List<Log__c>();
        
        for(id accountID : accountOldMap.keyset()){
            Account newAccount = (account)accountNewMap.get(accountID);
            Account oldAccount = (account)accountOldMap.get(accountID);
            // Fixed Code
            // Create new Users when the Octane number gets populated into the Account Record for
            // New customer Join through Lead merge and 
            // Change of lease transferred to new Account
            // The Customer_Status_Updated__c changed check is to prevent the Workflow(Customer Status Updated) from Retrigerring the AccountTrigger
            if(oldAccount.Octane_Customer_Number__c == null && 
               newAccount.Octane_Customer_Number__c != oldAccount.Octane_Customer_Number__c &&
               newAccount.Customer_Status_Updated__c == oldAccount.Customer_Status_Updated__c &&
               newAccount.Mobile_Octane_Customer_Number__c == null){
                   //if(newAccount.Lease_Transferred_from__c != NULL)
                	accountUpdatedOctaneNumbers.add(newAccount); 
                   system.debug('accountUpdatedOctaneNumbers created :::::::'+accountUpdatedOctaneNumbers);
               }
                
            
            // Mobile Code 
            // Existing Mobile customer is joining as a new Fixed customer
            if(((newAccount.Octane_Customer_Number__c != oldAccount.Octane_Customer_Number__c) && 
                newAccount.Customer_Status_Updated__c == oldAccount.Customer_Status_Updated__c &&
                newAccount.Mobile_Octane_Customer_Number__c != NULL) ||
               // Existing Fixed customer is joining as a new Mobile customer   
               ((newAccount.Mobile_Octane_Customer_Number__c != oldAccount.Mobile_Octane_Customer_Number__c) && 
               //Need to check Mobile Serv Layer that while ading the Mob Octane Number does it also update the Mob Cust Status
                newAccount.Customer_Status_Updated__c == oldAccount.Customer_Status_Updated__c &&
                newAccount.Octane_Customer_Number__c != NULL) ||
               // Change of lease transferred to an exisitng Mobile Customer 
               // Change of lease transferred to a new Customer handled in updateOctaneNumberCreateUser by creating new User
               (newAccount.Octane_Customer_Number__c != oldAccount.Octane_Customer_Number__c) && 
               (newAccount.Lease_Transferred_from__c != NULL))
                accountOctaneNumbers.add(newAccount);
            
            // Admin Request to updating the erroneous Octane Number to correct Octane Number
            // Fixed Oct no change
            if((oldAccount.Octane_Customer_Number__c != null && 
               newAccount.Octane_Customer_Number__c != null &&
               newAccount.Octane_Customer_Number__c != oldAccount.Octane_Customer_Number__c && 
               newAccount.Customer_Status_Updated__c == oldAccount.Customer_Status_Updated__c) ||
            // Mobile Oct no change
              (oldAccount.Mobile_Octane_Customer_Number__c != null && 
               newAccount.Mobile_Octane_Customer_Number__c != null &&
               newAccount.Mobile_Octane_Customer_Number__c != oldAccount.Mobile_Octane_Customer_Number__c && 
               newAccount.Mobile_Customer_Status__c == oldAccount.Mobile_Customer_Status__c)){
                   accountOctaneNumbers.add(newAccount);  
               }  
        }
        
        if(accountUpdatedOctaneNumbers.isEmpty() && accountOctaneNumbers.isEmpty())
            return;

        if(!accountUpdatedOctaneNumbers.isEmpty()){
            // get all the Contacts related to the Accounts that have Octane Numbers updated
            system.debug('accountUpdatedOctaneNumbers:::::::::'+accountUpdatedOctaneNumbers);
            List<Contact> contactsRelatedToAccounts = [SELECT Id, FirstName, LastName, Email, Contact_Role__c, Octane_Customer_Number__c,
                                                       AccountId, Account.Lease_Transferred_from__c
                                                       FROM Contact
                                                       WHERE Accountid IN : accountUpdatedOctaneNumbers
                                                       AND Contact_Role__c = 'Primary'];
            
            // Create Users for each Account 
            if(!contactsRelatedToAccounts.isEmpty()){
                for(Contact con : contactsRelatedToAccounts){
                    if(!existingUser.containsKey(con.id)) {
                        user u = ContactTriggerHelperIdentity.createUser(con);
                        if(con.account.Lease_Transferred_from__c != null) {
                            passwordResetUserList.add(u);
                        }
                        newUsersAndExistingUserList.add(u);
                    }
                }    
            }
        }
        
        if(!accountOctaneNumbers.isEmpty()){
            updateUserOctaneNumber = [SELECT Id, Accountid, Broadband_Octane_Id__c, Mobile_Octane_Id__c 
                                      FROM User
                                      WHERE AccountId in : accountOctaneNumbers];
            system.debug('accountNewMap:::::'+accountNewMap);
            for(user u : updateUserOctaneNumber){
                Account acc = (Account)accountNewMap.get(u.Accountid);
                u.Broadband_Octane_Id__c = acc.Octane_Customer_Number__c;
                u.Mobile_Octane_Id__c    = acc.Mobile_Octane_Customer_Number__c;
                newUsersAndExistingUserList.add(u);
            }
        }
        
        try {
            srList = database.upsert(newUsersAndExistingUserList, false);
            //Error Logging
            for (Database.UpsertResult sr : srList) {
                if (!sr.isSuccess()) {
                    // Operation failed, so get all errors
                    for (Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Fields that affected this error: ' + err.getFields());
                        logs.add(GlobalUtil.createLog(
                                     'ERROR: ', err.getMessage(),
                                     'User Create Error',
                                     'request:' + '\n' + err.getFields() + '\n' + JSON.serialize(newUsersAndExistingUserList) + '\n'));
                    }
                }
            }
            if(!logs.isEmpty()){
                system.debug('logs:::::::'+logs);
                insert logs;
            }
        } catch (DMLException e){
            newUsersAndExistingUserList[0].addError('The Error is '+e.getMessage());
        }
        // Send password reset Email for Change Of Lease Users
        for (user u : passwordResetUserList) {
            system.debug('password Reset ::::' + u);
            if(NUll != u.Id)
                system.resetPassword(u.Id, true);
        }
    }    
}