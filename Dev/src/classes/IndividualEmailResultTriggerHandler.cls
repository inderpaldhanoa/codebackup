/*
  @author  : vijaykumar (vijayakumarvm.m@infosys.com)
  @created : 12/12/2016
  @Description : util class for trigger handlers on Individual email result object
 */
Public class IndividualEmailResultTriggerHandler{

    public static final DateTime myDateTime = DateTime.newInstance(2016, 12, 19, 0, 39, 51);// process records created after this date and time
    
    Public static void UpdateEmailSent(List<et4ae5__IndividualEmailResult__c> IEResult, boolean isInsert){
        list<Marketing_Service_Email__c> emailService=new list<Marketing_Service_Email__c>();
        list<task> taskList=new list<task>();
        list<id>ierId=new list<id>();
        List<Id> emailSendid= new List<Id>();
             for(et4ae5__IndividualEmailResult__c eIe:IEResult){
                 if(eIe.createddate>myDateTime)//check Created date falls above specified date
                     ierId.add(eIe.id);
             }
             
              List<et4ae5__IndividualEmailResult__c> ler = new List<et4ae5__IndividualEmailResult__c>();
        ler=[SELECT CreatedById,CreatedDate,et4ae5__SendDefinition__r.et4ae5__JobId__c,et4ae5__SoftBounce__c,et4ae5__Contact__c
             FROM et4ae5__IndividualEmailResult__c where id IN:ierId];
             
        list<string> jobsID=new list<string>();
        for(et4ae5__IndividualEmailResult__c jI:ler){
            if(jI.et4ae5__SendDefinition__r.et4ae5__JobId__c!=null)
                jobsID.add(jI.et4ae5__SendDefinition__r.et4ae5__JobId__c);    
        }
             
        List< Marketing_Service_Email__c> lemailSer= new List<Marketing_Service_Email__c>();
        Map<string, Marketing_Service_Email__c> mapemailSer= new  Map<string, Marketing_Service_Email__c>();
           
        lemailSer=[SELECT id,Individual_Email_Result__c,Contact__c,JobID__c FROM Marketing_Service_Email__c where JobID__c  IN: jobsID];
            for(et4ae5__IndividualEmailResult__c jI:ler){       
                 for(Marketing_Service_Email__c msefor: lemailSer){
                     if(jI.et4ae5__Contact__c==msefor.Contact__c){
                         mapemailSer.put(msefor.JobID__c,msefor);
                     }
                 }
            }
     
          for(et4ae5__IndividualEmailResult__c Ie:ler){
              if(mapemailSer.containsKey(Ie.et4ae5__SendDefinition__r.et4ae5__JobId__c)){ 
                Marketing_Service_Email__c mesff=new Marketing_Service_Email__c(id=mapemailSer.get(Ie.et4ae5__SendDefinition__r.et4ae5__JobId__c).id);
                if(Ie.et4ae5__Contact__c==mapemailSer.get(Ie.et4ae5__SendDefinition__r.et4ae5__JobId__c).Contact__c){
                        mesff.Individual_Email_Result__c=Ie.id;
                        emailService.add(mesff);
                }
              }
        
    }
   
        if(!emailService.isEmpty()){
            update emailService;
        }
       
    }
    
}