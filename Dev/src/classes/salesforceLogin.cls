@RestResource(urlMapping='/salesforceLogin')
global class salesforceLogin {
	@HttpPost
    global static boolean credentials(string username,string password)
    {
        List<User> oldUser = [SELECT id, AEMPassword__c
                              FROM User
                              WHERE username = :username
                              AND AEMPassword__c != NULL
                              LIMIT 1];
        system.debug('oldUser:::'+oldUser);
        if(!oldUser.isempty()){
            boolean result = passwordDecoder.checkPaswordValidity(password, oldUser[0].AEMPassword__c);
            system.debug('result::::'+result);
                if(result){
                    system.setPassword(oldUser[0].id, password);
                    oldUser[0].AEMPassword__c = '';
                    update oldUser;
                    return true; 
                }else{
                    return false;
                }    
        } else {
            return false;
        }
    }
    
}