/*
  @author  : Shrinivas Kalkote (shrinivas_kalkote@infosys.com)
  @created : 07/07/2016
  @Description : This batch class links survey provided by user with transcript object so. This will help in identifying survey against live agent transcript.
*/
global class LinkSurveyWithLiveChatBatch implements Database.Batchable<SurveyTaker__c>, Schedulable{

   global List<SurveyTaker__c> start(Database.BatchableContext BC)
   {
      return [SELECT Id, Live_Chat_Key__c, Live_Chat_Transcript__c, (SELECT Id, Response__c, Survey_Question__r.Name FROM Survey_Question_Answers__r WHERE Survey_Question__r.Name = 'Would you like a copy of this interaction?' OR Survey_Question__r.Name = 'Please provide your email address.') FROM SurveyTaker__c WHERE Live_Chat_Key__c != NULL AND Live_Chat_Transcript__c = NULL];
   }

   global void execute(Database.BatchableContext BC, List<SurveyTaker__c> lstSurveyTaker)
   {
     
     map<String, SurveyTaker__c> mapChatKey = new map<String, SurveyTaker__c>();
     List<LiveChatTranscript> lstLiveChatTranscript = new List<LiveChatTranscript>();
     
     for(SurveyTaker__c oSurveyTaken : lstSurveyTaker)
     {
         mapChatKey.put(oSurveyTaken.Live_Chat_Key__c, oSurveyTaken);
     }
     
     Map<String, SurveyQuestionResponse__c> mapSurveyQuestionResponse = new Map<String, SurveyQuestionResponse__c>();
     List<SurveyQuestionResponse__c> lstSurveyQuestionResponse;
     for(LiveChatTranscript oLiveChatTranscript: [SELECT Id, ChatKey, Send_Transcript_Via_Email__c, Email__c FROM LiveChatTranscript WHERE ChatKey IN: mapChatKey.keyset()])
     {
         ((SurveyTaker__c)mapChatKey.get(oLiveChatTranscript.ChatKey)).put('Live_Chat_Transcript__c', oLiveChatTranscript.Id);
         
         /*oSurveyQuestionResponse = !((SurveyTaker__c)mapChatKey.get(oLiveChatTranscript.ChatKey)).Survey_Question_Answers__r.isEmpty() ? 
                        ((SurveyTaker__c)mapChatKey.get(oLiveChatTranscript.ChatKey)).Survey_Question_Answers__r[0]:null; */
         lstSurveyQuestionResponse = ((SurveyTaker__c)mapChatKey.get(oLiveChatTranscript.ChatKey)).Survey_Question_Answers__r;
         system.debug('lstSurveyQuestionResponse:: ' + lstSurveyQuestionResponse);
         if(!lstSurveyQuestionResponse.isEmpty())
         {
             mapSurveyQuestionResponse.put(lstSurveyQuestionResponse[0].Survey_Question__r.Name, lstSurveyQuestionResponse[0]);
             if(lstSurveyQuestionResponse.size() == 2)
                 mapSurveyQuestionResponse.put(lstSurveyQuestionResponse[1].Survey_Question__r.Name, lstSurveyQuestionResponse[1]);
         }
         
         system.debug('mapSurveyQuestionResponse:: ' + mapSurveyQuestionResponse);
         
         if(lstSurveyQuestionResponse != NULL && lstSurveyQuestionResponse.size() > 0 && mapSurveyQuestionResponse.containsKey('Would you like a copy of this interaction?') && mapSurveyQuestionResponse.get('Would you like a copy of this interaction?').Response__c.equalsIgnoreCase('0'))
         {
             oLiveChatTranscript.Send_Transcript_Via_Email__c = true;
             oLiveChatTranscript.Email__c = mapSurveyQuestionResponse.containsKey('Please provide your email address.') ? mapSurveyQuestionResponse.get('Please provide your email address.').Response__c:oLiveChatTranscript.Email__c;
             lstLiveChatTranscript.add(oLiveChatTranscript);
         }
         
         system.debug('lstLiveChatTranscript:: ' + lstLiveChatTranscript);
     }

     update mapChatKey.values();
     update lstLiveChatTranscript;
    }

   global void finish(Database.BatchableContext BC){
   }
   
   global void execute(SchedulableContext SC) {
      LinkSurveyWithLiveChatBatch oLinkSurveyWithLiveChatBatch = new LinkSurveyWithLiveChatBatch(); 
      Database.executeBatch(oLinkSurveyWithLiveChatBatch, 2000);  
   }
}