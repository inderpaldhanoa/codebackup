/*
  @author  : Kala John K(kalajohn_k@infosys.com)
  @created : 14/10/2016
  @Description : Test class for LeadPopController class
*/
@isTest
private class LeadPopControllerTest 
{

  //This test method is to test the positive scenario , input:Phone of Lead = '0435667897' , output: Lead Id with parameter phone ='0435667897'   
  static testmethod void LeadPopControllerMethod1() 
   {
       
        Account acct = TestUtil.creatTestAccount();
        insert acct;      
          
        Lead ld = TestUtil.creatTestLead(acct.id);    
        insert ld;
          
        ApexPages.currentPage().getParameters().put('Phone', '0435667897');
          
        Test.startTest();     
        LeadPopController leadPopClr = new LeadPopController();
        Pagereference leadPop = leadPopClr.routeToLead();     
        Lead leadTest = [select id from Lead];
        Pagereference leadPopTest = new PageReference(System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + leadTest.id);       
        System.assertEquals(leadPop.getURL(), leadPopTest.getURL());  
        Test.stopTest();      
   }
  
  //This test method is to test the negative scenario , input:Phone of Lead = '0435667897' , output: Lead Id with parameter phone ='0435664562'   
  static testmethod void LeadPopControllerMethod2() 
   {
       
        Account acct = TestUtil.creatTestAccount();
        insert acct;      
          
        Lead ld = TestUtil.creatTestLead(acct.id);    
        insert ld;
          
        ApexPages.currentPage().getParameters().put('Phone', '0435664562');
          
        Test.startTest();     
        LeadPopController leadPopClr = new LeadPopController();
        Pagereference leadPop = leadPopClr.routeToLead();     
        Lead leadTest = [select id from Lead];
        Pagereference leadPopTest = new PageReference(System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + label.Unknown_Unknown_Contact_value);       
        System.assertEquals(leadPop.getURL(), leadPopTest.getURL());  
        Test.stopTest();      
   }
  
  //This test method is to test the negative scenario , input:Phone of Lead = '0435667897' , output: Lead Id with parameter phone =''   
  static testmethod void LeadPopControllerMethod3() 
   {
       
        Account acct = TestUtil.creatTestAccount();
        insert acct;      
          
        Lead ld = TestUtil.creatTestLead(acct.id);    
        insert ld;
          
        ApexPages.currentPage().getParameters().put('Phone', '');
          
        Test.startTest();     
        LeadPopController leadPopClr = new LeadPopController();
        Pagereference leadPop = leadPopClr.routeToLead();     
        Lead leadTest = [select id from Lead];
        Pagereference leadPopTest = new PageReference(System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + Label.Unknown_Unknown_Contact_value);       
        System.assertEquals(leadPop.getURL(), leadPopTest.getURL());  
        Test.stopTest();      
   }
}