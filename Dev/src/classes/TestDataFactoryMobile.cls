/************************************************************************************************************
    Apex Class Name   : TestDataFactoryMobile.cls
    Version           : 1.0
    Created Date      : 24 July 2017
    Function          : Mobile test data factory class
    Modification Log  :
    Developer                 Date                Description
    -----------------------------------------------------------------------------------------------------------
    Tom Clayton               24/07/2017          Created class
************************************************************************************************************/
@isTest
public class TestDataFactoryMobile {
    /* For creating and inserting x Accounts with y Contacts each*/
    public static void createAccountsWithContacts(Integer numAccts, Integer numContactsPerAcct) {
        Map<String, Schema.RecordTypeInfo> accountRecordTypeInfo = GlobalUtil.getRecordTypeByName('Account'); // Get all RecordType info for Account
        Integer baseoctane = 100000;
        // Make a list of Accounts and insert
        List<Account> accts = new List<Account>();
        for (Integer i = 0; i < numAccts; i++) {
            Account account = new Account(Name = 'TestAccountA' + String.valueOf(i),
                                          Mobile_Customer_Status__c = 'Active',
                                          Customer_Status__c = 'Active',
                                          RecordTypeId = accountRecordTypeInfo.get('Residential').getRecordTypeId(),
                                          Mobile_Octane_Customer_Number__c = String.valueOf(i + baseoctane),
                                          Octane_Customer_Number__c =  String.valueOf(i + 5600));
            // When we have time, it would be better to use this commented out version to make use of SmartFactory
            /*  Account account = (Account)SmartFactory.createSObject('Account', true);
                account.RecordTypeId = accountRecordTypeInfo.get('Residential').getRecordTypeId();
                account.Mobile_Octane_Customer_Number__c = String.valueOf(i);
                account.Octane_Customer_Number__c = '56' + String.valueOf(i);*/
            accts.add(account);
        }
        insert accts;
        // Make a list of Contacts for Accounts and insert
        List<Contact> cons = new List<Contact>();
        for (Integer j = 0; j < numAccts; j++) {
            Account acct = accts[j];
            for (Integer k = numContactsPerAcct * j; k < numContactsPerAcct * (j + 1); k++) {
                cons.add(new Contact(FirstName = 'TestFName' + k,
                                     LastName = 'TestLName' + k,
                                     AccountId = acct.Id,
                                     Authorized__c = true,
                                     Birthdate = Date.parse('11/12/2000'),
                                     Contact_Role__c = 'Primary',
                                     MobilePhone = '040022200' + String.valueOf(k).right(1),
                                     Email = 'tester' + String.valueOf(k) + '@blah.com'));
                // When we have time, it would be better to use this commented out version to make use of SmartFactory
                /*  Contact contact = (Contact)SmartFactory.createSObject('Contact', true);
                    contact.FirstName = 'TestFirstName' + String.valueOf(k);
                    contact.LastName = 'TestLastName' + String.valueOf(k);
                    contact.AccountId = acct.Id;
                    contact.Birthdate = Date.parse('23/12/2000');
                    contact.Contact_Role__c = 'Primary';
                    contact.MobilePhone = '040022200' + String.valueOf(k).right(1);
                    contact.Email = 'tester' + String.valueOf(k) + '@blah.com';
                    cons.add(contact);*/
            }
        }
        insert cons;
    }

    /* For creating x Accounts */
    public static List<Account> createAccounts(Integer numberOfAccounts) {
        Map<String, Schema.RecordTypeInfo> accountRecordTypeInfo = GlobalUtil.getRecordTypeByName('Account'); // Get all RecordType info for Account
        List<Account> accounts = new List<Account>();
        Integer baseoctane = 100000;
        for (Integer i = 0; i < numberOfAccounts; i++) {
            Account account = new Account();
            //Account account = (Account)SmartFactory.createSObject('Account', true);
            account.Name = 'Name' + String.valueOf(i);
            account.RecordTypeId = accountRecordTypeInfo.get('Residential').getRecordTypeId();
            account.Mobile_Octane_Customer_Number__c = String.valueOf(i + baseoctane);
            account.Octane_Customer_Number__c = '56' + String.valueOf(i);
            accounts.add(account);
        }
        return accounts;
    }

    /* For creating x Contacts */
    public static List<Contact> createContacts(Integer numberOfContacts) {
        List<Contact> contacts = new List<Contact>();
        for (Integer i = 0; i < numberOfContacts; i++) {
            Contact contact = new Contact();
            //Contact contact = (Contact)SmartFactory.createSObject('Contact', true);
            contact.FirstName = 'TestFirstName' + String.valueOf(i);
            contact.LastName = 'TestLastName' + String.valueOf(i);
            contact.Birthdate = Date.parse('23/12/2000');
            contact.Authorized__c = true;
            contact.Contact_Role__c = 'Primary';
            contact.MobilePhone = '040022200' + String.valueOf(i).right(1);
            contact.Email = 'testerx' + String.valueOf(i) + '@blah.com';
            contacts.add(contact);
        }
        return contacts;
    }

    /* For creating x PublicKnowledgeTopics */
    public static List<Public_Knowledge_Topic__c> createPublicKnowledgeTopics(Integer numberOfpubKnows) {
        List<Public_Knowledge_Topic__c> pubKnows = new List<Public_Knowledge_Topic__c>();
        for (Integer i = 0; i < numberOfpubKnows; i++) {
            Public_Knowledge_Topic__c pubKnow = new Public_Knowledge_Topic__c(Name = 'Fix a thing ' + String.valueOf(i),
                    Details__c = 'Ways to do things ' + String.valueOf(i),
                    Title__c = 'Thing 9000',
                    Active__c = true,
                    Product__c = 'Mobile');
            // When we have time, it would be better to use this commented out version to make use of SmartFactory
            /*  Public_Knowledge_Topic__c pubKnow = (Public_Knowledge_Topic__c)SmartFactory.createSObject('Public_Knowledge_Topic__c', true);
                pubKnow.Name = 'Fix a thing ' + String.valueOf(i);
                pubKnow.Details__c = 'Ways to do things ' + String.valueOf(i);
                pubKnow.Title__c = 'Thing 9000';
                pubKnow.Active__c = true;
                pubKnow.Product__c = 'Mobile';*/
            pubKnows.add(pubKnow);
        }
        return pubKnows;
    }

    /* For creating x Cases */
    public static List<Case> createCases(Integer numberOfCases) {
        Map<String, Schema.RecordTypeInfo> caseRecordTypeInfo = GlobalUtil.getRecordTypeByName('Case'); // Get all RecordType info for Case
        List<Case> cases = new List<Case>();
        for (Integer i = 0; i < numberOfCases; i++) {
            Case case1 = new Case(Subject = 'Need to fix the stuff ' + String.valueOf(i),
                                  RecordTypeId = caseRecordTypeInfo.get('Mobile Support').getRecordTypeId(),
                                  Status = 'New',
                                  Origin = 'Email',
                                  Description = 'I am having trouble with an xyz and an abc.');
            // When we have time, it would be better to use this commented out version to make use of SmartFactory
            /*  Case case1 = (Case)SmartFactory.createSObject('Case', true);
                case1.Subject = 'Need to fix the stuff ' + String.valueOf(i);
                case1.RecordTypeId = caseRecordTypeInfo.get('Mobile Support').getRecordTypeId();
                case1.Status = 'New';
                case1.Origin = 'Email';
                case1.Description = 'I am having trouble with an xyz and an abc.';*/
            cases.add(case1);
        }
        return cases;
    }

    public static List<Asset> createAssets(Integer numberOfAssets) {
        Map<String, Schema.RecordTypeInfo> assetRecordTypeInfo = GlobalUtil.getRecordTypeByName('Asset'); // Get all RecordType info for Case
        List<Asset> assets = new List<Asset>();
        Integer simNo = 456789;
        for (Integer i = 0; i < numberOfAssets; i++) {
            Asset asset1 = new Asset();
            //Asset asset1 = (Asset)SmartFactory.createSObject('Asset');
            asset1.Name = 'Name' + String.valueOf(i);
            asset1.Sim__c = String.valueOf(simNo + i);
            assets.add(asset1);
        }
        return assets;
    }

    /* For creating a Knowledge Article (with Knowledge Category) */
    public static FAQ__kav createKnowledgeArticle() {
        FAQ__kav kav = new FAQ__kav(Troubleshooting_Questions__c = 'These are some questions? Question two?',
                                    Title = 'Test KAV',
                                    Summary = 'Test KAV summary',
                                    Language  = 'en_US',
                                    Leave_it_With_Us__c = true,
                                    Related_Topics__c = '<a href="/articles/FAQ/test-title?" target="_blank">Test title?</a>',
                                    Login_State__c = 'Login required');
        // Problem here where there no valid value for Login_State__c?
        insert kav;
        FAQ__DataCategorySelection dcs = new FAQ__DataCategorySelection(DataCategoryName = 'Manage Account',
                ParentId = kav.Id);
        insert dcs;
        return kav;
    }

    /* For creating x Service Transactions */
    public static List<Service_Transactions__c> createServiceTransactions(Integer numberOfServTrans) {
        List<Service_Transactions__c> servTrans = new List<Service_Transactions__c>();
        for (Integer i = 0; i < numberOfServTrans; i++) {
            Service_Transactions__c servT = new Service_Transactions__c(ID__c = 'NUM' + i);
            //Service_Transactions__c servT = (Service_Transactions__c)SmartFactory.createSObject('Service_Transactions__c', true);
            servTrans.add(servT);
        }
        return servTrans;
    }

    public static void createKnowledgeArticleBulk(integer articles) {
        List<FAQ__kav> listKav = new List<FAQ__kav>();
        for (Integer i = 0; i < articles; i++) {
            FAQ__kav faq = new FAQ__kav();
            FAQ__kav kav = new FAQ__kav(Troubleshooting_Questions__c = 'These are some questions? Question two?',
                                        Title = 'Test KAV',
                                        Summary = 'Test KAV summary',
                                        Language  = 'en_US',
                                        Leave_it_with_us__c = false,
                                        Login_State__c = 'Login required',
                                        URLNAME = 'testUrl' + i,
                                        Related_Topics__c = '<a href="/articles/FAQ/test-title1?" target="_blank">Test title?</a>');
            listKav.add(faq);
        }
        // Problem here where there no valid value for Login_State__c?
        insert listKav;
        List<FAQ__DataCategorySelection> listDataCategory = new List<FAQ__DataCategorySelection>();
        for (FAQ__kav kav : [Select KnowledgeArticleId, id From FAQ__kav where iD in :listKav]) {
            FAQ__DataCategorySelection dcs = new FAQ__DataCategorySelection(DataCategoryName = 'Manage Account',
                    ParentId = kav.Id);
            listDataCategory.add(dcs);
            KbManagement.PublishingService.publishArticle(kav.KnowledgeArticleId, true);
        }
        insert listDataCategory;
    }
}