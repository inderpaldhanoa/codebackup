/************************************************************************************************************
* Apex Class Name   : AttachmentTriggerHandler.cls
* Version           : 1.0
* Created Date      : 29 March 2017
* Function          : Handler for triggers on Attachment object.
* Modification Log  :
* Developer             Date                Description
* -----------------------------------------------------------------------------------------------------------
* Tom Clayton           29/03/2017          Created Class.
* Tom Clayton           31/03/2017          Added call to save logs.
************************************************************************************************************/

public with sharing class AttachmentTriggerHandler implements TriggerInterface {
    private Boolean runAfterProcess = false;
    public static Boolean executeTrigger = true;

    // This public method caches related data for before triggers, called once on start of the trigger execution
    public void cacheBefore() {
        System.debug(LoggingLevel.ERROR, '====>cacheBefore is called');
        AttachmentTriggerHelper.initializeProperties();
    }

    // This public method caches related data for after triggers, called once on start of the trigger execution
    public void cacheAfter() {
        System.debug(LoggingLevel.ERROR, '====>cacheAfter is called');
        AttachmentTriggerHelper.initializeProperties();
    }

    // This public method processes the business logic on 1 sObject at before insert
    public void beforeInsert(sObject newSObj) {}

    // This public method process the business logic on 1 sObject at after insert
    public void afterInsert(sObject newSObj) {
        Attachment eachAttachment = (Attachment) newSObj;
        if (AttachmentTriggerHandler.executeTrigger) {
            AttachmentTriggerHelper.processAttachment(eachAttachment);
        }
    }

    // This public method process the business logic on 1 sObject at before update
    public void beforeUpdate(sObject oldSObj, sObject newSObj) {}

    // This public method process the business logic on 1 sObject at after update
    public void afterUpdate(sObject oldSObj, sObject newSObj) {}

    // This public method process the business logic on 1 sObject at before delete
    public void beforeDelete(sObject oldSObj) {}

    // This public method process the business logic on 1 sObject at after delete
    public void afterDelete(sObject oldSObj) {}

    // This public method process the business logic on 1 sObject at after undelete
    public void afterUndelete(sObject newSObj) {}

    /**
    *   Ater all rows have been processed this method will be called
    *   Usualy DML are placed in this method
    */
    public void afterProcessing() {
        //Avoid calling DML if not required
        if (trigger.isAfter & trigger.isInsert) {
            AttachmentTriggerHelper.updateConfirmedAssets();
            AttachmentTriggerHelper.updateRelatedDeliverySummary();
            AttachmentTriggerHelper.saveLogs();
            AttachmentTriggerHelper.afterProcessingResetInitialization();
        }
    }
}