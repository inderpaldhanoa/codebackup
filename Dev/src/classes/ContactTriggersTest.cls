@isTest
private class ContactTriggersTest
{
    public static Map<Id,Schema.RecordTypeInfo> caseRecordTypeInfoId;
    public static Map<String,Schema.RecordTypeInfo> caseRecordTypeInfoName;
    public static Map<Id,Schema.RecordTypeInfo> accRecordTypeInfoId;
    public static Map<String,Schema.RecordTypeInfo> accRecordTypeInfoName;
    public static Account sAccount;
    
    @testSetup static void setupTestData()
    {
        // Load RecordType
        caseRecordTypeInfoId    = GlobalUtil.getRecordTypeById('Case'); //getting all Recordtype for the Sobject
        caseRecordTypeInfoName  = GlobalUtil.getRecordTypeByName('Case');//getting all Recordtype for the Sobject
        accRecordTypeInfoId     = GlobalUtil.getRecordTypeById('Account'); //getting all Recordtype for the Sobject
        accRecordTypeInfoName   = GlobalUtil.getRecordTypeByName('Account');//getting all Recordtype for the Sobject
        
        sAccount                            = new Account();
        sAccount.RecordTypeId               = accRecordTypeInfoName.get('Residential').getRecordTypeId();
        sAccount.Name                       = 'Test Account';
        sAccount.Phone                      = '0456123789';
        sAccount.Customer_Status__c      = 'Active';
        insert sAccount;
    }
    
    static testmethod void ContactTrigger()
    {
        Test.startTest();
        sAccount = [Select id FROM Account];
        Contact sContact = new Contact();
        sContact.FirstName  = 'SalesforceTest';
        sContact.LastName  = 'Smithtest';
        sContact.AccountId  = sAccount.Id;
        sContact.Contact_Role__c = 'Primary';
        insert sContact;
        System.assertNotEquals(sContact.Subscribe_Token__c,'');
        Test.stopTest();

    }
    
    static testmethod void createOnlyOnePrimaryContactPositiveTest(){
        
        Test.startTest();
        sAccount = [Select id FROM Account];
        Contact sContact = new Contact();
        sContact.FirstName  = 'SalesforceTest';
        sContact.LastName  = 'Smithtest';
        sContact.AccountId  = sAccount.Id;
        sContact.Contact_Role__c = 'Primary';
        sContact.authorized__c=true;
        insert sContact;
        
        Contact sContact1 = new Contact();
        sContact1.FirstName  = 'SalesforceTest1';
        sContact1.LastName  = 'Smithtest1';
        sContact1.AccountId  = sAccount.Id;
        sContact1.Contact_Role__c = 'Delegated Authority';
        sContact1.authorized__c=true;
        insert sContact1;
        
        System.assert(sContact.Id != NULL);
        
        /*Check validation for only 2 contacts per account*/
        
        Contact sContact2 = new Contact();
        sContact2.FirstName  = 'SalesforceTest1';
        sContact2.LastName  = 'Smithtest1';
        sContact2.AccountId  = sAccount.Id;
        sContact2.Contact_Role__c = 'Delegated Authority';
        sContact2.authorized__c=true;
        list<contact> c=new list<contact>{sContact2};
        Database.SaveResult[] srList=database.insert(c,false);
        
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully inserted account. Account ID: ' + sr.getId());
            }
            else {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    system.assertEquals('You cannot create more than 2 Authorized contacts',err.getMessage());   
                }
            }
        }
        
        Test.stopTest();        
        
    }
    
    static testmethod void createOnlyOnePrimaryContactNegativeTest(){
        
        Test.startTest();
        sAccount = [Select id FROM Account];
        Contact sContact = new Contact();
        sContact.FirstName  = 'SalesforceTest';
        sContact.LastName  = 'Smithtest';
        sContact.AccountId  = sAccount.Id;
        sContact.Contact_Role__c = 'Primary';
        insert sContact;
        
        try{
            sContact = new Contact();
            sContact.FirstName  = 'SalesforceTest1';
            sContact.LastName  = 'Smithtest1';
            sContact.AccountId  = sAccount.Id;
            sContact.Contact_Role__c = 'Primary';
            insert sContact;
        } catch(exception e){
            System.assert(e != NULL );
        }
        Test.stopTest();        
        
    }
    
    static testmethod void alwaysHavePrimaryContact(){
    
        Test.startTest();
        sAccount = [Select id FROM Account];
        Contact sContact = new Contact();
        sContact.FirstName  = 'SalesforceTest';
        sContact.LastName  = 'Smithtest';
        sContact.AccountId  = sAccount.Id;
        sContact.Contact_Role__c = 'Primary';
        insert sContact;
        
        try{
            sContact.Contact_Role__c = 'Delegated Authority';
            update sContact;
        } catch(exception e){
            System.assert(e != NULL );
        }
        Test.stopTest(); 
    }
    
    static testmethod void changeAccountNameTest(){
    
        Test.startTest();
        sAccount = [Select id FROM Account];
        Contact sContact = new Contact();
        sContact.FirstName  = 'SalesforceTest';
        sContact.LastName  = 'Smithtest';
        sContact.AccountId  = sAccount.Id;
        sContact.Contact_Role__c = 'Primary';
        insert sContact;
        
        try{
            sContact.FirstName  = 'Fname';
            sContact.LastName  = 'Lname';
            update sContact;
            
            Account acc = new Account();
            acc = [Select Name from Account where id = :sAccount.Id];
            System.assertEquals(acc.Name, sContact.FirstName+' '+ sContact.LastName);
        } catch(exception e){
            
        }
        Test.stopTest(); 
    }
}