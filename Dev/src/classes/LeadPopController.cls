public class LeadPopController{

public String leadId {get; set;}

public static string CONST_COUNTRY_CODE = '+61';

    public LeadPopController()
    {
        String sPhoneNum = Apexpages.currentpage().getparameters().get('Phone');
        system.debug('sPhoneNum:: ' + sPhoneNum);
        List<Lead> lstLeads;
        if(String.isNotEmpty(sPhoneNum))
        {
            if(sPhoneNum.startsWith(CONST_COUNTRY_CODE))
            {
                sPhoneNum = sPhoneNum.replace(CONST_COUNTRY_CODE, '');
                sPhoneNum = !sPhoneNum.startsWith('0') ? 0 + sPhoneNum : sPhoneNum;
            }
            
            lstLeads = [SELECT Id FROM Lead WHERE Phone =: sPhoneNum limit 1];
        }else{
            lstLeads = new List<Lead>();
        }
        //check if lead is present in Lead object if not then return Unknown Unknown contact so activity will be logged aginst this contact
        leadId = !lstLeads.isEmpty() ? lstLeads[0].Id : Label.Unknown_Unknown_Contact_value;
    }
    
    public Pagereference routeToLead()
    {   
        return new PageReference(System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + leadId);
    }

}