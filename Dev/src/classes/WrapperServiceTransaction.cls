/************************************************************************************************************
* Apex Class Name	: WrapperServiceTransaction.cls
* Version 			: 1.0 
* Created Date  	: 20 April 2017
* Function 			: Wrapper class for Mobile API Service Transaction generation.
* Modification Log	:
* Developer				Date				Description
* -----------------------------------------------------------------------------------------------------------
* Tom Clayton			20/04/2017 			Created Class.
* Tom Clayton			24/04/2017 			More accurate name for parse method.
* Tom Clayton			27/04/2017 			Additional fields.
* Tom Clayton			01/05/2017 			Reorganised fields into those required for update and create.
* Tom Clayton			18/05/2017 			Added new feilds. Removed the possibility of child records.
* Tom Clayton			07/08/2017 			New field requested.
************************************************************************************************************/

public class WrapperServiceTransaction {
    public string octaneId;
    public string action;
    public string agentId;
    public string billingCycle;
    public string completionDate;
    public string gifterName;
    public string gifterMobileNo;
    public string mobileNo;
    public string offerContractTerm;
    public string offerDescriptions;
    public string offerIds;
    public string offerType;
    public string offerVersionDateValidFrom;
    public string offerVersionDateValidTo;
    public string offerVersionNumber;
    public string offerPrice;
    public string totalPrice;
    public string processDate;
    public string receiverName;
    public string receiverMobileNo;
    public string rejectCode;
    public string rejectReason;
    public string dataGiftSize;
    public string source;
    public string status;
    public string subStatus;
    public string subType;
    public string submittedDate;
    public string type;
    public string telFlowId;
    public string newSIMNumber;
    public string initiatingUserId;
    public string paymentDueDate;
    public string receiverOctaneId;
    public string accountBalance;
	public static WrapperServiceTransaction parseTransaction(String json) {
		return (WrapperServiceTransaction) System.JSON.deserialize(json, WrapperServiceTransaction.class);
	}
}