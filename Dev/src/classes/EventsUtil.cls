public with sharing class EventsUtil 
{
    
    //keys
    private static final String REQUEST_STATUS = 'status';
    private static final String SEGMENT_STATUS = 'sub_status';
    private static final String SUBTYPE = 'order_subtype';
    private static final String APPT_TYPE = 'appointment_type';
    private static final String APPT_DATE = 'appointment_date';
    private static final String APPT_ID = 'appointment_id';
    private static final String APPT_START = 'appointment_start_time';
    private static final String APPT_END = 'appointment_end_time';
    private static final String SUBMITTED = 'submitted_date';
    private static final String REQUESTED = 'requested_date';
    private static final String COMPLETED = 'completed_date';
    private static final String SPID = 'sp_order_id';
    private static final String PROVIDERID = 'provider_ref';
    private static final String SERVICENO = 'service_number';
    private static final String ALARM = 'order_alarm';
    private static final String STATUS_HELD = 'Held';
    private static final String STATUS_POD = 'POD';
    private static final String STATUS_REJECTED = 'Rejected';
    private static final String STATUS_COMPLETED = 'Completed';
    private static final String POD_SUB_STATUS = 'Send Documents';
    private static final String STATUS_WITHDRAWN = 'Withdrawn';
    private static final String EVENT_SUBTYPE = 'subtype';
    private static final String COMPLETED_STATE = 'Completed';
    private static final String DSL_REQ_STATUS_HOLD = 'On Hold';
    private static final String DSL_SEG_STATUS_HELD = 'Held';
    private static final String NBN_COMPLETE = 'RQC';
    private static final String ACTIVE_STATE = 'Active';
    private static final String IN_PROGRESS = 'In Progress';
    private static final String DSL_REQ_STATUS_REJECTED = 'Waiting Customer Re';
    private static final String CONNECT_OUTSTANDING = 'Connect Outstanding';
    private static final String FIELD = 'SERVICE LOCATION';
    private static final String EXCHANGE = 'EXCHANGE';
    private static final String NBN_APPT = 'NBN_Confirmation';
    private static final String EVT_SUB_STATUS = 'STATUS';
    private static final String EVT_SUB_APPT = 'APPOINTMENT';
    private static final String PRODUCT_TYPE = 'product_type';
    private static final String TWO_STEP_ORDERS = 'Two Step Orders';
    private static final String CONNECT_TYPE= 'connect_type';
    private static final Datetime Release_Date =DateTime.newInstance(2017, 03, 21, 11, 0, 0);
    
    
    
    private static final Map<String, String> REQUEST_STATUS_MEANING = new Map<String, String>
    {
        'RQIN' => 'Initial', 'RQT' => 'Transmitted', 'RQUTC' => 'Unable to complete',
            'RQU' => 'Under Assessment', 'RQH' => 'On Hold', 'RQA' => 'Active',
            'RQWD' => 'Withdrawn', 'RQC' => 'Completed', 'RQR' => 'Replaced',
            'RQJ' => 'Rejected', 'RQUTP' => 'Unable to Process'
            };
                
                private static String caseFields = 'Order_Health__c, CreatedDate, Service__c, SLA_Missed__c, Unhealthy_Order_Status__c, Unhealthy_Status_Reason__c, '+
                'TELSTRA_REFERENCE__c, Customer_Octane_Number__c, Status, RecordTypeId, isClosed, contactId, CaseNumber,'+
                'Appointment_Date__c, Appointment_Start_Time__c, Case__c,  X1_Business_day__c, Appointment_End_Time__c,  Proof_Of_Dwelling_Status__c, Sub_Status__c, Move_In_Date__c, Request_Status__c, Segment_Status__c';
    
    
    public static void populateSMSTemplate(SFWrapperObject.Notification notifObj, List<SFWrapperObject.Notification> smsList,
                                           Map<String, sObject> accountMap, Map<String,String> messageAttributesMap, 
                                           Map<String,String> eventAttributesMap, List<Log__c> logs,List<String> messagingErrors)
    {
        
        try
        {
            String firstName = getName(notifObj.accountObj);
            notifObj.messageAttributes.add('firstname:'+ firstName);
            Case ifbotCase = getLatestOrderCase(notifObj.accountObj, messageAttributesMap.get(SPID));
            boolean isMoveRequest = isMoveRequest(ifbotCase);
                        
            if(messageAttributesMap.get(PRODUCT_TYPE) != 'NBN')
            {
                if(messageAttributesMap.get(REQUEST_STATUS) == COMPLETED_STATE && messageAttributesMap.get(COMPLETED) != null && 
                   isOrderStatusCompleted(ifbotCase))
                {
                    notifObj.smsTemplateCode = (isMoveRequest ? 'Move-Activation-DSL' : 'JOIN-ADSL');
                    smsList.add(notifObj);
                }
                else if(messageAttributesMap.get(REQUEST_STATUS) == DSL_REQ_STATUS_HOLD && messageAttributesMap.get(SEGMENT_STATUS) == DSL_SEG_STATUS_HELD && 
                        isOrderStatusNotInHeld(ifbotCase) && ifbotCase.CreatedDate >= Release_Date)
                {
                    notifObj.smsTemplateCode = 'INFO-048';
                    smsList.add(notifObj);
                }
                else if(messageAttributesMap.get(APPT_TYPE) == FIELD && String.isNotBlank(messageAttributesMap.get(APPT_DATE)) &&
                        String.isNotBlank(messageAttributesMap.get(APPT_START)) && String.isNotBlank(messageAttributesMap.get(APPT_END)))
                {
                    if(isMoveRequest){
                         if(messageAttributesMap.get(SEGMENT_STATUS) == IN_PROGRESS && messageAttributesMap.get(REQUEST_STATUS) == ACTIVE_STATE &&
                            eventAttributesMap.get(EVENT_SUBTYPE) == EVT_SUB_STATUS)
                            {
                                notifObj.smsTemplateCode = 'Move-Appt-Tech-DSL';
                                smsList.add(notifObj);
                            }
                    }
                    if(messageAttributesMap.get(SEGMENT_STATUS) != CONNECT_OUTSTANDING && eventAttributesMap.get(EVENT_SUBTYPE) == EVT_SUB_APPT && 
                                  isDSLAppointmentChanged(ifbotCase, messageAttributesMap)) 
                    {
                        notifObj.smsTemplateCode = 'FIELD-REAPPT-DSL';
                        smsList.add(notifObj);
                    }        
                    
                }
                
                else if(messageAttributesMap.get(APPT_TYPE) == EXCHANGE && String.isNotBlank(messageAttributesMap.get(APPT_DATE)))
                {
                    if(isMoveRequest){
                        if(messageAttributesMap.get(SEGMENT_STATUS) == IN_PROGRESS && messageAttributesMap.get(REQUEST_STATUS) == ACTIVE_STATE &&
                            eventAttributesMap.get(EVENT_SUBTYPE) == EVT_SUB_STATUS)
                            {
                            notifObj.messageAttributes.add('plus2days:'+ addBusinessDays(messageAttributesMap.get(APPT_DATE), 2));
                            notifObj.smsTemplateCode = 'Move-Appt-Excg-DSL';
                            smsList.add(notifObj);
                            }
                    }
                     if(messageAttributesMap.get(SEGMENT_STATUS) != CONNECT_OUTSTANDING && eventAttributesMap.get(EVENT_SUBTYPE) == EVT_SUB_APPT && 
                              isDSLAppointmentChanged(ifbotCase, messageAttributesMap))
                    {
                        notifObj.messageAttributes.add('plus2days:'+ addBusinessDays(messageAttributesMap.get(APPT_DATE), 2));
                        notifObj.smsTemplateCode = 'EXCG-REAPPT-DSL';
                        smsList.add(notifObj);  
                    }
                    
                }                
            }
            else
            {
                
                if( messageAttributesMap.get(SEGMENT_STATUS) == NBN_COMPLETE && isNBNOrderStatusCompleted(ifbotCase) )
                {
                    notifObj.smsTemplateCode =  (isMoveRequest ? 'Move-Activation-NBN' : 'JOIN-NBN');
                    smsList.add(notifObj);
                }
                else if(messageAttributesMap.get(APPT_TYPE) == NBN_APPT && String.isNotBlank(messageAttributesMap.get(APPT_DATE)))
                {
                    if(eventAttributesMap.get(EVENT_SUBTYPE) == EVT_SUB_APPT && isAppointmentChanged(ifbotCase, messageAttributesMap))
                    {
                        notifObj.smsTemplateCode = (isMoveRequest ? 'Move-Appt-Tech-NBN' : 'NBN-APPT');
                        smsList.add(notifObj);
                    }
                    
                }
            }
        }
        catch(Exception e)
        {
            messagingErrors.add('Exception in sending SMS: ' + e.getMessage());
            logs.add(GlobalUtil.createLog('ERROR', e.getMessage().abbreviate(225), 'populateSMSTemplate',
                                          JSON.Serialize(notifObj)));
        }
    }
    
    private static boolean isOrderStatusNotInHeld(Case caseObj)
    {
        if(caseObj != null && caseObj.Request_Status__c == 'On Hold' )
        {
            return false;
        }
        return true;
    }
    
    private static boolean isOrderStatusCompleted(Case caseObj)
    {
        if(caseObj != null && caseObj.Request_Status__c == COMPLETED_STATE )
        {
            return false;
        }
        return true;
    }
    
    private static boolean isNBNOrderStatusCompleted(Case caseObj)
    {
        if(caseObj != null && caseObj.Segment_Status__c == COMPLETED_STATE)
        {
            return false;
        }
        return true;
    }
    
    /*private static boolean isOrderSubStatusInProgress(Case caseObj)
{
if(caseObj != null && 
caseObj.Segment_Status__c == IN_PROGRESS)
{
return false;
}
return true;
}*/
    
    private static boolean isDSLAppointmentChanged(Case caseObj, Map<String,String> messageAttributesMap)
    {
        if(messageAttributesMap.get(APPT_TYPE) == FIELD && caseObj != null && ( caseObj.Appointment_Date__c == null || (caseObj.Appointment_Date__c == date.parse(messageAttributesMap.get(APPT_DATE))
                                                                                                                        && (caseObj.Appointment_Start_Time__c == messageAttributesMap.get(APPT_START) &&
                                                                                                                            caseObj.Appointment_End_Time__c == messageAttributesMap.get(APPT_END)))))
        {
            return false;
        }
        if(messageAttributesMap.get(APPT_TYPE) == EXCHANGE && caseObj != null && ( caseObj.Appointment_Date__c == null ||caseObj.Appointment_Date__c == date.parse(messageAttributesMap.get(APPT_DATE))))
        {
            return false;
        }       
        return true;
    }
    
    private static boolean isAppointmentChanged(Case caseObj, Map<String,String> messageAttributesMap)
    {
        if(caseObj != null && String.isNotBlank(messageAttributesMap.get(APPT_DATE)) &&
           caseObj.Appointment_Date__c == date.parse(messageAttributesMap.get(APPT_DATE)))
        {
            if(messageAttributesMap.get(APPT_TYPE) == EXCHANGE)
            {
                return false;
            }
            else if (caseObj.Appointment_Start_Time__c == messageAttributesMap.get(APPT_START) &&
                     caseObj.Appointment_End_Time__c == messageAttributesMap.get(APPT_END))
            {
                return false;
            }
        }
        return true;
    }
    
    private static String getName(Account accObj)
    {
        for(Contact conObj : accObj.Contacts)
        {
            if(conObj.Contact_Role__c == 'Primary')
            {
                return conObj.firstname;
            }
        }
        
        return '';
    }
    
    public static String addBusinessDays(String apptDate, Integer noOfDays)
    {
        List<BusinessHours> bh = [SELECT Id FROM BusinessHours where name = 'Notification Hours'];
        
        Date apptDt = date.parse(apptDate);
        Datetime dt = datetime.newInstanceGmt(apptDt.year(), apptDt.month(),apptDt.day());
        long msToAdd = noOfDays * 24 * 60 * 60 * 1000;
        
        if(bh != null && !bh.isEmpty())
        {
            Datetime convertedDt = BusinessHours.addGmt(bh[0].Id, dt, msToAdd);
            return date.newinstance(convertedDt.year(), convertedDt.month(), convertedDt.day()).format();
        }
        
        return apptDt.addDays(noOfDays).format();
        
    }
    
    private static DateTime convertToDateTime(String dateTimeString)
    {
        List<String> convertedDateTimeValues = dateTimeString.replaceAll('[^0-9]',':').split(':');
        DateTime convertedDateTime  = DateTime.newInstance(Integer.valueOf(convertedDateTimeValues[0]), Integer.valueOf(convertedDateTimeValues[1]), 
                                                           Integer.valueOf(convertedDateTimeValues[2]), Integer.valueOf(convertedDateTimeValues[3]), Integer.valueOf(convertedDateTimeValues[4]), 
                                                           1);
        return  convertedDateTime;
    }
    
    public static void updateCaseFields(SFWrapperObject.Notification notifObj, List<Case> updatedCase, 
                                        Map<String,String> messageAttributesMap, List<Log__c> logs)
    {
        
        Case updatedCaseObj;
        
        try
        {            
            Case ifbotCase = getLatestOrderCase(notifObj.accountObj, messageAttributesMap.get(SPID));
            
            if(ifbotCase != null)
            {
                updatedCaseObj = new Case(id = ifbotCase.Id);
                
                if(String.isNotBlank(messageAttributesMap.get(REQUEST_STATUS))) updatedCaseObj.Request_Status__c = messageAttributesMap.get(REQUEST_STATUS);
                if(String.isNotBlank(messageAttributesMap.get(SUBTYPE))) updatedCaseObj.Order_Subtype_Octane__c =  messageAttributesMap.get(SUBTYPE);
                if(String.isNotBlank(messageAttributesMap.get(APPT_TYPE))) updatedCaseObj.Appointment_Type__c  = messageAttributesMap.get(APPT_TYPE);
                if(String.isNotBlank(messageAttributesMap.get(APPT_DATE))) updatedCaseObj.Appointment_Date__c  = date.parse(messageAttributesMap.get(APPT_DATE));
                if(String.isNotBlank(messageAttributesMap.get(APPT_START))) updatedCaseObj.Appointment_Start_Time__c  =  messageAttributesMap.get(APPT_START);                
                if(String.isNotBlank(messageAttributesMap.get(APPT_END))) updatedCaseObj.Appointment_End_Time__c  =  messageAttributesMap.get(APPT_END);
                if(String.isNotBlank(messageAttributesMap.get(SUBMITTED))) updatedCaseObj.Telstra_Submitted_Date__c  = convertToDateTime(messageAttributesMap.get(SUBMITTED));
                if(String.isNotBlank(messageAttributesMap.get(REQUESTED))) updatedCaseObj.Customer_Requested_Date__c  =  messageAttributesMap.get(REQUESTED);
                if(String.isNotBlank(messageAttributesMap.get(COMPLETED))) updatedCaseObj.Completed_Date__c  = convertToDateTime(messageAttributesMap.get(COMPLETED));                
                if(String.isNotBlank(messageAttributesMap.get(PROVIDERID))) updatedCaseObj.Provider_Reference__c = messageAttributesMap.get(PROVIDERID);
                if(String.isNotBlank(messageAttributesMap.get(SERVICENO))) updatedCaseObj.Service__c = messageAttributesMap.get(SERVICENO);                
                
                if(messageAttributesMap.get(PRODUCT_TYPE) != 'NBN')
                {
                    if(String.isNotBlank(messageAttributesMap.get(SEGMENT_STATUS))) updatedCaseObj.Segment_Status__c = messageAttributesMap.get(SEGMENT_STATUS);
                    
                    if(ifbotCase.CreatedDate>=Release_Date && ifbotCase.Sub_Status__c!= TWO_STEP_ORDERS )
                    {
                    system.debug('See data from custom setting'+ Belong_General_Custom_Setting__c.getValues('Release_Date'));
                        updateCaseStatus(messageAttributesMap.get(REQUEST_STATUS), messageAttributesMap.get(SEGMENT_STATUS), messageAttributesMap.get(APPT_DATE), updatedCaseObj, ifbotCase);
                        if(messageAttributesMap.get(SEGMENT_STATUS)!='Connect Outstanding'  && (ifbotCase.Proof_Of_Dwelling_Status__c != Null))
                        {
                            updatedCaseObj.Proof_Of_Dwelling_Status__c = 'POD Complete';
                        }
                    } 
                    
                }
                else
                {
                    if( messageAttributesMap.get(SEGMENT_STATUS) == NBN_COMPLETE && isNBNOrderStatusCompleted(ifbotCase) )
                    {
                        updatedCaseObj.Completed_Date__c  = datetime.now();
                    }
                    if(String.isNotBlank(messageAttributesMap.get(APPT_ID))) updatedCaseObj.Appointment_ID__c  =  messageAttributesMap.get(APPT_ID);
                    if(String.isNotBlank(messageAttributesMap.get(SEGMENT_STATUS))) updatedCaseObj.Segment_Status__c = REQUEST_STATUS_MEANING.get(messageAttributesMap.get(SEGMENT_STATUS));
                    if(String.isNotBlank(messageAttributesMap.get(SPID))) updatedCaseObj.sp_order_id__c  = messageAttributesMap.get(SPID);
                    if(String.isNotBlank(messageAttributesMap.get(ALARM))) updatedCaseObj.Telflow_Alarm__c = messageAttributesMap.get(ALARM);
                    if(String.isNotBlank(messageAttributesMap.get(CONNECT_TYPE))) updatedCaseObj.Nbn_order_type__C= messageAttributesMap.get(CONNECT_TYPE);
                }
                
                updatedCase.add(updatedCaseObj);
            }
            else
            {
                logs.add(GlobalUtil.createLog('WARN', 'No Related IFBOT Case to Update', 'updateCaseFields',
                                              JSON.Serialize(notifObj)));
            }
            
        }
        catch(Exception e)
        {
            logs.add(GlobalUtil.createLog('ERROR', e.getMessage().abbreviate(225), 'updateCaseFields',
                                          JSON.Serialize(notifObj)));
        }
        
    }
    
    private static Case getLatestOrderCase(Account accObj, String caseNo)
    {
        if(String.isNotBlank(caseNo))
        {
            return getCaseByNumber(caseNo);
        }
        else
        {
            Map<String,Schema.RecordTypeInfo> caseRecordTypeInfoName;
            caseRecordTypeInfoName  = GlobalUtil.getRecordTypeByName('Case');
            Id ifbotCaseId     = caseRecordTypeInfoName.get('IFBOT').getrecordTypeId();
            Id moveCaseId     = caseRecordTypeInfoName.get('Move Request').getrecordTypeId();
            Id reconnectCaseId     = caseRecordTypeInfoName.get('Reconnecting Customer').getrecordTypeId();
            
            if( !accObj.cases.isEmpty())
            {
                for(Case caseObj : accObj.cases) 
                {
                    if(caseObj.Request_Status__c != 'Completed' && (caseObj.RecordTypeId == ifbotCaseId || caseObj.RecordTypeId == moveCaseId ||
                                                                    caseObj.RecordTypeId == reconnectCaseId))
                    {
                        return caseObj;
                    }
                }
            }
        }
        return null;
    }
    
    private static boolean isMoveRequest(Case caseObj)
    {
        Map<String,Schema.RecordTypeInfo> caseRecordTypeInfoName;
        caseRecordTypeInfoName  = GlobalUtil.getRecordTypeByName('Case');
        Id moveCaseId     = caseRecordTypeInfoName.get('Move Request').getrecordTypeId();
         
        if(null != caseObj && caseObj.RecordTypeId == moveCaseId)
        {
            return true;
        }
            
        return false;
    }

    private static Case getCaseByNumber(String caseNo)
    {
        List<sObject> output = new List<sObject>();
        
        String query = 'Select '+ caseFields ;        
        query += ' FROM Case';
        query += ' WHERE CaseNumber =: caseNo';
        
        for (SObject o: Database.query(query))
        {
            output.add(o);
        }
        
        if(!output.isEmpty())
        {
            return ((Case)output[0]);
        }
        
        return null;
    }
    
    public static boolean validAppointmentYear(String apptDate, List<Log__c> logs )
    {
        boolean validAppointmentYear = false;
        Date apptDt;
        
        if(String.isNotBlank(apptDate))
        {
            try
            {
                apptDt = date.parse( apptDate);
            }
            catch(Exception e)
            {
                logs.add(GlobalUtil.createLog('ERROR', e.getMessage().abbreviate(225), 'InvalidDate',
                                              apptDate));
                return validAppointmentYear = false;
            }
            Date additional_one_year = date.today().addYears(1);
            if(apptDt.year() <=  additional_one_year.year())
            {
                validAppointmentYear = true;
            }
        }
        else
        {
            return validAppointmentYear = true;
        }
        
        return validAppointmentYear;
    }
    
    
    
    public static Void updateCaseStatus(String Req_status, String Seg_status,String Appointmentdate, Case updatedCasestatus, Case Originalcase)
    {
        Id moveCaseRecordtypeId     = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Move Request').getrecordTypeId();
        
        if(String.isNotBlank(Req_status))
        {
            If(String.isNotBlank(Seg_status)){
                if(Req_status == DSL_REQ_STATUS_HOLD && Seg_status == DSL_SEG_STATUS_HELD)
                {
                    
                    updatedCasestatus.status = STATUS_HELD;
                    updatedCasestatus.Sub_Status__c = '';
                    
                    updatedCasestatus.Held_Date__c= System.today();
                    system.debug('convert date to string'+system.today().format());
                    updatedCasestatus.Follow_Up_Date__c= date.parse(addBusinessDays(system.today().format(), 2));
                    
                    system.debug('Followupdate'+updatedCasestatus.Follow_Up_Date__c);
                    system.debug('Case status'+updatedCasestatus.status );  
                    
                }
                
                else  if(Req_status==ACTIVE_STATE && Seg_status==CONNECT_OUTSTANDING && Originalcase.status!=STATUS_POD && Originalcase.Proof_Of_Dwelling_Status__c== NULL)
                {
                    
                    updatedCasestatus.status = STATUS_POD;
                    updatedCasestatus.Sub_Status__c = POD_SUB_STATUS;
                    updatedCasestatus.POD_date__c= System.today();
                    
                    if(Originalcase.Move_In_Date__c!=Null){
                        If(Originalcase.Move_In_Date__c<=system.Today())
                        {
                            updatedCasestatus.Follow_Up_Date__c= date.parse(addBusinessDays(system.today().format(), 1));
                        }
                        else{
                            updatedCasestatus.Follow_Up_Date__c= date.parse(addBusinessDays(Originalcase.Move_In_Date__c.format(), -1));
                            system.debug('negative followup date'+ updatedCasestatus.Follow_Up_Date__c);
                        }
                        
                    }
                    else{
                        updatedCasestatus.Follow_Up_Date__c= date.parse(addBusinessDays(system.today().format(), 1));
                    }
                    
                }
                
                else  if(Req_status==ACTIVE_STATE && Seg_status==IN_PROGRESS)
                {
                    
                    updatedCasestatus.status = STATUS_COMPLETED;
                    If(String.isNotBlank(Appointmentdate)){
                        Date Apptdate= date.parse(Appointmentdate);
                        updatedCasestatus.Follow_Up_Date__c= date.parse(addBusinessDays(Appointmentdate, -2));
                        system.debug('Appointment Date followup-date'+updatedCasestatus.Follow_Up_Date__c);
                    }
                    else{
                        updatedCasestatus.Follow_Up_Date__c= date.parse(addBusinessDays(system.today().format(), 2));
                        system.debug('Appointment Date followup-date'+updatedCasestatus.Follow_Up_Date__c);
                        
                        
                    }
                }
                
                else  if(Req_status==COMPLETED_STATE && Seg_status==COMPLETED_STATE)
                {
                    updatedCasestatus.status = STATUS_COMPLETED;
                    updatedCasestatus.Sub_Status__c = '';
                    
                }
                
              /*  else  if(Req_status==STATUS_WITHDRAWN && Seg_status==STATUS_WITHDRAWN)
                {
                    updatedCasestatus.status = STATUS_WITHDRAWN;
                    updatedCasestatus.Sub_Status__c = '';
                    
                }*/
                else if (Req_status==DSL_REQ_STATUS_REJECTED && (Seg_status==IN_PROGRESS || Seg_status== DSL_SEG_STATUS_HELD))
                {
                    
                    updatedCasestatus.status = STATUS_REJECTED;
                    updatedCasestatus.Rejected_date__c= System.today();
                    updatedCasestatus.Sub_Status__c = '';
                    updatedCasestatus.Follow_Up_Date__c= date.parse(addBusinessDays(system.today().format(), 1));
                    
                    
                }
            }
            
            else  if(Req_status==DSL_REQ_STATUS_REJECTED){
                
                updatedCasestatus.status = STATUS_REJECTED;
                updatedCasestatus.Sub_Status__c = '';
                
                updatedCasestatus.Rejected_date__c= System.today();
                updatedCasestatus.Follow_Up_Date__c= date.parse(addBusinessDays(system.today().format(), 1));
                
            } 
        }                      
    }
    
}