public class FutureMethods {
    
    // Function used to update the User's Username field based on changes in the Contact
    @future
    public static void updateUsernamesFromContact(list<id> contactsIDUsername){
        list<user> userListUpdateUserName = [SELECT id, email, username
                                             FROM user 
                                             WHERE contactid IN :contactsIDUsername];	
        for(user u : userListUpdateUserName){
            if(u.email != u.Username){
                u.username = u.email;
            }
        }
        if(!userListUpdateUserName.isEmpty()){
            try{
                update userListUpdateUserName;      
            } catch (DMLException e){
                userListUpdateUserName[0].addError('The Error is '+e.getMessage());
            }
        }        
    }  
}