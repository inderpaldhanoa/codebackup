global class GuidUtil {

    global static String NewGuid() {

        Blob aes = Crypto.generateAesKey(128);
        String hex = EncodingUtil.convertToHex(aes);
        return hex;
    }
}