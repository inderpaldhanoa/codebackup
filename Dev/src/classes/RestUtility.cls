//This is class the utility class for REST service

public class RestUtility {
    public static Integer STATUS_OK = 200;
    public static Integer STATUS_BAD = 400;
    public static Integer STATUS_FORBIDDEN = 403;
    public static Integer STATUS_NOTFOUND = 404;
    public static Integer STATUS_CONFLICT = 409;
    public static Integer STATUS_ISE = 500;
    /** The header name that we should return the error message back on */
    public final static String HEADER_MESSAGE = 'Message';
    //Http Request Object
    public Http createHttpRequest() {
        Http http = new Http();
        return http;
    }

    //Http Request Setup
    public HttpRequest createHttpRequest(String requestBody,
                                         String APICertificate,
                                         String endPointURL,
                                         String setMethord,
                                         Map<String, String> httpHeader) {
        HttpRequest request = new HttpRequest();
        if (APICertificate != '') {
            request.setClientCertificateName(APICertificate);
        }
        request.setEndpoint(endPointURL);
        request.setMethod(setMethord);
        if (httpHeader.size() > 0) {
            for (String htr : httpHeader.keySet()) {
                request.setHeader(htr, httpHeader.get(htr));
            }
        }
        if (setMethord == 'POST') {
            request.setBody(requestBody);
        }
        return request;
    }

    //Http Request Response Setup
    public HttpResponse createHttpResponse(HttpRequest request, Http http ) {
        HttpResponse response = http.send(request);
        return response;
    }
    //get the Rest response
    public static WrapperHTTPResponse getResponse(String status,
            String message,
            Integer statuscode,
            List<Object> results,
            List<log__c> listErrors) {
        List<WrapperHTTPResponse.Error> listError = new List<WrapperHTTPResponse.Error>();
        //Attach error header and response to the body of restresponse
        RestResponse res = Restcontext.response;
        res.statusCode = statuscode;
        res.addHeader(HEADER_MESSAGE, message);
        if (NULL != listErrors) {
            for (Log__c log : listErrors) {
                if ('ERROR'.equalsignoreCase(log.LEVEL__c)) {
                    listError.add(
                        new WrapperHTTPResponse.Error(500, log.Message__c, log)
                    );
                }
            }
        }
        WrapperHTTPResponse responseBody = new WrapperHTTPResponse(status, message, statuscode, results, listError);
        res.responseBody = Blob.valueOf(
                               JSON.serialize(
                                   responseBody
                               )
                           );
        System.debug('RestResponse*************' + responseBody);
        return new WrapperHTTPResponse(status, message, statuscode, results, listError);
    }
}