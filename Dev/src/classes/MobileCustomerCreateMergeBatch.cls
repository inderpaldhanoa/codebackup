/************************************************************************************************************
* Apex Class Name	: MobileCustomerCreateMergeBatch.cls
* Version 			: 1.0
* Created Date  	: 15 March 2017
* Description		:
* Modification Log	:
* Developer				Date				Description
* -----------------------------------------------------------------------------------------------------------
* Girish P  			15/03/2017 			Created Class.
* Tom Clayton 			04/04/2017 			Commented out - this class was just notes from Girish to me. Can delete.
************************************************************************************************************/

global class MobileCustomerCreateMergeBatch implements Database.Batchable<sObject> ,
	Database.AllowsCallouts {
	public String deliverySummaryId {get; set;}
	public String attachmentId {get; set;}

	public String query;

	public MobileCustomerCreateMergeBatch(String deliverySummaryId,
	                                      String attachmentId) {
		this.deliverySummaryId = deliverySummaryId;
		this.attachmentId = attachmentId;
	}	// Start method to get data
	global Database.QueryLocator start(Database.BatchableContext bc) {
		return Database.getQueryLocator([Select id
		                                 from Delivery_Summary__c
		                                 where id = :deliverySummaryId]);
	}
	global void execute(Database.BatchableContext bc, List<sObject> records) {
	}

	global void finish(Database.BatchableContext bc) {
		sendDispatchFileDetails(deliverySummaryId, attachmentId);
	}
	/*
	    @input  : dispatch file information object
	    @output : returns customer Mapping id
	    @Description : send disatch file info to service layer
	  */
	public class DispatchFileNotification {
		String deliverySummaryId {set;}
		String attachmentId {set;}
		public DispatchFileNotification(String deliverySummaryId, String attachmentId) {
			this.deliverySummaryId = deliverySummaryId;
			this.attachmentId = attachmentId;
		}
	}
	//@future(callout = true)
	public static void sendDispatchFileDetails(String deliveryId, String extractid) {
		Http http = new Http();
		HttpRequest request = new HttpRequest();
		request.setEndpoint('https://mobile.belongco.de/api/v1/dispatch/dispatches');
		request.setMethod('POST');
		system.debug('Successfully Sent File Info to Service layer:: ');
		DispatchFileNotification dispatchFileNotification =  new DispatchFileNotification(deliveryId, extractid);
		request.setHeader('Content-Type', 'application/json');
		request.setBody(JSOn.serialize(dispatchFileNotification));
		HttpResponse oResponse = http.send(request);
		system.debug('response' + oResponse);
		system.debug('Successfully Sent File Info to Service layer:: ' + JSOn.serialize(dispatchFileNotification));
		if (NULL != oResponse
		        && oResponse.getStatusCode() == 200) {
			system.debug('Successfully Sent File Info to Service layer:: ');
		}
		else {
			//TODO: Poll the service layer untill we got a 200
			Log__c oLog = GlobalUtil.createLog('ERROR', 'Cannot Send Dispatch File details to Service Layer', 'sendDispatchFileDetails()', oResponse.toString());
			insert oLog;
			Delivery_Summary__c deliverySum = new Delivery_Summary__c(Id = deliveryId,
			        Delivery_File_Status__c = 'Delivery Dispatch Error');
			update deliverySum;
		}
	}
}