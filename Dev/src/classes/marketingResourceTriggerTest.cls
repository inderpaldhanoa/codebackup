@isTest
public class marketingResourceTriggerTest{

    public static testMethod void unitTest(){
        user u=new user(id=UserInfo.getUserId());
        system.runas(u){
            
            account acc=new account();
        acc.name='test mrk';
        insert acc;
        
        contact c=new contact();
        c.firstname='sdf';
        c.lastname='asdf';
        c.email='vmv@gmail.com';
        c.contact_role__c='Primary';
        c.accountid=acc.id;
        insert c;
        contact c1=new contact();
        c1.firstname='sdf';
        c1.lastname='yest';
        c1.email='infy@infy.com';
        c1.contact_role__c='Delegated Authority';
        c1.accountid=acc.id;
        insert c1;
        
        account acc1=new account();
        acc1.name='test mrk1';
        insert acc1;
                        
        contact c3=new contact();
        c3.firstname='sdf';
        c3.lastname='asdf';
        c3.email='vmv@gmail.com';
        c3.contact_role__c='Primary';
        c3.accountid=acc1.id;
        insert c3;
        contact c4=new contact();
        c4.firstname='sdf';
        c4.lastname='yest';
        c4.email='infy@infy.com';
        c4.contact_role__c='Delegated Authority';
        c4.accountid=acc1.id;
        insert c4;
         
            Marketing_Resource__c mr=new Marketing_Resource__c();
            mr.account__c=acc.id;
            insert mr;
            
            Marketing_Resource__c mrK=[select id,contact__c from Marketing_Resource__c where id=:mr.id];
            //system.assertEquals(string.valueof(c.id),string.valueof(mrK.contact__c));
            
            //account a1=[select id,(select id,lastname from contacts where contact_role__c='Primary') from account where name='test mrk1'];
            mr.account__c=acc1.id;
            update mr;
            
            Marketing_Resource__c mrK1=[select id,contact__c from Marketing_Resource__c where id=:mr.id];
            //system.assertEquals(c3.id,mrK1.contact__c);
            
            Marketing_Resource__c mr2=new Marketing_Resource__c();
            mr2.account__c=acc1.id;
            try{
                insert mr2;
            }catch(DmlException e){
               // system.assert(null,mr2.id);
            }
        }
    }
}