/************************************************************************************************************
* Apex Class Name	: WrapperVerifyMobileNo.cls
* Version 			: 1.0 
* Created Date  	: 19 July 2017
* Function 			: Wrapper class for Mobile API mobile no porting verification.
* Modification Log	:
* Developer				Date				Description
* -----------------------------------------------------------------------------------------------------------
* Tom Clayton			19/07/2017 			Created Class.
************************************************************************************************************/

public class WrapperVerifyMobileNo {
    public string mobileNo;
    public string email;
    public string firstName;
    public string lastName;
    public string code;
	public static WrapperVerifyMobileNo parseVerification(String json) {
		return (WrapperVerifyMobileNo) System.JSON.deserialize(json, WrapperVerifyMobileNo.class);
	}
}