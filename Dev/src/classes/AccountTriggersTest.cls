@isTest
private class AccountTriggersTest
{
	public static Map<String,Schema.RecordTypeInfo> accRecordTypeInfoName;
	public static Account sAccount;

	@testSetup static void setupTestData()
    {
    	accRecordTypeInfoName	= GlobalUtil.getRecordTypeByName('Account');

    	sAccount                            = new Account();
        sAccount.RecordTypeId               = accRecordTypeInfoName.get('Residential').getRecordTypeId();
        sAccount.Name                       = 'Test Account';
        sAccount.Phone                      = '0456123789';
        sAccount.Customer_Status__c      = 'Active';
        insert sAccount;
    }

    static testmethod void testAccountTrigger()
    {
        Test.startTest();
        sAccount = [Select id,Encryption_Key__c FROM Account];        
        System.assert(String.isNotBlank(sAccount.Encryption_Key__c));
        Test.stopTest();
    }
}