public class WrapperIdentityUser {
	public String message;
	public User user;
	public Boolean isSuccess;
	public WrapperIdentityUser(Boolean isSuccess, String message, User user) {
		this.message = message;
		this.user = user;
		this.isSuccess = isSuccess;
	}
}