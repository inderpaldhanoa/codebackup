/************************************************************************************************************
* Apex Class Name   : MobileBatchSendDispatchFileNotification.cls
* Version           : 1.0
* Created Date      : 15 March 2017
* Function          : This batch just send a notification to service layer. The reason to create this batch class just for notification was.
						Notification is a callout and in finish of MobileBatchPrepareAssetHeader we are already doing a DML after which cannot
						call a webservice,a hence to overcome this this batch was created

* Modification Log  :
* Developer             Date                Description
* -----------------------------------------------------------------------------------------------------------
* Girish P           07/04/2017
************************************************************************************************************/
public class MobileBatchSendDispatchFileNotification implements Database.Batchable<sObject>, Database.AllowsCallouts {
	public String deliverySummaryId {get; set;}
	public String attachmentId {get; set;}
	public MobileBatchSendDispatchFileNotification(String deliverySummaryId,
	        String attachmentId) {
		this.deliverySummaryId = deliverySummaryId;
		this.attachmentId = attachmentId;
	}
	//Start method to get data
	//Dummy query on delivery Object.
	public Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator([Select id from Delivery_Summary__c where id = :deliverySummaryId]);
	}

	public void execute(Database.BatchableContext BC, List<sObject> scope) {
		//DO nothing
	}

	public void finish(Database.BatchableContext BC) {
		sendDispatchFileDetails(deliverySummaryId, attachmentId);
	}
	/*
	@input  : dispatch file information object
	@output : returns customer Mapping id
	@Description : send dispatch file info to service layer
	*/
	public static void sendDispatchFileDetails(String deliveryId, String extractid) {
		String deliveryStatus;
		Delivery_Summary__c deliverySum = new Delivery_Summary__c(Id = deliveryId);
		system.debug('Successfully Sent File Info to Service layer:: ');
		DispatchFileNotification dispatchFileNotification =  new DispatchFileNotification(deliveryId, extractid);
		//
		RestUtility oRestUtility = new RestUtility();
		String endPointURL = System.label.MOBILE_DISPATCH_FILE_NOTIFICATION_ENDPOINT;
		//Hardcoding API endpoint as it is static and as of now this is not an authenticated API
		//https://mobile.belongco.de/api/v1/dispatch/dispatches
		//String endPointURL = 'https://mobile-dev.belongtest.com.au/api/v1/dispatch/dispatches';
		Map<String, String> mapRequestHeader = new Map<String, String>();
		String token;
		mapRequestHeader.put('Content-Type', 'application/json');
		// mapRequestHeader.put('Authorization', 'Basic ' + base64String);
		HttpRequest oResquest = oRestUtility.createHttpRequest(JSON.serialize(dispatchFileNotification),
		                        '',
		                        endPointURL,
		                        'POST',
		                        mapRequestHeader);
		//Http http = new Http();
		HttpResponse oResponse = oRestUtility.createHttpResponse(oResquest, new Http());
		deliverySum.Delivery_JSON_Service_Layer__c = 'Request Sent: ' + '\n' + JSON.serialize(dispatchFileNotification) + 'Response recieved: ' + '\n' + oResponse.toString();
		//HttpResponse oResponse =  http.send(oResquest);
		if (NULL != oResponse
		        && oResponse.getStatusCode() == 202) {
			system.debug('Successfully Sent File Info to Service layer::' + deliverySum);
		}
		else {
			//TODO: Poll the service layer untill we got a 200
			Log__c oLog = GlobalUtil.createLog('ERROR', 'Cannot Send Dispatch File details to Service Layer', 'sendDispatchFileDetails()', oResponse.toString());
			insert oLog;
			deliverySum.Delivery_File_Status__c = 'Delivery Error';
		}
		update deliverySum;
	}/*
	    @input  : dispatch file information object
	    @output : returns customer Mapping id
	    @Description : send disatch file info to service layer
	  */
	public class DispatchFileNotification {
		String deliverySummaryId {set;}
		String attachmentId {set;}
		public DispatchFileNotification(String deliverySummaryId, String attachmentId) {
			this.deliverySummaryId = deliverySummaryId;
			this.attachmentId = attachmentId;
		}
	}
}