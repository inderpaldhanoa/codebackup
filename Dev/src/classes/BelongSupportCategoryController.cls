/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  @created : 12/05/2016
  @Description : Controller Class for Belong Support Category page.
*/
public with sharing class BelongSupportCategoryController
{
    /* ***** HANDLE TO CURRENT INSTANCE OF CONTROLLER (to be passed to rendered VF components, avoids re-instantiation of controller) ***** */
    public BelongSupportCategoryController pkbCatCon { get { return this; } }
    
    /* ***** KAV EXTENSION, used in VF pages when need to derive article type name from whole object name ***** */
    public String categorySelected      { get; set; }
    public String FAQTitle              { get; set; }
    private String dataCategoryAPI      { get; set; }
    public Public_Knowledge_Product__c productSelected { get; set;}
    
    public String keywordsMeta          { get; set; }
    public String descriptionMeta       { get; set; }
    public String titleMeta             { get; set; }
    public String urlMetaOG             { get; set; }
    public String keywordsMetaOG        { get; set; }
    public String descriptionMetaOG     { get; set; }
    public String titleMetaOG           { get; set; }
    public String imageMetaOG           { get; set; }
    
    public Map<String,List<Public_Knowledge_Product__c>> knowledgeProducts          { get; set; }
    public Map<String,Public_Knowledge_URL_Mapping__c> knowledgeURL                 { get; set; }
    public List<Public_Knowledge_Product_Display_Setting__c> knowledgeSettingProd   { get; set; }
    public Public_Knowledge_URL_Mapping__c knowledgeMetaSetting                     { get; set; }
    public List<FAQ__kav> FAQArticles                                               { get; set; }
    

    public String publishStatus { get { return DEFAULT_PUBLISH_STATUS; } }
    
    public static String DEFAULT_PUBLISH_STATUS = 'online';
    public static String DEFAULT_SITE_NAME = Site.getName();
    public static String DEFAULT_PAGE_TITLE = 'Belong Support - Category';
    public String siteName { get { return DEFAULT_SITE_NAME; } }
    public Boolean isSite { get { return String.isNotBlank(Site.getName()); } }
    
    public String pageTitle 
    {
        get;        
        set;
    }
    
    /* ***** CONTROLLER EXTENSION CONSTRUCTOR ***** */
    public BelongSupportCategoryController(){}
    public BelongSupportCategoryController(ApexPages.StandardController sc) 
    {
        knowledgeProducts    = new Map<String,List<Public_Knowledge_Product__c>>();
        FAQArticles          = new List<FAQ__kav>();
        
        knowledgeSettingProd = KnowledgeSiteUtil.buildSettingsDisplay();
        knowledgeProducts    = KnowledgeSiteUtil.buildProductDisplay();
        knowledgeURL         = KnowledgeSiteUtil.buildURLMapping();
        
        system.debug(logginglevel.error, 'knowledgeProducts => ' + knowledgeProducts);
        
        if (ApexPages.currentPage().getParameters().containsKey('cat'))
        {
            categorySelected = ApexPages.currentPage().getParameters().get('cat');
            system.debug(logginglevel.error, 'categorySelected => ' + categorySelected);
            if(categorySelected.right(2).IsNumeric())
            {
                getFAQArticles();
            }
        }
        
        constructMetadata();
    }
    
    public void constructMetadata()
    {
        knowledgeMetaSetting = knowledgeURL.get('Product');
        
        integer sizeMetaTitle = 70;
        String normTitleMeta = '';
        String normDescMeta = '';
        pageTitle = DEFAULT_PAGE_TITLE;
        if(String.IsNotBlank(knowledgeMetaSetting.SERP_Title__c))
        {
            sizeMetaTitle -= knowledgeMetaSetting.SERP_Title__c.length() + 1;
        }
        if(String.IsNotBlank((String)(productSelected.get(knowledgeMetaSetting.Meta_Title__c))))
        {
            normTitleMeta = (String)(productSelected.get(knowledgeMetaSetting.Meta_Title__c));
            if(normTitleMeta.length() > sizeMetaTitle)
            {
                normTitleMeta = normTitleMeta.substring(0, sizeMetaTitle);
            }
            pageTitle = normTitleMeta;
        } 
        
        if(String.IsNotBlank(knowledgeMetaSetting.SERP_Title__c))
        {
            normTitleMeta +=  ' ' + knowledgeMetaSetting.SERP_Title__c;
            pageTitle = normTitleMeta;
        }
        
        if(String.IsNotBlank((String)(productSelected.get(knowledgeMetaSetting.Meta_Description__c))))
        {
            normDescMeta  = (String)(productSelected.get(knowledgeMetaSetting.Meta_Description__c));
            if(normDescMeta.length() > 155)
            {
                normDescMeta         +=  normDescMeta.substring(0, 155);
            }
        }
        
        titleMeta           = normTitleMeta;
        descriptionMeta     = normDescMeta;
        
        sizeMetaTitle = 70;
        normTitleMeta = '';
        normDescMeta = '';
        
        if(String.IsNotBlank((String)(productSelected.get(knowledgeMetaSetting.Meta_OG_Title__c))))
        {
            normTitleMeta = (String)(productSelected.get(knowledgeMetaSetting.Meta_OG_Title__c));
            if(normTitleMeta.length() > sizeMetaTitle)
            {
                normTitleMeta = normTitleMeta.substring(0, sizeMetaTitle);
            }
        }
        
        if(String.IsNotBlank(knowledgeMetaSetting.SERP_Title__c))
        {
            normTitleMeta +=  ' ' + knowledgeMetaSetting.SERP_Title__c;
        }
        
        if(String.IsNotBlank((String)(productSelected.get(knowledgeMetaSetting.Meta_OG_Description__c))))
        {
            normDescMeta  = (String)(productSelected.get(knowledgeMetaSetting.Meta_OG_Description__c));
            if(normDescMeta.length() > 155)
            {
                normDescMeta         +=  normDescMeta.substring(0, 155);
            }
        }
        
        titleMetaOG         = normTitleMeta;
        descriptionMetaOG   = normDescMeta;
        
        if(String.IsNotBlank((String)(productSelected.get(knowledgeMetaSetting.Meta_Keyword__c))))
        {
            keywordsMeta = (String)(productSelected.get(knowledgeMetaSetting.Meta_Keyword__c)); 
        }

        if(String.IsNotBlank((String)(productSelected.get(knowledgeMetaSetting.Meta_OG_Keywords__c))))
        {
            keywordsMetaOG = (String)(productSelected.get(knowledgeMetaSetting.Meta_OG_Keywords__c));   
        }
        
        if(String.IsNotBlank(knowledgeMetaSetting.Meta_OG_URL__c))
        {
            urlMetaOG = knowledgeMetaSetting.Meta_OG_URL__c;    
        }
        
        /*if(String.IsNotBlank(knowledgeMetaSetting.Meta_OG_Description__c))
        {
            descriptionMetaOG = knowledgeMetaSetting.Meta_OG_Description__c;    
        }*/
        
        if(String.IsNotBlank(knowledgeMetaSetting.Meta_OG_Image__c))
        {
            imageMetaOG = knowledgeMetaSetting.Meta_OG_Image__c;    
        }
    }
    
    public String getProductCategory()
    {
        String CatType = '';
        if(String.IsNotEmpty(categorySelected))
        {
            categorySelected = categorySelected.toLowerCase();
            CatType = categorySelected.substring(0, categorySelected.length()-2);
        }
        return CatType;
    }
    
    public void getFAQArticles()
    {
        FAQArticles.clear();
        FAQTitle = '';
        
        String prodCat = getProductCategory();
        
        for(Public_Knowledge_Product_Display_Setting__c pKwDispProd : knowledgeSettingProd)
        {
            if(pKwDispProd.Name == prodCat)
            {
                FAQTitle += pKwDispProd.Product_Label__c + ' > ';
            }
        }
        
        system.debug(logginglevel.error, 'prodCat => ' + prodCat);
        system.debug(logginglevel.error, 'knowledgeProducts => ' + knowledgeProducts);
        if(knowledgeProducts.containsKey(prodCat))
        {
            for(Public_Knowledge_Product__c kwProd : knowledgeProducts.get(prodCat))
            {
                if(kwProd.Name == categorySelected)
                {
                    productSelected = kwProd;
                    FAQTitle += kwProd.Title__c;
                    dataCategoryAPI = kwProd.Data_Category__c;
                }
            }
        }
        
        String query = 'Select id, Title, UrlName, Normalize_URL_Name__c FROM FAQ__kav '; 
        query += 'WHERE PublishStatus = \'' + DEFAULT_PUBLISH_STATUS +'\' AND Language = \'en_US\' ';
        query += 'WITH DATA CATEGORY Category__c AT ' + dataCategoryAPI;
        query += ' Order by Weighted_Score__c ASC NULLS LAST';
        
        system.debug(logginglevel.error, 'query => ' + query);
        for (SObject sObj: Database.query(query))
        {
            FAQ__kav fqaKv = (FAQ__kav) sObj;
            FAQArticles.add(fqaKv);
        }
        system.debug(logginglevel.error, 'FAQArticles => ' + FAQArticles);
    }
}