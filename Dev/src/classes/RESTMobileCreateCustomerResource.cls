/*
    @author  : Girish P(Girish.pauriyal@infosys.com)
    @created : 22/03/2017
    @Description : API Endpoint for creating customer
*/
@RestResource(urlMapping = '/MobileCustomer/V1/*')
global without sharing class RESTMobileCreateCustomerResource {
	/*
	    @input  : void
	    @output	: returns Lead id for newly created lead
	    @Description : POST API Endpoint for creating Mobile Lead
	*/
	@HttpPost
	global static void createCustomer() {
		Savepoint sp = Database.setSavepoint();
		MobileRestUtility.logs = new List<Log__c>();
		RestRequest req = RestContext.request;
		RestResponse res = Restcontext.response;
		//set header as JSON
		res.addHeader('Content-Type', 'application/json');
		//call post helper class to process the POST request
		String restResponse ;
		//append the response to request body
		if (String.isEmpty(req.requestBody.toString())) {
			restResponse = 'JSON Body is required for Post requests.';
			restResponse = RestUtility.getResponse('Error', restResponse, 404, NULL, NULL).status;
		}
		else {
			//if valid JSON is
			restResponse = MobileRestCustomerApiHelper.customerAPIPostHelper(req.requestBody.toString()).status;
		}
		System.debug('restResponse***' + restResponse);
		if (restResponse.equalsIgnoreCase(MobileRestUtility.STATUS_EXCEPTION)
		        || restResponse.equalsIgnoreCase(MobileRestUtility.HTTP_ERROR)) {
			Database.rollback(sp);
		}
		if (!MobileRestUtility.logs.isEmpty()) {
			insert MobileRestUtility.logs;
		}
	}
	@HttpPut
	global static void updateCustomer() {
		MobileRestUtility.logs = new List<Log__c>();
		Savepoint sp = Database.setSavepoint();
		RestRequest req = RestContext.request;
		RestResponse res = Restcontext.response;
		//set header as JSON
		res.addHeader('Content-Type', 'application/json');
		//call post helper class to process the POST request
		String restResponse ;
		//append the response to request body
		if (String.isEmpty(req.requestBody.toString())) {
			restResponse = 'No JSON Input Recieved';
			restResponse = RestUtility.getResponse('Error', restResponse, 404, NULL, NULL).status;
		}
		else {
			restResponse = MobileRestCustomerApiHelper.customerAPIPutHelper(req.requestBody.toString()).status;
		}
		if (restResponse.equalsIgnoreCase(MobileRestUtility.STATUS_EXCEPTION)
		        || restResponse.equalsIgnoreCase(MobileRestUtility.HTTP_ERROR)) {
			Database.rollback(sp);
		}
		if (!MobileRestUtility.logs.isEmpty()) {
			insert MobileRestUtility.logs;
		}
	}@HttpPatch
	global static void patchCustomer() {
		MobileRestUtility.logs = new List<Log__c>();
		Savepoint sp = Database.setSavepoint();
		RestRequest req = RestContext.request;
		RestResponse res = Restcontext.response;
		//set header as JSON
		res.addHeader('Content-Type', 'application/json');
		//call post helper class to process the POST request
		String restResponse ;
		//append the response to request body
		if (String.isEmpty(req.requestBody.toString())) {
			restResponse = 'No JSON Input Recieved';
			restResponse = RestUtility.getResponse('Error', restResponse, 404, NULL, NULL).status;
		}
		else {
			restResponse = MobileRestCustomerApiHelper.customerAPIPatchHelper(req.requestBody.toString()).status;
		}
		if (restResponse.equalsIgnoreCase(MobileRestUtility.STATUS_EXCEPTION)
		        || restResponse.equalsIgnoreCase(MobileRestUtility.HTTP_ERROR)) {
			Database.rollback(sp);
		}
		if (!MobileRestUtility.logs.isEmpty()) {
			insert MobileRestUtility.logs;
		}
	}

	@HttpGet
	global static void getCustomer() {
		MobileRestUtility.logs = new List<Log__c>();
		Savepoint sp = Database.setSavepoint();
		RestRequest req = RestContext.request;
		RestResponse res = Restcontext.response;
		//set header as JSON
		res.addHeader('Content-Type', 'application/json');
		//call post helper class to process the POST request
		String restResponse ;
		//append the response to request body
		if (NULL == req.params) {
			restResponse = 'No JSON Input Recieved';
			restResponse = RestUtility.getResponse('Error', restResponse, 404, NULL, NULL).status;
		}
		else {
			restResponse = MobileRestCustomerApiHelper.customerAPIGetHelper(req.params).status;
		}
		if (restResponse.equalsIgnoreCase(MobileRestUtility.STATUS_EXCEPTION)
		        || restResponse.equalsIgnoreCase(MobileRestUtility.HTTP_ERROR)) {
			Database.rollback(sp);
		}
		if (!MobileRestUtility.logs.isEmpty()) {
			insert MobileRestUtility.logs;
		}
	}
}