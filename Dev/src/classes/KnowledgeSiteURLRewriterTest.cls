@isTest
private class KnowledgeSiteURLRewriterTest 
{
	@testSetup static void setupTestData() 
    {
        TestUtil.createKnowledgeSettings();
    }
    
    static testMethod void RequestURLTest() 
    {
       KnowledgeSiteURLRewriter kwWritter = new KnowledgeSiteURLRewriter();
       String URL;
       boolean x = kwWritter.isSite;
       PageReference pageRef;
       
       url = '/voice/join/Test-Article';
       pageRef = new PageReference(URL);
       pageRef = kwWritter.mapRequestUrl(pageRef);
       
       url = '/Search?search=nbn';
       pageRef = new PageReference(URL);
       pageRef = kwWritter.mapRequestUrl(pageRef);
       
       url = '/voice/join';
       pageRef = new PageReference(URL);
       pageRef = kwWritter.mapRequestUrl(pageRef);
       
       url = '/voice/TEST';
       pageRef = new PageReference(URL);
       pageRef = kwWritter.mapRequestUrl(pageRef);
       
       url = '/voice';
       pageRef = new PageReference(URL);
       pageRef = kwWritter.mapRequestUrl(pageRef);
       
       url = '/TEST';
       pageRef = new PageReference(URL);
       pageRef = kwWritter.mapRequestUrl(pageRef);
       
    }
    
    static testMethod void GenerateURLTest() 
    {
        KnowledgeSiteURLRewriter kwWritter = new KnowledgeSiteURLRewriter();
        String URL;
       	PageReference pageRef;
       	List<PageReference> pageRefList = new List<PageReference>();
        
        url = 'Belong_Support_Article/artName=Test-Article';
		pageRef = new PageReference(URL);
		pageRefList.add(pageRef);
		
		url = 'Belong_Support_Category/cat=Voice01';
		pageRef = new PageReference(URL);
		pageRefList.add(pageRef);
		
		url = 'Belong_Support_Search_Results?search=nbn';
		pageRef = new PageReference(URL);
		pageRefList.add(pageRef);
		
        kwWritter.generateUrlFor(pageRefList);
    }
}