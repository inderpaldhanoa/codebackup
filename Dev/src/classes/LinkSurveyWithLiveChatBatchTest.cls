/*
  @author  : Kala John K(kalajohn_k@infosys.com)
  @created : 12/09/2016
  @Description : Test class for LinkSurveyWithLiveChatBatch class
*/
@isTest
public class LinkSurveyWithLiveChatBatchTest 
{
 
 //This test method is to test the positive scenario , input:Live_chat_key__C of Surveytaker = 'Test' & chatkey of Live_Chat_Transcript__c = 'Test' , output: Live_Chat_Transcript__c  of SurveyTaker= LiveChatTranscript  
  static testmethod void SurveyWithLiveChatBatchTest1() 
  {
          
        Account acct = TestUtil.creatTestAccount();
        insert acct;
        
        Contact cnt = TestUtil.creatTestContact(acct.Id);
        insert cnt;
        
        Case cse = TestUtil.creatTestCase(acct.id,cnt.id);
        insert cse;
        
        Order ordr = TestUtil.creatTestOrder(acct.id, 'Test');
        insert ordr;
        
        Survey__c survey = TestUtil.creatTestSurvey();
        insert survey;
        
        LiveChatTranscript lievChat = TestUtil.creatTestLiveChat();
        insert lievChat;
        
        SurveyTaker__c srvTaker = TestUtil.creatTestSurveyTaker(ordr.id, cnt.id, cse.id, survey.id, 'Test' );
        insert srvTaker; 
       
        LinkSurveyWithLiveChatBatch surveyLink = new LinkSurveyWithLiveChatBatch(); 
        Test.startTest();           
          Database.executeBatch(surveyLink);          
        Test.stopTest();      
        
        SurveyTaker__c srvTk = [select id, Live_Chat_Transcript__c from SurveyTaker__c];
        LiveChatTranscript lvCt = [select id  from LiveChatTranscript];

        System.assertEquals(srvTk.Live_Chat_Transcript__c, lvCt.id);  
               
  }
 
 //This test method is to test the batch scheduler method , input:Live_chat_key__C of Surveytaker = 'Test' & chatkey of Live_Chat_Transcript__c = 'Test' , output: Live_Chat_Transcript__c  of SurveyTaker= LiveChatTranscript   
  static testmethod void SurveyWithLiveChatSchedulerTest() 
  {
          
        Account acct = TestUtil.creatTestAccount();
        insert acct;
        
        Contact cnt = TestUtil.creatTestContact(acct.Id);
        insert cnt;
        
        Case cse = TestUtil.creatTestCase(acct.id,cnt.id);
        insert cse;
        
        Order ordr = TestUtil.creatTestOrder(acct.id, 'Test');
        insert ordr;
        
        Survey__c survey = TestUtil.creatTestSurvey();
        insert survey;
        
        LiveChatTranscript lievChat = TestUtil.creatTestLiveChat();
        insert lievChat;
        
        SurveyTaker__c srvTaker = TestUtil.creatTestSurveyTaker(ordr.id, cnt.id, cse.id, survey.id, 'Test' );
        insert srvTaker;     
        
        LinkSurveyWithLiveChatBatch surveyLink = new LinkSurveyWithLiveChatBatch(); 
        String sch ='0 0 0 * * ?';
        Test.startTest();
        system.schedule('Survey% Link@ #chat', sch, surveyLink);
        Test.stopTest();
        
        SurveyTaker__c srvTk = [select id, Live_Chat_Transcript__c from SurveyTaker__c];
        LiveChatTranscript lvCt = [select id  from LiveChatTranscript];       
               
  }

//This test method is to test the negative scenario , input:Live_chat_key__C of Surveytaker = 'test1' & chatkey of Live_Chat_Transcript__c = 'Test' , output: Live_Chat_Transcript__c  of SurveyTaker= null  
static testmethod void SurveyWithLiveChatBatchTest2() 
  {
          
        Account acct = TestUtil.creatTestAccount();
        insert acct;
        
        Contact cnt = TestUtil.creatTestContact(acct.Id);
        insert cnt;
        
        Case cse = TestUtil.creatTestCase(acct.id,cnt.id);
        insert cse;
        
        Order ordr = TestUtil.creatTestOrder(acct.id, 'Test');
        insert ordr;
        
        Survey__c survey = TestUtil.creatTestSurvey();
        insert survey;
        
        LiveChatTranscript lievChat = TestUtil.creatTestLiveChat();
        insert lievChat;
        
        SurveyTaker__c srvTaker = TestUtil.creatTestSurveyTaker(ordr.id, cnt.id, cse.id, survey.id, 'test1' );
        insert srvTaker;       
               
        LinkSurveyWithLiveChatBatch surveyLink = new LinkSurveyWithLiveChatBatch();
        Test.startTest();            
          Database.executeBatch(surveyLink);          
        Test.stopTest();        
        
        SurveyTaker__c srvTk = [select id, Live_Chat_Transcript__c from SurveyTaker__c];
        LiveChatTranscript lvCt = [select id  from LiveChatTranscript];

        System.assertEquals(srvTk.Live_Chat_Transcript__c, null);  
               
  }
}