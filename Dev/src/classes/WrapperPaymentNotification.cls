public class WrapperPaymentNotification {
	public String type;	//Outstanding payment
	public String subType;	//Day 1
	public String status;	//In Progress
	public String subStatus;	//Payment failed notification sent
	public String octaneCustomerID;	//88888888
	public String orderId;	//ddmmyyyy88888888-stno
	public Integer rejectCode;	//2001
	public String rejectReason;	//Insufficient funds
	public Decimal accountBalance;	//25
	public String duedate;	//2017-01-01
	public String submittedDate;	//2017-01-01
	public String lastUpdatedDate;	//2017-01-01
	public String userID;	//payment-job
	public String source;	//System
	public String taskID;
	public static WrapperPaymentNotification parse(String json) {
		return (WrapperPaymentNotification) System.JSON.deserialize(json, WrapperPaymentNotification.class);
	}


}