<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Chat_History_To_Customer</fullName>
        <description>Send Chat History To Customer</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>hello@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Notification_Email_Templates/Chat_History_With_Belong</template>
    </alerts>
    <rules>
        <fullName>Send Chat Transcript To Customer</fullName>
        <actions>
            <name>Send_Chat_History_To_Customer</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>LiveChatTranscript.Send_Transcript_Via_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
