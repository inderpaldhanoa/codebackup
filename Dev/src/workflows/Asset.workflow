<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notification_to_customer</fullName>
        <description>Notification to customer</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Email_Templates/Asset_Dispatch_delivery_notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_Customer</fullName>
        <description>Notify Customer</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Email_Templates/Asset_Dispatch_delivery_notification</template>
    </alerts>
    <rules>
        <fullName>Asset - Notification to Customer when Asset is Dispatched</fullName>
        <actions>
            <name>Notify_Customer</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Asset.Status</field>
            <operation>equals</operation>
            <value>Dispatched</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
